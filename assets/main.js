import React, { Component } from 'react'
import { Provider, TitleBar, useAppBridge } from '@shopify/app-bridge-react'
import Routes from './Routes'
import enTranslations from '@shopify/polaris/locales/en.json'
import { AppProvider, Toast, Frame, Banner } from '@shopify/polaris'
import '@shopify/polaris/dist/styles.css'
import './styles/app.css'
import { HOST_URL, API_KEY, easy_path } from './env'
import AxiosObj from './components/AxiosIns'
import { RedirectDispatch } from './common/Common'

const config = {
  apiKey: API_KEY,
  host: host,
}

const faqButton = {
  content: 'FAQ',
  url: HOST_URL + 'landing-page/faqs',
  target: 'REMOTE',
  external: true,
}
const homeButton = {
  content: 'Dashboard',
  url: `${easy_path}/dashboard`,
}
const returnListButton = {
  content: 'Returns',
  url: `${easy_path}/returns`,
}

function AppTitleBarComponent({ title }) {
  const app = useAppBridge()
  window.appBridge = app

  const generalConfiguration = {
    content: 'General Configuration',
    url: `${easy_path}/general-configuration`,
    disabled: title === 'General Configuration',
  }
  const emailConfiguration = {
    content: 'Email Configuration',
    url: `${easy_path}/email-configs`,
    disabled: title === 'Email Configuration',
  }
  const returnPageConfigureButton = {
    content: 'Return Page Configuration',
    url: `${easy_path}/front-configure`,
    disabled: title === 'Return Page Configuration',
  }
  const translation = {
    content: 'Label Translations',
    url: `${easy_path}/translations`,
    disabled: title === 'Label Translations',
  }
  const configurations = [
    {
      title: 'Configurations',
      actions: [
        generalConfiguration,
        emailConfiguration,
        returnPageConfigureButton,
        translation,
      ],
    },
  ]

  return (
    <TitleBar
      title={title}
      primaryAction={faqButton}
      secondaryActions={[homeButton, returnListButton]}
      actionGroups={configurations}
    />
  )
}

export default class Main extends Component {
  constructor(props) {
    super(props)
    const tb_title = 'Dashboard'
    this.state = {
      tb_title,
      toastMsg: '',
      toastType: false,
      notification: null,
      topBarStyle: 'block',
    }
  }

  componentDidMount() {
    AxiosObj.get('/notifications')
      .then((res) => {
        const res_data = res.data
        const returnData = res_data.data || null

        if (returnData) {
          this.setState({ notification: returnData })
        }
      })
      .catch(() => {})
      .then(() => {})
  }

  handleStateChange = (val) => {
    if (typeof val === 'object') this.setState(val)
    else this.setState({ tb_title: val })

    if (val !== 'Changelog' && val !== 'Setup') {
      this.setState({ topBarStyle: 'block' })
    } else this.setState({ topBarStyle: 'none' })
  }

  setNoticeToast = (msg = '') => {
    this.setState({ toastMsg: msg, toastType: false })
  }

  setErrorToast = (msg = '') => {
    this.setState({ toastMsg: msg, toastType: true })
  }

  render() {
    const {
      tb_title,
      toastMsg,
      toastType,
      notification,
      topBarStyle,
    } = this.state
    return (
      <AppProvider i18n={enTranslations}>
        <Provider config={config}>
          <AppTitleBarComponent title={tb_title} />
          <Frame>
            <TopBanner notification={notification} topBarStyle={topBarStyle} />
            <Routes
              updateTitle={this.handleStateChange}
              setNoticeToast={this.setNoticeToast}
              setErrorToast={this.setErrorToast}
            />
            <MyToast
              message={toastMsg}
              error={toastType}
              toggleActive={this.setNoticeToast}
            />
          </Frame>
        </Provider>
      </AppProvider>
    )
  }
}

function MyToast({ message = '', error = false, toggleActive }) {
  if (message.trim() === '') return null
  return (
    <Toast content={message} onDismiss={() => toggleActive()} error={error} />
  )
}

function TopBanner({ notification, topBarStyle }) {
  if (!notification) return null
  return (
    <div
      style={{ padding: '1.6rem 3.2rem 0', display: topBarStyle }}
      id="bannerTop"
    >
      <Banner
        title={notification.title}
        status={
          notification.type === 1
            ? 'info'
            : notification.type === 2
            ? 'warning'
            : notification.type === 3
            ? 'success'
            : notification.type === 4
            ? 'critical'
            : ''
        }
        secondaryAction={
          notification.actions
            ? {
                content: notification.actions.content,
                onAction: () => {
                  RedirectDispatch(`${easy_path}${notification.actions.url}`)
                },
              }
            : null
        }
      >
        <p dangerouslySetInnerHTML={{ __html: notification.content }} />
      </Banner>
    </div>
  )
}
