import React, { Component } from 'react'
import { Page, Layout, Badge } from '@shopify/polaris'
import { RedirectDispatch } from '../common/Common'
import Versions from './versions'
import { easy_path } from '../env'

const versionsList = [
  { label: '2020-09-12', version: '20200912' },
  { label: '2020-08-10', version: '20200810' },
  { label: '2020-07-01', version: '20200701' },
]

export default class Changelog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      label: 'Versions',
      currentVersion: props.match.params.version,
    }
  }

  componentDidMount() {
    this.props.updateTitle('Changelog')
    // document.getElementById('bannerTop').style.display = 'none'
  }

  handleVersionChange = (version, label) => {
    this.setState({ label: `Versions: ${label}`, currentVersion: version })
    RedirectDispatch(`${easy_path}/changelog/${version}`)
  }

  render() {
    const versions = versionsList.map((v, i) => {
      return {
        content: (
          <>
            {v.label} {i === 0 ? <Badge status="success">Latest</Badge> : ''}
          </>
        ),
        onAction: () => {
          this.handleVersionChange(v.version, v.label)
        },
      }
    })

    return (
      <Page
        fullWidth
        breadcrumbs={[
          {
            content: 'Go to Dashboard',
            onAction: () => {
              document.getElementById('bannerTop').style.display = 'block'
              RedirectDispatch(`${easy_path}/dashboard`)
            },
          },
        ]}
        actionGroups={[
          {
            title: this.state.label,
            actions: versions,
          },
        ]}
      >
        <Layout>
          <Layout.Section>
            <Versions version={this.state.currentVersion} />
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}
