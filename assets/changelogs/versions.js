import React from 'react'
import {
  Heading,
  TextContainer,
  TextStyle,
  Badge,
  Stack,
  Card,
} from '@shopify/polaris'

const Versions = ({ version }) => {
  let comp

  switch (version) {
    case '20200912':
      comp = <Version20200912 />
      break
    case '20200810':
      comp = <Version20200810 />
      break
    case '20200701':
      comp = <Version20200701 />
      break
  }
  return (
    <Card title={`Version ${version}`}>
      <Card.Section>{comp}</Card.Section>
    </Card>
  )
}

export default Versions

const Version20200912 = () => {
  return (
    <Stack vertical spacing="loose">
      <TextContainer>
        <Heading>
          Upload file {'<'} 2MB <Badge status="info">Fixes</Badge>
        </Heading>
        <TextStyle>
          Restrict customers to upload only files less than 2 MB.
        </TextStyle>
      </TextContainer>
      <TextContainer>
        <Heading>
          Multiple return request creation <Badge status="info">Fixes</Badge>
        </Heading>
        <TextStyle></TextStyle>
      </TextContainer>
    </Stack>
  )
}

const Version20200810 = () => {
  return (
    <Stack vertical spacing="loose">
      <TextContainer>
        <Heading>
          Upload file {'<'} 2MB <Badge status="info">Fixes</Badge>
        </Heading>
        <TextStyle>
          Restrict customers to upload only files less than 2 MB.
        </TextStyle>
      </TextContainer>
      <TextContainer>
        <Heading>
          Multiple return request creation <Badge status="info">Fixes</Badge>
        </Heading>
        <TextStyle></TextStyle>
      </TextContainer>
    </Stack>
  )
}

const Version20200701 = () => {
  return (
    <Stack vertical spacing="loose">
      <TextContainer>
        <Heading>
          Upload file {'<'} 2MB <Badge status="info">Fixes</Badge>
        </Heading>
        <TextStyle>
          Restrict customers to upload only files less than 2 MB.
        </TextStyle>
      </TextContainer>
      <TextContainer>
        <Heading>
          Multiple return request creation <Badge status="info">Fixes</Badge>
        </Heading>
        <TextStyle></TextStyle>
      </TextContainer>
    </Stack>
  )
}
