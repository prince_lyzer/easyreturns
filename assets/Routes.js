import React from 'react'
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom'
import MyRouter from './MyRouter'
import RoutePropagator from './RoutePropagator'

import Home from './components/Home'
import GeneralConfigs from './components/general_configs/GeneralConfigs'
import EmailConfigs from './components/email_configs/email-configs'
import EditMailTemplate from './components/email_configs/edit-mail-template'
import ReturnList from './components/return_list/return-list'
import ReturnOrderDesc from './components/return_list/return-order-desc'
import CreateReturn from './components/return_list/create-return'
import ReturnPageConfigure from './components/return_page_configure/return-page-configure'
import LabelTranslations from './components/translations/label-translations'
import NotFound from './components/NotFound'
import Setup from './components/Setup'
import Changelog from './changelogs/changelog'
import { OnBoarding } from './components/onboarding'
import { easy_path, org_easy_path } from './env'

const Routes = (myprops) => {
  const { history, updateTitle, setNoticeToast, setErrorToast } = myprops
  return (
    <BrowserRouter>
      <MyRouter />
      <RoutePropagator />
      <Switch>
        <Route exact path={`${org_easy_path}/install`}>
          <Redirect to={`${easy_path}/`} />
        </Route>

        <Route
          exact
          path={`${easy_path}/`}
          render={(props) => <Home {...props} {...myprops} />}
        ></Route>

        <Route
          exact
          path={`${easy_path}/app-setup`}
          render={(props) => <Setup {...props} {...myprops} />}
        ></Route>

        <Route
          exact
          path={`${easy_path}/onboarding`}
          render={(props) => <OnBoarding {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/dashboard`}
          render={(props) => <Home {...props} {...myprops} />}
        ></Route>

        <Route
          exact
          path={`${easy_path}/returns/new`}
          render={(props) => <CreateReturn {...props} {...myprops} />}
        ></Route>

        <Route
          exact
          path={`${easy_path}/returns/:request_id`}
          render={(props) => <ReturnOrderDesc {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/returns`}
          render={(props) => <ReturnList {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/general-configuration`}
          render={(props) => <GeneralConfigs {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/email-configs`}
          render={(props) => <EmailConfigs {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/email/:email_id`}
          render={(props) => <EditMailTemplate {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/front-configure`}
          render={(props) => <ReturnPageConfigure {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/translations`}
          render={(props) => <LabelTranslations {...props} {...myprops} />}
        ></Route>

        <Route
          path={`${easy_path}/changelog/:version`}
          render={(props) => <Changelog {...props} {...myprops} />}
        ></Route>

        <Route
          render={(props) => <NotFound {...props} updateTitle={updateTitle} />}
        ></Route>
      </Switch>
    </BrowserRouter>
  )
}

export default Routes
