import React from 'react'
import {
  Page,
  Layout,
  Card,
  List,
  TextStyle,
  Badge,
  Button,
  Icon,
} from '@shopify/polaris'
import { HintMajor } from '@shopify/polaris-icons'
import { RedirectDispatch } from '../common/Common'
import { easy_path } from '../env'

export function OnBoarding() {
  return (
    <Page
      fullWidth
      title="Setup Instruction"
      subtitle="We did almost everything for you, There is only one thing is
    left for you to complete app setup."
    >
      <Layout>
        <Layout.Section>
          <Card
            title={
              <>
                Theme Code Paste <Badge status="info">Important</Badge>
              </>
            }
            secondaryFooterActions={[
              {
                content: 'Go to Return Page Configuration',
                onAction: () => {
                  RedirectDispatch(`${easy_path}/front-configure`)
                },
              },
            ]}
          >
            <Card.Section>
              <TextStyle variation="subdued">
                You have to manually make the following changes in your theme.
                You can watch the video or contact us for setup.
              </TextStyle>
              <br />
              <br />
              <List type="number">
                <List.Item>
                  Go to 'Return Page Configurations' under Configurations menu.
                </List.Item>
                <List.Item>
                  Copy and paste the 'Return Page Link' code into specified
                  template file.
                </List.Item>
              </List>
            </Card.Section>
          </Card>
        </Layout.Section>
        <Layout.Section secondary>
          <Card
            title={
              <div className="d-flex">
                <div>
                  <Icon source={HintMajor} color="success" />
                </div>
                &nbsp;Page Tour
              </div>
            }
          >
            <Card.Section>
              <TextStyle>
                At top right corner of every page, There is a 'Page Tour' button
                to provides a tour of that page.
              </TextStyle>
            </Card.Section>
          </Card>
        </Layout.Section>
      </Layout>
    </Page>
  )
}
