import React, { useState, useCallback, useEffect } from 'react'
import {
  Card,
  Pagination,
  Filters,
  Heading,
  ChoiceList,
  Icon,
  DatePicker,
  ResourceList,
  ResourceItem,
  TextStyle,
  Stack,
  Checkbox,
  TextContainer,
} from '@shopify/polaris'
import { MomentFormat, DateFormat, RedirectDispatch } from '../../common/Common'
import { easy_path } from '../../env'
import SearchMajor from '../SearchMajor.svg'
import { SortMinor, CalendarMinor } from '@shopify/polaris-icons'
import { ReturnStatusBadge, ReturnTypeBadge } from './sub-components'

export const ReturnListDataTable2WithFilters = ({
  returnRequests,
  currentPage,
  totalPages,
  handlePagination,
  openConfirmationModal,
  selectedItems,
  setSelectedItems,
  headerSticky,
}) => {
  const [bulkChecked, setBulkChecked] = useState(false)
  const [showHeader, setShowHeader] = useState(false) // show polaris selectable action header

  useEffect(() => {
    if (isEmpty(selectedItems)) {
      handleShowHeaderChange(false)
      handleBulkCheck(false)
    }
  }, [selectedItems])

  const handleShowHeaderChange = useCallback(
    (newValue) => {
      if (window.innerWidth > 360) setShowHeader(newValue)
      else setShowHeader(true)
    },
    [showHeader],
  )

  const handleBulkCheck = useCallback(
    (isChecked) => {
      setBulkChecked(isChecked)
      handleShowHeaderChange(isChecked)

      if (isChecked) {
        setSelectedItems({ selectedItems: returnRequests.map((r) => r.id) })
      }
    },
    [returnRequests],
  )

  const handleSelectedItems = useCallback(
    (items) => {
      setSelectedItems({ selectedItems: items })
      if (items.length == 0) {
        handleShowHeaderChange(false)
        setBulkChecked(false)
      } else {
        handleShowHeaderChange(true)
      }
    },
    [selectedItems],
  )

  return (
    <ReturnRequestList
      returnRequests={returnRequests}
      selectedItems={selectedItems}
      handleSelectedItems={handleSelectedItems}
      bulkChecked={bulkChecked}
      handleBulkCheck={handleBulkCheck}
      showHeader={showHeader}
      currentPage={currentPage}
      totalPages={totalPages}
      handlePagination={handlePagination}
      openConfirmationModal={openConfirmationModal}
      headerSticky={headerSticky}
    />
  )
}

export const ReturnListFilters = ({
  selectedReturnStatus,
  handleRequestStatusChange,
  searchStr,
  handleSearchStrChange,
  helpText,
  selectedSortStatus,
  handleSortChange,
  noRecords,
  selectedBtwDates,
  handleBtwDateChange,
  selectedReturnType,
  handleRequestTypeChange,
}) => {
  const appliedFilters = []
  if (!isEmpty(selectedReturnType)) {
    const key = 'returnType'
    appliedFilters.push({
      key,
      label: returnStatusLabels(key, selectedReturnType),
      onRemove: () => handleRequestTypeChange(),
    })
  }

  if (!isEmpty(selectedReturnStatus)) {
    const key = 'returnStatus'
    appliedFilters.push({
      key,
      label: returnStatusLabels(key, selectedReturnStatus),
      onRemove: () => handleRequestStatusChange(),
    })
  }

  if (!isEmpty(selectedBtwDates)) {
    const key = 'date'
    appliedFilters.push({
      key,
      label: returnBtwDateLabels(key, selectedBtwDates),
      onRemove: () => handleBtwDateChange(null),
    })
  }

  const [queryValue, setQueryValue] = useState(searchStr)
  const handleFiltersQueryChange = useCallback((value) => {
    setQueryValue(value)
    handleSearchStrChange(value === '' ? null : value)
  }, [])
  const handleQueryValueRemove = useCallback(() => {
    setQueryValue(null)
    handleSearchStrChange(null)
  }, [])

  return (
    <Filters
      queryValue={queryValue}
      filters={filters({
        selectedReturnStatus,
        handleRequestStatusChange,
        selectedSortStatus,
        handleSortChange,
        selectedBtwDates,
        handleBtwDateChange,
        selectedReturnType,
        handleRequestTypeChange,
      })}
      appliedFilters={appliedFilters}
      queryPlaceholder="Filter returns"
      onQueryChange={handleFiltersQueryChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={() => {}}
      helpText={helpText}
      disabled={noRecords}
    />
  )
}

function renderItem(item) {
  const { id, order_name, email, return_status, return_type, date_add } = item
  const url = `${easy_path}/returns/${id}`
  return (
    <ResourceItem
      id={id}
      onClick={() => {
        RedirectDispatch(`${easy_path}/returns/${id}`)
      }}
      accessibilityLabel={`View details for ${id}`}
    >
      <Stack distribution="fill">
        <Stack.Item>
          <TextStyle variation="strong">#{id}</TextStyle>
        </Stack.Item>
        <Stack.Item>{order_name}</Stack.Item>
        <Stack.Item>{email || '-'}</Stack.Item>
        <Stack.Item>{ReturnStatusBadge(return_status)}</Stack.Item>
        <Stack.Item>{ReturnTypeBadge(return_type)}</Stack.Item>
        <Stack.Item>
          {DateFormat(date_add, 'MMM DD, YYYY [at] hh:mm a')}
        </Stack.Item>
      </Stack>
    </ResourceItem>
  )
}

function returnStatusLabels(key, value) {
  switch (key) {
    case 'returnStatus':
      if (value[0] === '1') return 'Open'
      else if (value[0] === '2') return 'Accepted'
      else if (value[0] === '3') return 'Completed'
      else if (value[0] === '4') return 'Cancelled'
      else if (value[0] === '5') return 'Archived'
      break
    case 'returnType':
      if (value[0] === '1') return 'Refund'
      else if (value[0] === '2') return 'Exchange'
      break
    default:
      return value
  }
}

function returnBtwDateLabels(key, value) {
  const start = MomentFormat(value.start, 'MMM D, YYYY ')
  const end = MomentFormat(value.end, 'MMM D, YYYY')
  return `${start} - ${end}`
}

function isEmpty(value) {
  if (Array.isArray(value)) {
    return value.length === 0
  } else {
    return value === '' || value == null
  }
}

function ReturnRequestList({
  returnRequests,
  selectedItems,
  handleSelectedItems,
  bulkChecked,
  handleBulkCheck,
  showHeader,
  currentPage,
  totalPages,
  handlePagination,
  openConfirmationModal,
  headerSticky,
}) {
  const resourceName = {
    singular: 'return',
    plural: 'returns',
  }

  const promotedBulkActions = [
    {
      content: 'Accept returns',
      onAction: () => {
        openConfirmationModal('accept_return_requests', selectedItems)
      },
    },
  ]

  const bulkActions = [
    {
      content: 'Complete returns',
      onAction: () => {
        openConfirmationModal('complete_return_requests', selectedItems)
      },
    },
    {
      content: 'Archive returns',
      onAction: () => {
        openConfirmationModal('archive_return_requests', selectedItems)
      },
    },
  ]

  if (returnRequests.length === 0) return <NoResults />
  return (
    <>
      <div className="return-list">
        <RequestHeaders
          showHeader={showHeader}
          bulkChecked={bulkChecked}
          handleBulkCheck={handleBulkCheck}
          headerSticky={headerSticky}
        />
        <ResourceList
          resourceName={resourceName}
          items={returnRequests}
          renderItem={renderItem}
          selectedItems={selectedItems}
          onSelectionChange={handleSelectedItems}
          promotedBulkActions={promotedBulkActions}
          bulkActions={bulkActions}
          showHeader={showHeader}
          selectable
        />
      </div>
      <div className="return-pagination">
        <ReturnListPagination
          currentPage={currentPage}
          totalPages={totalPages}
          handlePagination={handlePagination}
        />
      </div>
    </>
  )
}

function RequestHeaders({
  showHeader,
  bulkChecked,
  handleBulkCheck,
  headerSticky,
}) {
  return (
    <div
      className={'returnList--header ' + (!showHeader ? '' : 'd-none')}
      style={headerSticky}
    >
      <Card.Section subdued>
        <Stack distribution="fill">
          <Stack.Item>
            <Checkbox
              label="Bulk check"
              checked={bulkChecked}
              onChange={handleBulkCheck}
              labelHidden={true}
            />
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">Return Id</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">Order Name</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">Customer Email</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">Return Status</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">Return Type</TextStyle>
          </Stack.Item>
          <Stack.Item>
            <TextStyle variation="strong">Created At</TextStyle>
          </Stack.Item>
        </Stack>
      </Card.Section>
    </div>
  )
}

function NoResults() {
  return (
    <div style={{ textAlign: 'center' }}>
      <img src={`../${SearchMajor}`} width="80px" />
      <div style={{ paddingBottom: '40px', paddingTop: '20px' }}>
        <Heading>No return request found</Heading>
      </div>
    </div>
  )
}

function ReturnListPagination({ currentPage, totalPages, handlePagination }) {
  currentPage = parseInt(currentPage)
  return (
    <Card.Section subdued>
      <Pagination
        label={`Showing ${currentPage} of ${totalPages} Pages`}
        hasPrevious={currentPage > 1}
        onPrevious={() => handlePagination(currentPage - 1)}
        hasNext={currentPage < totalPages}
        onNext={() => handlePagination(currentPage + 1)}
        previousTooltip="Previous"
        nextTooltip="Next"
      />
    </Card.Section>
  )
}

const filters = ({
  selectedReturnStatus,
  handleRequestStatusChange,
  selectedSortStatus,
  handleSortChange,
  selectedBtwDates,
  handleBtwDateChange,
  selectedReturnType,
  handleRequestTypeChange,
}) => {
  return [
    {
      key: 'returnType',
      label: 'Type',
      filter: (
        <ChoiceList
          title="Type"
          titleHidden
          choices={[
            { label: 'Refund', value: '1' },
            { label: 'Exchange', value: '2' },
          ]}
          selected={selectedReturnType || []}
          onChange={handleRequestTypeChange}
          allowMultiple={false}
        />
      ),
      shortcut: true,
    },
    {
      key: 'returnStatus',
      label: 'Status',
      filter: (
        <ChoiceList
          title="Status"
          titleHidden
          choices={[
            { label: 'Open', value: '1' },
            { label: 'Accepted', value: '2' },
            { label: 'Completed', value: '3' },
            { label: 'Canceled', value: '4' },
            { label: 'Archived', value: '5' },
          ]}
          selected={selectedReturnStatus || []}
          onChange={handleRequestStatusChange}
          allowMultiple={false}
        />
      ),
      shortcut: true,
    },
    {
      key: 'sort',
      label: (
        <div className="d-flex align-items-center">
          <Icon source={SortMinor}></Icon>&nbsp;Sort
        </div>
      ),
      filter: (
        <ChoiceList
          title="Sort"
          titleHidden
          choices={[
            { label: 'Date (newest first)', value: 'date_add desc' },
            { label: 'Date (oldest first)', value: 'date_add asc' },
            { label: 'Customer email (A-Z)', value: 'email asc' },
            { label: 'Customer email (Z-A)', value: 'email desc' },
            { label: 'Order name (ascending)', value: 'order_name asc' },
            { label: 'Order name (descending)', value: 'order_name desc' },
          ]}
          selected={selectedSortStatus || []}
          onChange={handleSortChange}
          allowMultiple={false}
        />
      ),
      shortcut: true,
    },
    {
      key: 'date',
      label: (
        <div className="d-flex align-items-center">
          <Icon source={CalendarMinor}></Icon>&nbsp;Date
        </div>
      ),
      filter: (
        <DatePickerExample
          selectedBtwDates={selectedBtwDates}
          setSelectedDates={handleBtwDateChange}
        />
      ),
    },
  ]
}

function DatePickerExample({ selectedBtwDates, setSelectedDates }) {
  const now = new Date()
  const [{ month, year }, setDate] = useState({
    month: now.getMonth(),
    year: now.getFullYear(),
  })

  const handleMonthChange = useCallback(
    (month, year) => setDate({ month, year }),
    [],
  )

  return (
    <DatePicker
      month={month}
      year={year}
      onChange={(val) => setSelectedDates(val)}
      onMonthChange={handleMonthChange}
      selected={selectedBtwDates}
      multiMonth={true}
      allowRange={true}
    />
  )
}

export const confirmationModalContent = (modalFor, totalRequest = 0) => {
  let confirmModalContent = {}

  if (modalFor === 'accept_return_requests') {
    confirmModalContent = {
      actionType: null,
      e:
        'Customers will receive `Return Accepted Mail` only after return is accepted successfully. Mail will be send only if Return Accepted mail notification is enabled.',
      title: `Accept ${totalRequest} return request(s)`,
    }
  } else if (modalFor === 'complete_return_requests') {
    confirmModalContent = {
      actionType: null,
      e:
        'Customers will receive `Return Completed Mail` only after return is completed successfully. Mail will be send only if Return Completed mail notification is enabled.',
      title: `Complete ${totalRequest} return request(s)`,
    }
  } else if (modalFor === 'archive_return_requests') {
    confirmModalContent = {
      actionType: 'danger',
      e:
        'Only Completed and Cancelled returns will be archived. Archived request will not shown in list view. To get archive request, change status to Archive.',
      title: `Archive ${totalRequest} return request(s)`,
    }
  } else {
    return false
  }

  return {
    title: confirmModalContent.title,
    content: (
      <TextContainer>
        <TextStyle>
          <TextStyle> {confirmModalContent.e}</TextStyle>
        </TextStyle>
      </TextContainer>
    ),
  }
}
