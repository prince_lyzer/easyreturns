import React from 'react'
import {
  DescriptionList,
  Card,
  Thumbnail,
  TextContainer,
  TextStyle,
  TextField,
  Layout,
  Select,
  ChoiceList,
  DataTable,
  FormLayout,
  Button,
} from '@shopify/polaris'
import {
  FulfillmentStatus,
  PaymentStatus,
  ReturnTypeBadge,
  ReturnStatusBadge,
} from './sub-components'
import {
  moneyCurrency,
  RedirectDispatch,
  DateFormat,
} from '../../common/Common'
import img_placeholder from '../screenshots/placeholder-images-image_large.png'
import { hasIn, isEmpty, toPairs } from 'lodash'
import { easy_path } from '../../env'

export const ReturnLineItemsDataTable = ({
  orderLineItems,
  returnLineItems,
  moneyFormat,
  returnReasons,
  handleReturnItemsQty,
  handleReturnItemReason,
  returnArr,
}) => {
  const rows = toPairs(returnLineItems).map((item) => {
    const orderLineItem = orderLineItems[item[0]]
    return [
      item[0],
      orderLineItem.src ? (
        <Thumbnail source={orderLineItem.src} alt="" size="small" />
      ) : (
        <Thumbnail
          source={`../../${img_placeholder}`}
          alt="Image"
          size="small"
        />
      ),
      orderLineItem.title, // 2
      orderLineItem.price, // 3
      item[1], // returnable quantity 4
      item[1], // 5
      orderLineItem.sku || '-',
    ]
  })

  const rowsD = rows.map((item) => {
    let line_item_id = item[0]
    return [
      item[1],
      <>
        {item[2]} <br />
        <TextStyle variation="subdued">sku: {item[6]}</TextStyle>
      </>,
      moneyCurrency(item[3], moneyFormat),
      <TextField
        type="number"
        value={
          hasIn(returnArr, `${line_item_id}.qty`)
            ? returnArr[line_item_id]['qty'].toString()
            : '0'
        }
        max={item[4]}
        min="0"
        suffix={`/ ${item[4]}`}
        onChange={(val) => handleReturnItemsQty(line_item_id, val)}
      />,
      <Select
        disabled={
          hasIn(returnArr, `${line_item_id}.qty`)
            ? returnArr[line_item_id]['qty'] == 0
            : true
        }
        placeholder="Choose Reason for Return"
        options={returnReasons}
        value={
          hasIn(returnArr, `${line_item_id}.reason`)
            ? returnArr[line_item_id]['reason']
            : ''
        }
        onChange={(val) => handleReturnItemReason(line_item_id, val)}
      />,
    ]
  })

  return (
    <DataTable
      columnContentTypes={['text', 'text', 'numeric', 'numeric', 'numeric']}
      headings={['Image', 'Product Title', 'Price', 'Return Qty', null]}
      hoverable={false}
      verticalAlign="middle"
      rows={rowsD}
    />
  )

  // return (
  //   <table className="f-table">
  //     <thead>
  //       <tr>
  //         <th style={{ width: '75px' }}>Image</th>
  //         <th>Product Title</th>
  //         <th>Price</th>
  //         <th className="text-right">Quantity</th>
  //         <th className="text-right">Return Qty</th>
  //         <th className="text-right"></th>
  //       </tr>
  //     </thead>
  //     <tbody>
  //       {rows.map((item) => {
  //         let line_item_id = item[0]
  //         return (
  //           <React.Fragment key={item[0]}>
  //             <tr key={line_item_id}>
  //               <td>{item[1]}</td>
  //               <td>
  //                 {item[2]} <br />
  //                 {item[6]}
  //               </td>
  //               <td
  //                 className="no-wrap"
  //                 dangerouslySetInnerHTML={{
  //                   __html: moneyCurrency(item[3], moneyFormat),
  //                 }}
  //               ></td>
  //               <td className="text-right">{item[4]}</td>
  //               <td className="text-right">
  //                 <TextField
  //                   type="number"
  //                   value={
  //                     hasIn(returnArr, `${line_item_id}.qty`)
  //                       ? returnArr[line_item_id]['qty'].toString()
  //                       : '0'
  //                   }
  //                   max={item[5]}
  //                   min="0"
  //                   suffix={item[4]}
  //                   onChange={(val) => handleReturnItemsQty(line_item_id, val)}
  //                 />
  //               </td>
  //               <td
  //                 key={`reason_${item[0]}`}
  //                 style={{ backgroundColor: '#f0f8ff' }}
  //               >
  //                 <Select
  //                   labelInline
  //                   placeholder="Choose return reason"
  //                   options={returnReasons}
  //                 />
  //               </td>
  //             </tr>
  //             <tr
  //               key={`reason_${item[0]}`}
  //               style={{ backgroundColor: '#f0f8ff' }}
  //             ></tr>
  //           </React.Fragment>
  //         )
  //       })}
  //     </tbody>
  //   </table>
  // )
}

export const OrderLineItemsDataTable = ({
  itemsNotReturned,
  orderLineItems,
  moneyFormat,
}) => {
  if (itemsNotReturned.length == 0) {
    return (
      <TextContainer>
        <TextStyle>No other order items found</TextStyle>
      </TextContainer>
    )
  }

  return (
    <table className="f-table">
      <thead>
        <tr>
          <th>Image</th>
          <th>Name</th>
          <th>Sku</th>
          <th>Price</th>
          <th className="text-right">Total Quantity</th>
        </tr>
      </thead>
      <tbody>
        {Object.entries(itemsNotReturned).map(([line_item_id, item_qty], i) => {
          const orderLineItem = orderLineItems[line_item_id]

          return (
            <React.Fragment key={i}>
              <tr key={line_item_id}>
                <td>
                  {orderLineItem.image_url ? (
                    <Thumbnail
                      source={orderLineItem.image_url}
                      alt=""
                      size="small"
                    />
                  ) : (
                    <Thumbnail
                      source={`../../${img_placeholder}`}
                      alt="Image"
                      size="small"
                    />
                  )}
                </td>
                <td>
                  {orderLineItem.product_title +
                    (orderLineItem.variant_title &&
                      ` - ${orderLineItem.variant_title}`)}
                </td>
                <td>{orderLineItem.item_sku ? orderLineItem.item_sku : '-'}</td>
                <td
                  className="no-wrap"
                  dangerouslySetInnerHTML={{
                    __html: moneyCurrency(orderLineItem.price, moneyFormat),
                  }}
                ></td>
                <td className="text-right">{orderLineItem.quantity}</td>
              </tr>
            </React.Fragment>
          )
        })}
      </tbody>
    </table>
  )
}

export const PaymentOptions = ({
  isCodOrder,
  paymentOptions,
  selectedPaymentMethod,
  handleStateChange,
  handlePaymentInputs,
  paymentInputs,
  returnMethod,
}) => {
  if (!isCodOrder || returnMethod == '2') return null

  paymentOptions.map((po, index) => {
    paymentOptions[index]['renderChildren'] = (selected) => (
      <UserPaymentInputs
        selected={selected}
        handle={po.value}
        key={po.value}
        handlePaymentInputs={handlePaymentInputs}
        paymentInputs={paymentInputs}
      />
    )
  })

  // console.log('paymentOptions', paymentOptions)

  return (
    <Layout.Section oneHalf>
      <Card title="Choose payment options for refund amount">
        <Card.Section>
          <ChoiceList
            title=""
            choices={paymentOptions}
            selected={selectedPaymentMethod}
            onChange={(val) => {
              handleStateChange({ selectedPaymentMethod: val })
            }}
          />
        </Card.Section>
      </Card>
    </Layout.Section>
  )
}

function UserPaymentInputs({
  selected,
  handle,
  handlePaymentInputs,
  paymentInputs,
}) {
  if (!selected) return null

  if (handle === 'bank_details') {
    return (
      <FormLayout>
        <TextField
          type="text"
          placeholder="Bank account number"
          id="bank_account_no"
          showCharacterCount={true}
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle)
              ? paymentInputs[handle]['bank_account_no']
              : ''
          }
        />
        <TextField
          type="text"
          placeholder="Bank IFSC Code"
          id="bank_ifsc_code"
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle)
              ? paymentInputs[handle]['bank_ifsc_code']
              : ''
          }
        />
        <TextField
          type="text"
          placeholder="Bank account holder's name"
          id="bank_holder_name"
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle)
              ? paymentInputs[handle]['bank_holder_name']
              : ''
          }
        />
      </FormLayout>
    )
  } else if (handle === 'upi_id') {
    return (
      <FormLayout>
        <TextField
          type="text"
          placeholder="Enter UPI Id"
          id="upi_id"
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle) ? paymentInputs[handle]['upi_id'] : ''
          }
        />
      </FormLayout>
    )
  } else if (handle === 'google_pay') {
    return (
      <FormLayout>
        <TextField
          type="text"
          placeholder="Enter Google Pay Mobile Number"
          id="google_pay"
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle)
              ? paymentInputs[handle]['google_pay']
              : ''
          }
        />
      </FormLayout>
    )
  } else if (handle === 'phonepe_mobile_no') {
    return (
      <FormLayout>
        <TextField
          type="text"
          placeholder="Enter Phonepe mobile number"
          id="phonepe_mobile_no"
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle)
              ? paymentInputs[handle]['phonepe_mobile_no']
              : ''
          }
        />
      </FormLayout>
    )
  } else if (handle === 'paytm_mobile_no') {
    return (
      <FormLayout>
        <TextField
          type="text"
          placeholder="Enter Paytm mobile number"
          id="paytm_mobile_no"
          onChange={handlePaymentInputs}
          value={
            hasIn(paymentInputs, handle)
              ? paymentInputs[handle]['paytm_mobile_no']
              : ''
          }
        />
      </FormLayout>
    )
  } else if (handle === 'coupon_code') {
    return null
  }
}

export const OrderInfoCard = ({ orderDetails, itemsReturned }) => {
  const customerDetails = orderDetails.customer_details
  const shippingAddress = orderDetails.shipping_address

  return (
    <Layout>
      <Layout.Section>
        <Card
          title={
            <>
              Order details{' '}
              <TextStyle variation="strong">#{orderDetails.name}</TextStyle>{' '}
              {FulfillmentStatus(orderDetails.fulfillment_status)}{' '}
              {PaymentStatus(orderDetails.payment_status)}
            </>
          }
          sectioned
          actions={[
            {
              content: `View order`,
              onAction: () => {
                RedirectDispatch(`/orders/${orderDetails.id}`, 1, true)
              },
            },
          ]}
        >
          <DescriptionList
            spacing="tight"
            items={[
              {
                term: 'Order created at',
                description: orderDetails.created_at,
              },
              {
                term: 'Customer name',
                description: customerDetails.name || '-',
              },
              {
                term: 'Customer email',
                description: customerDetails.email || '-',
              },
            ]}
          />
        </Card>
        <PreviousReturns itemsReturned={itemsReturned} />
      </Layout.Section>
      <Layout.Section secondary>
        <Card title="Shipping address" sectioned>
          <TextContainer spacing="tight">
            <p>
              {shippingAddress.name}
              <br />
              {shippingAddress.address1}
              <br />
              {shippingAddress.address2}
              <br />
              {shippingAddress.city}
              <br />
              {shippingAddress.country} {shippingAddress.province}{' '}
              {shippingAddress.zip}
            </p>
          </TextContainer>
        </Card>
      </Layout.Section>
    </Layout>
  )
}

const PreviousReturns = ({ itemsReturned }) => {
  if (isEmpty(itemsReturned)) return null

  const rowMarkup = toPairs(itemsReturned).map((item) => [
    <Button
      plain
      onClick={() => RedirectDispatch(`${easy_path}/returns/${item[0]}`)}
    >
      <TextStyle variation="strong">#{item[0]}</TextStyle>
    </Button>,
    ReturnTypeBadge(item[1]['return_type']),
    ReturnStatusBadge(item[1]['return_status']),
    item[1]['return_line_items'].length,
    DateFormat(item[1]['date_add'], 'MMM DD, YYYY [at] hh:mm a'),
  ])

  return (
    <Card title="Previous returns">
      <DataTable
        columnContentTypes={['text', 'text', 'text', 'text', 'numeric']}
        headings={[
          'Return',
          'Return type',
          'Return status',
          'Total items returned',
          'Created at',
        ]}
        hoverable={false}
        verticalAlign="middle"
        rows={rowMarkup}
      />
    </Card>
  )
}
