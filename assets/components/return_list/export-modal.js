import React, { useState, useCallback } from 'react'
import { Modal, TextContainer, TextStyle, DatePicker } from '@shopify/polaris'

export const ExportBySkuModal = ({
  selectedDates,
  exportBySkuModalActive,
  exportDataLoading,
  handlePriAction,
  handleClose,
  handleDateChange,
}) => {
  return (
    <Modal
      open={exportBySkuModalActive}
      onClose={() => handleClose({ exportBySkuModalActive: false })}
      title={<>Export by sku</>}
      primaryAction={{
        content: 'Export data',
        loading: exportDataLoading,
        onAction: () => {
          handlePriAction('GET', 'exportData')
        },
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            handleClose({ exportBySkuModalActive: false })
          },
        },
      ]}
      limitHeight={false}
    >
      <Modal.Section>
        <TextContainer>
          <TextStyle>
            This export report gives you an idea why your products get returned
            by customer.
          </TextStyle>
          <br />
          <TextStyle>
            Kindly select date betweens to export sku wise reason and total
            return request respectively.
          </TextStyle>
          <br />
          <TextStyle variation="negative">
            Note: Products without Sku's will not count in this report.
          </TextStyle>
        </TextContainer>
        <br />
        <TextStyle variation="strong">Select date</TextStyle>
        <DatePickerExample
          selectedDates={selectedDates}
          setSelectedDates={handleDateChange}
        />
      </Modal.Section>
    </Modal>
  )
}

function DatePickerExample({ selectedDates, setSelectedDates }) {
  const now = new Date()
  const [{ month, year }, setDate] = useState({
    month: now.getMonth(),
    year: now.getFullYear(),
  })

  const handleMonthChange = useCallback(
    (month, year) => setDate({ month, year }),
    [],
  )

  return (
    <DatePicker
      month={month}
      year={year}
      onChange={(val) => setSelectedDates({ selectedDates: val })}
      onMonthChange={handleMonthChange}
      selected={selectedDates}
      multiMonth={true}
      allowRange={true}
    />
  )
}
