import React from 'react'
import AxiosObj from '../AxiosIns'
import _, { filter } from 'lodash'
import {
  Page,
  Thumbnail,
  Layout,
  Badge,
  Spinner,
  Card,
  Banner,
  Stack,
  TextContainer,
  TextStyle,
} from '@shopify/polaris'
import { ProductReturnsMinor } from '@shopify/polaris-icons'
import {
  RedirectDispatch,
  AjaxErrorHandle,
  appendToUrl,
  ConfirmationModal,
} from '../../common/Common'
import { ReturnListPageSkeleton } from '../PageSkeletons/pageSkeleton'
import { PageTour } from '../../common/page-tour'
import { ExportBySkuModal } from './export-modal'
import {
  ReturnListDataTable2WithFilters,
  confirmationModalContent,
  ReturnListFilters,
} from './return-list-datatable2'
import { GetReturnListParams } from './sub-components'
import { easy_path } from '../../env'
import { isEmpty } from 'lodash'

const helpTextOrg = ''

export default class ReturnList extends React.Component {
  constructor(props) {
    super(props)
    const now = new Date()
    this.state = {
      skeleton: true,
      returnRequests: [],
      totalRequest: 0,
      currentPage: 1,
      totalPages: 1,
      selectedReturnStatus: null,
      searchStr: null,
      helpText: helpTextOrg,
      selectedSortStatus: ['date_add desc'],
      exportBySkuModalActive: false,
      selectedDates: { start: now, end: now },
      selectedBtwDates: null,
      exportDataLoading: false,
      isTourOpen: false,
      noRecords: true,
      selectedReturnType: null,
      modalConfirm: false,
      modalConfirmLoading: false,
      confirmModalTitle: '',
      confirmModalContent: '',
      confirmModalPriAction: null,
      confirmModalPriBtnLabel: '',
      selectedItems: [],
      filterSticky: { position: 'relative' },
      headerSticky: { position: 'sticky' },
      errorBannerData: null,
    }
  }

  componentDidMount() {
    this.props.updateTitle('Returns')
    appendToUrl(this.props.location, { shop, host })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
    })

    RedirectDispatch(`${easy_path}/returns?${url_params}`)
    this.handleBackendRequest('GET', 'returnRequestList', params, 1)
  }

  handlePagination = (page) => {
    this.handleStateChange({
      filterSticky: { position: 'sticky' },
      headerSticky: { position: 'relative' },
      selectedItems: [],
    })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
      page,
    })

    RedirectDispatch(`${easy_path}/returns?${url_params}`)
    this.handleBackendRequest('GET', 'returnRequestList', params, 2)
    window.scrollTo(0, 0)
  }

  handleRequestStatusChange = (selectedStatus = []) => {
    this.setState({ selectedReturnStatus: [...selectedStatus] })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
      selectedStatus,
      page: 1,
    })

    RedirectDispatch(`${easy_path}/returns?${url_params}`)
    this.handleBackendRequest('GET', 'returnRequestList', params)
  }

  handleRequestTypeChange = (selectedType = []) => {
    this.setState({ selectedReturnType: [...selectedType] })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
      selectedType,
      page: 1,
    })
    RedirectDispatch(`${easy_path}/returns?${url_params}`)
    this.handleBackendRequest('GET', 'returnRequestList', params)
  }

  handleSortChange = (selectedSortStatus) => {
    this.setState({ selectedSortStatus: [...selectedSortStatus] })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
      selectedSortStatus,
    })
    RedirectDispatch(`${easy_path}/returns?${url_params}`)
    this.handleBackendRequest('GET', 'returnRequestList', params)
  }

  handleSearchStrChange = (searchStr) => {
    this.setState({ searchStr })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
      searchStr,
    })

    RedirectDispatch(`${easy_path}/returns?${url_params}`)

    if (searchStr === null || searchStr.length > 2) {
      this.handleBackendRequest('GET', 'returnRequestList', params)
      // return null;
    }
  }

  handleBtwDateChange = (selectedBtwDates) => {
    this.setState({ selectedBtwDates: selectedBtwDates })

    const { params, url_params } = GetReturnListParams({
      location: this.props.location,
      start: selectedBtwDates ? selectedBtwDates.start : '',
      end: selectedBtwDates ? selectedBtwDates.end : '',
    })

    RedirectDispatch(`${easy_path}/returns?${url_params}`)
    this.handleBackendRequest('GET', 'returnRequestList', params)
  }

  handleStateChange = (object) => {
    this.setState(object)
  }

  handleBackendRequest = (method, requestName, params = {}, initial = 0) => {
    let url

    if (requestName === 'exportData') {
      this.handleStateChange({ exportDataLoading: true })
      url = '/return-request/export'
      params = this.state.selectedDates
    } else if (requestName === 'returnRequestList') {
      url = '/return-requests'
      this.setState({
        helpText: (
          <div>
            <Spinner accessibilityLabel="Spinner example" size="small" /> {'  '}
            Loading return request
          </div>
        ),
      })
      if (initial === 1 && params.start) {
        this.handleStateChange({
          selectedBtwDates: {
            start: new Date(params.start),
            end: new Date(params.end),
          },
          selectedReturnStatus: [params.status],
        })
      }
    } else if (requestName === 'bulkReturnRequestAction') {
      this.handleStateChange({ modalConfirmLoading: true })
      url = '/return-request/bulk-actions'
    }

    const axiosObj = { method, url }
    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = params

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        const notice = res_data.notice || ''
        const error = res_data.error || ''

        if (requestName === 'exportData') {
          if (error !== '') {
            this.props.setErrorToast(error)
          } else if (notice !== '') {
            this.props.setNoticeToast(notice)
          }
        } else if (requestName === 'returnRequestList') {
          if (!isEmpty(return_data)) {
            this.setState({
              returnRequests: return_data.return_requests,
              totalRequest: return_data.total_return_requests,
              currentPage: return_data.page,
              totalPages: return_data.total_pages,
              selectedReturnStatus: return_data.selected_status,
              searchStr: return_data.search,
              skeleton: false,
              selectedReturnType: return_data.selected_type,
            })
            if (initial === 1) {
              this.setState({
                noRecords: return_data.count_all_return_requests ? false : true,
              })
            }
          }
        } else if (requestName === 'bulkReturnRequestAction') {
          if (error !== '') {
            this.props.setErrorToast(error)
          } else if (notice !== '') {
            this.props.setNoticeToast(notice)
          }

          if (!isEmpty(return_data)) {
            this.handlePagination(this.state.currentPage)
          }
        }
      })
      .catch((err) => {
        const backendError = err.response.data.data
        AjaxErrorHandle(err, this.props.setErrorToast)

        if (requestName === 'bulkReturnRequestAction') {
          return backendError
        }
      })
      .then((data) => {
        if (requestName === 'exportData') {
          this.handleStateChange({ exportDataLoading: false })
          this.handleStateChange({ exportBySkuModalActive: false })
        } else if (requestName === 'returnRequestList') {
          this.setState({
            helpText: helpTextOrg,
          })
          if (initial === 2) {
            this.handleStateChange({
              filterSticky: { position: 'relative' },
              headerSticky: { position: 'sticky' },
            })
          }
        } else if (requestName === 'bulkReturnRequestAction') {
          this.handleStateChange({
            modalConfirmLoading: false,
            modalConfirm: false,
            errorBannerData: data ?? null,
          })

          if (!this.state.errorBannerData) {
            this.handleStateChange({
              selectedItems: [],
            })
          }
        }
      })
  }

  openConfirmationModal = (modalFor, selectedReturns) => {
    if (modalFor === '') return
    let priAction = null,
      priActionBtnLabel,
      status

    const totalSelectedReturns =
      selectedReturns == 'All' ? 15 : selectedReturns.length

    const t_b = confirmationModalContent(modalFor, totalSelectedReturns)

    if (modalFor === 'accept_return_requests') {
      priActionBtnLabel = 'Accept return requests'
      status = 2
    } else if (modalFor === 'complete_return_requests') {
      priActionBtnLabel = 'Complete return requests'
      status = 3
    } else if (modalFor === 'archive_return_requests') {
      priActionBtnLabel = 'Archive return requests'
      status = 5
    }
    priAction = () =>
      this.handleBackendRequest('POST', 'bulkReturnRequestAction', {
        status: status,
        return_ids: selectedReturns,
      })

    this.setState({
      confirmModalTitle: t_b.title,
      modalConfirm: true,
      confirmModalPriAction: priAction,
      confirmModalContent: t_b.content,
      confirmModalPriBtnLabel: priActionBtnLabel,
    })
  }

  render() {
    const {
      totalRequest,
      currentPage,
      totalPages,
      selectedReturnStatus,
      searchStr,
      helpText,
      selectedSortStatus,
      selectedDates,
      exportBySkuModalActive,
      exportDataLoading,
      isTourOpen,
      selectedBtwDates,
      selectedReturnType,
      returnRequests,
      modalConfirm,
      modalConfirmLoading,
      confirmModalTitle,
      confirmModalContent,
      confirmModalPriAction,
      confirmModalPriBtnLabel,
      selectedItems,
      filterSticky,
      headerSticky,
    } = this.state

    if (this.state.skeleton) {
      return <ReturnListPageSkeleton title="Return List" />
    }

    const filterStickyClass = {
      position: 'sticky',
      top: '0px',
      zIndex: '10000',
      background: 'white',
    }

    return (
      <Page
        fullWidth
        title={
          <div>
            Returns{' '}
            {totalRequest > 0 && <Badge status="info">{totalRequest}</Badge>}
          </div>
        }
        thumbnail={
          <Thumbnail
            source={ProductReturnsMinor}
            size="small"
            alt="Return Icon"
          />
        }
        primaryAction={{
          content: 'Create return',
          onAction: () => RedirectDispatch(`${easy_path}/returns/new`),
          primary: true,
        }}
        secondaryActions={[
          {
            content: 'Page Tour',
            onAction: () => {
              this.handleStateChange({ isTourOpen: true })
            },
          },
          {
            content: 'Export',
            onAction: () => {
              this.handleStateChange({ exportBySkuModalActive: true })
            },
            outline: true,
          },
        ]}
      >
        <ExportBySkuModal
          selectedDates={selectedDates}
          exportBySkuModalActive={exportBySkuModalActive}
          exportDataLoading={exportDataLoading}
          handlePriAction={this.handleBackendRequest}
          handleClose={this.handleStateChange}
          handleDateChange={this.handleStateChange}
        />

        <ConfirmationModal
          title={confirmModalTitle}
          openModal={modalConfirm}
          handleClose={this.handleStateChange}
          modalLoading={modalConfirmLoading}
          content={confirmModalContent}
          primaryAction={confirmModalPriAction}
          confirmBtnLabel={confirmModalPriBtnLabel}
        />

        <PageTour
          isTourOpen={isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="Returns"
        />

        <Layout>
          <Layout.Section>
            <ErrorBanner
              errorBannerData={this.state.errorBannerData}
              handleStateChange={this.handleStateChange}
            />
            <Card>
              <div className="returnList--filter" style={filterSticky}>
                <Card.Section>
                  <ReturnListFilters
                    selectedReturnStatus={selectedReturnStatus}
                    handleRequestStatusChange={this.handleRequestStatusChange}
                    searchStr={searchStr}
                    handleSearchStrChange={this.handleSearchStrChange}
                    helpText={helpText}
                    selectedSortStatus={selectedSortStatus}
                    handleSortChange={this.handleSortChange}
                    noRecords={this.state.noRecords}
                    selectedBtwDates={selectedBtwDates}
                    handleBtwDateChange={this.handleBtwDateChange}
                    selectedReturnType={selectedReturnType}
                    handleRequestTypeChange={this.handleRequestTypeChange}
                  />
                </Card.Section>
              </div>
              <ReturnListDataTable2WithFilters
                returnRequests={returnRequests}
                currentPage={currentPage}
                totalPages={totalPages}
                handlePagination={this.handlePagination}
                openConfirmationModal={this.openConfirmationModal}
                selectedItems={selectedItems}
                setSelectedItems={this.handleStateChange}
                headerSticky={headerSticky}
              />
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}

const ErrorBanner = ({ errorBannerData, handleStateChange }) => {
  if (!errorBannerData) return null

  return (
    <div>
      <Banner
        title="Warning"
        status="warning"
        onDismiss={() => {
          handleStateChange({ errorBannerData: null })
        }}
      >
        <Stack vertical>
          <Stack.Item>
            <TextContainer>
              <TextStyle>
                Some returns need coupon code for refund amount, so complete
                those return requests manually.
              </TextStyle>
            </TextContainer>
          </Stack.Item>
          {errorBannerData.map((id) => {
            return (
              <Stack.Item key={id}>
                <TextStyle variation="strong">#{id}</TextStyle> ,
              </Stack.Item>
            )
          })}
        </Stack>
      </Banner>
      <div className="mb-25"></div>
    </div>
  )
}
