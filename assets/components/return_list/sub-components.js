import React, { useState, useCallback } from 'react'
import {
  Badge,
  Modal,
  TextContainer,
  TextField,
  TextStyle,
  Stack,
  Banner,
  DropZone,
  List,
  Thumbnail,
  Caption,
} from '@shopify/polaris'

export function GetReturnListParams({
  location,
  page = null,
  selectedStatus: status = '',
  searchStr = false,
  selectedSortStatus = '',
  start = null,
  end = null,
  selectedType = '',
}) {
  let qPage = queryParam(location.search, 'page'),
    qStatus = queryParam(location.search, 'status'),
    qSearchStr = queryParam(location.search, 'search'),
    qSortOrder = queryParam(location.search, 'order'),
    qStart = queryParam(location.search, 'start'),
    qEnd = queryParam(location.search, 'end'),
    qType = queryParam(location.search, 'type')

  let params = {},
    url_params = remainingParams(location.search, [
      'page',
      'status',
      'search',
      'order',
      'type',
      'start',
      'end',
    ])

  qPage = page ? page : qPage ? qPage.trim() : ''

  if (Array.isArray(status)) qStatus = status.length > 0 ? status.join(',') : ''
  else qStatus = qStatus ? qStatus.trim() : ''

  if (typeof searchStr === 'string') qSearchStr = searchStr
  else
    qSearchStr = searchStr === null ? '' : qSearchStr ? qSearchStr.trim() : ''

  if (Array.isArray(selectedSortStatus)) qSortOrder = selectedSortStatus[0]
  else
    qSortOrder =
      qSortOrder === null
        ? 'date_add desc'
        : qSortOrder
        ? decodeURI(qSortOrder.trim())
        : ''

  qStart = start !== null ? start : qStart ? new Date(qStart) : ''
  qEnd = end !== null ? end : qEnd ? new Date(qEnd) : ''

  if (Array.isArray(selectedType))
    qType = selectedType.length > 0 ? selectedType.join(',') : ''
  else qType = qType ? qType.trim() : ''

  if (qPage !== '') {
    params = { ...params, page: qPage }
    url_params['page'] = qPage
  }

  if (qStatus !== '') {
    params = { ...params, status: qStatus }
    url_params['status'] = qStatus
  }

  if (qSearchStr !== '') {
    params = { ...params, search: qSearchStr }
    url_params['search'] = qSearchStr
  }

  if (qSortOrder !== '') {
    params = { ...params, order: qSortOrder }
    url_params['order'] = qSortOrder //encodeURI(qSortOrder)
  }

  if (qStart !== '') {
    params = { ...params, start: qStart }
    url_params['start'] = qStart
  }

  if (qEnd !== '') {
    params = { ...params, end: qEnd }
    url_params['end'] = qEnd
  }

  if (qType !== '') {
    params = { ...params, type: qType }
    url_params['type'] = qType
  }

  const urlParams = new URLSearchParams(url_params)
  // console.log(params)
  // console.log(urlParams.toString())

  return {
    params,
    url_params: urlParams.toString(),
  }
}

export function queryParam(searchStr, param) {
  const query = new URLSearchParams(searchStr)
  const token = query.get(param)
  return token
}

export function remainingParams(searchStr, excludes) {
  let allParams = new URLSearchParams(searchStr)
  let remainingParams = {}

  for (let param of allParams.entries()) {
    if (excludes.includes(param[0]) === false) {
      remainingParams[param[0]] = param[1]
    }
  }

  return remainingParams
}

export function FulfillmentStatus(status) {
  if (status === 'fulfilled')
    return <Badge progress="complete">Fulfilled</Badge>
  else if (status === null)
    return (
      <Badge progress="incomplete" status="attention">
        Unfulfilled
      </Badge>
    )
  else if (status === 'partial')
    return (
      <Badge progress="partiallyComplete" status="warning">
        Partially fulfilled
      </Badge>
    )

  return null
}

export function PaymentStatus(status) {
  if (status === 'authorized')
    return (
      <Badge progress="incomplete" status="attention">
        Authorized
      </Badge>
    )
  else if (status === 'pending')
    return (
      <Badge progress="incomplete" status="warning">
        Pending
      </Badge>
    )
  else if (status === 'paid')
    return (
      <Badge progress="complete" status="success">
        Paid
      </Badge>
    )
  else if (status === 'partially_paid')
    return (
      <Badge progress="partiallyComplete" status="warning">
        Partially paid
      </Badge>
    )
  else if (status === 'refunded')
    return (
      <Badge progress="complete" status="success">
        Refunded
      </Badge>
    )
  else if (status === 'voided')
    return <Badge progress="incomplete">Voided</Badge>
  else if (status === 'partially_refunded')
    return (
      <Badge progress="partiallyComplete" status="warning">
        Partially refunded
      </Badge>
    )
  else if (status === 'unpaid')
    return (
      <Badge progress="incomplete" status="info">
        Unpaid
      </Badge>
    )

  return null
}

export function ReturnStatusBadge(status) {
  if (status == 1) return <Badge status="info">Open</Badge>
  else if (status == 2)
    return (
      <Badge progress="partiallyComplete" status="attention">
        Accepted
      </Badge>
    )
  else if (status == 3)
    return (
      <Badge progress="complete" status="success">
        Completed
      </Badge>
    )
  else if (status == 4) return <Badge status="critical">Canceled</Badge>
  else if (status == 5) return <Badge>Archived</Badge>
}

export function ReturnTypeBadge(type) {
  if (type == 1) return <Badge status="info">Refund</Badge>
  else if (type == 2) return <Badge status="attention">Exchange</Badge>
}

export const CancelReturnModal = ({
  active,
  requestId,
  cancelReason,
  loading,
  handleInputChange,
  handlePriAction,
}) => {
  return (
    <Modal
      open={active}
      onClose={() => handleInputChange({ cancelReturnModalActive: false })}
      title={
        <>
          Cancel return request #
          <TextStyle variation="strong">{requestId}</TextStyle>
        </>
      }
      loading={loading}
      primaryAction={{
        content: 'Cancel return',
        onAction: () => {
          handlePriAction(4)
        },
        loading: loading,
        disabled: cancelReason && cancelReason.length === 0 ? true : false,
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            handleInputChange({ cancelReturnModalActive: false })
          },
          disabled: loading,
        },
      ]}
    >
      <Modal.Section>
        <Stack vertical>
          <Stack.Item>
            <TextContainer>
              <p>are you sure you want to cancel this return request ?</p>
            </TextContainer>
          </Stack.Item>
          <Stack.Item>
            <TextField
              label="Cancel reason (required)"
              type="string"
              value={cancelReason}
              multiline={3}
              onChange={(v) => handleInputChange({ cancelReason: v })}
              helpText="The cancel reason will sent to customer through Cancel Request mail notification, and also visible in return request page."
            />
          </Stack.Item>
        </Stack>
      </Modal.Section>
    </Modal>
  )
}

export const CompleteReturnModal = ({
  active,
  requestId,
  loading,
  handleInputChange,
  handlePriAction,
  paymentType,
}) => {
  const [couponCode, setCouponCode] = useState('')
  const [couponCodeError, setCouponCodeError] = useState(false)
  const [files, setFiles] = useState([])
  const [rejectedFiles, setRejectedFiles] = useState([])

  const handleDrop = useCallback(
    (_droppedFiles, acceptedFiles, rejectedFiles) => {
      setFiles((files) => [...files, ...acceptedFiles])
      setRejectedFiles(rejectedFiles)
    },
    [],
  )

  const handleClose = useCallback(() => {
    setFiles([])
    handleInputChange({ completeReturnModalActive: false })
  })

  const handleCouponCode = useCallback((val) => {
    val = val.trim()
    setCouponCode(val)

    if (val === '') {
      setCouponCodeError('Coupon code is required.')
    } else {
      setCouponCodeError(false)
    }
  })

  return (
    <Modal
      open={active}
      onClose={handleClose}
      title={
        <>
          Complete return request #
          <TextStyle variation="strong">{requestId}</TextStyle>
        </>
      }
      // loading={loading}
      primaryAction={{
        content: 'Complete return',
        onAction: () => {
          handlePriAction(3, { files, coupon_code: couponCode })
        },
        loading: loading,
        disabled:
          paymentType === 'coupon_code' && couponCode === '' ? true : false,
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: handleClose,
          disabled: loading,
        },
      ]}
    >
      <Modal.Section>
        <Stack vertical>
          <Stack.Item>
            <TextContainer>
              <TextStyle>
                Before completing this return request, Please make sure you
                received refunded items and initiated the refund to the customer
                account.
              </TextStyle>
            </TextContainer>
          </Stack.Item>
          {paymentType === 'coupon_code' && (
            <CouponCodeInput
              couponCode={couponCode}
              handleCouponCode={handleCouponCode}
              couponCodeError={couponCodeError}
            />
          )}
          <Stack.Item>
            <DropZoneWithImageFileUpload
              files={files}
              handleDrop={handleDrop}
              rejectedFiles={rejectedFiles}
            />
          </Stack.Item>
        </Stack>
      </Modal.Section>
    </Modal>
  )
}

function DropZoneWithImageFileUpload({ files, handleDrop, rejectedFiles }) {
  const hasError = rejectedFiles.length > 0
  // const fileUpload = !files.length && <DropZone.FileUpload />
  const fileUpload = <DropZone.FileUpload />
  const uploadedFiles = files.length > 0 && (
    <Stack vertical>
      {files.map((file, index) => (
        <Stack alignment="center" key={index}>
          <Thumbnail
            size="small"
            alt={file.name}
            source={window.URL.createObjectURL(file)}
          />
          <div>
            {file.name} <Caption>{file.size} bytes</Caption>
          </div>
        </Stack>
      ))}
    </Stack>
  )

  const errorMessage = hasError && (
    <Banner
      title="The following images couldn’t be uploaded:"
      status="critical"
    >
      <List type="bullet">
        {rejectedFiles.map((file, index) => (
          <List.Item key={index}>
            {`"${file.name}" is not supported. File type must be .gif, .jpg, .png or .svg.`}
          </List.Item>
        ))}
      </List>
    </Banner>
  )

  return (
    <Stack vertical>
      {errorMessage}
      <TextContainer>
        <TextStyle>Attach a screenshot (optional)</TextStyle>
      </TextContainer>
      {uploadedFiles}
      <div style={{ width: 50, height: 50 }}>
        <DropZone
          accept="image/*"
          type="image"
          onDrop={handleDrop}
          // variableHeight
          allowMultiple={false}
        >
          {fileUpload}
        </DropZone>
      </div>
      <Stack.Item>
        <TextContainer>
          <TextStyle variation="negative">
            Attached screenshots will be attach in Complete Return mail.
          </TextStyle>
        </TextContainer>
      </Stack.Item>
    </Stack>
  )
}

function CouponCodeInput({ couponCode, handleCouponCode, couponCodeError }) {
  return (
    <TextField
      label="Coupon code"
      type="string"
      value={couponCode}
      onChange={handleCouponCode}
      requiredIndicator={true}
      error={couponCodeError}
      helpText={
        couponCodeError != ''
          ? null
          : 'Enter coupon code which will be sent to customer in return confirmation mail.'
      }
      clearButton={true}
      onClearButtonClick={() => handleCouponCode('')}
    />
  )
}
