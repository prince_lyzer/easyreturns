import React from 'react'
import {
  Page,
  Layout,
  Card,
  Badge,
  Heading,
  TextContainer,
  TextStyle,
} from '@shopify/polaris'
import {
  ArchiveMinor,
  CancelSmallMinor,
  MobileAcceptMajor,
} from '@shopify/polaris-icons'
import AxiosObj from '../AxiosIns'
import {
  RedirectDispatch,
  AjaxErrorHandle,
  LoadingModal,
  appendToUrl,
  DateFormat,
  PreviewHtmlModal,
  PreviewModal,
} from '../../common/Common'
import { ReturnOrderDescPageSkeleton } from '../PageSkeletons/pageSkeleton'
import { PageTour } from '../../common/page-tour'
import {
  ReturnStatusBadge,
  CancelReturnModal,
  CompleteReturnModal,
  ReturnTypeBadge,
} from './sub-components'
import {
  ReturnLineItemsDataTable,
  OrderLineItemsDataTable,
  CustomerDetailsCard,
  OrderDetails,
  PaymentDetails,
  ShippingDetailsCard,
  ReturnQueryForm,
  AllReturnQueries,
  ReturnImages,
} from './return-order-desc-util'
import { easy_path } from '../../env'
import { isEmpty } from 'lodash'
import { HOST_URL } from '../../env'

class ReturnOrderDesc extends React.Component {
  constructor(props) {
    super(props)
    const request_id = props.match.params.request_id
    this.state = {
      customerDetails: [],
      orderDetails: [],
      requestDetails: [],
      orderLineItems: [],
      returnLineItems: [],
      itemsNotReturned: [],
      requestId: request_id,
      hasNext: false,
      hasPrevious: false,
      cancelReturnModalActive: false,
      cancelReason: '',
      moneyFormat: '',
      cancelModalLoading: false,
      customerNote: '',
      isTourOpen: false,
      modalLoading: false,
      completeReturnModalActive: false,
      completeModalLoading: false,
      previewModalActive: false,
      previewModalContent: '',
      previewModalLoading: false,
      returnQuery: '',
      returnQueries: [],
      returnImages: [],
      previewImageModalLoading: false,
      returnImageSrc: {},
      previousReturnImage: false,
      nextReturnImage: false,
      priActionLoading: false,
      paymentDetails: null,
    }
  }

  componentDidMount() {
    this.props.updateTitle(`Return #${this.state.requestId}`)
    appendToUrl(this.props.location, { shop, host })
    this.handleBackendRequest('GET', 'getDetails')
  }

  handlePagination = (id) => {
    RedirectDispatch(`${easy_path}/returns/${id}`)
    this.props.updateTitle(`Return #${id}`)
    this.handleBackendRequest('GET', 'getDetails', { id }, true)
  }

  handleRequestStatus = (status, otherData = null) => {
    this.handleBackendRequest('POST', 'updateReturnStatus', {
      status,
      ...otherData,
    })
  }

  handleBackendRequest = (
    method,
    requestName,
    params = {},
    newRequest = false,
  ) => {
    let url,
      data = {},
      status
    const requestId = newRequest ? params.id : this.state.requestId
    const { cancelReason } = this.state

    if (requestName === 'getDetails') {
      if (newRequest) this.setState({ requestDetails: [] })
      url = `/return-requests/${requestId}`
    } else if (requestName === 'updateReturnStatus') {
      url = `/return-request/update-status/${requestId}`
      status = params.status
      data = new FormData()
      data.append('status', status)

      if (status === 4) {
        this.setState({ cancelModalLoading: true })
        data.append('cancel_reason', cancelReason)
      } else if (status === 3) {
        const files = params.files
        this.setState({ completeModalLoading: true })
        files.forEach((file, i) => {
          data.append(`attachments[${i}]`, file)
        })
        data.append('coupon_code', params.coupon_code)
      } else {
        this.setState({ modalLoading: true })
      }
    } else if (requestName === 'ReturnQuerySubmit') {
      this.setState({ previewModalLoading: true })
      url = `/return-request/contact/${requestId}`
      data = { query: this.state.returnQuery }
    } else if (requestName === 'previewQuery') {
      this.setState({ modalLoading: true })
      url = `/return-request/contact/preview/${requestId}`
      data = { query: this.state.returnQuery }
    } else if (requestName === 'getExternalWindowToken') {
      url = `/return-request/token/${requestId}`
    }

    const axiosObj = { method, url }

    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = data

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        const notice = res_data.notice || ''
        const error = res_data.error || ''

        if (requestName === 'getDetails') {
          const canceled_reason = return_data.request_details.canceled_reason

          this.setState({
            customerDetails: return_data.customer_details,
            orderDetails: return_data.order_details,
            requestDetails: return_data.request_details,
            orderLineItems: return_data.order_line_items,
            returnLineItems: return_data.return_line_items,
            itemsNotReturned: return_data.line_items_not_returned,
            hasPrevious: return_data.has_prev,
            hasNext: return_data.has_next,
            requestId: requestId,
            cancelReason: canceled_reason ? canceled_reason : '',
            moneyFormat: return_data.money_format,
            customerNote: return_data.request_details['customer_note'],
            returnQueries: return_data.return_queries,
            returnImages: return_data.return_images,
            paymentDetails: getReturnPaymentDetails(
              return_data.request_details.payment_details,
            ),
          })
        } else if (requestName === 'updateReturnStatus') {
          if (!isEmpty(return_data)) {
            this.setState({
              requestDetails: return_data.request_details,
              paymentDetails: getReturnPaymentDetails(
                return_data.request_details.payment_details,
              ),
            })
          }

          if (notice !== '') this.props.setNoticeToast(notice)

          if (typeof return_data.cancel_reason !== 'undefined') {
            this.handleStateChange({ cancelReturnModalActive: false })
          }
        } else if (requestName === 'ReturnQuerySubmit') {
          if (notice !== '') this.props.setNoticeToast(notice)
          this.handleStateChange({
            returnQueries: return_data.return_queries,
            returnQuery: '',
          })
        } else if (requestName === 'previewQuery') {
          this.setState({
            previewModalContent: res_data,
            previewModalActive: true,
          })
        } else if (requestName === 'getExternalWindowToken') {
          this.handleStateChange({ previewImageModalLoading: false })
          RedirectDispatch(
            `${HOST_URL}external?token=${return_data.token}&id=${return_data.img_id}`,
            2,
            true,
          )
        }
      })
      .catch((err) => {
        AjaxErrorHandle(
          err,
          this.props.setErrorToast,
          'return-order-desc',
          this.props.history,
        )

        if (requestName === 'ReturnQuerySubmit') return Promise.resolve(false)
      })
      .then((data) => {
        if (requestName === 'updateReturnStatus') {
          if (status === 4) {
            this.setState({
              cancelModalLoading: false,
            })
          } else if (status === 3) {
            this.setState({
              completeModalLoading: false,
              completeReturnModalActive: false,
            })
          } else {
            this.setState({ modalLoading: false })
          }
        } else if (requestName === 'ReturnQuerySubmit') {
          if (typeof data != 'undefined' && data === false) {
            this.setState({
              previewModalLoading: false,
            })
          } else
            this.setState({
              previewModalLoading: false,
              previewModalActive: false,
            })
        } else if (requestName === 'previewQuery') {
          this.setState({ modalLoading: false })
        } else if (requestName === 'getExternalWindowToken') {
          this.handleStateChange({
            priActionLoading: false,
          })
        }
      })
  }

  handleStateChange = (obj) => {
    this.setState(obj)
  }

  handleImageCarousel = (currentImageId, type) => {
    const returnImages = this.state.returnImages
    returnImages.map((image, key) => {
      if (currentImageId === image.id) {
        if (type === -1 && key !== 0) {
          this.handleStateChange({
            returnImageSrc: {
              src: `${HOST_URL}uploads/returnImages/${
                returnImages[key - 1].name
              }`,
              id: returnImages[key - 1].id,
            },
            previousReturnImage: key - 1 > 0 ? true : false,
            nextReturnImage: key < returnImages.length ? true : false,
          })
          return
        } else if (type === 1 && key !== returnImages.length) {
          this.handleStateChange({
            returnImageSrc: {
              src: `${HOST_URL}uploads/returnImages/${
                returnImages[key + 1].name
              }`,
              id: returnImages[key + 1].id,
            },
            previousReturnImage: true,
            nextReturnImage: key + 2 === returnImages.length ? false : true,
          })
          return
        }
      }
    })
  }

  handleNewWindowAction = (currentImageId) => {
    this.handleStateChange({ priActionLoading: true })
    this.handleBackendRequest('GET', 'getExternalWindowToken', {
      image_id: currentImageId,
    })
  }

  render() {
    const {
      requestDetails,
      requestId,
      hasNext,
      hasPrevious,
      returnLineItems,
      orderLineItems,
      itemsNotReturned,
      orderDetails,
      customerDetails,
      moneyFormat,
      customerNote,
      cancelReason,
      cancelReturnModalActive,
      cancelModalLoading,
      modalLoading,
      completeReturnModalActive,
      completeModalLoading,
      returnQueries,
      previewModalActive,
      previewModalContent,
      previewModalLoading,
      returnQuery,
      returnImages,
      previewImageModalLoading,
      returnImageSrc,
      previousReturnImage,
      nextReturnImage,
      priActionLoading,
      paymentDetails,
    } = this.state

    if (requestDetails.length === 0) {
      return <ReturnOrderDescPageSkeleton />
    }

    return (
      <Page
        fullWidth
        breadcrumbs={[
          {
            content: 'Return Order',
            onAction: () => {
              RedirectDispatch(`${easy_path}/returns`)
            },
          },
        ]}
        title={`#${requestId}`}
        titleMetadata={
          <>
            {ReturnTypeBadge(requestDetails.return_type)}{' '}
            {ReturnStatusBadge(requestDetails.return_status)}
          </>
        }
        subtitle={DateFormat(
          requestDetails.date_add,
          'MMMM D, YYYY [at] HH:mm a',
        )}
        pagination={pagination({
          hasPrevious,
          hasNext,
          handlePagination: this.handlePagination,
        })}
        actionGroups={getActionLabels({
          requestStatus: requestDetails.return_status,
          handleRequestStatus: this.handleRequestStatus,
        })}
        secondaryActions={[
          {
            content: 'Page Tour',
            onAction: () => {
              this.handleStateChange({ isTourOpen: true })
            },
          },
        ]}
      >
        <CancelReturnModal
          active={cancelReturnModalActive}
          requestId={requestId}
          cancelReason={cancelReason}
          loading={cancelModalLoading}
          handleInputChange={this.handleStateChange}
          handlePriAction={this.handleRequestStatus}
        />
        <CompleteReturnModal
          active={completeReturnModalActive}
          requestId={requestId}
          loading={completeModalLoading}
          handleInputChange={this.handleStateChange}
          handlePriAction={this.handleRequestStatus}
          paymentType={paymentDetails ? paymentDetails.payment_type : null}
        />
        <PreviewModal
          openModal={previewImageModalLoading}
          src={returnImageSrc}
          closeModal={this.handleStateChange}
          modalLoading="previewImageModalLoading"
          relative={false}
          title="Return Images"
          handleImageCarousel={this.handleImageCarousel}
          hasPreviousImage={previousReturnImage}
          hasNextImage={nextReturnImage}
          handleNewWindowAction={this.handleNewWindowAction}
          priActionLoading={priActionLoading}
        />
        <PreviewHtmlModal
          active={previewModalActive}
          title="Preview Query Mail"
          previewModalContent={previewModalContent}
          closeModal={this.handleStateChange}
          primaryAction={{
            content: 'Send mail',
            onAction: () =>
              this.handleBackendRequest('POST', 'ReturnQuerySubmit', {
                query: returnQuery,
              }),
            loading: previewModalLoading,
          }}
          previewModalLoading={previewModalLoading}
        />
        <LoadingModal modalLoading={modalLoading} />
        <PageTour
          isTourOpen={this.state.isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="ReturnOrderDesc"
        />
        <Layout>
          <Layout.Section>
            <div className="returned-line-items">
              <Card
                title={
                  <Heading>
                    Return Items{' '}
                    <span className="badge-left">
                      <Badge status="info">{returnLineItems.length}</Badge>
                    </span>
                  </Heading>
                }
                primaryFooterAction={primaryFooterAction({
                  requestStatus: requestDetails.return_status,
                  handleRequestStatus: this.handleRequestStatus,
                  handleStateChange: this.handleStateChange,
                })}
                secondaryFooterActions={secondaryFooterAction({
                  requestStatus: requestDetails.return_status,
                  handleStateChange: this.handleStateChange,
                })}
              >
                <Card.Section>
                  <ReturnLineItemsDataTable
                    returnLineItems={returnLineItems}
                    orderLineItems={orderLineItems}
                    moneyFormat={moneyFormat}
                  />
                </Card.Section>

                {customerNote && (
                  <Card.Section subdued title="Customer note">
                    <TextContainer>
                      <p>{customerNote}</p>
                    </TextContainer>
                  </Card.Section>
                )}

                {requestDetails.return_status === 4 && cancelReason !== '' && (
                  <Card.Section title="Canceled reason">
                    <TextContainer>
                      <TextStyle>{cancelReason}</TextStyle>
                    </TextContainer>
                  </Card.Section>
                )}
              </Card>
            </div>

            <div className="order-line-items">
              <Card
                title={
                  <Heading>
                    Other Items{' '}
                    <Badge status="info">
                      {Object.entries(itemsNotReturned).length}
                    </Badge>
                  </Heading>
                }
                sectioned
              >
                <OrderLineItemsDataTable
                  itemsNotReturned={itemsNotReturned}
                  orderLineItems={orderLineItems}
                  moneyFormat={moneyFormat}
                />
              </Card>
            </div>

            <div className="return-images">
              <Card
                title={
                  <Heading>
                    Return Images <Badge>{returnImages.length}</Badge>
                  </Heading>
                }
              >
                <Card.Section>
                  <ReturnImages
                    images={returnImages}
                    handleClick={this.handleStateChange}
                  />
                </Card.Section>
              </Card>
            </div>

            <div className="customer-queries mt-16">
              <Card title={<Heading>Return Queries</Heading>}>
                <Card.Section>
                  <ReturnQueryForm
                    returnQuery={returnQuery}
                    handleChange={this.handleStateChange}
                    handleBackendRequest={this.handleBackendRequest}
                  />

                  <AllReturnQueries queries={returnQueries} />
                </Card.Section>
              </Card>
            </div>
          </Layout.Section>

          <Layout.Section secondary>
            <div className="card_sec_desc_list order-details">
              <Card
                title={`Order details ${orderDetails.order_name}`}
                actions={[
                  {
                    content: `View order`,
                    onAction: () => {
                      RedirectDispatch(
                        `/orders/${orderDetails.order_id}`,
                        1,
                        true,
                      )
                    },
                  },
                ]}
              >
                <OrderDetails orderDetails={orderDetails} />
                <PaymentDetails payment_details={paymentDetails} />
              </Card>
            </div>

            <div className="card_sec_desc_list customer-details mt-16">
              <Card
                title="Customer details"
                actions={[
                  {
                    content: `#${customerDetails.customer_id}`,
                    onAction: () => {
                      RedirectDispatch(
                        `/customers/${customerDetails.customer_id}`,
                        1,
                        true,
                      )
                    },
                  },
                ]}
              >
                <CustomerDetailsCard
                  customerDetails={customerDetails}
                  notificationEmail={requestDetails.notification_email}
                />
                <ShippingDetailsCard
                  shippingDetails={orderDetails.shipping_address}
                />
              </Card>
            </div>
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}

export default ReturnOrderDesc

const secondaryFooterAction = ({ requestStatus, handleStateChange }) => {
  let content = '',
    next_status,
    destructive = false,
    icon = null
  if (requestStatus === 1) {
    // pending
    icon = CancelSmallMinor
    content = 'Cancel Request'
    next_status = 4
    destructive = true
  } else if (requestStatus === 2) {
    // initiated or Accepted
    // content = "Request Completed"
    // next_status = 3;
  } else if (requestStatus === 3) {
    // Processed
  } else if (requestStatus === 4) {
    // Cancelled
  }

  if (content === '') return null
  return [
    {
      content: content,
      icon: icon,
      destructive: destructive,
      onAction: () => {
        handleStateChange({ cancelReturnModalActive: true })
      },
    },
  ]
}

const primaryFooterAction = ({
  requestStatus,
  handleRequestStatus,
  handleStateChange,
}) => {
  let content = '',
    next_status = 0,
    icon = null
  if (requestStatus === 1) {
    // pending
    icon = MobileAcceptMajor
    content = 'Accept Request'
    next_status = 2
  } else if (requestStatus === 2) {
    // initiated or Accepted
    content = 'Complete Request'
    next_status = 3
  } else if (requestStatus === 3) {
    // Processed
  } else if (requestStatus === 4) {
    // Cancelled
  }

  if (content === '') return

  const obj = {
    content: content,
    onAction: () => {
      next_status === 3
        ? handleStateChange({ completeReturnModalActive: true })
        : handleRequestStatus(next_status)
    },
  }

  if (icon) obj.icon = icon

  return obj
}

const getActionLabels = ({ requestStatus, handleRequestStatus }) => {
  if (requestStatus !== 3) return []
  return [
    {
      title: 'Action',
      accessibilityLabel: 'Return order action',
      actions: [
        {
          content: 'Archive',
          icon: ArchiveMinor,
          accessibilityLabel: 'Archive return request',
          onAction: () => {
            handleRequestStatus(5)
          },
        },
      ],
    },
  ]
}

const pagination = ({ hasPrevious, hasNext, handlePagination }) => {
  return {
    hasPrevious: hasPrevious ? true : false,
    hasNext: hasNext ? true : false,
    nextTooltip: 'Next',
    previousTooltip: 'Previous',
    onNext: () => {
      handlePagination(hasNext)
    },
    onPrevious: () => {
      handlePagination(hasPrevious)
    },
  }
}

function getReturnPaymentDetails(payment_details) {
  let paymentDetails = null

  if (payment_details) {
    payment_details = JSON.parse(payment_details)
    const payment_type = Object.keys(payment_details)[0]
    const payment_detail = payment_details[payment_type]
    paymentDetails = { payment_type, payment_detail }
  }
  return paymentDetails
}
