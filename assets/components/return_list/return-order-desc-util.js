import React, { useCallback, useState } from 'react'
import {
  DescriptionList,
  Card,
  Thumbnail,
  TextContainer,
  TextStyle,
  ExceptionList,
  TextField,
  Button,
  Stack,
  Collapsible,
  Link,
  Tooltip,
  Icon,
} from '@shopify/polaris'
import {
  EmailMajor,
  SendMajor,
  QuestionMarkMinor,
} from '@shopify/polaris-icons'
import { FulfillmentStatus, PaymentStatus } from './sub-components'
import { moneyCurrency, MomentFormat } from '../../common/Common'
import img_placeholder from '../screenshots/placeholder-images-image_large.png'
import { HOST_URL } from '../../env'

export const ReturnLineItemsDataTable = ({
  returnLineItems,
  orderLineItems,
  moneyFormat,
}) => {
  const rows = returnLineItems.map((item) => {
    const orderLineItem = orderLineItems[item.line_item_id]
    return [
      item.id,
      orderLineItem.image_url ? (
        <Thumbnail source={orderLineItem.image_url} alt="" size="small" />
      ) : (
        <Thumbnail
          source={`../../${img_placeholder}`}
          alt="Image"
          size="small"
        />
      ),
      orderLineItem.product_title +
        (orderLineItem.variant_title && ` - ${orderLineItem.variant_title}`),
      orderLineItem.price,
      orderLineItem.quantity,
      item.return_qty,
      item.return_reason,
      orderLineItem.item_sku ? orderLineItem.item_sku : '-',
    ]
  })

  return (
    <table className="f-table">
      <thead>
        <tr>
          <th style={{ width: '75px' }}>Image</th>
          <th>Product Title</th>
          <th>SKU</th>
          <th>Price</th>
          <th className="text-right">Quantity</th>
          <th className="text-right">Return Qty</th>
        </tr>
      </thead>
      <tbody>
        {rows.map((item) => {
          return (
            <React.Fragment key={item[0]}>
              <tr key={item[0]}>
                <td>{item[1]}</td>
                <td>{item[2]}</td>
                <td>{item[7]}</td>
                <td
                  className="no-wrap"
                  dangerouslySetInnerHTML={{
                    __html: moneyCurrency(item[3], moneyFormat),
                  }}
                ></td>
                <td className="text-right">{item[4]}</td>
                <td className="text-right">{item[5]}</td>
              </tr>
              <tr
                key={`reason_${item[0]}`}
                style={{ backgroundColor: '#f0f8ff' }}
              >
                <td colSpan="6" className="text-left">
                  <TextContainer>
                    <TextStyle variation="strong">
                      <span style={{ paddingRight: '10px' }}>
                        Return reason
                      </span>
                    </TextStyle>
                    <TextStyle>{item[6]}</TextStyle>
                  </TextContainer>
                </td>
              </tr>
            </React.Fragment>
          )
        })}
      </tbody>
      <tfoot></tfoot>
    </table>
  )
}

export const OrderLineItemsDataTable = ({
  itemsNotReturned,
  orderLineItems,
  moneyFormat,
}) => {
  if (itemsNotReturned.length == 0) {
    return (
      <TextContainer>
        <TextStyle>No other order items found</TextStyle>
      </TextContainer>
    )
  }

  return (
    <table className="f-table">
      <thead>
        <tr>
          <th>Image</th>
          <th>Name</th>
          <th>Sku</th>
          <th>Price</th>
          <th className="text-right">Total Quantity</th>
        </tr>
      </thead>
      <tbody>
        {Object.entries(itemsNotReturned).map(([line_item_id, item_qty], i) => {
          const orderLineItem = orderLineItems[line_item_id]

          return (
            <React.Fragment key={i}>
              <tr key={line_item_id}>
                <td>
                  {orderLineItem.image_url ? (
                    <Thumbnail
                      source={orderLineItem.image_url}
                      alt=""
                      size="small"
                    />
                  ) : (
                    <Thumbnail
                      source={`../../${img_placeholder}`}
                      alt="Image"
                      size="small"
                    />
                  )}
                </td>
                <td>
                  {orderLineItem.product_title +
                    (orderLineItem.variant_title &&
                      ` - ${orderLineItem.variant_title}`)}
                </td>
                <td>{orderLineItem.item_sku ? orderLineItem.item_sku : '-'}</td>
                <td
                  className="no-wrap"
                  dangerouslySetInnerHTML={{
                    __html: moneyCurrency(orderLineItem.price, moneyFormat),
                  }}
                ></td>
                <td className="text-right">{orderLineItem.quantity}</td>
              </tr>
            </React.Fragment>
          )
        })}
      </tbody>
    </table>
  )
}

export const CustomerDetailsCard = ({ customerDetails, notificationEmail }) => {
  return (
    <Card.Section>
      <DescriptionList
        spacing="tight"
        items={[
          {
            term: 'Name',
            description: customerDetails.name || '-',
          },
          {
            term: 'Email',
            description: customerDetails.email || '-',
          },
          {
            term: 'Customer Since',
            description: customerDetails.customer_since,
          },
          {
            term: (
              <div className="d-flex">
                Notification Email{'  '}
                <Tooltip
                  active={false}
                  content="This email is used to communicate to customer regarding this return."
                >
                  <Icon source={QuestionMarkMinor} />
                </Tooltip>
              </div>
            ),
            description: notificationEmail
              ? notificationEmail
              : customerDetails.email || '-',
          },
        ]}
      />
    </Card.Section>
  )
}

export const OrderDetails = ({ orderDetails }) => {
  return (
    <Card.Section>
      <DescriptionList
        spacing="tight"
        items={[
          {
            term: 'Id',
            description: `#${orderDetails.order_id}`,
          },
          // {
          //   term: "Order Total",
          //   description: "Rs. 1000.0",
          // },
          {
            term: 'Created At',
            description: orderDetails.order_created_at,
          },
          {
            term: 'Fulfillment Status',
            description: FulfillmentStatus(orderDetails.fulfillment_status),
          },
          {
            term: 'Payment Status',
            description: PaymentStatus(orderDetails.payment_status),
          },
        ]}
      />
    </Card.Section>
  )
}

export const PaymentDetails = ({ payment_details }) => {
  if (!payment_details) return null
  const payment_type = payment_details.payment_type
  const payment_detail = payment_details.payment_detail
  let list = [],
    payment_type_label

  if (payment_type === 'bank_details') {
    payment_type_label = 'Bank Account'

    list = [
      {
        term: 'Account Number',
        description: payment_detail.bank_account_no,
      },
      {
        term: 'IFSC Code',
        description: payment_detail.bank_ifsc_code,
      },
      {
        term: 'Holder Name',
        description: payment_detail.bank_holder_name,
      },
    ]
  } else if (payment_type === 'paytm_mobile_no') {
    payment_type_label = 'Paytm Mobile Number'

    list = [
      {
        term: 'Paytm Mobile No',
        description: payment_detail.paytm_mobile_no,
      },
    ]
  } else if (payment_type === 'upi_id') {
    payment_type_label = 'UPI Id'
    list = [
      {
        term: 'UPI Id',
        description: payment_detail.upi_id,
      },
    ]
  } else if (payment_type === 'phonepe_mobile_no') {
    payment_type_label = 'Phonepe'
    list = [
      {
        term: 'Phonepe Mobile No',
        description: payment_detail.phonepe_mobile_no,
      },
    ]
  } else if (payment_type === 'google_pay') {
    payment_type_label = 'Google Pay'
    list = [
      {
        term: 'Google Pay',
        description: payment_detail.google_pay,
      },
    ]
  } else if (payment_type === 'coupon_code') {
    payment_type_label = 'Coupon Code'
    list = [
      {
        term: 'Coupon code',
        description: payment_detail.coupon_code ?? '-N/A',
      },
    ]
  }

  if (typeof payment_type_label === 'undefined') {
    return null
  }

  return (
    <Card.Section title={`Payment Details / ${payment_type_label}`}>
      <div className="card_sec_desc_list">
        <DescriptionList spacing="tight" items={list} />
      </div>
    </Card.Section>
  )
}

export const ShippingDetailsCard = ({ shippingDetails }) => {
  if (!shippingDetails) return null
  shippingDetails = JSON.parse(shippingDetails)

  return (
    <Card.Section title="Shipping Address">
      <TextContainer spacing="tight">
        <p>
          {shippingDetails.name}
          <br />
          {shippingDetails.address1}
          <br />
          {shippingDetails.address2}
          <br />
          {shippingDetails.city} {shippingDetails.province}{' '}
          {shippingDetails.zip}
          <br />
          {shippingDetails.country}
        </p>
      </TextContainer>
    </Card.Section>
  )
}

export const ReturnQueryForm = ({
  returnQuery,
  handleChange,
  handleBackendRequest,
}) => {
  const [disabled, setDisabled] = useState(true)

  const updateQuery = useCallback((v) => {
    handleChange({ returnQuery: v })
    setDisabled(v.trim().length ? false : true)
  })

  return (
    <div>
      <TextField
        placeholder="Write your query here..."
        value={returnQuery}
        onChange={updateQuery}
        helpText="Directly send a query to customer contact email address"
        multiline={1}
        connectedRight={
          <Button
            disabled={disabled}
            icon={SendMajor}
            onClick={() => handleBackendRequest('POST', 'previewQuery')}
          >
            SEND
          </Button>
        }
      />
    </div>
  )
}

export const AllReturnQueries = ({ queries }) => {
  const [open, setOpen] = useState(false)
  const handleToggle = useCallback(() => setOpen((open) => !open), [])

  const items = queries.map((i) => {
    return {
      icon: EmailMajor,
      description: (
        <div style={{ marginTop: '10px' }}>
          <TextContainer spacing="tight">
            <TextStyle variation="subdued">
              {MomentFormat(i.created_at, 'MMMM D, YYYY [at] HH:mm a')}
            </TextStyle>
            <p style={{ color: '#000' }}>{i.message}</p>
          </TextContainer>
        </div>
      ),
    }
  })

  return (
    <div style={{ marginTop: '15px' }}>
      <Stack vertical>
        {queries.length && (
          <Button
            onClick={handleToggle}
            ariaExpanded={open}
            ariaControls="basic-collapsible"
            size="slim"
          >
            Previous Queries
          </Button>
        )}

        <Collapsible
          open={open}
          id="basic-collapsible"
          transition={{ duration: '500ms', timingFunction: 'ease-in-out' }}
          expandOnPrint
        >
          <ExceptionList items={items} />
        </Collapsible>
      </Stack>
    </div>
  )
}

export const ReturnImages = ({ images, handleClick }) => {
  if (!images.length) {
    return (
      <TextContainer>
        <TextStyle>No return images uploaded by customers.</TextStyle>
      </TextContainer>
    )
  }

  const all_images = images.map((image, key) => {
    return (
      <div key={image.id} className="return-image-div">
        <Link
          onClick={() =>
            handleClick({
              previewImageModalLoading: true,
              returnImageSrc: {
                src: `${HOST_URL}uploads/returnImages/${image.name}`,
                id: image.id,
              },
              previousReturnImage: key ? true : false,
              nextReturnImage: key === images.length - 1 ? false : true,
            })
          }
        >
          <img
            src={`${HOST_URL}uploads/returnImages/${image.name}`}
            alt={image.original_name}
            className="return-image"
          />
        </Link>
      </div>
    )
  })

  return <div className="d-flex return-image-container">{all_images}</div>
}
