import React, { useCallback, useEffect, useRef } from 'react'
import {
  Page,
  Layout,
  Card,
  Heading,
  Stack,
  TextField,
  Button,
  RadioButton,
  TextStyle,
  PageActions,
  Banner,
  List,
  Caption,
} from '@shopify/polaris'
import { ExternalMinor } from '@shopify/polaris-icons'
import AxiosObj from '../AxiosIns'
import {
  RedirectDispatch,
  AjaxErrorHandle,
  LoadingModal,
  appendToUrl,
  ConfirmationModal,
} from '../../common/Common'
import {
  ReturnLineItemsDataTable,
  PaymentOptions,
  OrderInfoCard,
} from './create-return-util'
import { easy_path } from '../../env'
import { indexOf, isEmpty, unset } from 'lodash'

class CreateReturn extends React.Component {
  constructor(props) {
    super(props)
    const request_id = props.match.params.request_id
    this.state = {
      orderDetails: [],
      orderLineItems: [],
      returnLineItems: [],
      customerDetails: [],
      itemsReturned: [],
      returnReasons: [],
      isCodOrder: false,
      paymentOptions: [],
      selectedPaymentMethod: [],
      paymentInputs: {},
      configs: [],
      notificationEmail: '',
      customerContactEmail: null,
      returnArr: [],
      returnMethod: 1,
      customerNote: '',
      searchString: null,
      searchBtnLoading: false,
      searchError: null,
      responseError: null,
      returnRequestId: null,
      modalLoading: false,
      modalConfirm: false,
      modalConfirmLoading: false,
      goBackToReturns: false,
    }
  }

  componentDidMount() {
    this.props.updateTitle(`Return order `)
    appendToUrl(this.props.location, { shop, host })
  }

  findOrder = () => {
    let { searchString } = this.state
    searchString = searchString ? searchString.trim() : ''
    if (searchString == '')
      this.handleStateChange({
        searchError: 'Enter valid shopify order number',
      })

    if (searchString)
      this.handleBackendRequest('GET', 'findOrder', { q: searchString })
  }

  handleBackendRequest = (method, requestName, params = {}) => {
    let url,
      data = {}

    if (requestName === 'findOrder') {
      this.handleStateChange({ searchBtnLoading: true })
      url = `/returns/new`
    } else if (requestName === 'saveReturnForm') {
      this.handleStateChange({ responseError: null, modalLoading: true })
      const {
        orderDetails,
        returnMethod,
        returnArr,
        customerNote,
        selectedPaymentMethod,
        paymentInputs,
        customerContactEmail,
      } = this.state
      url = `/returns/${orderDetails.id}`
      data = {
        return_items: returnArr,
        return_method: returnMethod,
        customer_note: customerNote,
        payment_option: !isEmpty(selectedPaymentMethod)
          ? selectedPaymentMethod[0]
          : 0,
        payment_details: paymentInputs[selectedPaymentMethod] || [],
        notification_email: customerContactEmail,
      }
    }

    const axiosObj = { method, url }

    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = data

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        const notice = res_data.notice || ''
        const error = res_data.error || ''

        if (notice.trim() !== '') {
          this.props.setNoticeToast(notice)
        }

        if (requestName === 'findOrder') {
          if (error.trim() !== '') {
            const errorMessage = isEmpty(return_data) ? (
              error
            ) : (
              <>
                {error}
                <Button
                  plain
                  icon={ExternalMinor}
                  onClick={() => {
                    RedirectDispatch(`/orders/${return_data.id}`, 1, true)
                  }}
                >
                  View order
                </Button>
              </>
            )
            this.handleStateChange({ searchError: errorMessage })
          } else if (!isEmpty(return_data)) {
            this.setState({
              orderDetails: return_data.order_details,
              orderLineItems: return_data.line_items,
              returnLineItems: return_data.return_line_items,
              itemsReturned: return_data.returned_line_items,
              customerDetails: return_data.customer_details,
              returnReasons: return_data.return_reasons,
              isCodOrder: return_data.is_cod_order,
              paymentOptions: return_data.payment_options,
              configs: return_data.configs,
              returnMethod:
                indexOf(return_data.configs.return_methods, '1') !== -1 ? 1 : 2,
              notificationEmail: return_data.notification_email,
            })
          }
        } else if (requestName === 'saveReturnForm') {
          if (error.trim() !== '') {
            this.props.setErrorToast(error.trim())
            if (!isEmpty(return_data))
              this.handleStateChange({ responseError: return_data })
          }

          this.setState({ returnRequestId: return_data.id })
        }
      })
      .catch((err) => {
        AjaxErrorHandle(err, this.props.setErrorToast)

        if (requestName === 'ReturnQuerySubmit') return Promise.resolve(false)
      })
      .then((data) => {
        if (requestName === 'findOrder') {
          this.setState({ searchBtnLoading: false })
        } else if (requestName === 'saveReturnForm') {
          this.handleStateChange({ modalLoading: false })
          window.scrollTo(0, 0)
        }
      })
  }

  handleStateChange = (obj) => {
    this.setState(obj)
  }

  handleClearButton = () => {
    this.handleStateChange({ searchString: null, searchError: null })
  }

  handleSearchChange = (val) => {
    this.handleStateChange({ searchString: val, searchError: null })
  }

  handleReturnItemsQty = (line_item_id, qty) => {
    // console.log(line_item_id, qty)
    qty = parseInt(qty)
    let { returnArr } = this.state
    let updatedReturnArr = returnArr

    if (qty > 0) {
      updatedReturnArr = {
        ...returnArr,
        [line_item_id]: { ...returnArr[line_item_id], qty },
      }
    } else {
      unset(updatedReturnArr, line_item_id)
    }

    this.handleStateChange({ returnArr: updatedReturnArr })
  }

  handleReturnItemReason = (line_item_id, reason) => {
    // console.log(line_item_id, reason)
    let { returnArr } = this.state
    let updatedReturnArr = returnArr

    if (reason && reason != '') {
      updatedReturnArr = {
        ...returnArr,
        [line_item_id]: { ...returnArr[line_item_id], reason },
      }
    }
    // else {
    //   unset(updatedReturnArr, line_item_id)
    // }

    this.handleStateChange({ returnArr: updatedReturnArr })
  }

  handlePaymentInputs = (value, id) => {
    const { selectedPaymentMethod, paymentInputs } = this.state
    let userPaymentInputs = paymentInputs

    if (!userPaymentInputs[selectedPaymentMethod]) {
      userPaymentInputs[selectedPaymentMethod] = {}
    }

    userPaymentInputs[selectedPaymentMethod] = {
      ...userPaymentInputs[selectedPaymentMethod],
      [id]: value,
    }

    this.handleStateChange({ paymentInputs: userPaymentInputs })
  }

  handleCancelBtn = () => {
    this.handleStateChange({ modalConfirm: true, goBackToReturns: false })
  }

  handleConfirmModal = () => {
    if (this.state.goBackToReturns) {
      RedirectDispatch(`${easy_path}/returns`)
      return
    }

    this.handleStateChange({ modalConfirm: false })
    this.resetReturnForm()
  }

  resetReturnForm = () => {
    this.handleStateChange({
      orderDetails: [],
      returnArr: [],
      customerNote: '',
      selectedPaymentMethod: [],
      paymentInputs: [],
      returnRequestId: null,
      searchString: null,
    })
  }

  handleBreadCrumbs = () => {
    if (isEmpty(this.state.orderDetails))
      RedirectDispatch(`${easy_path}/returns`)
    else {
      this.handleStateChange({ modalConfirm: true, goBackToReturns: true })
    }
  }

  render() {
    const {
      orderDetails,
      orderLineItems,
      returnLineItems,
      itemsReturned,
      returnReasons,
      isCodOrder,
      paymentOptions,
      configs,
      notificationEmail,
      customerContactEmail,
      returnArr,
      searchString,
      searchBtnLoading,
      searchError,
      responseError,
      returnRequestId,
      modalLoading,
      returnMethod,
      selectedPaymentMethod,
      paymentInputs,
      customerNote,
      modalConfirm,
      modalConfirmLoading,
    } = this.state

    // if (requestDetails.length === 0) {
    //   return <ReturnOrderDescPageSkeleton />
    // }

    return (
      <Page
        fullWidth
        breadcrumbs={[
          {
            content: 'Return Order',
            onAction: this.handleBreadCrumbs,
          },
        ]}
        title={`Return order`}
        titleMetadata={
          !isEmpty(orderDetails) ? <Heading>{orderDetails.name}</Heading> : ''
        }
        primaryAction={
          !isEmpty(orderDetails) && !returnRequestId
            ? {
                content: 'Save return',
                onAction: () =>
                  this.handleBackendRequest('POST', 'saveReturnForm'),
              }
            : null
        }
        secondaryActions={
          !isEmpty(orderDetails) && !returnRequestId
            ? [
                {
                  content: 'Cancel return',
                  onAction: this.handleCancelBtn,
                },
              ]
            : []
        }
      >
        <LoadingModal modalLoading={modalLoading} />
        <ConfirmationModal
          title="Unsaved changes"
          openModal={modalConfirm}
          handleClose={this.handleStateChange}
          modalLoading={modalConfirmLoading}
          content={
            <TextStyle>
              If you leave this page, any unsaved changes will be lost.
            </TextStyle>
          }
          primaryAction={this.handleConfirmModal}
          confirmBtnLabel="Leave Page"
        />
        <SearchOrder
          hasOrder={!isEmpty(orderDetails)}
          handleSearchChange={this.handleSearchChange}
          handleClearButton={this.handleClearButton}
          findOrder={this.findOrder}
          searchString={searchString}
          searchError={searchError}
          searchBtnLoading={searchBtnLoading}
        />
        <ErrorBanner
          responseError={responseError}
          handleStateChange={this.handleStateChange}
        />
        <SuccessBanner
          returnRequestId={returnRequestId}
          resetReturnForm={this.resetReturnForm}
        />
        <ReturnOrderForm
          orderDetails={orderDetails}
          orderLineItems={orderLineItems}
          returnLineItems={returnLineItems}
          itemsReturned={itemsReturned}
          configs={configs}
          returnReasons={returnReasons}
          handleReturnItemsQty={this.handleReturnItemsQty}
          handleReturnItemReason={this.handleReturnItemReason}
          returnArr={returnArr}
          isCodOrder={isCodOrder}
          handleStateChange={this.handleStateChange}
          paymentOptions={paymentOptions}
          returnMethod={returnMethod}
          selectedPaymentMethod={selectedPaymentMethod}
          handlePaymentInputs={this.handlePaymentInputs}
          paymentInputs={paymentInputs}
          customerNote={customerNote}
          handleBackendRequest={this.handleBackendRequest}
          handleCancelBtn={this.handleCancelBtn}
          returnRequestId={returnRequestId}
          notificationEmail={notificationEmail}
          customerContactEmail={customerContactEmail}
        />
      </Page>
    )
  }
}

export default CreateReturn

const ReturnOrderForm = ({
  orderDetails,
  orderLineItems,
  returnLineItems,
  itemsReturned,
  configs,
  returnReasons,
  handleReturnItemsQty,
  handleReturnItemReason,
  returnArr,
  isCodOrder,
  returnMethod,
  handleStateChange,
  paymentOptions,
  selectedPaymentMethod,
  handlePaymentInputs,
  paymentInputs,
  customerNote,
  handleBackendRequest,
  handleCancelBtn,
  returnRequestId,
  notificationEmail,
  customerContactEmail,
}) => {
  if (isEmpty(orderDetails)) {
    return null
  }

  return (
    <>
      <OrderInfoCard
        orderDetails={orderDetails}
        itemsReturned={itemsReturned}
      />
      <div className="mb-25"></div>
      <Layout>
        <Layout.Section>
          <Card title={<Heading>Select items quantity for return</Heading>}>
            <ReturnLineItemsDataTable
              orderLineItems={orderLineItems}
              returnLineItems={returnLineItems}
              moneyFormat={configs.money_format}
              returnReasons={returnReasons}
              handleReturnItemsQty={handleReturnItemsQty}
              handleReturnItemReason={handleReturnItemReason}
              returnArr={returnArr}
            />
          </Card>
        </Layout.Section>
      </Layout>
      <div className="mb-25"></div>
      <Layout>
        <Layout.Section oneHalf={!isCodOrder ? false : true}>
          <ReturnMethods
            availReturnMethods={configs.return_methods}
            returnMethod={returnMethod}
            handleStateChange={handleStateChange}
          />
          <Card>
            <NotificationEmail
              notificationEmail={notificationEmail}
              customerContactEmail={customerContactEmail}
              handleStateChange={handleStateChange}
            />
            <CustomerNote
              returnMethod={returnMethod}
              configs={configs}
              customerNote={customerNote}
              handleStateChange={handleStateChange}
            />
          </Card>
        </Layout.Section>

        <PaymentOptions
          paymentOptions={paymentOptions}
          isCodOrder={isCodOrder}
          handleStateChange={handleStateChange}
          selectedPaymentMethod={selectedPaymentMethod}
          handlePaymentInputs={handlePaymentInputs}
          paymentInputs={paymentInputs}
          returnMethod={returnMethod}
        />
      </Layout>
      <div className="mb-25"></div>
      {!returnRequestId && (
        <PageActionFooter
          handlePriAction={handleBackendRequest}
          handleCancelBtn={handleCancelBtn}
        />
      )}
    </>
  )
}

const CustomerNote = ({
  returnMethod,
  configs,
  customerNote,
  handleStateChange,
}) => {
  const {
    exchange_note,
    has_customer_note,
    req_customer_note,
    req_exchange_note,
  } = configs
  let showNote,
    requiredNote = false

  if (returnMethod === 1) {
    showNote = has_customer_note
    requiredNote = has_customer_note && req_customer_note ? true : false
  } else if (returnMethod === 2) {
    showNote = exchange_note
    requiredNote = exchange_note && req_exchange_note ? true : false
  }

  if (!showNote) return null

  return (
    <Card.Section>
      <TextField
        label="Comments"
        multiline={2}
        value={customerNote}
        onChange={(val) => handleStateChange({ customerNote: val })}
        requiredIndicator={requiredNote}
        showCharacterCount={true}
        placeholder="Write comment on this return..."
      />
    </Card.Section>
  )
}

const NotificationEmail = ({
  notificationEmail,
  customerContactEmail,
  handleStateChange,
}) => {
  if (notificationEmail != '') return null

  return (
    <Card.Section>
      <TextField
        label="Notification email"
        value={customerContactEmail}
        onChange={(val) => handleStateChange({ customerContactEmail: val })}
        requiredIndicator={true}
        placeholder="Customer contact email address"
        helpText="This email address is used to send return order notification mails to customer."
      />
    </Card.Section>
  )
}

const ReturnMethods = ({
  availReturnMethods,
  returnMethod,
  handleStateChange,
}) => {
  const handleChange = useCallback((_isChecked, newValue) => {
    handleStateChange({ returnMethod: newValue === 'refund' ? 1 : 2 })
  })

  return (
    <Card title="">
      <Card.Section>
        <Stack>
          <Heading>Choose return method</Heading>
          {indexOf(availReturnMethods, '1') !== -1 && (
            <RadioButton
              label="Refund"
              checked={returnMethod === 1}
              id="refund"
              name="return_methods"
              onChange={handleChange}
            />
          )}

          {indexOf(availReturnMethods, '2') !== -1 && (
            <RadioButton
              label="Exchange"
              checked={returnMethod === 2}
              id="exchange"
              name="return_methods"
              onChange={handleChange}
            />
          )}
        </Stack>
      </Card.Section>
    </Card>
  )
}

const SearchOrder = ({
  hasOrder,
  handleSearchChange,
  handleClearButton,
  findOrder,
  searchString,
  searchError,
  searchBtnLoading,
}) => {
  if (hasOrder) return null
  return (
    <Layout>
      <Layout.Section>
        <Card title="" sectioned subdued>
          <Stack alignment="leading">
            <Stack.Item>
              <TextField
                type="text"
                id="search_order_text"
                placeholder="1000"
                value={searchString}
                onChange={handleSearchChange}
                disabled={searchBtnLoading}
                clearButton
                onClearButtonClick={handleClearButton}
                error={searchError}
                prefix="Enter shopify order number #"
                connectedRight={
                  <Button onClick={findOrder} loading={searchBtnLoading}>
                    Get order
                  </Button>
                }
              />
            </Stack.Item>
          </Stack>
        </Card>
      </Layout.Section>
    </Layout>
  )
}

const PageActionFooter = ({ handlePriAction, handleCancelBtn }) => {
  return (
    <PageActions
      primaryAction={{
        content: 'Save changes',
        onAction: () => handlePriAction('POST', 'saveReturnForm'),
      }}
      secondaryActions={[
        {
          content: 'Cancel return',
          onAction: handleCancelBtn,
        },
      ]}
    />
  )
}

const ErrorBanner = ({ responseError, handleStateChange }) => {
  if (isEmpty(responseError)) return null

  const handleBannerDismiss = useCallback(() => {
    handleStateChange({ responseError: null })
  })

  const items = responseError.map((item) => {
    return {
      term: item.name,
      description:
        item.err === -1
          ? 'Cannot return this item'
          : `Only ${item.err} quantity can be returned.`,
    }
  })

  const banner_error = useRef()
  useEffect(() => banner_error.current.focus(), [])

  return (
    <>
      <Banner
        title="No return available for these items"
        status="critical"
        onDismiss={handleBannerDismiss}
        ref={banner_error}
      >
        <List>
          {items.map((item, key) => {
            return (
              <List.Item key={key}>
                {item.term}
                <Caption>
                  <TextStyle variation="negative">{item.description}</TextStyle>
                </Caption>
              </List.Item>
            )
          })}
        </List>
      </Banner>
      <div className="mb-25"></div>
    </>
  )
}

const SuccessBanner = ({ returnRequestId, resetReturnForm }) => {
  if (!returnRequestId) return null

  const banner_success = useRef()
  useEffect(() => banner_success.current.focus(), [])

  return (
    <>
      <Banner
        title={`Return #${returnRequestId} created successfully`}
        status="success"
        action={{
          content: 'View created return',
          onAction: () => {
            RedirectDispatch(`${easy_path}/returns/${returnRequestId}`)
          },
        }}
        secondaryAction={{
          content: 'Create new return',
          onAction: resetReturnForm,
        }}
        ref={banner_success}
      ></Banner>
      <div className="mb-25"></div>
    </>
  )
}
