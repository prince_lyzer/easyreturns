import React, { useState, useCallback } from 'react'
import {
  Card,
  DataTable,
  Pagination,
  Filters,
  Heading,
  ChoiceList,
  Icon,
  DatePicker,
} from '@shopify/polaris'
import { MomentFormat } from '../../common/Common'
import SearchMajor from '../SearchMajor.svg'
import { SortMinor, CalendarMinor } from '@shopify/polaris-icons'

export const ReturnListDataTableWithFilters = ({
  returnRequests,
  handleSort,
  currentPage,
  totalPages,
  handlePagination,
  selectedReturnStatus,
  handleRequestStatusChange,
  searchStr,
  handleSearchStrChange,
  helpText,
  selectedSortStatus,
  handleSortChange,
  noRecords,
  selectedBtwDates,
  handleBtwDateChange,
  selectedReturnType,
  handleRequestTypeChange,
}) => {
  const appliedFilters = []
  if (!isEmpty(selectedReturnType)) {
    const key = 'returnType'
    appliedFilters.push({
      key,
      label: returnStatusLabels(key, selectedReturnType),
      onRemove: () => handleRequestTypeChange(),
    })
  }

  if (!isEmpty(selectedReturnStatus)) {
    const key = 'returnStatus'
    appliedFilters.push({
      key,
      label: returnStatusLabels(key, selectedReturnStatus),
      onRemove: () => handleRequestStatusChange(),
    })
  }

  if (!isEmpty(selectedBtwDates)) {
    const key = 'date'
    appliedFilters.push({
      key,
      label: returnBtwDateLabels(key, selectedBtwDates),
      onRemove: () => handleBtwDateChange(null),
    })
  }

  const [queryValue, setQueryValue] = useState(searchStr)
  const handleFiltersQueryChange = useCallback((value) => {
    setQueryValue(value)
    handleSearchStrChange(value === '' ? null : value)
  }, [])
  const handleQueryValueRemove = useCallback(() => {
    setQueryValue(null)
    handleSearchStrChange(null)
  }, [])
  return (
    <Card>
      <Card.Section>
        <Filters
          queryValue={queryValue}
          filters={filters({
            selectedReturnStatus,
            handleRequestStatusChange,
            selectedSortStatus,
            handleSortChange,
            selectedBtwDates,
            handleBtwDateChange,
            selectedReturnType,
            handleRequestTypeChange,
          })}
          appliedFilters={appliedFilters}
          queryPlaceholder="Filter returns"
          onQueryChange={handleFiltersQueryChange}
          onQueryClear={handleQueryValueRemove}
          onClearAll={() => {}}
          helpText={helpText}
          disabled={noRecords}
        />
      </Card.Section>
      <ReturnOrderDataTable
        returnRequests={returnRequests}
        handleSort={handleSort}
        currentPage={currentPage}
        totalPages={totalPages}
        handlePagination={handlePagination}
      />
    </Card>
  )
}

function returnStatusLabels(key, value) {
  switch (key) {
    case 'returnStatus':
      if (value[0] === '1') return 'Open'
      else if (value[0] === '2') return 'Accepted'
      else if (value[0] === '3') return 'Completed'
      else if (value[0] === '4') return 'Cancelled'
      else if (value[0] === '5') return 'Archived'
      break
    case 'returnType':
      if (value[0] === '1') return 'Refund'
      else if (value[0] === '2') return 'Exchange'
      break
    default:
      return value
  }
}

function returnBtwDateLabels(key, value) {
  const start = MomentFormat(value.start, 'MMM D, YYYY ')
  const end = MomentFormat(value.end, 'MMM D, YYYY')
  return `${start} - ${end}`
}

function isEmpty(value) {
  if (Array.isArray(value)) {
    return value.length === 0
  } else {
    return value === '' || value == null
  }
}

function ReturnOrderDataTable({
  returnRequests,
  handleSort,
  currentPage,
  totalPages,
  handlePagination,
}) {
  if (returnRequests.length === 0) return <NoResults />
  return (
    <DataTable
      columnContentTypes={[
        'text',
        'text',
        'text',
        'text',
        'numeric',
        'numeric',
      ]}
      headings={[
        'Return Id',
        'Order Name',
        'Customer Email',
        'Return Status',
        'Return Type',
        'Created At',
      ]}
      rows={ReturnRequests(returnRequests)}
      totals=""
      sortable={[false, false, false, false, false]}
      defaultSortDirection="descending"
      initialSortColumnIndex={4}
      onSort={(index, direction) => handleSort(index, direction)}
      footerContent={ReturnListPagination(
        currentPage,
        totalPages,
        handlePagination,
      )}
    />
  )
}

const ReturnRequests = (returnRequests) => {
  return returnRequests.map((request) => {
    return [
      <LinkReact to={`${easy_path}/returns/${request.id}`}>
        #{request.id}
      </LinkReact>,
      request.order_name,
      request.email || '-',
      ReturnStatusBadge(request.return_status),
      ReturnTypeBadge(request.return_type),
      DateFormat(request.date_add, 'MMM DD, YYYY [at] hh:mm a'),
    ]
  })
}

function NoResults() {
  return (
    <div style={{ textAlign: 'center' }}>
      <img src={`../${SearchMajor}`} width="80px" />
      <div className="py-15">
        <Heading>No return request found</Heading>
      </div>
    </div>
  )
}

function ReturnListPagination(currentPage, totalPages, handlePagination) {
  currentPage = parseInt(currentPage)
  return (
    <Pagination
      label={`Showing ${currentPage} of ${totalPages} Pages`}
      hasPrevious={currentPage > 1}
      onPrevious={() => handlePagination(currentPage - 1)}
      hasNext={currentPage < totalPages}
      onNext={() => handlePagination(currentPage + 1)}
      previousTooltip="Previous"
      nextTooltip="Next"
    />
  )
}

const filters = ({
  selectedReturnStatus,
  handleRequestStatusChange,
  selectedSortStatus,
  handleSortChange,
  selectedBtwDates,
  handleBtwDateChange,
  selectedReturnType,
  handleRequestTypeChange,
}) => {
  return [
    {
      key: 'returnType',
      label: 'Type',
      filter: (
        <ChoiceList
          title="Type"
          titleHidden
          choices={[
            { label: 'Refund', value: '1' },
            { label: 'Exchange', value: '2' },
          ]}
          selected={selectedReturnType || []}
          onChange={handleRequestTypeChange}
          allowMultiple={false}
        />
      ),
      shortcut: true,
    },
    {
      key: 'returnStatus',
      label: 'Status',
      filter: (
        <ChoiceList
          title="Status"
          titleHidden
          choices={[
            { label: 'Open', value: '1' },
            { label: 'Accepted', value: '2' },
            { label: 'Completed', value: '3' },
            { label: 'Canceled', value: '4' },
            { label: 'Archived', value: '5' },
          ]}
          selected={selectedReturnStatus || []}
          onChange={handleRequestStatusChange}
          allowMultiple={false}
        />
      ),
      shortcut: true,
    },
    {
      key: 'sort',
      label: (
        <div className="d-flex align-items-center">
          <Icon source={SortMinor}></Icon>&nbsp;Sort
        </div>
      ),
      filter: (
        <ChoiceList
          title="Sort"
          titleHidden
          choices={[
            { label: 'Date (newest first)', value: 'date_add desc' },
            { label: 'Date (oldest first)', value: 'date_add asc' },
            { label: 'Customer email (A-Z)', value: 'email asc' },
            { label: 'Customer email (Z-A)', value: 'email desc' },
            { label: 'Order name (ascending)', value: 'order_name asc' },
            { label: 'Order name (descending)', value: 'order_name desc' },
          ]}
          selected={selectedSortStatus || []}
          onChange={handleSortChange}
          allowMultiple={false}
        />
      ),
      shortcut: true,
    },
    {
      key: 'date',
      label: (
        <div className="d-flex align-items-center">
          <Icon source={CalendarMinor}></Icon>&nbsp;Date
        </div>
      ),
      filter: (
        <DatePickerExample
          selectedBtwDates={selectedBtwDates}
          setSelectedDates={handleBtwDateChange}
        />
      ),
    },
  ]
}

function DatePickerExample({ selectedBtwDates, setSelectedDates }) {
  const now = new Date()
  const [{ month, year }, setDate] = useState({
    month: now.getMonth(),
    year: now.getFullYear(),
  })

  const handleMonthChange = useCallback(
    (month, year) => setDate({ month, year }),
    [],
  )

  return (
    <DatePicker
      month={month}
      year={year}
      onChange={(val) => setSelectedDates(val)}
      onMonthChange={handleMonthChange}
      selected={selectedBtwDates}
      multiMonth={true}
      allowRange={true}
    />
  )
}
