import React, { Component } from 'react'
import { Page, Layout, Card, Heading } from '@shopify/polaris'
import SearchMajor from './SearchMajor.svg'

export default class NotFound extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Page fullWidth>
        <Layout>
          <Layout.Section>
            <Card>
              <Card.Section>
                <div style={{ textAlign: 'center', padding: '5rem' }}>
                  <img src={`../${SearchMajor}`} width="80px" />
                  <div className="py-15">
                    <Heading>
                      Sorry, the page you are looking for could not be found.
                    </Heading>
                  </div>
                </div>
              </Card.Section>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}
