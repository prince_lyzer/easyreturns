import React from 'react'
import {
  Page,
  Thumbnail,
  Layout,
  Card,
  FormLayout,
  TextField,
  PageActions,
  Button,
} from '@shopify/polaris'
import { LanguageMinor } from '@shopify/polaris-icons'
import '@shopify/polaris/dist/styles.css'
import AxiosObj from '../AxiosIns'
import {
  AjaxErrorHandle,
  LoadingModal,
  appendToUrl,
  PreviewModal,
  ConfirmationModal,
} from '../../common/Common'
import { TranslationPageSkeleton } from '../PageSkeletons/pageSkeleton'
import return_link_text_preview from '../screenshots/return-link-text-review.png'
import { confirmationModalContent } from '../email_configs/sub-components'

export default class ReturnPageConfigure extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      returnLabels: [],
      modalLoading: false,
      previewModalLoading: false,
      previewSrc: '',
      modalConfirm: false,
      confirmModalPriAction: null,
      confirmModalTitle: '',
      confirmModalContent: '',
      modalConfirmLoading: false,
    }
  }

  handlePreview = (src) => {
    this.setState({ previewModalLoading: true, previewSrc: src })
  }

  componentDidMount() {
    this.props.updateTitle('Label Translations')
    appendToUrl(this.props.location, { shop, host })

    this.handleBackendRequests('GET', 'getDetails', {})
  }

  handleStateChange = (obj) => {
    this.setState(obj)
  }

  handleReturnLabelChange = (return_label, id) => {
    id = id.substr(16)

    let returnLabelsTemp = this.state.returnLabels.map((item) => {
      if (item.id.toString() == id) {
        item.value = return_label
        return item
      }
      return item
    })
    this.setState({ returnLabels: [...returnLabelsTemp] })
  }

  openConfirmationModal = (modalFor) => {
    let confirmModalContent = ''

    confirmModalContent = confirmationModalContent(modalFor)

    let method = 'GET'

    if (modalFor === 'resetReturnLabels') {
      method = 'POST'
    }

    this.setState({
      modalConfirm: true,
      confirmModalTitle: confirmModalContent.title,
      confirmModalPriAction: () => {
        this.handleBackendRequests(method, modalFor)
      },
      confirmModalContent: confirmModalContent.content,
    })
  }

  handleBackendRequests = (method = 'GET', requestName, params = {}) => {
    let url = '',
      data = {}

    if (requestName === 'getDetails') {
      url = `/label-translate`
    } else if (requestName === 'onSaveChanges') {
      this.setState({ modalLoading: true })
      url = `/label-translate`
      const { returnLabels: return_labels } = this.state
      data = { return_labels }
    } else if (requestName === 'resetReturnLabels') {
      this.setState({ modalConfirmLoading: true })
      url = `/label-translate/reset`
      data = {}
    }

    let axiosObj = { method, url }
    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = data

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        const notice = res_data.notice || ''
        const error = res_data.error || ''

        if (notice.trim() !== '') {
          this.props.setNoticeToast(res_data.notice)
        } else if (error.trim() !== '') {
          this.props.setErrorToast(res_data.error)
        }

        if (requestName === 'onSaveChanges') {
        } else if (requestName === 'getDetails') {
          this.setState({
            returnLabels: return_data.return_labels,
          })
        } else if (requestName === 'resetReturnLabels') {
          this.handleBackendRequests('GET', 'getDetails', {})
        }
      })
      .catch((err) => {
        AjaxErrorHandle(err, this.props.setErrorToast)
      })
      .then(() => {
        if (requestName === 'onSaveChanges') {
          this.setState({ modalLoading: false })
        } else if (requestName === 'resetReturnLabels') {
          this.setState({ modalConfirm: false, modalConfirmLoading: false })
        }
      })
  }

  render() {
    if (this.state.returnLabels.length === 0) {
      return <TranslationPageSkeleton title="Label Translation" />
    }

    const {
      modalLoading,
      returnLabels,
      previewModalLoading,
      previewSrc,
      modalConfirm,
      confirmModalPriAction,
      confirmModalTitle,
      confirmModalContent,
      modalConfirmLoading,
    } = this.state

    return (
      <Page
        fullWidth
        title="Label Translations"
        subtitle="From here you can translate labels and notification text."
        thumbnail={
          <Thumbnail source={LanguageMinor} size="small" alt="Language Icon" />
        }
        primaryAction={{
          content: 'Reset to Original',
          destructive: true,
          onAction: () => {
            this.openConfirmationModal('resetReturnLabels')
          },
        }}
      >
        <LoadingModal modalLoading={modalLoading} />
        <PreviewModal
          openModal={previewModalLoading}
          src={previewSrc}
          closeModal={this.handleStateChange}
        />
        <ConfirmationModal
          title={confirmModalTitle}
          openModal={modalConfirm}
          handleClose={this.handleStateChange}
          primaryAction={confirmModalPriAction}
          content={confirmModalContent}
          modalLoading={modalConfirmLoading}
        />

        <Layout>
          <Layout.Section>
            <Card sectioned title="Return order page response codes">
              <FormLayout>
                {returnLabels.map((label) => {
                  return (
                    <TextField
                      id={`return_label_id_${label.id}`}
                      key={label.id}
                      label={label.name}
                      value={label.value}
                      onChange={this.handleReturnLabelChange}
                      helpText={LabelHelpText(label.id, this.handlePreview)}
                    />
                  )
                })}
              </FormLayout>
            </Card>
          </Layout.Section>
        </Layout>
        <div className="mb-25"></div>
        <PageActionFooter handlePriAction={this.handleBackendRequests} />
      </Page>
    )
  }
}

const PageActionFooter = ({ handlePriAction }) => {
  return (
    <PageActions
      primaryAction={{
        content: 'Save changes',
        onAction: () => {
          handlePriAction('POST', 'onSaveChanges', {})
        },
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            window.location.reload()
          },
        },
      ]}
    />
  )
}

const LabelHelpText = (id, handleClick) => {
  const helpTexts = {
    '2': <div>When a customer try to return a unfulfilled order.</div>,
    '6': (
      <div>
        When a customer try to return an item without entering return quantity.
      </div>
    ),
    '7': (
      <div>
        When a customer try to return an item without selecting return reason.
      </div>
    ),
    '9': (
      <div>
        When a customer try to return an item quantity more than ordered
        quantity.
      </div>
    ),
    '11': (
      <div>
        When a customer try to submit order return without entering note.
      </div>
    ),
    '12': (
      <div>
        This is a return link text in order history, For more details{' '}
        <Button plain onClick={() => handleClick(return_link_text_preview)}>
          click here.
        </Button>
      </div>
    ),
    '13': (
      <div>
        This error text displays when a customer request for a invalid order.
      </div>
    ),
  }

  return helpTexts[id]
}
