import axios from 'axios'
import { getSessionToken } from '@shopify/app-bridge-utils'
import { HOST_URL } from '../env'

const AxiosObj = axios.create({
  baseURL: `${HOST_URL}api`,
})
// intercept all requests on this axios instance
AxiosObj.interceptors.request.use(function (config) {
  return getSessionToken(appBridge) // requires an App Bridge instance
    .then((token) => {
      // append your request headers with an authenticated token
      config.headers['Authorization'] = `Bearer ${token}`
      // config.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      return config
    })
})

// export your axios instance to use within your app
export default AxiosObj
