import React from 'react'
import { DescriptionList } from '@shopify/polaris'

export function MailVariables({ mailType }) {
  mailType = parseInt(mailType)
  let variables = [
    {
      term: 'order_name',
      description: 'Return Order Name',
    },
    {
      term: 'return_request_id',
      description: 'Return Request Id',
    },
    {
      term: 'return_line_items',
      description: 'Return Line items Table',
    },
    {
      term: 'return_method',
      description: 'Return method- Exchange or Refund',
    },
  ]

  const customerNameVariable = {
    term: 'customer_name',
    description: 'Customer Full Name',
  }

  const customerNoteVariable = {
    term: 'customer_note',
    description: 'Customer note',
  }

  if (mailType === 1) {
    const otherVariables = [
      customerNameVariable,
      {
        term: 'is_cod_order',
        description: 'Check for COD order',
      },
      {
        term: 'payment_type',
        description: 'Payment type name',
      },
      {
        term: 'payment_type_handle',
        description: 'Payment type handle',
      },
      {
        term: 'bank_account_number',
        description: 'Bank Account Number',
      },
      {
        term: 'bank_ifsc_code',
        description: 'Bank IFSC Code',
      },
      {
        term: 'bank_holder_name',
        description: 'Bank Holder Name',
      },
      {
        term: 'paytm_mobile_no',
        description: 'Paytm Mobile number',
      },
      {
        term: 'phonepe_mobile_no',
        description: 'Phonepe Mobile number',
      },
      {
        term: 'google_pay',
        description: 'Google pay Mobile number',
      },
      {
        term: 'upi_id',
        description: 'UPI ID',
      },
      customerNoteVariable,
    ]
    variables = [...variables, ...otherVariables]
  } else if (mailType === 2) {
    variables.push(customerNameVariable)
  } else if (mailType === 3) {
    const otherVariables = [
      customerNameVariable,
      {
        term: 'attachments|raw',
        description: 'Attach screenshot',
      },
      customerNoteVariable,
      {
        term: 'coupon_code',
        description: 'Coupon code',
      },
    ]
    variables = [...variables, ...otherVariables]
  } else if (mailType === 4) {
    const otherVariables = [
      customerNameVariable,
      {
        term: 'cancelled_reason',
        description: 'Return cancelled reason',
      },
    ]
    variables = [...variables, ...otherVariables]
  } else if (mailType === 5) {
    const otherVariables = [
      {
        term: 'customer_email',
        description: 'Customer email',
      },
      customerNoteVariable,
    ]
    variables = [...variables, ...otherVariables]
  } else if (mailType === 6) {
    variables.push({
      term: 'query',
      description: 'Return query',
    })
  }

  return (
    <div className="variable_list">
      <DescriptionList spacing="tight" items={variables} />
    </div>
  )
}
