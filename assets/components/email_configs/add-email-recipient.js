import React, { useCallback, useState } from 'react'
import {
  Card,
  TextContainer,
  DescriptionList,
  TextStyle,
  Button,
  TextField,
} from '@shopify/polaris'
import { DeleteMinor } from '@shopify/polaris-icons'
import { maxRecipients } from '../../env'

export const AddEmailRecipientCard = ({
  mail_id,
  recipients,
  handleBackendRequest,
  recipientLoading,
}) => {
  mail_id = Number(mail_id)
  if (mail_id !== 5) return null

  return (
    <Card title="Mail Recipients">
      <AddRecipientCard
        totalRecipients={recipients.length}
        handleNewRecipient={handleBackendRequest}
        recipientLoading={recipientLoading}
      />
      <Card.Section>
        <TextContainer spacing="loose">
          <TextStyle>
            This email notification goes to below address(s):
          </TextStyle>
          <br />
          <TextStyle variation="negative">
            Note: Provide at least one address. (
            {`Max ${maxRecipients} allowed`})
          </TextStyle>
        </TextContainer>
        <RecipientMails
          recipients={recipients}
          handleDelete={handleBackendRequest}
        />
      </Card.Section>
    </Card>
  )
}

const RecipientMails = ({ recipients, handleDelete }) => {
  const items = recipients.map((r) => {
    return {
      term: <span>{r.email}</span>,
      description: (
        <DeleteMailButton handleDelete={handleDelete} email={r.email} />
      ),
    }
  })

  return (
    <div className="justify_between_list">
      <DescriptionList spacing="tight" items={items} />
    </div>
  )
}

const DeleteMailButton = ({ handleDelete, email }) => {
  return (
    <Button
      icon={DeleteMinor}
      size="slim"
      outline
      destructive={true}
      onClick={() => {
        handleDelete('GET', 'deleteRecipient', {
          recipient_email: email,
        })
      }}
    ></Button>
  )
}

const AddRecipientCard = ({
  totalRecipients,
  handleNewRecipient,
  recipientLoading,
}) => {
  if (totalRecipients >= maxRecipients) return null

  const [email, setEmail] = useState('')

  const handleChange = useCallback(
    (val) => {
      setEmail(val)
    },
    [email],
  )

  const handleClearButtonClick = useCallback(() => setEmail(''), [])

  return (
    <Card.Section>
      <TextField
        label=""
        type="email"
        value={email}
        onChange={handleChange}
        placeholder="Enter recipient address"
        clearButton
        onClearButtonClick={handleClearButtonClick}
        autoComplete={false}
        connectedRight={
          <Button
            onClick={() =>
              handleNewRecipient('GET', 'addNewRecipientAction', {
                recipientEmail: email,
              })
            }
            loading={recipientLoading}
            disabled={email === '' ? true : false}
          >
            Add Recipient
          </Button>
        }
      />
    </Card.Section>
  )
}
