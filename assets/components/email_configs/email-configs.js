import React from 'react'
import AxiosObj from '../AxiosIns'
import {
  Page,
  Thumbnail,
  Layout,
  Card,
  FormLayout,
  PageActions,
  TextContainer,
  TextStyle,
  TextField,
} from '@shopify/polaris'
import {
  CopyCode,
  LoadingModal,
  AjaxErrorHandle,
  appendToUrl,
  ConfirmationModal,
} from '../../common/Common'
import { SettingsMinor } from '@shopify/polaris-icons'
import { EmailConfigsPageSkeleton } from '../PageSkeletons/pageSkeleton'
import { PageTour } from '../../common/page-tour'
import { confirmationModalContent } from '../return_page_configure/confirmation-modal-util'
import {
  CustomerEmails,
  MerchantEmails,
  EditEmailStyleCardSection,
  EmailSignature,
  EditEmailHeaderCardSection,
  BrandLogo,
} from './sub-components'

export default class EmailConfigs extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      customerMails: [],
      merchantMails: [],
      userEmailDetails: [],
      emailStyle: '',
      emailHeader: '',
      brandLogo: '',
      emailSignature: '',
      isTourOpen: false,
      modalConfirm: false,
      modalConfirmLoading: false,
      confirmModalTitle: '',
      confirmModalContent: '',
      confirmModalPriAction: null,
      customerReplyEmail: '',
      modalLoading: false,
    }
  }

  componentDidMount() {
    this.props.updateTitle('Email Configuration')
    appendToUrl(this.props.location, { shop, host })
    this.handleBackendRequests('GET', 'getDetails')
  }

  handleStateChange = (obj) => {
    this.setState(obj)
  }

  openConfirmationModal = (modalFor) => {
    if (modalFor === '') return

    const t_b = confirmationModalContent(modalFor)
    let priAction

    if (modalFor === 'resetEmailStyleCode')
      priAction = () =>
        this.handleBackendRequests('GET', 'resetCode', {
          reset_code: modalFor,
        })

    this.setState({
      confirmModalTitle: t_b.title,
      modalConfirm: true,
      confirmModalPriAction: priAction,
      confirmModalContent: t_b.content,
    })
  }

  handleBackendRequests = (method = 'GET', requestName, params = {}) => {
    let url = '',
      data = {}

    if (requestName === 'getDetails') {
      url = `/mail-templates`
    } else if (requestName === 'onSaveChanges') {
      this.setState({ modalLoading: true })
      url = `/configure-email`
      const {
        emailStyle: email_style,
        emailHeader: email_header,
        brandLogo: brand_logo,
        emailSignature: email_signature,
        customerReplyEmail: customer_reply_email,
      } = this.state

      data = {
        email_style,
        email_header,
        brand_logo,
        email_signature,
        customer_reply_email,
      }
    } else if (requestName === 'resetCode') {
      this.setState({ modalConfirmLoading: true })
      url = `/mail-templates/action/reset`
    }

    let axiosObj = { method, url }
    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = data

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        const notice = res_data.notice || ''
        const error = res_data.error || ''

        if (notice.trim() !== '') {
          this.props.setNoticeToast(notice)
        } else if (error.trim() !== '') {
          this.props.setErrorToast(error)
        }

        if (requestName === 'resetCode') {
          this.setState({ emailStyle: return_data.email_style })
        } else if (requestName === 'onSaveChanges') {
          this.setState({
            customerReplyEmail: return_data.customer_reply_email,
          })
        } else if (requestName === 'getDetails') {
          this.setState({
            customerMails: return_data.email_templates.filter(
              (e) => e.mail_type === 1,
            ),
            merchantMails: return_data.email_templates.filter(
              (e) => e.mail_type === 2,
            ),
            emailStyle: return_data.email_style,
            emailHeader: return_data.email_header,
            emailSignature: return_data.email_signature,
            brandLogo: return_data.brand_logo,
            customerReplyEmail: return_data.customer_reply_email,
            userEmailDetails: return_data.user_email_details,
          })
        }
      })
      .catch((err) => {
        if (requestName === '')
          AjaxErrorHandle(
            err,
            this.props.setErrorToast,
            'email-configs',
            this.props.history,
          )
        else AjaxErrorHandle(err, this.props.setErrorToast)
      })
      .then(() => {
        if (requestName === 'resetCode') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        } else if (requestName === 'onSaveChanges') {
          this.setState({ modalLoading: false })
        }
      })
  }

  render() {
    const {
      customerMails,
      merchantMails,
      modalLoading,
      isTourOpen,
      emailHeader,
      emailStyle,
      emailSignature,
      brandLogo,
      modalConfirm,
      confirmModalTitle,
      modalConfirmLoading,
      confirmModalContent,
      confirmModalPriAction,
      customerReplyEmail,
      userEmailDetails,
    } = this.state

    if (customerMails.length === 0) {
      return <EmailConfigsPageSkeleton title="Email Configurations" />
    }

    return (
      <Page
        fullWidth
        title="Email Configurations"
        thumbnail={
          <Thumbnail source={SettingsMinor} size="sm" alt="Setting Icon" />
        }
        secondaryActions={[
          {
            content: 'Page Tour',
            onAction: () => {
              this.handleStateChange({ isTourOpen: true })
            },
          },
        ]}
      >
        <LoadingModal modalLoading={modalLoading} />
        <ConfirmationModal
          title={confirmModalTitle}
          openModal={modalConfirm}
          handleClose={this.handleStateChange}
          modalLoading={modalConfirmLoading}
          content={confirmModalContent}
          primaryAction={confirmModalPriAction}
        />
        <PageTour
          isTourOpen={isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="EmailConfigs"
        />
        <Layout>
          <Layout.AnnotatedSection
            title="Customer Email Notifications"
            description="Email Notifications."
          >
            <div className="customer-emails">
              <Card sectioned>
                <CustomerEmails
                  customerMails={customerMails}
                  userEmailDetails={userEmailDetails}
                />
              </Card>
            </div>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Merchant Email Notifications"
            description="Email Notifications."
          >
            <div className="merchant-email">
              <Card sectioned>
                <MerchantEmails
                  merchantMails={merchantMails}
                  userEmailDetails={userEmailDetails}
                />
              </Card>
            </div>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Configure Email"
            description="Here you can configure email template."
          >
            <FormLayout>
              <Card>
                <EditEmailStyleCardSection
                  handleCopy={CopyCode}
                  openConfirmationModal={this.openConfirmationModal}
                  emailStyle={emailStyle}
                  handleChange={this.handleStateChange}
                  setNoticeToast={this.props.setNoticeToast}
                />

                <EditEmailHeaderCardSection
                  handleCopy={CopyCode}
                  emailHeader={emailHeader}
                  handleChange={this.handleStateChange}
                  setNoticeToast={this.props.setNoticeToast}
                />

                <Card.Section
                  title="Edit signature of customer mail templates"
                  actions={[
                    {
                      content: 'Copy code',
                      onAction: () => {
                        CopyCode(
                          'mail_signature_code',
                          this.props.setNoticeToast,
                        )
                      },
                    },
                  ]}
                >
                  <EmailSignature
                    emailSignature={emailSignature}
                    handleChange={this.handleStateChange}
                  />
                </Card.Section>

                <div className="brand-logo">
                  <Card.Section>
                    <BrandLogo
                      brandLogo={brandLogo}
                      handleChange={this.handleStateChange}
                    />
                  </Card.Section>
                </div>
              </Card>
            </FormLayout>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Customer Reply To Email"
            description={
              <TextContainer>
                <TextStyle>
                  If a customer attempts to reply to any automated messages sent
                  by this app this email will be used.
                  <br />
                  <br />
                  If no email is set the shop's contact email address will be
                  used.
                </TextStyle>
              </TextContainer>
            }
          >
            <FormLayout>
              <div className="customer-notification-address">
                <Card title="Custom Notification Address">
                  <Card.Section>
                    <TextField
                      label=""
                      value={customerReplyEmail}
                      spellCheck={false}
                      onChange={(val) =>
                        this.handleStateChange({ customerReplyEmail: val })
                      }
                      helpText="This is the reply-to address of automated mail sent to customers for their returns."
                    />
                  </Card.Section>
                </Card>
              </div>
            </FormLayout>
          </Layout.AnnotatedSection>
        </Layout>

        <div className="mb-25"></div>

        <PageActions
          primaryAction={{
            content: 'Save changes',
            onAction: () => {
              this.handleBackendRequests('POST', 'onSaveChanges')
            },
          }}
          secondaryActions={[
            {
              content: 'Cancel',
            },
          ]}
        />
      </Page>
    )
  }
}
