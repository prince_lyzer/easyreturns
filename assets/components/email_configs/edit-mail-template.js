import React from 'react'
import {
  Page,
  Layout,
  Card,
  TextField,
  PageActions,
  TextContainer,
  TextStyle,
  FormLayout,
  Badge,
} from '@shopify/polaris'
import { EmailMajor, ViewMajor } from '@shopify/polaris-icons'
import {
  AjaxErrorHandle,
  LoadingModal,
  ConfirmationModal,
  RedirectDispatch,
  appendToUrl,
  PreviewHtmlModal,
} from '../../common/Common'
import AxiosObj from '../AxiosIns'
import { EditMailTemplatePageSkeleton } from '../PageSkeletons/pageSkeleton'
import { MailVariables } from './mail-variables'
import { PageTour } from '../../common/page-tour'
import { AddEmailRecipientCard } from './add-email-recipient'
import { easy_path } from '../../env'
import { confirmationModalContent } from './sub-components'

export default class EditMailTemplates extends React.Component {
  constructor(props) {
    super(props)
    const emailId = props.match.params.email_id
    this.state = {
      merchantEmailAddress: '',
      emailId,
      content: '',
      subject: '',
      description: '',
      pageTitle: '',
      resetDisabled: true,
      recipients: [],
      recipientLoading: false,
      previewModalActive: false,
      previewModalContent: '',
      active: false,
      modalLoading: false,
      modalConfirm: false,
      confirmModalPriAction: null,
      confirmModalTitle: '',
      confirmModalContent: '',
      modalConfirmLoading: false,
      isTourOpen: false,
    }
  }

  componentDidMount() {
    this.props.updateTitle(`Email Configurations`)
    appendToUrl(this.props.location, { shop, host })

    this.handleBackendRequest('GET', 'getDetails', {})
  }

  openConfirmationModal = (modalFor) => {
    let confirmModalContent = ''
    const { pageTitle, active } = this.state

    confirmModalContent = confirmationModalContent(modalFor, {
      title: pageTitle,
      active,
    })

    let method = 'GET'

    if (modalFor === 'resetMailTemplate') {
      method = 'POST'
    }

    this.setState({
      modalConfirm: true,
      confirmModalTitle: confirmModalContent.title,
      confirmModalPriAction: () => {
        this.handleBackendRequest(method, modalFor)
      },
      confirmModalContent: confirmModalContent.content,
    })
  }

  handleStateChange = (object) => {
    this.setState(object)
  }

  handleBackendRequest = (method, requestName, params) => {
    let url
    let data

    const emailId = this.state.emailId

    if (requestName === 'deleteRecipient') {
      const recipient_email = params.recipient_email.trim()
      if (recipient_email === '') {
        this.props.setErrorToast('Something went wrong')
      }

      if (this.state.recipients.length === 1) {
        this.props.setErrorToast(
          'Add another recipient address before deleting this address',
        )
        return
      }

      url = `/mail-templates/delete-recipient/${emailId}`
    } else if (requestName === 'testEmail') {
      this.setState({ modalConfirmLoading: true })
      url = `/mail-templates/test/${emailId}`
    } else if (requestName === 'SubmitForm') {
      this.setState({ modalLoading: true })
      url = `/mail-templates/${emailId}`
      data = { subject: this.state.subject, content: this.state.content }
    } else if (requestName === 'updateEmailStatus') {
      this.setState({ modalConfirmLoading: true })
      url = `/mail-templates/update-status/${emailId}`
    } else if (requestName === 'previewEmail') {
      this.setState({ modalLoading: true })
      url = `/mail-templates/preview/${emailId}`
      data = { subject: this.state.subject, content: this.state.content }
    } else if (requestName === 'resetMailTemplate') {
      this.setState({ modalConfirmLoading: true })
      url = `/mail-templates/reset/${emailId}`
    } else if (requestName === 'addNewRecipientAction') {
      const recipientEmail = params.recipientEmail

      if (recipientEmail.trim === '') {
        this.props.setErrorToast('Email is required')
        return
      }

      params = { recipient_email: recipientEmail }
      url = `/mail-templates/add-recipient/${emailId}`
      this.setState({ recipientLoading: true })
    } else if (requestName === 'getDetails') {
      url = `/mail-templates/${emailId}`
    }

    let axiosObj = { method: method, url: url }

    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = data

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []

        if (requestName === 'deleteRecipient') {
          this.props.setNoticeToast(res_data.notice)
          this.setState({ recipients: res_data.data.recipients })
        } else if (requestName === 'testEmail') {
          this.props.setNoticeToast(res_data.notice)
        } else if (requestName === 'SubmitForm') {
          this.setState({ resetDisabled: return_data.id ? false : true })
          this.props.setNoticeToast(res_data.notice)
        } else if (requestName === 'updateEmailStatus') {
          this.props.setNoticeToast(res_data.notice)
          this.setState({ active: !this.state.active })
        } else if (requestName === 'previewEmail') {
          this.setState({
            previewModalContent: res_data,
            previewModalActive: true,
          })
        } else if (requestName === 'resetMailTemplate') {
          this.props.setNoticeToast(res_data.notice)
          this.setState({
            content: return_data['content'],
            subject: return_data['subject'],
          })
        } else if (requestName === 'addNewRecipientAction') {
          this.props.setNoticeToast(res_data.notice)
          this.setState({ recipients: return_data.recipients })
        } else if (requestName === 'getDetails') {
          this.setState({
            description: return_data.description,
            resetDisabled: return_data.id ? false : true,
            content: return_data.content,
            pageTitle: return_data.title,
            subject: return_data.subject,
            recipients: return_data.recipients,
            active: return_data.active,
            merchantEmailAddress: return_data.merchant_mail_id,
          })
        }
      })
      .catch((err) => {
        if (requestName === 'getDetails') {
          AjaxErrorHandle(
            err,
            this.props.setErrorToast,
            'email-configs',
            this.props.history,
          )
        } else {
          AjaxErrorHandle(err, this.props.setErrorToast)
        }
      })
      .then(() => {
        if (requestName === 'testEmail') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        } else if (requestName === 'SubmitForm') {
          this.setState({ modalLoading: false })
        } else if (requestName === 'updateEmailStatus') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        } else if (requestName === 'previewEmail') {
          this.setState({ modalLoading: false })
        } else if (requestName === 'resetMailTemplate') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        } else if (requestName === 'addNewRecipientAction') {
          this.setState({ recipientLoading: false })
        }
      })
  }

  sendTestMailModal = () => {
    const { modalConfirmLoading, merchantEmailAddress } = this.state

    this.setState({
      modalConfirm: true,
      modalLoading: modalConfirmLoading,
      confirmModalTitle: 'Send test mail',
      confirmModalPriAction: () => {
        this.handleBackendRequest('GET', 'testEmail')
      },
      confirmModalContent: (
        <TextContainer>
          <TextStyle>Mail will be send to </TextStyle>
          <TextStyle variation="strong">{merchantEmailAddress} </TextStyle>
          email address.
          <TextStyle variation="negative">
            {' '}
            Do you want to send mail at this address?
          </TextStyle>
          <br />
          <TextStyle variation="subdued">
            Please check mail in spam folder if not found in inbox.
          </TextStyle>
        </TextContainer>
      ),
    })
  }

  render() {
    const {
      emailId,
      resetDisabled,
      description,
      pageTitle,
      subject,
      content,
      recipients,
      active,
      modalConfirm,
      confirmModalPriAction,
      confirmModalTitle,
      confirmModalContent,
      modalConfirmLoading,
      previewModalContent,
      previewModalActive,
      recipientLoading,
    } = this.state

    if (pageTitle === '') {
      return <EditMailTemplatePageSkeleton />
    }

    const pageSecondaryActions = [
      {
        content: 'Page Tour',
        onAction: () => {
          this.handleStateChange({ isTourOpen: true })
        },
      },
      {
        content: 'Test Email',
        icon: EmailMajor,
        onAction: () => {
          this.sendTestMailModal()
        },
      },
      {
        content: 'Preview',
        icon: ViewMajor,
        onAction: () => {
          this.handleBackendRequest('POST', 'previewEmail', {})
        },
      },
    ]

    const pageActionGroups = [
      {
        title: 'Action',
        accessibilityLabel: 'Action group label',
        actions: [
          {
            content: active ? 'Disable Notification' : 'Enable Notification',
            accessibilityLabel: active
              ? 'Disable this email'
              : 'Enable this email',
            onAction: () => {
              this.openConfirmationModal('updateEmailStatus')
            },
          },
          {
            content: 'Reset mail',
            destructive: true,
            disabled: resetDisabled,
            onAction: () => {
              this.openConfirmationModal('resetMailTemplate')
            },
          },
        ],
      },
    ]

    return (
      <Page
        fullWidth
        breadcrumbs={[
          {
            content: 'Email Configs',
            onAction: () => {
              RedirectDispatch(`${easy_path}/email-configs`, 0)
            },
          },
        ]}
        title={pageKaTitle({ pageTitle, active })}
        secondaryActions={pageSecondaryActions}
        actionGroups={pageActionGroups}
      >
        <ConfirmationModal
          title={confirmModalTitle}
          openModal={modalConfirm}
          handleClose={this.handleStateChange}
          primaryAction={confirmModalPriAction}
          content={confirmModalContent}
          modalLoading={modalConfirmLoading}
        />

        <PreviewHtmlModal
          active={previewModalActive}
          title={
            <>
              Preview Mail for{' '}
              <TextStyle variation="strong">{pageTitle}</TextStyle>
            </>
          }
          previewModalContent={previewModalContent}
          closeModal={this.handleStateChange}
        />

        <LoadingModal modalLoading={this.state.modalLoading} />
        <PageTour
          isTourOpen={this.state.isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="EditMailTemplates"
        />
        <Layout>
          <Layout.Section>
            <Card title="" sectioned>
              <TextContainer>
                <TextStyle variation="subdued">{description}</TextStyle>
              </TextContainer>
              <br />
              <FormLayout>
                <div className="mail-subject">
                  <TextField
                    label="Email Subject"
                    type="string"
                    value={subject}
                    onChange={(v) => this.handleStateChange({ subject: v })}
                  />
                </div>
                <div className="mail-template">
                  <TextField
                    label="Email Content"
                    value={content}
                    onChange={(v) => this.handleStateChange({ content: v })}
                    multiline={4}
                    id="text_400"
                    spellCheck={false}
                  />
                </div>
              </FormLayout>
            </Card>
          </Layout.Section>

          <Layout.Section secondary>
            <Card title="Variables" sectioned>
              <TextContainer spacing="loose">
                <TextStyle>
                  You can use the following variables to customize this email
                  content.
                </TextStyle>
                <TextStyle variation="strong">
                  Variable name must be enclosed within {`{{ var_name }}`}
                </TextStyle>
              </TextContainer>
              <div className="mail-variables mt-15">
                <MailVariables mailType={emailId} />
              </div>
            </Card>

            <AddEmailRecipientCard
              mail_id={emailId}
              recipients={recipients}
              handleBackendRequest={this.handleBackendRequest}
              recipientLoading={recipientLoading}
            />
          </Layout.Section>
        </Layout>
        <div className="mb-25"></div>
        <PageFooterActions priAction={this.handleBackendRequest} />
      </Page>
    )
  }
}

const pageKaTitle = ({ pageTitle, active }) => {
  const badge = active ? (
    <Badge status="success">Active</Badge>
  ) : (
    <Badge>Disable</Badge>
  )
  return (
    <>
      {pageTitle} Mail {badge}
    </>
  )
}

const PageFooterActions = ({ priAction }) => {
  return (
    <PageActions
      primaryAction={{
        content: 'Save changes',
        onAction: () => {
          priAction('POST', 'SubmitForm')
        },
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            window.location.reload(false)
          },
        },
      ]}
    />
  )
}
