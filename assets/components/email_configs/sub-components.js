import React, { useState } from 'react'
import { Link as LinkReact } from 'react-router-dom'
import {
  DescriptionList,
  TextField,
  Tooltip,
  List,
  TextStyle,
  TextContainer,
  Card,
  Icon,
} from '@shopify/polaris'
import { easy_path } from '../../env'
import { TickMinor, CancelSmallMinor } from '@shopify/polaris-icons'

export const CustomerEmails = ({ customerMails, userEmailDetails }) => {
  return (
    <DescriptionList
      items={customerMails.map((mail) => {
        if (mail.mail_type !== 1) return ''

        return {
          term: (
            <div className="d-flex">
              <CardTooltip
                isCreated={userEmailDetails[mail.id] || 1}
                content={['Active', 'Disabled']}
              />
              &nbsp;
              <LinkReact to={`${easy_path}/email/${mail.id}`}>
                {mail.title}
              </LinkReact>
            </div>
          ),
          description: mail.help_text,
        }
      })}
    />
  )
}

export const MerchantEmails = ({ merchantMails, userEmailDetails }) => {
  return (
    <DescriptionList
      items={merchantMails.map((mail_template) => {
        if (mail_template.mail_type !== 2) return ''

        return {
          term: (
            <div className="d-flex">
              <CardTooltip
                isCreated={userEmailDetails[mail_template.id] || 1}
                content={['Active', 'Disabled']}
              />
              &nbsp;
              <LinkReact to={`${easy_path}/email/${mail_template.id}`}>
                {mail_template.title}
              </LinkReact>
            </div>
          ),
          description: mail_template.help_text,
        }
      })}
    />
  )
}

export const EmailSignature = ({
  label = '',
  emailSignature,
  handleChange,
}) => {
  return (
    <div className="email-signature">
      <TextField
        label={label}
        value={emailSignature}
        onChange={(val) => handleChange({ emailSignature: val })}
        multiline={5}
        id="mail_signature_code"
        helpText={
          <div>
            For make changes, First copy this code, edit by any editor and then
            paste the updated code.
            <b>Do not change the variable_name.</b>
          </div>
        }
      />
    </div>
  )
}

export const BrandLogo = ({
  label = 'Brand Logo Link',
  brandLogo,
  handleChange,
}) => {
  return (
    <TextField
      label={label}
      value={brandLogo}
      spellCheck={false}
      onChange={(val) => handleChange({ brandLogo: val })}
      helpText={
        <div>
          File should be of 65 * 65 px.
          <Tooltip
            content={
              <List type="number">
                <List.Item>Go to Shopify Settings</List.Item>
                <List.Item>Go to Files</List.Item>
                <List.Item>Upload the logo</List.Item>
                <List.Item>And get the link</List.Item>
              </List>
            }
          >
            <TextStyle variation="strong">Steps to get link</TextStyle>
          </Tooltip>
        </div>
      }
    />
  )
}

export const confirmationModalContent = (
  modalFor,
  values = { title: '', active: false },
) => {
  let confirmModalContent = {}
  const isSureCreate = 'are you sure you want to create '
  const isSureReset = 'are you sure you want to reset '
  const isSureDelete = 'are you sure you want to delete '
  const isSureToggle =
    'are you sure you want to ' + (values.active ? 'disable ' : 'enable ')
  const changeableAction = 'This action can be changed'
  const fixedAction = 'This action can not be changed'

  if (modalFor === 'resetMailTemplate') {
    confirmModalContent = {
      s: isSureReset,
      t: values.title,
      e: 'mail. Subject and template will be updated from original values.',
      title: 'Reset mail',
    }
  } else if (modalFor === 'updateEmailStatus') {
    confirmModalContent = {
      s: isSureToggle,
      t: values.title,
      e: 'mail notification.',
      ca: false,
      title: values.active ? 'Disable mail' : 'Enable mail',
    }
  } else if (modalFor === 'resetReturnLabels') {
    confirmModalContent = {
      s: isSureReset,
      t: values.title,
      c: 'negative',
      e: 'return labels. Return labels will be updated from original values.',
      title: 'Reset Labels',
    }
  } else {
    return false
  }

  let isDanger = confirmModalContent.c || ''
  let isChangeable = confirmModalContent.ca || false

  return {
    title: confirmModalContent.title,
    content: (
      <TextContainer>
        <TextStyle>
          <TextStyle variation={isDanger}>{confirmModalContent.s}</TextStyle>
          {confirmModalContent.t !== '' && (
            <TextStyle variation="strong">{confirmModalContent.t}</TextStyle>
          )}
          <TextStyle> {confirmModalContent.e}</TextStyle>
          {isChangeable === true && (
            <TextStyle variation="strong"> {changeableAction}</TextStyle>
          )}
        </TextStyle>
      </TextContainer>
    ),
  }
}

export const EditEmailStyleCardSection = ({
  handleCopy,
  openConfirmationModal,
  setNoticeToast,
  label = '',
  emailStyle,
  handleChange,
}) => {
  const [id, setId] = useState('email-style-code')
  const [disabled, setDisabled] = useState(true)

  return (
    <div className="email-styles">
      <Card.Section
        title="Edit Style of customer mail templates"
        actions={[
          {
            content: 'Copy code',
            onAction: () => {
              handleCopy('email-style-code-edit', setNoticeToast)
            },
            disabled: disabled,
          },
          {
            content: 'Reset code',
            destructive: true,
            onAction: () => {
              openConfirmationModal('resetEmailStyleCode')
            },
          },
          {
            content: 'Edit',
            onAction: () => {
              setId('email-style-code-edit')
              setDisabled(false)
            },
            disabled: !disabled,
          },
        ]}
      >
        <TextField
          label={label}
          value={emailStyle}
          onChange={(val) => handleChange({ emailStyle: val })}
          multiline={10}
          id={id}
          spellCheck={false}
          helpText="For make changes, First copy this code, edit by any editor and then paste the updated code."
          disabled={disabled}
        />
      </Card.Section>
    </div>
  )
}

export const EditEmailHeaderCardSection = ({
  handleCopy,
  setNoticeToast,
  label = '',
  emailHeader,
  handleChange,
}) => {
  return (
    <div className="email-header">
      <Card.Section
        title="Edit header of mail templates"
        actions={[
          {
            content: 'Copy code',
            onAction: () => {
              handleCopy('mail_header_code', setNoticeToast)
            },
          },
        ]}
      >
        <TextField
          label={label}
          value={emailHeader}
          onChange={(val) => handleChange({ emailHeader: val })}
          multiline={5}
          id="mail_header_code"
          helpText={
            <div>
              For make changes, First copy this code, edit by any editor and
              then paste the updated code.
              <b>Do not change the variable_name.</b>
            </div>
          }
        />
      </Card.Section>
    </div>
  )
}

const CardTooltip = ({ isCreated, content }) => {
  isCreated = parseInt(isCreated)

  if (isCreated) {
    return (
      <Tooltip content={content[0]}>
        <Icon source={TickMinor} backdrop={true} color="primary" />
      </Tooltip>
    )
  }

  return (
    <Tooltip content={content[1]}>
      <Icon source={CancelSmallMinor} backdrop={true} color="subdued" />
    </Tooltip>
  )
}
