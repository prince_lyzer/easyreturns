import React from 'react'
import {
  TextContainer,
  TextStyle,
  Button,
  Checkbox,
  TextField,
  FormLayout,
  ChoiceList,
  Card,
} from '@shopify/polaris'
import { CopyCode, NoteText } from '../../common/Common'

export const RenderAllReturnReasons = ({
  returnReasons,
  handleChange,
  handleDelete,
}) => {
  if (returnReasons.length === 0)
    return (
      <TextContainer>
        <TextStyle variation="subdued">No return reasons added yet.</TextStyle>
      </TextContainer>
    )

  const returnInputs = returnReasons.map((row, index) => {
    return (
      <div key={index} style={{ marginBottom: '15px' }}>
        <TextField
          label=""
          id={`return_reason_id_${row.id}`}
          name={`return_reasons[${row.id}]`}
          value={row.label}
          onChange={handleChange}
          placeholder="Add New Reason"
          connectedRight={
            <Button outline destructive onClick={() => handleDelete(row.id)}>
              Delete
            </Button>
          }
        />
      </div>
    )
  })

  return returnInputs
}

export const RequiredReturnNotesField = ({
  returnNote,
  requiredReturnNote,
  handleStateChange,
}) => {
  if (!returnNote) return null
  return (
    <Checkbox
      label="Enable to compulsory note for a refund request"
      checked={requiredReturnNote}
      onChange={(val) => handleStateChange({ requiredReturnNote: val })}
      helpText=""
    />
  )
}

export const RequiredExchangeNotesField = ({
  exchangeNote,
  requiredExchangeNote,
  handleStateChange,
}) => {
  if (!exchangeNote) return null
  return (
    <Checkbox
      label="Enable to compulsory note for a exchange request"
      checked={requiredExchangeNote}
      onChange={(val) => handleStateChange({ requiredExchangeNote: val })}
      helpText=""
    />
  )
}

export const ExchangeNotesField = ({
  selectedMethods,
  exchangeNote,
  requiredExchangeNote,
  handleStateChange,
}) => {
  if (!selectedMethods.includes(2)) return null

  return (
    <Card.Section title="Exchange Customer Note">
      <FormLayout>
        <Checkbox
          label="Enable to show note in a exchange request"
          checked={exchangeNote}
          onChange={(val) => handleStateChange({ exchangeNote: val })}
          helpText=""
        />
        <RequiredExchangeNotesField
          exchangeNote={exchangeNote}
          requiredExchangeNote={requiredExchangeNote}
          handleStateChange={handleStateChange}
        />
      </FormLayout>
    </Card.Section>
  )
}

export const ReturnNotesField = ({
  selectedMethods,
  returnNote,
  requiredReturnNote,
  handleStateChange,
}) => {
  if (!selectedMethods.includes(1)) return null

  return (
    <Card.Section title="Refund Customer Note">
      <FormLayout>
        <Checkbox
          label="Enable to show note in a refund request"
          checked={returnNote}
          onChange={(val) => handleStateChange({ returnNote: val })}
          helpText=""
        />
        <RequiredReturnNotesField
          returnNote={returnNote}
          requiredReturnNote={requiredReturnNote}
          handleStateChange={handleStateChange}
        />
      </FormLayout>
    </Card.Section>
  )
}

export const ReturnMethods = ({ selectedReturnMethods, handleStateChange }) => {
  return (
    <ChoiceList
      allowMultiple
      title="" //Provided return methods to the customers
      choices={[
        {
          label: 'Refund',
          value: 1,
          helpText: 'Enable to provide Refund option to customers',
        },
        {
          label: 'Exchange',
          value: 2,
          helpText: 'Enable to provide Exchange option to customers',
        },
      ]}
      selected={selectedReturnMethods}
      onChange={(val) => handleStateChange({ selectedReturnMethods: val })}
    />
  )
}

export const RequiredImageUploadsField = ({
  imageUpload,
  requiredImageUpload,
  handleStateChange,
}) => {
  if (!imageUpload) return null
  return (
    <Checkbox
      label="Enable to compulsory image uploads when customer tries to submitting a return."
      checked={requiredImageUpload}
      onChange={(val) => handleStateChange({ requiredImageUpload: val })}
      helpText=""
    />
  )
}

export const UploadImagesField = ({
  imageUpload,
  requiredImageUpload,
  handleStateChange,
}) => {
  return (
    <Card.Section title="">
      <FormLayout>
        <Checkbox
          label="Enable to allow customers to upload images during submitting return."
          checked={imageUpload}
          onChange={(val) => handleStateChange({ imageUpload: val })}
          helpText=""
        />
        <RequiredImageUploadsField
          imageUpload={imageUpload}
          requiredImageUpload={requiredImageUpload}
          handleStateChange={handleStateChange}
        />
      </FormLayout>
    </Card.Section>
  )
}

export const GuestCheckoutField = ({
  guestCheckout,
  handleStateChange,
  returnCenterLink,
  toast,
}) => {
  let noteText = (
    <NoteText content="On Enable guest returns, Both Return center page and its template will be created in your online store." />
  )

  if (returnCenterLink != '')
    noteText = (
      <NoteText content="On Uncheck guest returns, Both Return center page and its template will be deleted from your online store." />
    )

  return (
    <FormLayout>
      <Checkbox
        label="Enable to allow customers to return if customer checkout as guest."
        checked={guestCheckout}
        onChange={(val) => handleStateChange({ guestCheckout: val })}
        helpText=""
      />
      {noteText}
      <ReturnCenterLink returnCenterLink={returnCenterLink} toast={toast} />
    </FormLayout>
  )
}

const ReturnCenterLink = ({ returnCenterLink, toast }) => {
  if (returnCenterLink === '') return ''

  return (
    <TextField
      label="Return Center Link"
      value={returnCenterLink}
      readOnly={true}
      id="return_center_link"
      helpText="" //"Replace { your_store_url } with your shopify store url. Ex: https://mystore.com"
      connectedRight={
        <Button onClick={() => CopyCode('return_center_link', toast)}>
          Copy
        </Button>
      }
    />
  )
}

export const AutoApproveRefundRequests = ({
  autoApproveRefundRequest,
  handleStateChange,
}) => {
  return (
    <Checkbox
      label="Automatically approve refund request"
      checked={autoApproveRefundRequest}
      onChange={(val) => handleStateChange({ autoApproveRefundRequest: val })}
      helpText="When a new refund request is created, It will be automatically approved."
    />
  )
}

export const AutoApproveExchangeRequests = ({
  autoApproveExchangeRequest,
  handleStateChange,
}) => {
  return (
    <Checkbox
      label="Automatically approve exchange request"
      checked={autoApproveExchangeRequest}
      onChange={(val) => handleStateChange({ autoApproveExchangeRequest: val })}
      helpText="When a new exchange request is created, It will be automatically approved."
    />
  )
}
