import React from 'react'
import {
  Page,
  Thumbnail,
  Layout,
  Card,
  FormLayout,
  TextField,
  ChoiceList,
  PageActions,
  Button,
  Select,
  Checkbox,
} from '@shopify/polaris'
import { SettingsMinor } from '@shopify/polaris-icons'
import AxiosObj from '../AxiosIns'
import { AjaxErrorHandle, LoadingModal, appendToUrl } from '../../common/Common'
import { GeneralConfigsPageSkeleton } from '../PageSkeletons/pageSkeleton'
import { PageTour } from '../../common/page-tour'
import {
  RenderAllReturnReasons,
  ReturnNotesField,
  ExchangeNotesField,
  ReturnMethods,
  UploadImagesField,
  GuestCheckoutField,
  AutoApproveRefundRequests,
  AutoApproveExchangeRequests,
} from './sub-components'
import { isEmpty } from 'lodash'

export default class GeneralConfigs extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      returnWindowStartsFrom: 0,
      returnAcceptableDays: '7',
      returnReasons: [],
      paymentOptions: [],
      selectedPaymentOptions: ['bank_details'],
      autoArchive: false,
      returnNote: false,
      requiredReturnNote: false,
      modalLoading: false,
      isTourOpen: false,
      selectedReturnMethods: [1],
      exchangeNote: false,
      requiredExchangeNote: false,
      imageUpload: false,
      requiredImageUpload: false,
      guestCheckout: false,
      returnCenterLink: '',
      autoApproveRefundRequest: false,
      autoApproveExchangeRequest: false,
    }
  }

  componentDidMount() {
    this.props.updateTitle('General Configuration')
    appendToUrl(this.props.location, { shop, host })

    AxiosObj.get('/general-configs')
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []

        this.setState({
          returnWindowStartsFrom: return_data.return_window_starts_from,
          returnAcceptableDays: return_data.return_acceptable_days,
          returnReasons: return_data.return_reasons,
          paymentOptions: return_data.payment_options,
          selectedPaymentOptions: return_data.selected_payment_options
            ? return_data.selected_payment_options
            : [],
          autoArchive: return_data.auto_archive,
          returnNote: return_data.return_note,
          requiredReturnNote: return_data.required_customer_note,
          exchangeNote: return_data.exchange_note,
          requiredExchangeNote: return_data.required_exchange_note,
          selectedReturnMethods: return_data.return_methods,
          imageUpload: return_data.image_upload,
          requiredImageUpload: return_data.required_image_upload,
          guestCheckout: return_data.guest_checkout,
          returnCenterLink: return_data.return_center_link,
          autoApproveRefundRequest: return_data.auto_approve_refund_request,
          autoApproveExchangeRequest: return_data.auto_approve_exchange_request,
        })
      })
      .catch((err) => {
        AjaxErrorHandle(err, this.props.setErrorToast)
      })
  }

  handleGeneralConfigsSubmit = () => {
    const {
      returnAcceptableDays,
      returnReasons,
      selectedPaymentOptions,
      returnWindowStartsFrom,
      autoArchive,
      returnNote,
      requiredReturnNote,
      exchangeNote,
      requiredExchangeNote,
      selectedReturnMethods,
      imageUpload,
      requiredImageUpload,
      guestCheckout,
      autoApproveRefundRequest,
      autoApproveExchangeRequest,
    } = this.state

    if (isEmpty(selectedReturnMethods)) {
      this.props.setErrorToast('Select Return method')
      return
    }

    this.setState({ modalLoading: true })

    AxiosObj.post('/general-configs', {
      return_acceptable_days: returnAcceptableDays,
      return_reasons: returnReasons,
      payment_options: selectedPaymentOptions,
      return_window_starts_from: returnWindowStartsFrom,
      auto_archive: autoArchive,
      customer_note: returnNote,
      required_customer_note: requiredReturnNote,
      exchange_note: exchangeNote,
      required_exchange_note: requiredExchangeNote,
      return_methods: selectedReturnMethods,
      image_upload: imageUpload,
      required_image_upload: requiredImageUpload,
      guest_checkout: guestCheckout,
      auto_approve_refund_request: autoApproveRefundRequest,
      auto_approve_exchange_request: autoApproveExchangeRequest,
    })
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        this.props.setNoticeToast(res_data.notice)

        if (!isEmpty(return_data))
          this.setState({ returnReasons: return_data.return_reasons })

        this.handleStateChange({
          returnCenterLink: return_data.return_center_link,
        })
      })
      .catch((err) => {
        AjaxErrorHandle(err, this.props.setErrorToast)
      })
      .then(() => {
        this.setState({ modalLoading: false })
      })
  }

  handleStateChange = (object) => {
    this.setState(object)
  }

  onHandleReturnReasonChange = (reason_label, reason_id) => {
    reason_id = reason_id.substr(17)

    let returnReasonsTemp = this.state.returnReasons.map((item, index) => {
      if (item.id.toString() == reason_id) {
        item.label = reason_label
        return item
      }
      return item
    })
    this.setState({ returnReasons: [...returnReasonsTemp] })
  }

  addNewReturnReason = () => {
    let random_id = Math.random().toString(36).slice(2)
    const newReason = { id: random_id, reason: '' }
    this.setState({ returnReasons: [...this.state.returnReasons, newReason] })
  }

  onDeleteReasonHandle = (reason_id) => {
    const currentReturnReasons = this.state.returnReasons

    this.setState({
      returnReasons: currentReturnReasons.filter((val) => {
        return val.id.toString() !== reason_id.toString()
      }),
    })
  }

  render() {
    if (this.state.paymentOptions.length === 0) {
      return <GeneralConfigsPageSkeleton title="General Configuration" />
    }

    const {
      modalLoading,
      isTourOpen,
      returnWindowStartsFrom,
      returnAcceptableDays,
      paymentOptions,
      selectedPaymentOptions,
      autoArchive,
      returnReasons,
      returnNote,
      requiredReturnNote,
      selectedReturnMethods,
      exchangeNote,
      requiredExchangeNote,
      imageUpload,
      requiredImageUpload,
      guestCheckout,
      returnCenterLink,
      autoApproveRefundRequest,
      autoApproveExchangeRequest,
    } = this.state

    return (
      <Page
        fullWidth
        title="General Configuration"
        thumbnail={
          <Thumbnail source={SettingsMinor} size="small" alt="Setting Icon" />
        }
        secondaryActions={[
          {
            content: 'Page Tour',
            onAction: () => {
              this.handleStateChange({ isTourOpen: true })
            },
          },
        ]}
      >
        <LoadingModal modalLoading={modalLoading} />
        <PageTour
          isTourOpen={isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="GeneralConfigs"
        />
        <Layout>
          <Layout.AnnotatedSection
            title="General Configuration"
            description="App Basic Configuration."
          >
            <Card>
              <Card.Section>
                <FormLayout>
                  <Select
                    label="Count Return Days From"
                    options={[
                      { label: 'Item Fulfillment Date', value: '0' },
                      // { label: "Order Create Date", value: "1" },
                    ]}
                    onChange={(val) =>
                      this.handleStateChange({ returnWindowStartsFrom: val })
                    }
                    disabled={true}
                    value={returnWindowStartsFrom.toString()}
                    helpText="Return window start after Order Item Fulfillment, or Order Create date."
                  />
                  <div className="return-acceptable-days">
                    <TextField
                      label="Return Acceptable Days"
                      type="number"
                      value={returnAcceptableDays.toString()}
                      min="0"
                      placeholder="Example: 7 days"
                      onChange={(val) =>
                        this.handleStateChange({ returnAcceptableDays: val })
                      }
                      helpText="Returns window time for customers to return order items."
                    />
                  </div>
                </FormLayout>
              </Card.Section>
              <Card.Section title="Available Payment Options">
                <div className="payment-options">
                  <ChoiceList
                    allowMultiple
                    title="Selected payment options are available for customers to select for Refund order amount on a COD order only."
                    choices={paymentOptions.map((payment) => {
                      return {
                        label: payment.name,
                        value: payment.handle,
                        helpText: payment.description,
                      }
                    })}
                    selected={selectedPaymentOptions}
                    onChange={(val) =>
                      this.handleStateChange({ selectedPaymentOptions: val })
                    }
                  />
                </div>
              </Card.Section>
            </Card>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Return Reasons"
            description={
              <p>
                Configure your product return reasons.
                <br />
                Add, Edit or Delete return reasons.
                <br />
                In addition to the reason your customers can also add a note.
              </p>
            }
          >
            <Card>
              <div className="return-reasons">
                <Card.Section>
                  <FormLayout>
                    <RenderAllReturnReasons
                      returnReasons={returnReasons}
                      handleChange={this.onHandleReturnReasonChange}
                      handleDelete={this.onDeleteReasonHandle}
                    />
                    <Button onClick={this.addNewReturnReason}>
                      Add New Reason
                    </Button>
                  </FormLayout>
                </Card.Section>
              </div>
            </Card>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Order Return Methods"
            description={
              <p>
                By default customers are given the option to Refund order only.
                <br />
                If you wish to restrict customers to particular return methods
                you can do so here.
              </p>
            }
          >
            <div className="order-return-methods-div">
              <Card>
                <Card.Section title="Return Methods">
                  <ReturnMethods
                    selectedReturnMethods={selectedReturnMethods}
                    handleStateChange={this.handleStateChange}
                  />
                </Card.Section>

                <ReturnNotesField
                  selectedMethods={selectedReturnMethods}
                  returnNote={returnNote}
                  requiredReturnNote={requiredReturnNote}
                  handleStateChange={this.handleStateChange}
                />

                <ExchangeNotesField
                  selectedMethods={selectedReturnMethods}
                  exchangeNote={exchangeNote}
                  requiredExchangeNote={requiredExchangeNote}
                  handleStateChange={this.handleStateChange}
                />

                <Card.Section>
                  <Checkbox
                    label="Enable to archive return request after completion"
                    checked={autoArchive}
                    onChange={(val) =>
                      this.handleStateChange({ autoArchive: val })
                    }
                    helpText=""
                  />
                </Card.Section>
              </Card>
            </div>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Customer Image Upload"
            description="Allow customers to upload images when submitting return and compulsory image upload for returns."
          >
            <FormLayout>
              <div className="customer-image-upload">
                <Card title="">
                  <UploadImagesField
                    imageUpload={imageUpload}
                    requiredImageUpload={requiredImageUpload}
                    handleStateChange={this.handleStateChange}
                  />
                </Card>
              </div>
            </FormLayout>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Guest Returns"
            description={
              <p>
                Allow customers to return who checkout as a guest.
                <br />
                We create a return center when customer have to enter Email
                address/Phone number and order number to return.
              </p>
            }
          >
            <FormLayout>
              <div className="guest-returns">
                <Card>
                  <Card.Section>
                    <GuestCheckoutField
                      guestCheckout={guestCheckout}
                      handleStateChange={this.handleStateChange}
                      returnCenterLink={returnCenterLink}
                      toast={this.props.setNoticeToast}
                    />
                  </Card.Section>
                </Card>
              </div>
            </FormLayout>
          </Layout.AnnotatedSection>

          <Layout.AnnotatedSection
            title="Automatic Approvals"
            description={
              <p>
                When customer created a new return request, approve it
                automatically
              </p>
            }
          >
            <FormLayout>
              <div className="auto-approvals">
                <Card>
                  <Card.Section>
                    <AutoApproveRefundRequests
                      autoApproveRefundRequest={autoApproveRefundRequest}
                      handleStateChange={this.handleStateChange}
                    />
                    <AutoApproveExchangeRequests
                      autoApproveExchangeRequest={autoApproveExchangeRequest}
                      handleStateChange={this.handleStateChange}
                    />
                  </Card.Section>
                </Card>
              </div>
            </FormLayout>
          </Layout.AnnotatedSection>
        </Layout>

        <div className="mb-25"></div>

        <PageActions
          primaryAction={{
            content: 'Save changes',
            onAction: () => {
              this.handleGeneralConfigsSubmit()
            },
          }}
          secondaryActions={[
            {
              content: 'Cancel',
            },
          ]}
        />
      </Page>
    )
  }
}
