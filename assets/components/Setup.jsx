import React, { Component } from 'react'
import {
  Page,
  Layout,
  Card,
  Heading,
  Spinner,
  List,
  TextStyle,
  Banner,
  DisplayText,
} from '@shopify/polaris'
import AxiosObj from './AxiosIns'
import { AjaxErrorHandle, RedirectDispatch } from '../common/Common'
import { easy_path } from '../env'

export default class Setup extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.updateTitle('Setup')
    setTimeout(() => {
      AxiosObj.get(`/app-setup`)
        .then((res) => {
          const res_data = res.data
          const notice = res_data.notice || ''

          if (notice !== '') {
            this.props.setNoticeToast(notice)
          }
        })
        .catch((err) => {
          AjaxErrorHandle(err, this.props.setErrorToast, '', this.props.history)
        })
        .then(() => {
          setup = 0
          RedirectDispatch(`${easy_path}/onboarding`)
          // window.location.reload();
        })
    }, 1000)
  }

  render() {
    return (
      <Page fullWidth>
        <Layout>
          <Layout.Section>
            <Card>
              <Card.Section>
                <div
                  className="d-flex justify-content-center"
                  style={{ padding: '5rem' }}
                >
                  <div>
                    <div
                      className="d-flex"
                      style={{
                        alignItems: 'center',
                        textAlign: 'center',
                        marginBottom: '15px',
                      }}
                    >
                      <Spinner
                        accessibilityLabel="Spinner example"
                        size="large"
                      />{' '}
                      &nbsp; &nbsp;
                      <Heading>Sit tight, While we ready your app.</Heading>
                    </div>
                    <div style={{ marginLeft: '4.5rem' }}>
                      <List type="bullet">
                        <List.Item>
                          <TextStyle variation="subdued">
                            <DisplayText size="small">
                              Configuring app settings.
                            </DisplayText>
                          </TextStyle>
                        </List.Item>
                        <List.Item>
                          <TextStyle variation="subdued">
                            <DisplayText size="small">
                              Create return page template and style code snippet
                              in your theme.
                            </DisplayText>
                          </TextStyle>
                        </List.Item>
                        <List.Item>
                          <TextStyle variation="subdued">
                            <DisplayText size="small">
                              Creating returns order page in your storefront.
                            </DisplayText>
                          </TextStyle>
                        </List.Item>
                        <List.Item>
                          <TextStyle variation="subdued">
                            <DisplayText size="small">
                              Creating script tag asset in your theme.
                            </DisplayText>
                          </TextStyle>
                        </List.Item>
                      </List>
                    </div>
                  </div>
                </div>
                <Banner
                  status="info"
                  title="Have patiance, setup completed within 1 to 2 minutes."
                ></Banner>
              </Card.Section>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}
