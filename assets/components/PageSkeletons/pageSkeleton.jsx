import React from 'react'
import {
  SkeletonPage,
  Layout,
  Card,
  DescriptionList,
  SkeletonDisplayText,
  SkeletonBodyText,
  DataTable,
  TextContainer,
  Heading,
} from '@shopify/polaris'

import range from 'lodash/range'

export const HomePageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = true,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={
        <TextContainer>
          <Heading>{title}</Heading>
        </TextContainer>
      }
    >
      <Layout>
        <Layout.Section>
          <div className="d-flex justify-content-between skeleton-home">
            <div>
              <Card sectioned>
                <TextContainer>
                  <SkeletonDisplayText size="small" />
                  <SkeletonBodyText lines={1} />
                </TextContainer>
              </Card>
            </div>
            <div>
              <Card sectioned>
                <TextContainer>
                  <SkeletonDisplayText size="small" />
                  <SkeletonBodyText lines={1} />
                </TextContainer>
              </Card>
            </div>
            <div>
              <Card sectioned>
                <TextContainer>
                  <SkeletonDisplayText size="small" />
                  <SkeletonBodyText lines={1} />
                </TextContainer>
              </Card>
            </div>
            <div>
              <Card sectioned>
                <TextContainer>
                  <SkeletonDisplayText size="small" />
                  <SkeletonBodyText lines={1} />
                </TextContainer>
              </Card>
            </div>
          </div>
        </Layout.Section>
      </Layout>

      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={7} />
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  )
}

export const GeneralConfigsPageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = true,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={title}
    >
      <Layout>
        <Layout.AnnotatedSection title={<SkeletonBodyText lines={1} />}>
          <Card>
            <Card.Section>
              <TextContainer>
                <SkeletonDisplayText size="small" />
                <SkeletonBodyText lines={5} />
              </TextContainer>
            </Card.Section>
            <Card.Section>
              <SkeletonBodyText lines={2} />
            </Card.Section>
          </Card>
        </Layout.AnnotatedSection>

        <Layout.AnnotatedSection title={<SkeletonBodyText lines={1} />}>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={4} />
            </TextContainer>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </SkeletonPage>
  )
}

export const EmailConfigsPageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = true,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={
        <TextContainer>
          <Heading>{title}</Heading>
        </TextContainer>
      }
    >
      <Layout>
        <Layout.AnnotatedSection title={<SkeletonBodyText lines={2} />}>
          <Card sectioned>
            <DescriptionList
              items={range(4).map((i) => {
                return {
                  term: <SkeletonBodyText lines={1} />,
                  description: <SkeletonBodyText lines={1} />,
                }
              })}
            />
          </Card>
        </Layout.AnnotatedSection>

        <Layout.AnnotatedSection title={<SkeletonBodyText lines={2} />}>
          <Card>
            <Card.Section>
              <TextContainer>
                <SkeletonDisplayText size="small" />
                <SkeletonBodyText lines={5} />
              </TextContainer>
            </Card.Section>
            <Card.Section>
              <SkeletonBodyText lines={2} />
            </Card.Section>
          </Card>
        </Layout.AnnotatedSection>
      </Layout>
    </SkeletonPage>
  )
}

export const ReturnPageConfigurePageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = true,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={
        <TextContainer>
          <Heading>{title}</Heading>
        </TextContainer>
      }
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={1} />
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={7} />
            </TextContainer>
          </Card>
        </Layout.Section>

        <Layout.Section secondary>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={2} />
            </TextContainer>
          </Card>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={2} />
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
      <div className="mb-25"></div>
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={3} />
            </TextContainer>
          </Card>
        </Layout.Section>

        <Layout.Section secondary>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={2} />
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  )
}

export const EditMailTemplatePageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = true,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={
        <TextContainer>
          <SkeletonDisplayText size="medium" />
        </TextContainer>
      }
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={1} />
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={7} />
            </TextContainer>
          </Card>
        </Layout.Section>

        <Layout.Section secondary>
          <Card sectioned>
            <SkeletonDisplayText size="small" />
            <DescriptionList
              items={range(5).map((i) => {
                return {
                  term: <SkeletonBodyText lines={1} />,
                  description: <SkeletonBodyText lines={1} />,
                }
              })}
            />
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  )
}

export const ReturnListPageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = true,
}) => {
  const columnsCount = 6
  const columns = range(columnsCount).map((i) => (
    <SkeletonBodyText key={i} lines={1} />
  ))
  const headers = range(columnsCount).map((i) => (
    <SkeletonBodyText lines={1} key={i} />
  ))

  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={
        <TextContainer>
          <Heading>{title}</Heading>
        </TextContainer>
      }
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <SkeletonBodyText lines={1} />
            <br />
            <DataTable
              columnContentTypes={[
                'text',
                'text',
                'text',
                'text',
                'numeric',
                'numeric',
              ]}
              headings={headers}
              rows={range(columnsCount).map(() => columns)}
            />
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  )
}

export const ReturnOrderDescPageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 1,
  primaryAction = false,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={
        <TextContainer>
          <SkeletonDisplayText size="medium" />
        </TextContainer>
      }
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
            </TextContainer>
            <br />
            <table className="f-table">
              <tbody>
                {range(2).map((i) => {
                  return (
                    <tr key={i}>
                      <td>
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td>
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td className="text-right">
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td className="text-right">
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td className="text-right">
                        <SkeletonBodyText lines={1} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </Card>
        </Layout.Section>

        <Layout.Section secondary>
          <Card sectioned>
            <SkeletonDisplayText size="small" />
            <br />
            <SkeletonBodyText />
          </Card>
        </Layout.Section>

        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
            </TextContainer>
            <br />
            <table className="f-table">
              <tbody>
                {range(2).map((i) => {
                  return (
                    <tr key={i}>
                      <td>
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td>
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td className="text-right">
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td className="text-right">
                        <SkeletonBodyText lines={1} />
                      </td>
                      <td className="text-right">
                        <SkeletonBodyText lines={1} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </Card>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines="1" />
            </TextContainer>
          </Card>
        </Layout.Section>

        <Layout.Section secondary>
          <Card>
            <Card.Section sectioned>
              <SkeletonDisplayText size="small" />
              <br />
              <SkeletonBodyText />
            </Card.Section>
            <Card.Section>
              <SkeletonBodyText />
            </Card.Section>
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  )
}

export const TranslationPageSkeleton = ({
  breadcrumbs = false,
  title = '',
  countSecondaryAction = 0,
  primaryAction = false,
}) => {
  return (
    <SkeletonPage
      primaryAction={primaryAction}
      secondaryActions={countSecondaryAction}
      breadcrumbs={breadcrumbs}
      fullWidth={true}
      title={title}
    >
      <Layout>
        <Layout.Section>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={4} />
            </TextContainer>
          </Card>
          <Card sectioned>
            <TextContainer>
              <SkeletonDisplayText size="small" />
              <SkeletonBodyText lines={4} />
            </TextContainer>
          </Card>
        </Layout.Section>
      </Layout>
    </SkeletonPage>
  )
}
