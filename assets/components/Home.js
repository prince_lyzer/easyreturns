import React, { useState, useCallback } from 'react'
import {
  Page,
  Thumbnail,
  Layout,
  Card,
  Icon,
  Popover,
  Button,
  DatePicker,
  PageActions,
  Heading,
  Stack,
} from '@shopify/polaris'
import { HomeMajor, CalendarMinor } from '@shopify/polaris-icons'
import {
  AjaxErrorHandle,
  LoadingModal,
  RedirectDispatch,
  appendToUrl,
} from '../common/Common'
import moment from 'moment'
import Chart from 'chart.js'
import AxiosObj from './AxiosIns'
import { HomePageSkeleton } from './PageSkeletons/pageSkeleton'
import { PageTour } from '../common/page-tour'
import { easy_path } from '../env'

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    const now = new Date()

    this.state = {
      count: [],
      return_requests_state: [],
      loading: true,
      selectedDates: { start: now, end: now },
      popoverTitle: 'Today',
      modalLoading: false,
      isTourOpen: false,
    }
  }

  componentDidMount() {
    if (setup) {
      RedirectDispatch(`${easy_path}/app-setup`, 0)
      return
    }

    appendToUrl(this.props.location, { shop, host })

    this.props.updateTitle('Dashboard')
    this.getReturnsStates(this.state.selectedDates)
    renderChart()
  }

  componentWillUnmount() {
    // fix Warning: Can't perform a React state update on an unmounted component
    this.setState = (state, callback) => {
      return
    }
  }

  handleApplyChanges = (selectedDates) => {
    const start = moment(selectedDates.start).format('MMM DD, YYYY')
    const end = moment(selectedDates.end).format('MMM DD, YYYY')
    this.setState({
      popoverTitle: ` ${start} - ${end}`,
      selectedDates: selectedDates,
    })
    this.getReturnsStates(selectedDates)
    // renderChart()
  }

  componentDidUpdate() {
    renderChart(this.state.return_requests_state)
  }

  getReturnsStates = (selectedDates) => {
    // const { selectedDates } = this.state
    const start = moment(selectedDates.start).format('YYYY-MM-DD HH:mm')
    const end = moment(selectedDates.end).format('YYYY-MM-DD HH:mm')
    this.setState({ modalLoading: true })

    AxiosObj.get('/return-request/state', {
      params: {
        start: start,
        end: end,
      },
    })
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data
        if (return_data) {
          this.setState({
            count: return_data.count,
            return_requests_state: return_data.return_requests_state,
            loading: false,
          })
        }
      })
      .catch((err) => {
        AjaxErrorHandle(err, this.props.setErrorToast)
      })
      .then(() => {
        this.setState({ modalLoading: false })
      })
  }

  handleStateChange = (obj) => {
    this.setState(obj)
  }

  render() {
    const {
      loading,
      selectedDates,
      popoverTitle,
      modalLoading,
      isTourOpen,
      count: requestCounts,
    } = this.state

    if (loading) {
      return <HomePageSkeleton title="Dashboard" />
    }

    const primaryAction = (
      <DateRangePopover
        handleApplyChanges={this.handleApplyChanges}
        popoverTitle={popoverTitle}
      />
    )

    return (
      <Page
        fullWidth
        title="Dashboard"
        thumbnail={
          <Thumbnail source={HomeMajor} size="small" alt="Home Icon" />
        }
        primaryAction={primaryAction}
        secondaryActions={[
          {
            content: 'Page Tour',
            onAction: () => {
              this.handleStateChange({ isTourOpen: true })
            },
          },
        ]}
      >
        <LoadingModal modalLoading={modalLoading} />

        <PageTour
          isTourOpen={isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="Home"
        />

        <Layout>
          <Layout.Section>
            <div id="return-counts" className="mb-16">
              <CountCards
                requestCounts={requestCounts}
                selectedDates={selectedDates}
              />
            </div>
          </Layout.Section>
        </Layout>

        <Layout>
          <Layout.Section>
            <Card title="Returns Over Time" sectioned>
              <div
                className="chart-container"
                style={{ position: 'relative', height: '300px' }}
              >
                <canvas id="returnChart"></canvas>
              </div>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    )
  }
}

const renderChart = (props) => {
  if (!document.getElementById('returnChart')) return null
  var ctx = document.getElementById('returnChart').getContext('2d')
  const pending_requests = props
    ? props.map((prop) => prop.pending_request)
    : []
  const accepted_requests = props
    ? props.map((prop) => prop.accepted_request)
    : []
  const completed_requests = props
    ? props.map((prop) => prop.completed_request)
    : []
  const dateZ = props ? props.map((prop) => prop.dateZ) : []

  // console.log(window.myReturnChart)
  if (window.myReturnChart != undefined) {
    window.myReturnChart.destroy()
  }

  window.myReturnChart = new Chart(ctx, {
    type: 'bar',
    maintainAspectRatio: false,
    data: {
      labels: dateZ,
      datasets: [
        {
          barPercentage: 1,
          barThickness: 25, // Bar tickness in pixels
          minBarLength: 2, // min length of each bar in given dataset (height) in (Pixels)
          label: 'Pending Requests',
          data: pending_requests,
          backgroundColor: '#a4e8f2', // Background color of bars
          borderWidth: 2, // Width of bars border in (pixels)
          borderColor: '#8fe2ef', // Border Color of the bar
          borderSkipped: 'bottom', // Border does not show for 'Bottom'
          hoverBackgroundColor: '#9dedf9', // Background Color on Hover
          hoverBorderColor: '#a4e8f2', // Border color on Hover
          hoverBorderWidth: 1, // Border Width on Hover
          order: 0, // Bar order in multidataset, 1 comes first than 0
        },
        {
          categoryPercentage: 0.5,
          barPercentage: 1,
          barThickness: 25,
          minBarLength: 2,
          label: 'Accepted Requests',
          data: accepted_requests,
          backgroundColor: '#ffee93',
          borderWidth: { left: 0, top: 2, right: 2, bottom: 2 },
          borderColor: '#f5d782',
          borderSkipped: 'bottom',
          hoverBackgroundColor: '#f5d782',
          hoverBorderColor: '#ffee93',
          hoverBorderWidth: 1,
        },
        {
          categoryPercentage: 0.5,
          barPercentage: 1,
          barThickness: 25,
          minBarLength: 2,
          label: 'Completed Requests',
          data: completed_requests,
          backgroundColor: '#aee9d1',
          borderWidth: { left: 0, top: 2, right: 2, bottom: 2 },
          borderColor: '#8de2c0',
          borderSkipped: 'bottom',
          hoverBackgroundColor: '#8de2c0',
          hoverBorderColor: '#aee9d1',
          hoverBorderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true, // begins the y-axis from 0
              suggestedMax: 10,
            },
            scaleLabel: {
              display: true,
              labelString: 'Request Count',
              fontSize: 20,
            },
          },
        ],
        xAxis: [
          {
            gridlines: {
              offset: true,
              offsetGridLines: false,
            },
          },
          {
            stacked: false,
          },
        ],
      },
      tooltips: {
        enabled: true,
        mode: 'index',
        position: 'nearest',
        backgroundColor: 'rgba(0,0,0,.5)', // background color of tooltip
        caretPadding: 12, // spacing the tooltip from bar
      },
      responsive: true,
      maintainAspectRatio: false,
    },
  })
  return null
}

const DateRangePopover = ({ handleApplyChanges, popoverTitle }) => {
  const [popoverActive, setPopoverActive] = useState(false)

  const togglePopoverActive = useCallback(
    () => setPopoverActive((popoverActive) => !popoverActive),
    [],
  )

  const now = new Date()
  const [month, setMonth] = useState(() => now.getMonth())
  const [year, setYear] = useState(() => now.getFullYear())
  const [selectedDates, setSelectedDates] = useState(() => {
    return { start: now, end: now }
  })

  const handleMonthChange = useCallback((month, year) => {
    setMonth(month)
    setYear(year)
  })

  const handleSelectedDatesChange = useCallback((d) => {
    setSelectedDates(d.selectedDates)
  })

  const activator = (
    <Button onClick={togglePopoverActive} disclosure>
      <span className="d-flex align-items-center">
        <Icon source={CalendarMinor} /> {popoverTitle}
      </span>
    </Button>
  )

  return (
    <Popover
      active={popoverActive}
      activator={activator}
      onClose={togglePopoverActive}
      fullHeight={true}
      fixed={true}
    >
      <Popover.Pane fixed>
        <Popover.Section>
          <p>Select date range</p>
        </Popover.Section>
      </Popover.Pane>

      <Popover.Pane>
        <Popover.Section>
          <DatePickerExample
            now={now}
            month={month}
            year={year}
            selectedDates={selectedDates}
            setSelectedDates={handleSelectedDatesChange}
            handleMonthChange={handleMonthChange}
          />
        </Popover.Section>
      </Popover.Pane>

      <Popover.Pane fixed>
        <div className="p-x">
          <PageActions
            primaryAction={{
              content: 'Apply',
              onAction: () => {
                handleApplyChanges(selectedDates)
                togglePopoverActive()
              },
            }}
            secondaryActions={[
              {
                content: 'Cancel',
                onAction: () => {
                  togglePopoverActive()
                },
              },
            ]}
          />
        </div>
      </Popover.Pane>
    </Popover>
  )
}

const DatePickerExample = ({
  now,
  month,
  year,
  selectedDates,
  setSelectedDates,
  handleMonthChange,
}) => {
  return (
    <DatePicker
      month={month}
      year={year}
      onChange={(d) => {
        setSelectedDates({ selectedDates: d })
      }}
      onMonthChange={handleMonthChange}
      selected={selectedDates}
      multiMonth={false}
      allowRange
      disableDatesAfter={now}
    />
  )
}

const CardTemplate = ({ text, className, count, status, selectedDates }) => {
  const start = moment(selectedDates.start)
  const end = moment(selectedDates.end)

  const handleCardClick = useCallback((status) => {
    const urlParams = new URLSearchParams({ status, start, end })
    RedirectDispatch(`${easy_path}/returns?${urlParams}`)
  })
  return (
    <Stack.Item>
      <div
        className={`frfi-card ${className}`}
        onClick={() => {
          handleCardClick(status)
        }}
      >
        <div className="d-flex justify-content-between">
          <Heading element="h2">{text}</Heading>
          <span className="badge-count">{count > 0 ? count : 'Zero'}</span>
        </div>
      </div>
    </Stack.Item>
  )
}

const CountCards = ({ requestCounts, selectedDates }) => {
  return (
    <Stack distribution="equalSpacing">
      <CardTemplate
        text="Returns Pending"
        className="info-card"
        count={requestCounts.total_pending_request}
        status={1}
        selectedDates={selectedDates}
      />
      <CardTemplate
        text="Returns Accepted"
        className="warning-card"
        count={requestCounts.total_accepted_requests}
        status={2}
        selectedDates={selectedDates}
      />
      <CardTemplate
        text="Returns Completed"
        className="success-card"
        count={requestCounts.total_completed_requests}
        status={3}
        selectedDates={selectedDates}
      />
      <CardTemplate
        text="Returns Cancelled"
        className="danger-card"
        count={requestCounts.total_cancelled_requests}
        status={4}
        selectedDates={selectedDates}
      />
    </Stack>
  )
}
