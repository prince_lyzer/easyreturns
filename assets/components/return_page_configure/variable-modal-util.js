import React from 'react'
import {
  DescriptionList,
  Modal,
  TextContainer,
  TextStyle,
} from '@shopify/polaris'

export function VariableModal({ openModal, closeModal, variablesFor }) {
  let content = ''
  if (variablesFor === 'return_response_page_variables')
    content = (
      <DescriptionList
        items={returnOrderResponsePageVariables}
        spacing="tight"
      />
    )
  else if (variablesFor === 'return_page_variables')
    content = (
      <DescriptionList items={returnOrderPageVariables} spacing="tight" />
    )
  if (!content) return false
  return (
    <Modal
      open={openModal}
      onClose={() => {
        closeModal({ modalVariable: false })
      }}
      title="Variables?"
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            closeModal({ modalVariable: false })
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer spacing="loose">
          <TextStyle variation="negative">
            * Variable name must be enclosed within {`{{ var_name }}`}.
          </TextStyle>
        </TextContainer>
        <div className="variable_list">{content}</div>
      </Modal.Section>
    </Modal>
  )
}

const returnOrderPageVariables = [
  {
    term: 'order_name',
    description: 'Return Order Name',
  },
  {
    term: 'return_request_id',
    description: 'Return Request Id',
  },
  {
    term: 'return_line_items',
    description: 'Return Line items Table',
  },
  {
    term: 'is_cod_order',
    description: 'Check for COD order',
  },
  {
    term: 'payment_type',
    description: 'Payment type name',
  },
  {
    term: 'payment_type_handle',
    description: 'Payment type handle',
  },
  {
    term: 'bank_account_number',
    description: 'Bank Account Number',
  },
  {
    term: 'bank_ifsc_code',
    description: 'Bank IFSC Code',
  },
  {
    term: 'bank_holder_name',
    description: 'Bank Holder Name',
  },
  {
    term: 'paytm_mobile_no',
    description: 'Paytm Mobile number',
  },
  {
    term: 'phonepe_mobile_no',
    description: 'Phonepe Mobile number',
  },
  {
    term: 'google_pay',
    description: 'Google pay Mobile number',
  },
  {
    term: 'upi_id',
    description: 'UPI ID',
  },
]

const returnOrderResponsePageVariables = [
  {
    term: 'order_details',
    description: 'Order details',
  },
  {
    term: 'request_id',
    description: 'Return request Id',
  },
  {
    term: 'return_items',
    description: 'Return line items details',
  },
  {
    term: 'line_items',
    description: 'Order line items details',
  },
  {
    term: 'money_format',
    description: 'Money format',
  },
  {
    term: 'is_cod_order',
    description: 'Is a cod order',
  },
  {
    term: 'payment_option',
    description: 'Payment option handle',
  },
  {
    term: 'payment_details',
    description: 'Payment details',
  },
  {
    term: 'customer_note',
    description: 'Customer note',
  },
  {
    term: 'return_method',
    description: 'Return method: Refund, Exchange',
  },
]
