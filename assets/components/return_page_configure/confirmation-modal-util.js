import React from 'react'
import { TextContainer, TextStyle } from '@shopify/polaris'

export const confirmationModalContent = (modalFor) => {
  let confirmModalContent = {}
  const isSureCreate = 'are you sure you want to create '
  const isSureReset = 'are you sure you want to reset '
  const isSureDelete = 'are you sure you want to delete '

  if (modalFor === 'create_return_page') {
    confirmModalContent = {
      s: isSureCreate,
      t: 'Easy Order Return',
      e: 'page for your online store.',
      title: 'Create Page',
    }
  } else if (modalFor === 'delete_return_page') {
    confirmModalContent = {
      s: isSureDelete,
      t: 'Easy Order Return',
      e: 'from your online store.',
      c: 'negative',
      title: 'Delete Page',
    }
  } else if (modalFor === 'create_template') {
    confirmModalContent = {
      s: isSureCreate,
      t: 'templates/page.easy_returns.liquid',
      e: 'asset in your theme.',
      title: 'Create Template',
    }
  } else if (modalFor === 'reset_template') {
    confirmModalContent = {
      s: isSureReset,
      t: 'templates/page.easy_returns.liquid',
      e: 'asset.',
      title: 'Reset Template',
    }
  } else if (modalFor === 'delete_template') {
    confirmModalContent = {
      s: isSureDelete,
      t: 'templates/page.easy_returns.liquid',
      e: 'asset from your theme.',
      c: 'negative',
      title: 'Delete Template',
    }
  } else if (modalFor === 'create_style_snippet') {
    confirmModalContent = {
      s: isSureCreate,
      t: 'snippets/easy_return_style.css.liquid',
      e: 'style snippet in your theme.',
      title: 'Create style snippet',
    }
  } else if (modalFor === 'reset_style_snippet') {
    confirmModalContent = {
      s: isSureReset,
      t: 'snippets/easy_return_style.css.liquid',
      e: 'style snippet in your theme.',
      title: 'Reset style snippet',
    }
  } else if (modalFor === 'delete_style_snippet') {
    confirmModalContent = {
      s: isSureDelete,
      t: 'snippets/easy_return_style.css.liquid',
      e: 'style snippet from your theme.',
      c: 'negative',
      title: 'Delete style snippet',
    }
  } else if (modalFor === 'create_script_tag') {
    confirmModalContent = {
      s: isSureCreate,
      t: 'easy_returns.js',
      e: 'script tag for your online store.',
      title: 'Create script tag',
    }
  } else if (modalFor === 'delete_script_tag') {
    confirmModalContent = {
      s: isSureDelete,
      t: 'easy_returns.js',
      e: 'script tag from your online store.',
      c: 'negative',
      title: 'Delete script tag',
    }
  } else if (modalFor === 'orderReturnPageCode') {
    confirmModalContent = {
      s: isSureReset,
      t: '',
      e: 'order return page code. This code is updated from the original code.',
      title: 'Reset order return page code',
    }
  } else if (modalFor === 'orderReturnResponseCode') {
    confirmModalContent = {
      s: isSureReset,
      t: '',
      e:
        'order return response code. This code is updated from the original code.',
      title: 'Reset order return response page code',
    }
  } else if (modalFor === 'resetEmailStyleCode') {
    confirmModalContent = {
      s: isSureReset,
      t: '',
      e: 'email style code. This code is updated from the original code.',
      title: 'Reset email style code',
    }
  } else if (
    modalFor === 'orderReturnPageCodePreview' ||
    modalFor === 'orderReturnResponseCodePreview' ||
    modalFor === 'returnCenterPageCodePreview'
  ) {
    confirmModalContent = {
      s: '',
      t: '',
      e:
        'Your store is password protected. Use the password to enter the store. Before previewing this page.',
      title: 'Preview page',
    }
  } else if (modalFor === 'create_return_center_page') {
    confirmModalContent = {
      s: isSureCreate,
      t: 'Easy Return Center',
      e: 'page for your online store.',
      title: 'Create Return Center Page',
    }
  } else if (modalFor === 'delete_return_center_page') {
    confirmModalContent = {
      s: isSureDelete,
      t: 'Easy Return Center',
      e: 'from your online store.',
      c: 'negative',
      title: 'Delete Return Center Page',
    }
  } else if (modalFor === 'create_return_center_page_template') {
    confirmModalContent = {
      s: isSureCreate,
      t: 'templates/page.easy_return_center.liquid',
      e: 'asset in your theme.',
      title: 'Create Return Center Liquid Template',
    }
  } else if (modalFor === 'reset_return_center_page_template') {
    confirmModalContent = {
      s: isSureReset,
      t: 'templates/page.easy_return_center.liquid',
      e: 'asset.',
      title: 'Reset Return Center Liquid Template',
    }
  } else if (modalFor === 'delete_return_center_page_template') {
    confirmModalContent = {
      s: isSureDelete,
      t: 'templates/page.easy_return_center.liquid',
      e: 'asset from your theme.',
      c: 'negative',
      title: 'Delete Return Center Liquid Template',
    }
  } else if (modalFor === 'returnCenterPageCode') {
    confirmModalContent = {
      s: isSureReset,
      t: '',
      e:
        'return center page code. This code is updated from the original code.',
      title: 'Reset return center page code',
    }
  } else {
    return false
  }

  let isDanger = confirmModalContent.c || ''

  return {
    title: confirmModalContent.title,
    content: (
      <TextContainer>
        <TextStyle>
          <TextStyle variation={`${isDanger}`}>
            {confirmModalContent.s}
          </TextStyle>
          {confirmModalContent.t !== '' && (
            <TextStyle variation="code">{confirmModalContent.t}</TextStyle>
          )}
          <TextStyle> {confirmModalContent.e}</TextStyle>
        </TextStyle>
      </TextContainer>
    ),
  }
}
