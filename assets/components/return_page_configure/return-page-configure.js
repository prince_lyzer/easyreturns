import React from 'react'
import {
  Page,
  Thumbnail,
  Layout,
  Card,
  FormLayout,
  TextStyle,
  TextContainer,
  PageActions,
  Checkbox,
} from '@shopify/polaris'
import { OnlineStoreMajor } from '@shopify/polaris-icons'
import '@shopify/polaris/dist/styles.css'
import AxiosObj from '../AxiosIns'
import {
  AjaxErrorHandle,
  LoadingModal,
  PreviewModal,
  ConfirmationModal,
  RedirectDispatch,
  appendToUrl,
  NoteText,
} from '../../common/Common'
import { ReturnPageConfigurePageSkeleton } from '../PageSkeletons/pageSkeleton'
import { PageTour } from '../../common/page-tour'
import {
  ReturnPageCard,
  ReturnPageTemplateCard,
  StyleSnippetCard,
  ScriptTagCard,
  ReturnCenterPageCard,
  ReturnCenterPageTemplateCard,
} from './page-utils'
import { confirmationModalContent } from './confirmation-modal-util'
import { VariableModal } from './variable-modal-util'
import {
  ThCodePaste,
  TdCodePaste,
  VariableCard,
  OrderReturnPageCodeCard,
  OrderReturnResponseCard,
  OrderReturnResponseVariableCard,
  VideoHelpSection,
  ThemeUpdateAlertBanner,
  ScreenshotPreviewSection,
  ReturnCenterPageCodeCard,
} from './sub-components'

export default class ReturnPageConfigure extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      orderReturnPageCode: '',
      orderReturnResponseCode: '',
      returnPageTitle: '',
      returnPageHandle: '',
      modalLoading: false,
      previewModalLoading: false,
      modalConfirm: false,
      modalConfirmLoading: false,
      confirmModalTitle: '',
      confirmModalContent: '',
      confirmModalPriAction: null,
      src: '',
      modalVariable: false,
      variablesFor: '',
      showReturnLinks: false,
      isTourOpen: false,
      isReturnPageCreated: false,
      isReturnPageTemplateCreated: false,
      isStyleSnippetCreated: false,
      isScriptTagCreated: false,
      processing: true,
      storePasswordEnabled: false,
      returnCenterPageCode: '',
      isReturnCenterPageCreated: false,
      isReturnCenterPageTemplateCreated: false,
      returnCenterPageHandle: '',
    }
  }

  componentDidMount() {
    this.props.updateTitle('Return Page Configuration')
    appendToUrl(this.props.location, { shop, host })

    this.handleBackendRequests('GET', 'getDetails', {})
  }

  handleStateChange = (obj) => {
    this.setState(obj)
  }

  handleBackendRequests = (method = 'GET', requestName, params) => {
    let url = '',
      data = {}

    if (requestName === 'getDetails') {
      url = `/front-configure`
    } else if (requestName === 'onSaveChanges') {
      this.setState({ modalLoading: true })
      url = `/front-configure`
      const {
        orderReturnPageCode: order_return_page_code,
        orderReturnResponseCode: order_return_response_code,
        returnPageTitle: return_page_title,
        returnPageHandle: return_page_handle,
        showReturnLinks: show_return_links,
        returnCenterPageCode: return_center_page_code,
      } = this.state
      data = {
        order_return_page_code,
        order_return_response_code,
        return_page_title,
        return_page_handle,
        show_return_links,
        return_center_page_code,
      }
    } else if (requestName === 'resetCode') {
      this.setState({ modalConfirmLoading: true })
      url = `/front-configure/reset`
    } else if (requestName === 'handleRegenerate') {
      this.setState({ modalConfirmLoading: true })
      url = `/front-configure/regenerate`
    } else if (requestName === 'previewChanges') {
      if (this.state.storePasswordEnabled)
        this.setState({ modalConfirmLoading: true })
      else this.setState({ modalLoading: true })

      url = `/front-configure/preview`
      data = params

      if (data.type === 'orderReturnPageCode') {
        data.order_return_page_code = this.state.orderReturnPageCode
      } else if (data.type === 'orderReturnResponseCode') {
        data.order_return_response_code = this.state.orderReturnResponseCode
      } else if (data.type === 'returnCenterPageCode') {
        data.return_center_page_code = this.state.returnCenterPageCode
      } else {
        return false
      }
    } else if (requestName === 'toggleReturnLinks') {
      this.setState({ modalConfirmLoading: true })
      url = `/front-configure/update`
      data = params
    } else {
      return
    }

    let axiosObj = { method, url }
    // console.log(data)
    if (method === 'GET') axiosObj.params = params
    else if (method === 'POST') axiosObj.data = data

    AxiosObj(axiosObj)
      .then((res) => {
        const res_data = res.data
        const return_data = res_data.data || []
        const notice = res_data.notice || ''
        const error = res_data.error || ''

        if (notice.trim() !== '') {
          this.props.setNoticeToast(res_data.notice)
        } else if (error.trim() !== '') {
          this.props.setErrorToast(res_data.error)
        }

        if (requestName === 'resetCode') {
          this.setState({ [params.reset_code]: return_data[params.reset_code] })
        } else if (requestName === 'handleRegenerate') {
          if (typeof return_data.is_return_page_created !== 'undefined') {
            this.setState({
              isReturnPageCreated: return_data.is_return_page_created,
            })
          }
          if (typeof return_data.is_return_template_created !== 'undefined') {
            this.setState({
              isReturnPageTemplateCreated:
                return_data.is_return_template_created,
            })
          }
          if (typeof return_data.is_style_snippet_created !== 'undefined') {
            this.setState({
              isStyleSnippetCreated: return_data.is_style_snippet_created,
            })
          }
          if (typeof return_data.is_script_tag_created !== 'undefined') {
            this.setState({
              isScriptTagCreated: return_data.is_script_tag_created,
            })
          }
          if (
            typeof return_data.is_return_center_page_created !== 'undefined'
          ) {
            this.setState({
              isReturnCenterPageCreated:
                return_data.is_return_center_page_created,
            })
          }
          if (
            typeof return_data.is_return_center_page_template_created !==
            'undefined'
          ) {
            this.setState({
              isReturnCenterPageTemplateCreated:
                return_data.is_return_center_page_template_created,
            })
          }
        } else if (requestName === 'onSaveChanges') {
        } else if (requestName === 'getDetails') {
          this.setState({
            orderReturnPageCode: return_data.order_return_page_code,
            orderReturnResponseCode: return_data.order_return_response_code,
            returnPageTitle: return_data.return_page_title,
            returnPageHandle: return_data.return_page_handle,
            showReturnLinks: return_data.show_return_links,
            isReturnPageCreated: return_data.is_return_page_created,
            isReturnPageTemplateCreated: return_data.is_return_template_created,
            isStyleSnippetCreated: return_data.is_style_snippet_created,
            isScriptTagCreated: return_data.is_script_tag_created,
            storePasswordEnabled: return_data.password_enabled,
            returnCenterPageCode: return_data.return_center_page_code,
            isReturnCenterPageCreated:
              return_data.is_return_center_page_created,
            isReturnCenterPageTemplateCreated:
              return_data.is_return_center_page_template_created,
            returnCenterPageHandle: return_data.return_center_page_handle,
          })
        } else if (requestName === 'toggleReturnLinks') {
        } else if (requestName === 'previewChanges') {
          if (params.type === 'returnCenterPageCode') {
            RedirectDispatch(
              `https://${shop}/pages/${this.state.returnCenterPageHandle}?preview=1&type=${params.type}`,
              2,
              true,
            )
          } else
            RedirectDispatch(
              `https://${shop}/pages/${this.state.returnPageHandle}?order_id=3664397041837&preview=1&type=${params.type}`,
              2,
              true,
            )
        }
      })
      .catch((err) => {
        AjaxErrorHandle(err, this.props.setErrorToast)
      })
      .then(() => {
        if (requestName === 'resetCode') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        } else if (requestName === 'handleRegenerate') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        } else if (requestName === 'previewChanges') {
<<<<<<< HEAD
          if (this.state.storePasswordEnabled)
            this.setState({ modalConfirmLoading: false, modalConfirm: false })
          else this.setState({ modalLoading: false })

          RedirectDispatch(
            `https://${shop}/pages/${this.state.returnPageHandle}?order_id=3664397041837&preview=1&type=${params.type}`,
            2,
            true,
          )
=======
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
>>>>>>> prince
        } else if (requestName === 'onSaveChanges') {
          this.setState({ modalLoading: false })
        } else if (requestName === 'getDetails') {
          this.setState({ processing: false })
        } else if (requestName === 'toggleReturnLinks') {
          this.setState({ modalConfirmLoading: false, modalConfirm: false })
        }
      })
  }

  openConfirmationModal = (modalFor) => {
    if (modalFor === '') return

    const t_b = confirmationModalContent(modalFor)
    let priAction = () =>
      this.handleBackendRequests('GET', 'handleRegenerate', {
        type: modalFor,
      })

    if (
      modalFor === 'orderReturnPageCode' ||
      modalFor === 'orderReturnResponseCode' ||
      modalFor === 'returnCenterPageCode'
    )
      priAction = () =>
        this.handleBackendRequests('GET', 'resetCode', {
          reset_code: modalFor,
        })
    else if (modalFor === 'orderReturnPageCodePreview')
      priAction = () =>
        this.handleBackendRequests('POST', 'previewChanges', {
          type: 'orderReturnPageCode',
        })
    else if (modalFor === 'orderReturnResponseCodePreview')
      priAction = () =>
        this.handleBackendRequests('POST', 'previewChanges', {
          type: 'orderReturnResponseCode',
        })
    else if (modalFor === 'returnCenterPageCodePreview')
      priAction = () =>
        this.handleBackendRequests('POST', 'previewChanges', {
          type: 'returnCenterPageCode',
        })

    this.setState({
      confirmModalTitle: t_b.title,
      modalConfirm: true,
      confirmModalPriAction: priAction,
      confirmModalContent: t_b.content,
    })
  }

  confirmCheckboxAction = (val) => {
    const content = val ? (
      <TextContainer>
        <TextStyle variation="">
          Are you sure you want to show return links to your customers at order
          listing section. Before enabling, Please check that following{' '}
          <TextStyle variation="code">Return Page</TextStyle>,{' '}
          <TextStyle variation="code">Return Page Template</TextStyle>,{' '}
          <TextStyle variation="code">Style Snippet</TextStyle> and
          <TextStyle variation="code">Script Tag</TextStyle> should be created.
          Otherwise things will not work correctly.
        </TextStyle>
      </TextContainer>
    ) : (
      <TextContainer>
        <TextStyle>
          Are you sure you want to hide return links.{' '}
          <TextStyle variation="strong">
            After this action your customer will not allowed to create new
            returns or check previous returns.
          </TextStyle>
        </TextStyle>
      </TextContainer>
    )

    this.setState({
      confirmModalTitle: (val ? 'Show' : 'Hide') + ' return links',
      modalConfirm: true,
      confirmModalPriAction: () => {
        this.handleStateChange({ showReturnLinks: val })
        this.handleBackendRequests('POST', 'toggleReturnLinks', {
          type: 'toggleReturnLinks',
          show_return_links: val,
        })
      },
      confirmModalContent: content,
    })
  }

  render() {
    if (this.state.processing) {
      return (
        <ReturnPageConfigurePageSkeleton title="Return Page Configuration" />
      )
    }

    const {
      modalLoading,
      modalVariable,
      variablesFor,
      previewModalLoading,
      src,
      confirmModalTitle,
      modalConfirm,
      modalConfirmLoading,
      confirmModalContent,
      confirmModalPriAction,
      showReturnLinks,
      isReturnPageCreated,
      isReturnPageTemplateCreated,
      isStyleSnippetCreated,
      isScriptTagCreated,
      orderReturnPageCode,
      orderReturnResponseCode,
      returnCenterPageCode,
      storePasswordEnabled,
      isReturnCenterPageCreated,
      isReturnCenterPageTemplateCreated,
    } = this.state

    return (
      <Page
        fullWidth
        title="Return Page Configuration"
        subtitle="From here you can setup your return page for customer. If something is not configured then you can manually do it from here."
        thumbnail={
          <Thumbnail source={OnlineStoreMajor} size="small" alt="Return Icon" />
        }
        secondaryActions={[
          {
            content: 'Page Tour',
            onAction: () => {
              this.handleStateChange({ isTourOpen: true })
            },
          },
        ]}
      >
        <ConfirmationModal
          title={confirmModalTitle}
          openModal={modalConfirm}
          handleClose={this.handleStateChange}
          modalLoading={modalConfirmLoading}
          content={confirmModalContent}
          primaryAction={confirmModalPriAction}
        />
        <VariableModal
          openModal={modalVariable}
          closeModal={this.handleStateChange}
          variablesFor={variablesFor}
        />
        <LoadingModal modalLoading={modalLoading} />
        <PreviewModal
          openModal={previewModalLoading}
          src={src}
          closeModal={this.handleStateChange}
        />
        <PageTour
          isTourOpen={this.state.isTourOpen}
          onRequestClose={this.handleStateChange}
          pageName="FrontConfigs"
        />

        <Layout>
          <Layout.Section>
            <Card title="Return Page Link">
              <Card.Section>
                <FormLayout>
                  <TextContainer>
                    <TextStyle variation="subdued">
                      Show return order page link in order listing on account
                      page.
                    </TextStyle>
                    <TextStyle variation="negative">
                      You have to manually make below changes in your store
                      theme.
                    </TextStyle>
                  </TextContainer>

                  <div className="liquid-code-paste">
                    <ThCodePaste toast={this.props.setNoticeToast} />
                    <br />
                    <TdCodePaste toast={this.props.setNoticeToast} />
                  </div>

                  <ScreenshotPreviewSection
                    handleClick={this.handleStateChange}
                  />
                  <VideoHelpSection handleClick={RedirectDispatch} />

                  <div className="enable-storefront">
                    <NoteText
                      content="If you add the above code, it will not visible to your
                        storefront unless you checked below option."
                    />
                    <br />
                    <Checkbox
                      label="Check this option for show return link in your storefront."
                      checked={showReturnLinks}
                      onChange={(val) => this.confirmCheckboxAction(val)}
                    />
                  </div>

                  <ThemeUpdateAlertBanner />
                </FormLayout>
              </Card.Section>
            </Card>
          </Layout.Section>
          <Layout.Section secondary>
            <ReturnPageCard
              modal={this.openConfirmationModal}
              isCreated={isReturnPageCreated}
            />
            <ReturnPageTemplateCard
              modal={this.openConfirmationModal}
              isCreated={isReturnPageTemplateCreated}
            />
            <StyleSnippetCard
              modal={this.openConfirmationModal}
              isCreated={isStyleSnippetCreated}
            />
            <ScriptTagCard
              modal={this.openConfirmationModal}
              isCreated={isScriptTagCreated}
            />
          </Layout.Section>
        </Layout>
        <div className="mb-25"></div>

        <Layout>
          <Layout.Section>
            <OrderReturnPageCodeCard
              handlePreview={this.handleBackendRequests}
              handleConfirmationModal={this.openConfirmationModal}
              orderReturnPageCode={orderReturnPageCode}
              handleChange={this.handleStateChange}
              storePasswordEnabled={storePasswordEnabled}
            />
          </Layout.Section>

          <Layout.Section secondary>
            <VariableCard handleAction={this.handleStateChange} />
          </Layout.Section>
        </Layout>
        <div className="mb-25"></div>

        <Layout>
          <Layout.Section>
            <OrderReturnResponseCard
              handlePreview={this.handleBackendRequests}
              handleConfirmationModal={this.openConfirmationModal}
              orderReturnResponseCode={orderReturnResponseCode}
              handleChange={this.handleStateChange}
              storePasswordEnabled={storePasswordEnabled}
            />
          </Layout.Section>
          <Layout.Section secondary>
            <OrderReturnResponseVariableCard
              handleAction={this.handleStateChange}
            />
          </Layout.Section>
        </Layout>
        <div className="mb-25"></div>

        <Layout>
          <Layout.Section>
            <ReturnCenterPageCodeCard
              handlePreview={this.handleBackendRequests}
              handleConfirmationModal={this.openConfirmationModal}
              returnCenterPageCode={returnCenterPageCode}
              handleChange={this.handleStateChange}
              storePasswordEnabled={storePasswordEnabled}
            />
          </Layout.Section>

          <Layout.Section secondary>
            <ReturnCenterPageCard
              modal={this.openConfirmationModal}
              isCreated={isReturnCenterPageCreated}
            />
            <ReturnCenterPageTemplateCard
              modal={this.openConfirmationModal}
              isCreated={isReturnCenterPageTemplateCreated}
            />
          </Layout.Section>
        </Layout>
        <div className="mb-25"></div>

        <PageActionFooter handlePriAction={this.handleBackendRequests} />
      </Page>
    )
  }
}

const PageActionFooter = ({ handlePriAction }) => {
  return (
    <PageActions
      primaryAction={{
        content: 'Save changes',
        onAction: () => {
          handlePriAction('POST', 'onSaveChanges', {})
        },
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            window.location.reload()
          },
        },
      ]}
    />
  )
}
