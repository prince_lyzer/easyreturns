import React, { useState } from 'react'
import {
  Card,
  FormLayout,
  TextField,
  TextContainer,
  TextStyle,
  DescriptionList,
  Button,
  Thumbnail,
  Link,
  Tooltip,
  Banner,
} from '@shopify/polaris'
import { CopyCode, NoteText } from '../../common/Common'
import { HintMajor } from '@shopify/polaris-icons'
import { HOST_URL } from '../../env'
import customer_account_liquid_page from '../screenshots/customer_account_page.jpg'
import customer_storefront_page from '../screenshots/customer_storefront_page.png'

export const ThCodePaste = ({ toast }) => {
  return (
    <TextField
      label={
        <>
          Copy and Paste below &lt;th&gt; tag at the end of &lt;thead&gt;
          element in&nbsp;
          <TextStyle variation="code"> customers/account.liquid </TextStyle>
          &nbsp;template.
        </>
      }
      value={`<th scope="col" class="frfi-return-order-th" style="display:none;">Return Order</th>`}
      readOnly={true}
      id="order_listing_th"
      helpText="Preview below screenshot for getting help in pasting this code in your theme code."
      connectedRight={
        <Button onClick={() => CopyCode('order_listing_th', toast)}>
          Copy
        </Button>
      }
    />
  )
}

export const TdCodePaste = ({ toast }) => {
  return (
    <TextField
      label={
        <>
          Copy and Paste below &lt;td&gt; tag at the end of &lt;tbody&gt;
          element in&nbsp;
          <TextStyle variation="code"> customers/account.liquid </TextStyle>
          &nbsp;template.
        </>
      }
      value={`<td data-label="Return Order" class="frfi-return-order" data-id="{{ order.id }}" style="display:none;"></td>`}
      readOnly={true}
      id="order_listing_td"
      helpText="Preview below screenshot for getting help in pasting this code in your theme code."
      connectedRight={
        <Button onClick={() => CopyCode('order_listing_td', toast)}>
          Copy
        </Button>
      }
    />
  )
}

export const VariableCard = ({ handleAction }) => {
  return (
    <Card
      title="Variables"
      secondaryFooterActions={[
        {
          content: 'Get all variables',
          size: 'slim',
          onAction: () =>
            handleAction({
              modalVariable: true,
              variablesFor: 'return_page_variables',
            }),
        },
      ]}
      footerActionAlignment="left"
    >
      <Card.Section>
        <TextContainer spacing="loose">
          <TextStyle>
            You can use given variables to customize return page content.
          </TextStyle>
        </TextContainer>
        <div className="card_sec_desc_list">
          <DescriptionList
            spacing="tight"
            items={[
              {
                term: 'order_name',
                description: 'Return Order Name.',
              },
              {
                term: 'return_request_id',
                description: 'Return Request Id.',
              },
            ]}
          />
        </div>
      </Card.Section>
    </Card>
  )
}

export const OrderReturnPageCodeCard = ({
  handlePreview,
  handleConfirmationModal,
  orderReturnPageCode,
  handleChange,
  storePasswordEnabled,
}) => {
  const [id, setId] = useState('code-static')
  const [disabled, setDisabled] = useState(true)

  return (
    <div className="edit-customer-return-page-code">
      <Card
        sectioned
        title="Edit Return Order Page Code"
        actions={[
          {
            content: 'Preview changes',
            onAction: () => {
              storePasswordEnabled
                ? handleConfirmationModal('orderReturnPageCodePreview')
                : handlePreview('POST', 'previewChanges', {
                    type: 'orderReturnPageCode',
                  })
            },
          },
          {
            content: 'Reset code',
            destructive: true,
            onAction: () => {
              handleConfirmationModal('orderReturnPageCode')
            },
          },
          {
            content: 'Edit',
            onAction: () => {
              setId('code-edit')
              setDisabled(false)
            },
            disabled: !disabled,
          },
        ]}
      >
        <FormLayout>
          <NoteText
            content="Preview the changes you made before save them, otherwise changes
              will be reflected on your customer return order page."
          />
          <TextField
            label=""
            value={orderReturnPageCode}
            spellCheck={false}
            onChange={(val) => handleChange({ orderReturnPageCode: val })}
            multiline={10}
            id={id}
            disabled={disabled}
          />
        </FormLayout>
      </Card>
    </div>
  )
}

export const OrderReturnResponseCard = ({
  handlePreview,
  handleConfirmationModal,
  orderReturnResponseCode,
  handleChange,
  storePasswordEnabled,
}) => {
  const [id, setId] = useState('code-static')
  const [disabled, setDisabled] = useState(true)

  return (
    <div className="edit-customer-response-code">
      <Card
        sectioned
        title="Edit Return Order Response Page Code"
        actions={[
          {
            content: 'Preview changes',
            onAction: () => {
              storePasswordEnabled
                ? handleConfirmationModal('orderReturnResponseCodePreview')
                : handlePreview('POST', 'previewChanges', {
                    type: 'orderReturnResponseCode',
                  })
            },
          },
          {
            content: 'Reset code',
            destructive: true,
            onAction: () => {
              handleConfirmationModal('orderReturnResponseCode')
            },
          },
          {
            content: 'Edit',
            onAction: () => {
              setId('code-edit')
              setDisabled(false)
            },
            disabled: !disabled,
          },
        ]}
      >
        <FormLayout>
          <NoteText
            content="Preview the changes you made before save them, otherwise changes
            will be reflected on your customer return order response page."
          />
          <TextField
            label=""
            value={orderReturnResponseCode}
            spellCheck={false}
            onChange={(val) => handleChange({ orderReturnResponseCode: val })}
            multiline={10}
            id={id}
            disabled={disabled}
          />
        </FormLayout>
      </Card>
    </div>
  )
}

export const OrderReturnResponseVariableCard = ({ handleAction }) => {
  return (
    <Card
      title="Variables"
      sectioned
      secondaryFooterActions={[
        {
          content: 'Get all variables',
          size: 'slim',
          onAction: () =>
            handleAction({
              modalVariable: true,
              variablesFor: 'return_response_page_variables',
            }),
        },
      ]}
      footerActionAlignment="left"
    >
      <TextContainer spacing="loose">
        <p>
          You can use given variables to customize return page response content.
        </p>
      </TextContainer>
      <div className="card_sec_desc_list">
        <DescriptionList
          spacing="tight"
          items={[
            {
              term: 'order_details',
              description: 'Order details.',
            },
            {
              term: 'request_id',
              description: 'Return request Id.',
            },
          ]}
        />
      </div>
    </Card>
  )
}

export const VideoHelpSection = ({ handleClick }) => {
  return (
    <div className="liquid-help">
      <Banner status="info" onDismiss={() => {}} icon={HintMajor}>
        <TextContainer>
          <div className="d-flex align-items-center">
            <TextStyle>
              Step by step guide in how to paste above code in theme file.
              <Button
                plain
                destructive
                onClick={() =>
                  handleClick(`${HOST_URL}liquid-code-paste-guide.mp4`, 2, true)
                }
              >
                &nbsp; Watch this video
              </Button>
            </TextStyle>
          </div>
        </TextContainer>
      </Banner>
    </div>
  )
}

export const ThemeUpdateAlertBanner = () => {
  return (
    <div className="theme-update-alert">
      <Banner title="Theme Update Alert" status="info">
        <TextContainer>
          <TextStyle>
            If you changed your store theme, then you will have to paste this
            code again in changed theme.
          </TextStyle>
        </TextContainer>
      </Banner>
    </div>
  )
}

export const ScreenshotPreviewSection = ({ handleClick }) => {
  return (
    <div className="d-flex">
      <div style={{ paddingRight: '10px' }} key="Customer account liquid page">
        <Tooltip content="Get help on paste above code">
          <Link
            onClick={() =>
              handleClick({
                previewModalLoading: true,
                src: customer_account_liquid_page,
              })
            }
          >
            <Thumbnail
              source={`../${customer_account_liquid_page}`}
              alt="Customer Account Liquid Page Code Preview"
            />
          </Link>
        </Tooltip>
      </div>
      <div style={{ paddingRight: '10px' }} key="Customer account page">
        <Tooltip content="Check how it looks after paste code">
          <Link
            onClick={() =>
              handleClick({
                previewModalLoading: true,
                src: customer_storefront_page,
              })
            }
          >
            <Thumbnail
              source={`../${customer_storefront_page}`}
              alt="Customer Account Page Preview"
            />
          </Link>
        </Tooltip>
      </div>
    </div>
  )
}

export const ReturnCenterPageCodeCard = ({
  handlePreview,
  handleConfirmationModal,
  returnCenterPageCode,
  handleChange,
  storePasswordEnabled,
}) => {
  const [id, setId] = useState('code-static')
  const [disabled, setDisabled] = useState(true)

  return (
    <div className="edit-return-center-page-code">
      <Card
        sectioned
        title="Edit Return Center Page Code"
        actions={[
          {
            content: 'Preview changes',
            onAction: () => {
              storePasswordEnabled
                ? handleConfirmationModal('returnCenterPageCodePreview')
                : handlePreview('POST', 'previewChanges', {
                    type: 'returnCenterPageCode',
                  })
            },
          },
          {
            content: 'Reset code',
            destructive: true,
            onAction: () => {
              handleConfirmationModal('returnCenterPageCode')
            },
          },
          {
            content: 'Edit',
            onAction: () => {
              setId('code-edit')
              setDisabled(false)
            },
            disabled: !disabled,
          },
        ]}
      >
        <FormLayout>
          <NoteText
            content="Preview the changes you made before save them, otherwise changes
              will be reflected on order return center page."
          />
          <TextField
            label=""
            value={returnCenterPageCode}
            spellCheck={false}
            onChange={(val) => handleChange({ returnCenterPageCode: val })}
            multiline={10}
            id={id}
            disabled={disabled}
          />
        </FormLayout>
      </Card>
    </div>
  )
}
