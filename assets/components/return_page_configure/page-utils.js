import React, { useState, useCallback } from 'react'
import {
  Card,
  TextContainer,
  TextStyle,
  Heading,
  Tooltip,
  Icon,
  Popover,
  Button,
  ActionList,
  Stack,
} from '@shopify/polaris'
import {
  DeleteMinor,
  CircleTickOutlineMinor,
  CircleCancelMajor,
  HorizontalDotsMinor,
  CodeMajor,
  ResetMinor,
} from '@shopify/polaris-icons'

export const StyleSnippetCard = ({ modal, isCreated }) => {
  const actions = [
    {
      content: 'Create snippet',
      icon: CodeMajor,
      onAction: () => {
        modal('create_style_snippet')
      },
    },
    {
      content: 'Reset snippet',
      icon: ResetMinor,
      onAction: () => {
        modal('reset_style_snippet')
      },
    },
    {
      content: 'Delete snippet',
      destructive: true,
      icon: DeleteMinor,
      onAction: () => {
        modal('delete_style_snippet')
      },
    },
  ]

  return (
    <Card
      title={
        <CardCustomHeader
          heading="Style Snippet"
          isCreated={isCreated}
          actions={actions}
        />
      }
      sectioned
    >
      <TextContainer>
        <TextStyle variation="subdued">
          <TextStyle variation="code">
            snippets/easy_returns.css.liquid
          </TextStyle>{' '}
          asset contains style(CSS) of order return page.
        </TextStyle>
      </TextContainer>
    </Card>
  )
}

export const ScriptTagCard = ({ modal, isCreated }) => {
  const actions = [
    {
      content: 'Create script tag',
      icon: CodeMajor,
      onAction: () => {
        modal('create_script_tag')
      },
    },
    {
      content: 'Delete script tag',
      destructive: true,
      icon: DeleteMinor,
      onAction: () => {
        modal('delete_script_tag')
      },
    },
  ]

  return (
    <Card
      title={
        <CardCustomHeader
          heading="Script Tag"
          isCreated={isCreated}
          actions={actions}
        />
      }
      sectioned
    >
      <TextContainer>
        <TextStyle variation="subdued">
          <TextStyle variation="code">easy_returns.js</TextStyle> script tag
          validate and process customers return request.
        </TextStyle>
      </TextContainer>
    </Card>
  )
}

export const ReturnPageCard = ({ modal, isCreated }) => {
  const actions = [
    {
      content: 'Create page',
      icon: CodeMajor,
      onAction: () => {
        modal('create_return_page')
      },
    },
    {
      content: 'Delete page',
      destructive: true,
      icon: DeleteMinor,
      onAction: () => {
        modal('delete_return_page')
      },
    },
  ]

  return (
    <Card
      title={
        <CardCustomHeader
          heading="Return Page"
          isCreated={isCreated}
          actions={actions}
        />
      }
      sectioned
    >
      <TextContainer>
        <TextStyle variation="subdued">
          <TextStyle variation="code">Easy Order Return</TextStyle> page is
          storefront order return page.
        </TextStyle>
        <br />
        <TextStyle>
          Page created in Online store/ Pages section of shopify admin.
        </TextStyle>
      </TextContainer>
    </Card>
  )
}

export const ReturnPageTemplateCard = ({ modal, isCreated }) => {
  const actions = [
    {
      content: 'Create template',
      icon: CodeMajor,
      onAction: () => {
        modal('create_template')
      },
    },
    {
      content: 'Reset template',
      icon: ResetMinor,
      onAction: () => {
        modal('reset_template')
      },
    },
    {
      content: 'Delete template',
      destructive: true,
      icon: DeleteMinor,
      onAction: () => {
        modal('delete_template')
      },
    },
  ]

  return (
    <Card
      title={
        <CardCustomHeader
          heading="Return Page Template"
          isCreated={isCreated}
          actions={actions}
        />
      }
      sectioned
    >
      <TextContainer>
        <TextStyle variation="subdued">
          <TextStyle variation="code">
            templates/page.easy_returns.liquid
          </TextStyle>{' '}
          asset contains return page code.
        </TextStyle>
      </TextContainer>
    </Card>
  )
}

export const ReturnCenterPageCard = ({ modal, isCreated }) => {
  const actions = [
    {
      content: 'Create page',
      icon: CodeMajor,
      onAction: () => {
        modal('create_return_center_page')
      },
    },
    {
      content: 'Delete page',
      destructive: true,
      icon: DeleteMinor,
      onAction: () => {
        modal('delete_return_center_page')
      },
    },
  ]

  return (
    <Card
      title={
        <CardCustomHeader
          heading="Return Center Page"
          isCreated={isCreated}
          actions={actions}
        />
      }
      sectioned
    >
      <TextContainer>
        <TextStyle variation="subdued">
          <TextStyle variation="code">Easy Return Center</TextStyle> page is
          storefront order return center page.
        </TextStyle>
        <br />
        <TextStyle>
          Page created in Online store/ Pages section of shopify admin.
        </TextStyle>
      </TextContainer>
    </Card>
  )
}

export const ReturnCenterPageTemplateCard = ({ modal, isCreated }) => {
  const actions = [
    {
      content: 'Create template',
      icon: CodeMajor,
      onAction: () => {
        modal('create_return_center_page_template')
      },
    },
    {
      content: 'Reset template',
      icon: ResetMinor,
      onAction: () => {
        modal('reset_return_center_page_template')
      },
    },
    {
      content: 'Delete template',
      destructive: true,
      icon: DeleteMinor,
      onAction: () => {
        modal('delete_return_center_page_template')
      },
    },
  ]

  return (
    <Card
      title={
        <CardCustomHeader
          heading="Return Center Page Liquid Template"
          isCreated={isCreated}
          actions={actions}
        />
      }
      sectioned
    >
      <TextContainer>
        <TextStyle variation="subdued">
          <TextStyle variation="code">
            templates/page.easy_return_center.liquid
          </TextStyle>{' '}
          asset contains return center page code.
        </TextStyle>
      </TextContainer>
    </Card>
  )
}

const CardTooltip = ({ isCreated }) => {
  if (isCreated) {
    return (
      <Tooltip content="Created">
        <Icon source={CircleTickOutlineMinor} backdrop={true} color="success" />
      </Tooltip>
    )
  }

  return (
    <Tooltip content="Not Created">
      <Icon source={CircleCancelMajor} backdrop={true} color="subdued" />
    </Tooltip>
  )
}

const CardCustomHeader = ({ heading, isCreated, actions }) => {
  const [popoverActive, setPopoverActive] = useState(false)

  const togglePopoverActive = useCallback(
    () => setPopoverActive((popoverActive) => !popoverActive),
    [],
  )

  return (
    <Stack alignment="center" spacing="extraTight">
      <Stack.Item>
        <CardTooltip isCreated={isCreated} />
      </Stack.Item>
      <Stack.Item fill>
        <Heading>{heading}</Heading>
      </Stack.Item>
      <Stack.Item>
        <Popover
          active={popoverActive}
          activator={
            <Button
              plain
              icon={HorizontalDotsMinor}
              onClick={togglePopoverActive}
            ></Button>
          }
          onClose={togglePopoverActive}
        >
          <ActionList items={actions} />
        </Popover>
      </Stack.Item>
    </Stack>
  )
}
