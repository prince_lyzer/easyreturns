import React, { useEffect, useState } from 'react'
import moment from 'moment'
import {
  Modal,
  Spinner,
  TextContainer,
  TextStyle,
  Icon,
} from '@shopify/polaris'
import { Redirect } from '@shopify/app-bridge/actions'
import Cookies from 'js-cookie'
import { easy_path } from '../env'
import {
  ArrowLeftMinor,
  ArrowRightMinor,
  ExternalMinor,
} from '@shopify/polaris-icons'

export function appendToUrl(path, obj) {
  const app = appBridge

  const params = new URLSearchParams(obj)
  // console.log(path.pathname, path.search)
  if (path.search === '')
    RedirectDispatch(path.pathname + '?' + params.toString())
  else RedirectDispatch(path.pathname + path.search + '&' + params.toString())
}

export function RedirectDispatch(url, redirect_to = 0, newContext = false) {
  const app = appBridge
  const redirect = Redirect.create(app)

  if (redirect_to === 0) {
    redirect.dispatch(Redirect.Action.APP, url)
  } else if (redirect_to === 1) {
    redirect.dispatch(Redirect.Action.ADMIN_PATH, {
      path: url,
      newContext: newContext,
    })
  } else if (redirect_to === 2) {
    redirect.dispatch(Redirect.Action.REMOTE, {
      url: url,
      newContext: newContext,
    })
  } else {
    return
  }
}

export function Divider() {
  return <div className="divider"></div>
}

export function AjaxErrorHandle(err, setErrorToast, reload = false, history) {
  // console.log(err) // Something Went Wrong (Sent from backend)
  // console.log(err.request) // Something Went Wrong (Sent from backend)
  //console.log(err.response.data); // Something Went Wrong (Sent from backend)
  //console.log(err.response.status); // 403
  //console.log(err.response.headers); // response headers
  //console.log(err.toJSON()); // err json object
  try {
    if (err.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      const backendError = err.response.data
      let errMsg = ''

      if (backendError) {
        if (typeof backendError.error !== 'undefined') {
          errMsg = backendError.error
        } else {
          errMsg = backendError.title
        }
      } else if (err.response.status === 500) {
        errMsg = err.message
      }

      if (errMsg !== '') setErrorToast(errMsg)

      if (reload === 'email-configs' && err.response.status === 404) {
        setTimeout(() => {
          history.replace(`${easy_path}/email-configs`)
        }, 1000)
      } else if (
        reload === 'return-order-desc' &&
        err.response.status === 500
      ) {
        setTimeout(() => {
          history.replace(`${easy_path}/returns`)
        }, 1000)
      }
    } else {
      // Something happened in setting up the request that triggered an Error
      // console.log("Error", error.message);
      setErrorToast(err.message)
    }

    // else if (err.request) {
    //   // The request was made but no response was received
    //   // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    //   // console.log("Error request", error.request);
    // }
  } catch (e) {
    // console.log(e, e.message, e.name);
  }
}

export function DateFormat(date, format = 'MMM DD YYYY HH:mm') {
  const utcDate = moment(date).format('MMM DD YYYY HH:mm')
  return moment.utc(utcDate).local().format(format)
}

export function MomentFormat(date, format = 'MMM DD YYYY HH:mm') {
  return moment(date).format(format)
}

export function CopyCode(id, setNoticeToast) {
  var copyText = document.getElementById(id)

  /* Select the text field */
  copyText.select()
  // copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand('copy')

  setNoticeToast('Code copied')
}

export function LoadingModal({ modalLoading }) {
  return (
    <Modal
      open={modalLoading}
      onClose={() => {}}
      title="Loading..."
      loading={false}
      titleHidden
    >
      <Modal.Section>
        <div className="text-center">
          <Spinner accessibilityLabel="Spinner example" size="large" />
          <br /> Please wait while request is processing
        </div>
      </Modal.Section>
    </Modal>
  )
}

export function PreviewModal({
  openModal,
  src,
  closeModal,
  modalLoading = 'previewModalLoading',
  relative = true,
  title = 'Preview',
  handleImageCarousel = null,
  hasPreviousImage = false,
  hasNextImage = false,
  handleNewWindowAction = null,
  priActionLoading = false,
}) {
  let secondaryActions = null
  let primaryAction = null
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(false)
  }, [src])

  if (handleImageCarousel) {
    secondaryActions = [
      {
        content: <Icon source={ArrowLeftMinor} color="base" />,
        onAction: () => {
          setLoading(true)
          handleImageCarousel(src.id, -1)
        },
        disabled: !hasPreviousImage,
      },
      {
        content: <Icon source={ArrowRightMinor} color="base" />,
        onAction: () => {
          setLoading(true)
          handleImageCarousel(src.id, +1)
        },
        disabled: !hasNextImage,
      },
    ]

    primaryAction = {
      content: 'New window',
      onAction: () => {
        handleNewWindowAction(src.id)
      },
      icon: ExternalMinor,
      loading: priActionLoading,
    }
  }
  return (
    <Modal
      large
      open={openModal}
      onClose={() => {
        closeModal({ [modalLoading]: false })
      }}
      titleHidden={handleImageCarousel ? true : false}
      title={title}
      loading={loading}
      secondaryActions={secondaryActions}
      primaryAction={primaryAction}
      noScroll={true}
      limitHeight={true}
    >
      <Modal.Section>
        <img
          src={relative ? `../${src}` : src.src}
          width="100%"
          height="auto"
        />
      </Modal.Section>
    </Modal>
  )
}

export function moneyCurrency(money, moneyFormat) {
  if (moneyFormat === '') return money
  else return moneyFormat.replace(/{{.*}}/, money)
}

export function setCookie(name, value) {
  Cookies.set(name, value, { secure: true, sameSite: 'none' })
  return true
}

export function getCookie(name) {
  const value = Cookies.get(name)
  return value
}

export function ConfirmationModal({
  title,
  openModal,
  handleClose,
  modalLoading = false,
  bodyLoading = false,
  content,
  primaryAction,
  confirmBtnLabel = 'Confirm',
}) {
  confirmBtnLabel = title === 'Preview page' ? 'Preview page' : confirmBtnLabel
  return (
    <Modal
      open={openModal}
      onClose={() => {
        handleClose({ modalConfirm: false, modalConfirmLoading: false })
      }}
      title={`${title} ?`}
      loading={bodyLoading}
      primaryAction={{
        content: confirmBtnLabel,
        onAction: primaryAction,
        loading: modalLoading,
      }}
      secondaryActions={[
        {
          content: 'Cancel',
          disabled: modalLoading,
          onAction: () => {
            handleClose({ modalConfirm: false, modalConfirmLoading: false })
          },
        },
      ]}
    >
      <Modal.Section>
        <TextContainer>{content}</TextContainer>
      </Modal.Section>
    </Modal>
  )
}

export const PreviewHtmlModal = ({
  active,
  title,
  previewModalContent,
  closeModal,
  primaryAction = null,
  previewModalLoading = false,
}) => {
  return (
    <Modal
      large
      open={active}
      onClose={() => closeModal({ previewModalActive: false })}
      // loading={previewModalLoading}
      title={title}
      primaryAction={primaryAction}
      secondaryActions={[
        {
          content: 'Cancel',
          onAction: () => {
            closeModal({ previewModalActive: false })
          },
          disabled: previewModalLoading,
        },
      ]}
    >
      <Modal.Section>
        <iframe
          className="html-email-preview bordered"
          srcDoc={previewModalContent}
          style={{
            border: 'none',
            width: '100%',
          }}
          onLoad={(frame) =>
            (frame.target.height =
              frame.target.contentWindow.document.body.scrollHeight + 40 + 'px')
          }
        ></iframe>
      </Modal.Section>
    </Modal>
  )
}

export const NoteText = ({ content }) => {
  return (
    <TextContainer>
      <TextStyle variation="negative">Note: </TextStyle>
      <TextStyle variation="subdued">{content}</TextStyle>
    </TextContainer>
  )
}
