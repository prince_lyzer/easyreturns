import React from 'react'
import Tour from 'reactour'
import { TextContainer, TextStyle, Heading, Icon } from '@shopify/polaris'
import { ArrowLeftMinor, ArrowRightMinor } from '@shopify/polaris-icons'

export const PageTour = ({ isTourOpen, onRequestClose, pageName }) => {
  const steps = tourSteps(pageName)
  if (!steps) return
  const accentColor = '#5cb7b7'
  return (
    <Tour
      steps={steps}
      isOpen={isTourOpen}
      onRequestClose={() => onRequestClose({ isTourOpen: false })}
      className="helper"
      rounded={5}
      accentColor={accentColor}
      badgeContent={(curr, tot) => `${curr} of ${tot}`}
      lastStepNextButton={
        <span style={{ fontWeight: 'bold', color: 'green' }}>Done</span>
      }
      nextButton={
        <span style={{ fontWeight: 'bold', color: 'green' }}>
          Next <Icon source={ArrowRightMinor} />
        </span>
      }
      prevButton={
        <span style={{ fontWeight: 'bold' }}>
          <Icon source={ArrowLeftMinor} /> Prev
        </span>
      }
      showNavigation={false}
      startAt={0}
      onAfterOpen={disableBody}
      onBeforeClose={enableBody}
    />
  )
}

function tourSteps(pageName) {
  if (pageName === 'Home') return HomePageSteps
  else if (pageName === 'Returns') return ReturnListSteps
  else if (pageName === 'GeneralConfigs') return GeneralConfigSteps
  else if (pageName === 'EmailConfigs') return EmailConfigSteps
  else if (pageName === 'FrontConfigs') return ReturnPageConfigSteps
  else if (pageName === 'EditMailTemplates') return EditMailTemplateSteps
  else if (pageName === 'ReturnOrderDesc') return ReturnOrderDescSteps
  else return null
}

function HelperContent({ title = '', body }) {
  return (
    <TextContainer>
      {title !== '' && (
        <div style={{ marginBottom: '10px' }}>
          <Heading>{title}</Heading>
        </div>
      )}

      <TextStyle>{body}</TextStyle>
    </TextContainer>
  )
}

function disableBody() {
  document.getElementsByTagName('body')[0].style.overflow = 'hidden'
}

function enableBody() {
  document.getElementsByTagName('body')[0].style.overflow = 'auto'
}

const HomePageSteps = [
  {
    selector: '.Polaris-Page-Header__PrimaryActionWrapper',
    content: () => {
      return (
        <HelperContent
          title="Choose Time Limits"
          body="Choose a time slots in which you want to generate returns report and
            re-render graph."
        />
      )
    },
  },
  {
    selector: '.info-card',
    content: () => {
      return (
        <HelperContent
          title="Count Returns Pending"
          body="Count those returns in a time limit which needs your action."
        />
      )
    },
  },
  {
    selector: '.warning-card',
    content: () => {
      return (
        <HelperContent
          title="Count Returns Accepted"
          body="Count those returns in a time limit which are accepted by you."
        />
      )
    },
  },
  {
    selector: '.success-card',
    content: () => {
      return (
        <HelperContent
          title="Count Returns Completed"
          body="Count those returns in a time limit which are completed. Returns
          will be completed when amount is refunded to customer."
        />
      )
    },
  },
  {
    selector: '.danger-card',
    content: () => {
      return (
        <HelperContent
          title="Count Returns Cancelled"
          body="Count those returns in a time limit which cancelled by you."
        />
      )
    },
  },
  {
    selector: '.chart-container',
    content: () => {
      return (
        <HelperContent
          title="Chart View"
          body="Return reports created in a given time frame."
        />
      )
    },
  },
]

const GeneralConfigSteps = [
  {
    selector: '.return-acceptable-days',
    content: () => {
      return (
        <HelperContent body="Total days within which a customer can return order after order fulfillment." />
      )
    },
  },
  {
    selector: '.payment-options',
    content: () => {
      return (
        <HelperContent
          body={
            <>
              Possible payment options for refunding customer amount.{' '}
              <TextStyle variation="strong">
                Selected options will be visible to customer only for a COD
                order.
              </TextStyle>
            </>
          }
        />
      )
    },
  },
  {
    selector: '.return-reasons',
    content: () => {
      return (
        <HelperContent
          title="Return Reasons"
          body="Manage all your return reasons. Easily add new or delete them from here."
        />
      )
    },
  },
  {
    selector: '.order-return-methods-div',
    content: () => {
      return (
        <HelperContent
          title="Other Return Methods"
          body="Here you can select return methods, make notes required, and auto archive order on completion."
        />
      )
    },
  },
  {
    selector: '.customer-image-upload',
    content: () => {
      return (
        <HelperContent
          title="Customer Image Upload"
          body="Here you can allow customers to upload images at the time of submitting a return and make it compulsory."
        />
      )
    },
  },
  {
    selector: '.guest-returns',
    content: () => {
      return (
        <HelperContent
          title="Guest Returns"
          body="Here you can allow customers to return order items if customer checkout as a guest user."
        />
      )
    },
  },
]

const EmailConfigSteps = [
  {
    selector: '.customer-emails',
    content: () => {
      return (
        <HelperContent
          title="Customer Emails"
          body="These mails sends to customer, you can edit them from here."
        />
      )
    },
  },
  {
    selector: '.merchant-email',
    content: () => {
      return (
        <HelperContent
          title="Merchant Emails"
          body="These mails sends to you, you can edit them from here."
        />
      )
    },
  },
  {
    selector: '.email-styles',
    content: () => {
      return (
        <HelperContent body="From here you can edit style(CSS) of mail templates." />
      )
    },
  },
  {
    selector: '.email-header',
    content: () => {
      return (
        <HelperContent body="From here you can edit customer mail templates header." />
      )
    },
  },
  {
    selector: '.email-signature',
    content: () => {
      return (
        <HelperContent body="From here you can edit customer mail templates signature or footer." />
      )
    },
  },
  {
    selector: '.brand-logo',
    content: () => {
      return (
        <HelperContent
          title="Brand Logo"
          body="Here you can add brand logo which shows in mail templates."
        />
      )
    },
  },
  {
    selector: '.customer-notification-address',
    content: () => {
      return (
        <HelperContent
          title="Customer Notification Address"
          body="Here you can update reply-to email address of emails sent to customers."
        />
      )
    },
  },
]

const EditMailTemplateSteps = [
  {
    selector: '.mail-subject',
    content: () => {
      return (
        <HelperContent body="This is the subject of the mail. You can edit from here." />
      )
    },
  },
  {
    selector: '.mail-template',
    content: () => {
      return (
        <HelperContent body="This is the content body of mail. You can edit the mail body from here." />
      )
    },
  },
  {
    selector: '.mail-variables',
    content: () => {
      return (
        <HelperContent
          title="Mail Variables"
          body="You can use following variables inside the mail content. Preview the mail before save the changes."
        />
      )
    },
  },
  {
    selector: '.Polaris-ActionMenu',
    content: () => {
      return (
        <HelperContent
          title="Mail Options"
          body="You can preview the mail, send test mail to you and stop or start sending mail to customers."
        />
      )
    },
  },
]

const ReturnPageConfigSteps = [
  {
    selector: '.liquid-code-paste',
    content: () => {
      return (
        <HelperContent
          title="Return Links"
          body="Copy and paste these codes in your theme template file. After code paste, return links will not visible to customer until you checked the below checkbox."
        />
      )
    },
  },
  {
    selector: '.liquid-help',
    content: () => {
      return (
        <HelperContent
          title="Liquid Code Help"
          body="Watch this video for help in code pasting."
        />
      )
    },
  },
  {
    selector: '.enable-storefront',
    content: () => {
      return (
        <HelperContent
          title="Important!"
          body="Check this option for showing return links to customer after paste the above codes."
        />
      )
    },
  },
  {
    selector: '.edit-customer-return-page-code',
    content: () => {
      return (
        <HelperContent
          title="Return Page Code"
          body="Edit the content of your return page. Preview the code before save the changes."
        />
      )
    },
  },
  {
    selector: '.edit-customer-response-code',
    content: () => {
      return (
        <HelperContent
          title="Return page Response Code"
          body="Edit the content of response page. This page will be shown after customer submit the return request."
        />
      )
    },
  },
  {
    selector: '.edit-return-center-page-code',
    content: () => {
      return (
        <HelperContent
          title="Return Center Page Code"
          body="Edit the content of return center page. This page will be shown if customer checkout as a guest and want to return order items."
        />
      )
    },
  },
]

const ReturnListSteps = [
  {
    selector: '.Polaris-Filters',
    content: () => {
      return (
        <HelperContent
          title="Filter Options"
          body="From here you can easily search any return request, sort them or get returns by a specific status."
        />
      )
    },
  },
  {
    selector: '.return-list',
    content: () => {
      return (
        <HelperContent
          title="Returns Request"
          body="Here you can manage every returns request. Click on requestId for more detail."
        />
      )
    },
  },
  {
    selector: '.return-pagination',
    content: () => {
      return (
        <HelperContent body="Easily get others returns request by clicking on next button." />
      )
    },
  },
  {
    selector: '.Polaris-Page-Header__PrimaryActionWrapper',
    content: () => {
      return (
        <HelperContent
          title="Export Returns Report"
          body="Here you can get a return report so that you can analyze why your products are returning and what is the big cause for returns."
        />
      )
    },
  },
]

const ReturnOrderDescSteps = [
  {
    selector: '.returned-line-items',
    content: () => {
      return (
        <HelperContent
          title="Returned Line Items"
          body="Here you find all the line items which are requested for return by a customer."
        />
      )
    },
  },
  // {
  //   selector: '.Polaris-Card__Footer',
  //   content: () => {
  //     return (
  //       <HelperContent
  //         title="Return Action"
  //         body="From here you can accept or cancel the return request."
  //       />
  //     )
  //   },
  // },
  {
    selector: '.order-line-items',
    content: () => {
      return (
        <HelperContent
          title="Order Line Items"
          body="Other order line items which are not returned by a customer."
        />
      )
    },
  },
  {
    selector: '.order-details',
    content: () => {
      return (
        <HelperContent
          title="Order Details"
          body="From here you find brief order details. click on `view order` for return to shopify order page."
        />
      )
    },
  },
  {
    selector: '.customer-details',
    content: () => {
      return (
        <HelperContent
          title="Customer Details"
          body="From here you find brief customer details. click on `view customer` for return to shopify customer page."
        />
      )
    },
  },
  {
    selector: '.customer-queries',
    content: () => {
      return (
        <HelperContent
          title="Return Queries"
          body="From here you can ask queries related to return request from customer by sending mail to his/her email address."
        />
      )
    },
  },
  {
    selector: '.return-images',
    content: () => {
      return (
        <HelperContent
          title="Return Images"
          body="View all images uploaded by a customer when submitting a return request."
        />
      )
    },
  },
  {
    selector: '.Polaris-Page-Header__PaginationWrapper',
    content: () => {
      return (
        <HelperContent
          title="Prev/Next Request"
          body="From here you can go to next or previous return request."
        />
      )
    },
  },
]
