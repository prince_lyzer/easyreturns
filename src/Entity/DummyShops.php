<?php

namespace App\Entity;

use App\Repository\DummyShopsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DummyShopsRepository::class)
 */
class DummyShops
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $myshopifyDomain;

    /**
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $active;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateAdd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMyshopifyDomain(): ?string
    {
        return $this->myshopifyDomain;
    }

    public function setMyshopifyDomain(string $myshopifyDomain): self
    {
        $this->myshopifyDomain = $myshopifyDomain;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }
}
