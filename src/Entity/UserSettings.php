<?php

namespace App\Entity;

use App\Repository\UserSettingsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserSettingsRepository::class)
 */
class UserSettings
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", options={"comment": "Shopify Merchant Id"})
     */
    private $merchantId;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Shopify Merchant Contact Email"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Shopify Store Custom Domain"})
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Shopify Customer Contact Email"})
     */
    private $customerEmail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $timezone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ianaTimezone;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Shopify Merchant Full Name"})
     */
    private $shopOwner;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Shopify Store myshopify domain"})
     */
    private $myshopifyDomain;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moneyFormat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moneyWithCurrencyFormat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moneyInEmailFormat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $moneyWithCurrencyInEmailsFormat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $province;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provinceCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $countryCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $countryName;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="bigint")
     */
    private $primaryLocationId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpd;

    /**
     * @ORM\Column(type="smallint", options={"comment": "Status have 3 values, 0- Uninstalled, 1- Payment Panding, 2- Payment Approved"})
     */
    private $status;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"comment": "Script tag Id created by us on Merchant shopify store."})
     */
    private $IdScriptTag;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $AccessToken;

    /**
     * @ORM\Column(type="text", nullable=true, options={"comment": "Email template style css"})
     */
    private $emailStyle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $emailHeader;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brandLogo;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $emailSignature;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $returnPageId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment": "Email address used to send mail to customer"})
     */
    private $CustomerContactEmail;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $themeId;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isOnboarding = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPasswordEnabled;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerReplyEmail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }

    public function setMerchantId(string $merchantId): self
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getCustomerEmail(): ?string
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail(string $customerEmail): self
    {
        $this->customerEmail = $customerEmail;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getIanaTimezone(): ?string
    {
        return $this->ianaTimezone;
    }

    public function setIanaTimezone(string $ianaTimezone): self
    {
        $this->ianaTimezone = $ianaTimezone;

        return $this;
    }

    public function getShopOwner(): ?string
    {
        return $this->shopOwner;
    }

    public function setShopOwner(string $shopOwner): self
    {
        $this->shopOwner = $shopOwner;

        return $this;
    }

    public function getMyshopifyDomain(): ?string
    {
        return $this->myshopifyDomain;
    }

    public function setMyshopifyDomain(string $myshopifyDomain): self
    {
        $this->myshopifyDomain = $myshopifyDomain;

        return $this;
    }

    public function getMoneyFormat(): ?string
    {
        return $this->moneyFormat;
    }

    public function setMoneyFormat(?string $moneyFormat): self
    {
        $this->moneyFormat = $moneyFormat;

        return $this;
    }

    public function getMoneyWithCurrencyFormat(): ?string
    {
        return $this->moneyWithCurrencyFormat;
    }

    public function setMoneyWithCurrencyFormat(?string $moneyWithCurrencyFormat): self
    {
        $this->moneyWithCurrencyFormat = $moneyWithCurrencyFormat;

        return $this;
    }

    public function getMoneyInEmailFormat(): ?string
    {
        return $this->moneyInEmailFormat;
    }

    public function setMoneyInEmailFormat(?string $moneyInEmailFormat): self
    {
        $this->moneyInEmailFormat = $moneyInEmailFormat;

        return $this;
    }

    public function getMoneyWithCurrencyInEmailsFormat(): ?string
    {
        return $this->moneyWithCurrencyInEmailsFormat;
    }

    public function setMoneyWithCurrencyInEmailsFormat(?string $moneyWithCurrencyInEmailsFormat): self
    {
        $this->moneyWithCurrencyInEmailsFormat = $moneyWithCurrencyInEmailsFormat;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(?string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getProvince(): ?string
    {
        return $this->province;
    }

    public function setProvince(?string $province): self
    {
        $this->province = $province;

        return $this;
    }

    public function getProvinceCode(): ?string
    {
        return $this->provinceCode;
    }

    public function setProvinceCode(?string $provinceCode): self
    {
        $this->provinceCode = $provinceCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): self
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getCountryName(): ?string
    {
        return $this->countryName;
    }

    public function setCountryName(string $countryName): self
    {
        $this->countryName = $countryName;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPrimaryLocationId(): ?string
    {
        return $this->primaryLocationId;
    }

    public function setPrimaryLocationId(string $primaryLocationId): self
    {
        $this->primaryLocationId = $primaryLocationId;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->dateUpd;
    }

    public function setDateUpd(?\DateTimeInterface $dateUpd): self
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIdScriptTag(): ?int
    {
        return $this->IdScriptTag;
    }

    public function setIdScriptTag(?int $IdScriptTag): self
    {
        $this->IdScriptTag = $IdScriptTag;

        return $this;
    }

    public function getAccessToken(): ?string
    {
        return $this->AccessToken;
    }

    public function setAccessToken(string $AccessToken): self
    {
        $this->AccessToken = $AccessToken;

        return $this;
    }

    public function getEmailStyle(): ?string
    {
        return $this->emailStyle;
    }

    public function setEmailStyle(?string $emailStyle): self
    {
        $this->emailStyle = $emailStyle;

        return $this;
    }

    public function getEmailHeader(): ?string
    {
        return $this->emailHeader;
    }

    public function setEmailHeader(?string $emailHeader): self
    {
        $this->emailHeader = $emailHeader;

        return $this;
    }

    public function getBrandLogo(): ?string
    {
        return $this->brandLogo;
    }

    public function setBrandLogo(?string $brandLogo): self
    {
        $this->brandLogo = $brandLogo;

        return $this;
    }

    public function getEmailSignature(): ?string
    {
        return $this->emailSignature;
    }

    public function setEmailSignature(?string $emailSignature): self
    {
        $this->emailSignature = $emailSignature;

        return $this;
    }

    public function getReturnPageId(): ?string
    {
        return $this->returnPageId;
    }

    public function setReturnPageId(?string $returnPageId): self
    {
        $this->returnPageId = $returnPageId;

        return $this;
    }

    public function getCustomerContactEmail(): ?string
    {
        return $this->CustomerContactEmail;
    }

    public function setCustomerContactEmail(?string $CustomerContactEmail): self
    {
        $this->CustomerContactEmail = $CustomerContactEmail;

        return $this;
    }

    public function getThemeId(): ?string
    {
        return $this->themeId;
    }

    public function setThemeId(?string $themeId): self
    {
        $this->themeId = $themeId;

        return $this;
    }

    public function getIsOnboarding(): ?bool
    {
        return $this->isOnboarding;
    }

    public function setIsOnboarding(bool $isOnboarding): self
    {
        $this->isOnboarding = $isOnboarding;

        return $this;
    }

    public function getIsPasswordEnabled(): ?bool
    {
        return $this->isPasswordEnabled;
    }

    public function setIsPasswordEnabled(?bool $isPasswordEnabled): self
    {
        $this->isPasswordEnabled = $isPasswordEnabled;

        return $this;
    }

    public function getCustomerReplyEmail(): ?string
    {
        return $this->customerReplyEmail;
    }

    public function setCustomerReplyEmail(?string $customerReplyEmail): self
    {
        $this->customerReplyEmail = $customerReplyEmail;

        return $this;
    }
}
