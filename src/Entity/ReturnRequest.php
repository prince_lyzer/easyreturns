<?php

namespace App\Entity;

use App\Repository\ReturnRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReturnRequestRepository::class)
 */
class ReturnRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", options={"comment":"Shopify Order Id"})
     */
    private $spOrderId;

    /**
     * @ORM\Column(type="integer", options={"comment":"Order Details Id"})
     */
    private $orderId;

    /**
     * @ORM\Column(type="integer", options={"comment":"Customer Table Id"})
     */
    private $customerId;

    /**
     * @ORM\Column(type="integer", options={"default": 1})
     */
    private $returnStatus;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpd;

    /**
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment": "Refund amount payment details in case of Cash On Delivery Order"})
     */
    private $paymentDetails;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $canceledReason;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerNote;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attachmentsId;

    /**
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $returnType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notificationEmail;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default": 0, "comment": "0- Website, 1- App"})
     */
    private $medium = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpOrderId(): ?int
    {
        return $this->spOrderId;
    }

    public function setSpOrderId(int $spOrderId): self
    {
        $this->spOrderId = $spOrderId;

        return $this;
    }

    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getCustomerId(): ?int
    {
        return $this->customerId;
    }

    public function setCustomerId(int $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function getReturnStatus(): ?int
    {
        return $this->returnStatus;
    }

    public function setReturnStatus(?int $returnStatus): self
    {
        $this->returnStatus = $returnStatus;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->dateUpd;
    }

    public function setDateUpd(?\DateTimeInterface $dateUpd): self
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getPaymentDetails(): ?string
    {
        return $this->paymentDetails;
    }

    public function setPaymentDetails(?string $paymentDetails): self
    {
        $this->paymentDetails = $paymentDetails;

        return $this;
    }

    public function getCanceledReason(): ?string
    {
        return $this->canceledReason;
    }

    public function setCanceledReason(?string $canceledReason): self
    {
        $this->canceledReason = $canceledReason;

        return $this;
    }

    public function getCustomerNote(): ?string
    {
        return $this->customerNote;
    }

    public function setCustomerNote(?string $customerNote): self
    {
        $this->customerNote = $customerNote;

        return $this;
    }

    public function getAttachmentsId(): ?string
    {
        return $this->attachmentsId;
    }

    public function setAttachmentsId(?string $attachmentsId): self
    {
        $this->attachmentsId = $attachmentsId;

        return $this;
    }

    public function getReturnType(): ?int
    {
        return $this->returnType;
    }

    public function setReturnType(int $returnType): self
    {
        $this->returnType = $returnType;

        return $this;
    }

    public function getNotificationEmail(): ?string
    {
        return $this->notificationEmail;
    }

    public function setNotificationEmail(?string $notificationEmail): self
    {
        $this->notificationEmail = $notificationEmail;

        return $this;
    }

    public function getMedium(): ?int
    {
        return $this->medium;
    }

    public function setMedium(?int $medium): self
    {
        $this->medium = $medium;

        return $this;
    }
}
