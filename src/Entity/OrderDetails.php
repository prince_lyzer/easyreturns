<?php

namespace App\Entity;

use App\Repository\OrderDetailsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderDetailsRepository::class)
 */
class OrderDetails
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private $orderId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $orderName;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Order Fulfillment Status"})
     */
    private $fulfillmentStatus;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Order Payment Status"})
     */
    private $paymentStatus;

    /**
     * @ORM\Column(type="integer", options={"comment": "Customer Details Id"})
     */
    private $customerId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $orderCreatedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpd;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"comment": "Is a Cash On Delivery Order"})
     */
    private $isCod;

    /**
     * @ORM\Column(type="float", nullable=true, options={"comment": "Total line item value"})
     */
    private $orderTotal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $shippingAddress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getOrderName(): ?string
    {
        return $this->orderName;
    }

    public function setOrderName(string $orderName): self
    {
        $this->orderName = $orderName;

        return $this;
    }

    public function getFulfillmentStatus(): ?string
    {
        return $this->fulfillmentStatus;
    }

    public function setFulfillmentStatus(string $fulfillmentStatus): self
    {
        $this->fulfillmentStatus = $fulfillmentStatus;

        return $this;
    }

    public function getPaymentStatus(): ?string
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(string $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getCustomerId(): ?int
    {
        return $this->customerId;
    }

    public function setCustomerId(int $customerId): self
    {
        $this->customerId = $customerId;

        return $this;
    }

    public function getOrderCreatedAt(): ?\DateTimeInterface
    {
        return $this->orderCreatedAt;
    }

    public function setOrderCreatedAt(\DateTimeInterface $orderCreatedAt): self
    {
        $this->orderCreatedAt = $orderCreatedAt;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->dateUpd;
    }

    public function setDateUpd(\DateTimeInterface $dateUpd): self
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    public function getIsCod(): ?bool
    {
        return $this->isCod;
    }

    public function setIsCod(bool $isCod): self
    {
        $this->isCod = $isCod;

        return $this;
    }

    public function getOrderTotal(): ?float
    {
        return $this->orderTotal;
    }

    public function setOrderTotal(?float $orderTotal): self
    {
        $this->orderTotal = $orderTotal;

        return $this;
    }

    public function getShippingAddress(): ?string
    {
        return $this->shippingAddress;
    }

    public function setShippingAddress(?string $shippingAddress): self
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }
}
