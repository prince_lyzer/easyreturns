<?php

namespace App\Entity;

use App\Repository\UserConfigurationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserConfigurationsRepository::class)
 */
class UserConfigurations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", options={"comment": "User_settings table user id."})
     */
    private $userId;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"comment": "Return window days limit."})
     */
    private $returnAcceptableDays;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"comment": "Refund amount in case of COD order."})
     */
    private $returnPaymentOptions;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpd;

    /**
     * @ORM\Column(type="smallint", options={"default": 0, "comment": "Return Windows Starts From, 0=> Item_fulfillment_date, 1=> Order_create_date"}, nullable=true)
     */
    private $returnWindowStartsFrom;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $returnLabels;

    /**
     * @ORM\Column(type="text", nullable=true, options={"comment": "Return order template page code"})
     */
    private $returnOrderPageCode;

    /**
     * @ORM\Column(type="text", nullable=true, options={"comment": "Return order request status code, after request submit"})
     */
    private $returnOrderResponseCode;

    /**
     * @ORM\Column(type="smallint", options={"default": 0, "comment": "Auto archive order on completion"}, nullable=true)
     */
    private $autoArchive;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default": 0, "comment": "Add a customer note text in return order"}, nullable=true)
     */
    private $customerNote = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default": "Easy Order Return", "comment": "customeor return order page title"})
     */
    private $returnPageTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default": "easy_order_return", "comment": "customeor return order page handle"})
     */
    private $returnPageHandle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $previewCode;

    /**
     * @ORM\Column(type="smallint", options={"default": 0, "comment": "show return links on customer account page, controlled from return_page_configuration."})
     */
    private $showReturnLinks = 0;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isPageTemplateCreated = 0;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isStyleSnippetCreated = 0;

    /**
     * @ORM\Column(type="smallint", options={"default" : 1})
     */
    private $requiredReturnNote = 1;

    /**
     * @ORM\Column(type="smallint", options={"default" : 1})
     */
    private $exchangeNote = 1;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $requiredExchangeNote = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default" : "1"})
     */
    private $returnMethods = "1";

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     */
    private $imageUpload = 0;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     */
    private $requiredImageUpload = 0;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default" : 0})
     */
    private $guestCheckout = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $returnCenterPageCode;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $returnCenterPageId;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default": 0})
     */
    private $isReturnCenterPageTemplateCreated = 0;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $autoApproveRefundReturns;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $autoApproveExchangeReturns;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getReturnAcceptableDays(): ?int
    {
        return $this->returnAcceptableDays;
    }

    public function setReturnAcceptableDays(?int $returnAcceptableDays): self
    {
        $this->returnAcceptableDays = $returnAcceptableDays;

        return $this;
    }

    public function getReturnPaymentOptions(): ?string
    {
        return $this->returnPaymentOptions;
    }

    public function setReturnPaymentOptions(?string $returnPaymentOptions): self
    {
        $this->returnPaymentOptions = $returnPaymentOptions;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->dateUpd;
    }

    public function setDateUpd(?\DateTimeInterface $dateUpd): self
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    public function getReturnWindowStartsFrom(): ?int
    {
        return $this->returnWindowStartsFrom;
    }

    public function setReturnWindowStartsFrom(
        ?int $returnWindowStartsFrom
    ): self {
        $this->returnWindowStartsFrom = $returnWindowStartsFrom;

        return $this;
    }

    public function getReturnLabels(): ?string
    {
        return $this->returnLabels;
    }

    public function setReturnLabels(?string $returnLabels): self
    {
        $this->returnLabels = $returnLabels;

        return $this;
    }

    public function getReturnOrderPageCode(): ?string
    {
        return $this->returnOrderPageCode;
    }

    public function setReturnOrderPageCode(?string $returnOrderPageCode): self
    {
        $this->returnOrderPageCode = $returnOrderPageCode;

        return $this;
    }

    public function getReturnOrderResponseCode(): ?string
    {
        return $this->returnOrderResponseCode;
    }

    public function setReturnOrderResponseCode(
        ?string $returnOrderResponseCode
    ): self {
        $this->returnOrderResponseCode = $returnOrderResponseCode;

        return $this;
    }

    public function getAutoArchive(): ?int
    {
        return $this->autoArchive;
    }

    public function setAutoArchive(?int $autoArchive): self
    {
        $this->autoArchive = $autoArchive;

        return $this;
    }

    public function getCustomerNote(): ?int
    {
        return $this->customerNote;
    }

    public function setCustomerNote(?int $customerNote): self
    {
        $this->customerNote = $customerNote;

        return $this;
    }

    public function getReturnPageTitle(): ?string
    {
        return $this->returnPageTitle;
    }

    public function setReturnPageTitle(?string $returnPageTitle): self
    {
        $this->returnPageTitle = $returnPageTitle;

        return $this;
    }

    public function getReturnPageHandle(): ?string
    {
        return $this->returnPageHandle;
    }

    public function setReturnPageHandle(?string $returnPageHandle): self
    {
        $this->returnPageHandle = $returnPageHandle;

        return $this;
    }

    public function getPreviewCode(): ?string
    {
        return $this->previewCode;
    }

    public function setPreviewCode(?string $previewCode): self
    {
        $this->previewCode = $previewCode;

        return $this;
    }

    public function getShowReturnLinks(): ?int
    {
        return $this->showReturnLinks;
    }

    public function setShowReturnLinks(int $showReturnLinks): self
    {
        $this->showReturnLinks = $showReturnLinks;

        return $this;
    }

    public function getIsPageTemplateCreated(): ?bool
    {
        return $this->isPageTemplateCreated;
    }

    public function setIsPageTemplateCreated(bool $isPageTemplateCreated): self
    {
        $this->isPageTemplateCreated = $isPageTemplateCreated;

        return $this;
    }

    public function getIsStyleSnippetCreated(): ?bool
    {
        return $this->isStyleSnippetCreated;
    }

    public function setIsStyleSnippetCreated(bool $isStyleSnippetCreated): self
    {
        $this->isStyleSnippetCreated = $isStyleSnippetCreated;

        return $this;
    }

    public function getRequiredReturnNote(): ?int
    {
        return $this->requiredReturnNote;
    }

    public function setRequiredReturnNote(int $requiredReturnNote): self
    {
        $this->requiredReturnNote = $requiredReturnNote;

        return $this;
    }

    public function getExchangeNote(): ?int
    {
        return $this->exchangeNote;
    }

    public function setExchangeNote(int $exchangeNote): self
    {
        $this->exchangeNote = $exchangeNote;

        return $this;
    }

    public function getRequiredExchangeNote(): ?int
    {
        return $this->requiredExchangeNote;
    }

    public function setRequiredExchangeNote(int $requiredExchangeNote): self
    {
        $this->requiredExchangeNote = $requiredExchangeNote;

        return $this;
    }

    public function getReturnMethods(): ?string
    {
        return $this->returnMethods;
    }

    public function setReturnMethods(?string $returnMethods): self
    {
        $this->returnMethods = $returnMethods;

        return $this;
    }

    public function getImageUpload(): ?int
    {
        return $this->imageUpload;
    }

    public function setImageUpload(?int $imageUpload): self
    {
        $this->imageUpload = $imageUpload;

        return $this;
    }

    public function getRequiredImageUpload(): ?int
    {
        return $this->requiredImageUpload;
    }

    public function setRequiredImageUpload(?int $requiredImageUpload): self
    {
        $this->requiredImageUpload = $requiredImageUpload;

        return $this;
    }

    public function getGuestCheckout(): ?int
    {
        return $this->guestCheckout;
    }

    public function setGuestCheckout(?int $guestCheckout): self
    {
        $this->guestCheckout = $guestCheckout;

        return $this;
    }

    public function getReturnCenterPageCode(): ?string
    {
        return $this->returnCenterPageCode;
    }

    public function setReturnCenterPageCode(?string $returnCenterPageCode): self
    {
        $this->returnCenterPageCode = $returnCenterPageCode;

        return $this;
    }

    public function getReturnCenterPageId(): ?string
    {
        return $this->returnCenterPageId;
    }

    public function setReturnCenterPageId(?string $returnCenterPageId): self
    {
        $this->returnCenterPageId = $returnCenterPageId;

        return $this;
    }

    public function getIsReturnCenterPageTemplateCreated(): ?int
    {
        return $this->isReturnCenterPageTemplateCreated;
    }

    public function setIsReturnCenterPageTemplateCreated(?int $isReturnCenterPageTemplateCreated): self
    {
        $this->isReturnCenterPageTemplateCreated = $isReturnCenterPageTemplateCreated;

        return $this;
    }

    public function getAutoApproveRefundReturns(): ?int
    {
        return $this->autoApproveRefundReturns;
    }

    public function setAutoApproveRefundReturns(int $autoApproveRefundReturns): self
    {
        $this->autoApproveRefundReturns = $autoApproveRefundReturns;

        return $this;
    }

    public function getAutoApproveExchangeReturns(): ?int
    {
        return $this->autoApproveExchangeReturns;
    }

    public function setAutoApproveExchangeReturns(int $autoApproveExchangeReturns): self
    {
        $this->autoApproveExchangeReturns = $autoApproveExchangeReturns;

        return $this;
    }
}
