<?php

namespace App\Entity;

use App\Repository\DefaultReasonsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DefaultReasonsRepository::class)
 */
class DefaultReasons
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, options={"comment": "Reason Text"})
     */
    private $label;

    /**
     * @ORM\Column(type="smallint", options={"comment": "Reason Status, 0=> Disabled, 1=> Active", "default": "1"})
     */
    private $active;

    /**
     * @ORM\Column(type="smallint", options={"comment": "Reason for, 1=> Return, 2=> Exchange", "default": "1"})
     */
    private $type;

    /**
     * @ORM\Column(type="datetime", options={"comment": "Reason Created At", "default": "CURRENT_TIMESTAMP"})
     */
    private $dateAdd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }
}
