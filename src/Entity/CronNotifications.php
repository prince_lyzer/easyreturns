<?php

namespace App\Entity;

use App\Repository\CronNotificationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CronNotificationsRepository::class)
 */
class CronNotifications
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $userId;

    /**
     * @ORM\Column(type="integer")
     */
    private $mailId;

    /**
     * @ORM\Column(type="integer")
     */
    private $returnId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $toAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $toName;

    /**
     * @ORM\Column(type="smallint", options={"default": 0, "comment": "0=> Pending, 1=> InQueue, 2=> Completed"})
     */
    private $status = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getMailId(): ?int
    {
        return $this->mailId;
    }

    public function setMailId(int $mailId): self
    {
        $this->mailId = $mailId;

        return $this;
    }

    public function getReturnId(): ?int
    {
        return $this->returnId;
    }

    public function setReturnId(int $returnId): self
    {
        $this->returnId = $returnId;

        return $this;
    }

    public function getToAddress(): ?string
    {
        return $this->toAddress;
    }

    public function setToAddress(string $toAddress): self
    {
        $this->toAddress = $toAddress;

        return $this;
    }

    public function getToName(): ?string
    {
        return $this->toName;
    }

    public function setToName(?string $toName): self
    {
        $this->toName = $toName;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }
}
