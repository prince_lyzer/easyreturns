<?php

namespace App\Entity;

use App\Repository\ReturnLineItemsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReturnLineItemsRepository::class)
 */
class ReturnLineItems
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", options={"comment":"Order Line Items Id"})
     */
    private $orderLineItemId;

    /**
     * @ORM\Column(type="integer")
     */
    private $returnQty;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $returnReason;

    /**
     * @ORM\Column(type="integer")
     */
    private $returnReasonId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpd;

    /**
     * @ORM\Column(type="integer")
     */
    private $returnRequestId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderLineItemId(): ?int
    {
        return $this->orderLineItemId;
    }

    public function setOrderLineItemId(int $orderLineItemId): self
    {
        $this->orderLineItemId = $orderLineItemId;

        return $this;
    }

    public function getReturnQty(): ?int
    {
        return $this->returnQty;
    }

    public function setReturnQty(int $returnQty): self
    {
        $this->returnQty = $returnQty;

        return $this;
    }

    public function getReturnReason(): ?string
    {
        return $this->returnReason;
    }

    public function setReturnReason(string $returnReason): self
    {
        $this->returnReason = $returnReason;

        return $this;
    }

    public function getReturnReasonId(): ?int
    {
        return $this->returnReasonId;
    }

    public function setReturnReasonId(int $returnReasonId): self
    {
        $this->returnReasonId = $returnReasonId;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    public function setDateAdd(\DateTimeInterface $dateAdd): self
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    public function getDateUpd(): ?\DateTimeInterface
    {
        return $this->dateUpd;
    }

    public function setDateUpd(\DateTimeInterface $dateUpd): self
    {
        $this->dateUpd = $dateUpd;

        return $this;
    }

    public function getReturnRequestId(): ?int
    {
        return $this->returnRequestId;
    }

    public function setReturnRequestId(int $returnRequestId): self
    {
        $this->returnRequestId = $returnRequestId;

        return $this;
    }
}
