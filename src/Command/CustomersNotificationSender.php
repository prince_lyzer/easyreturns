<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;

use App\Service\Helper;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\ReturnRequest;
use App\Entity\OrderDetails;
use App\Entity\CustomerDetails;
use App\Entity\OrderLineItems;
use App\Entity\ReturnLineItems;
use App\Entity\EmailTemplates;
use App\Entity\UserEmailTemplates;
use App\Entity\ReturnImages;
use App\Entity\CronNotifications;

class CustomersNotificationSender extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'return:send-mails';
    private $helper;
    private $em;

    public function __construct(Helper $helper, EntityManagerInterface $em)
    {
        $this->helper = $helper;
        $this->em = $em;

        parent::__construct();
    }

    protected function configure(): void
    {
        // ...
        $this->setDescription('Execute after every 1 minute to send email notifications to customer.')->setHelp("Command sends notifications to customers after every 1 minute.");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $cron_notifications = $this->em->getRepository(CronNotifications::class)->getAllCronNotifications();
        // dump($cron_notifications);die;
        if(!$cron_notifications) {
            // $output->writeln("No pending cron notifications.");
            return Command::SUCCESS;
        }
        
        $output->writeln('------------------------------------------------------');
        $output->writeln("Cron job starts at ".gmdate('Y-m-d H:i:s'));

        $notification_sent = [];
        foreach($cron_notifications as $user_id => $notifications) {
            $user = $this->em->getRepository(UserSettings::class)->findOneById($user_id);

            if(!$user) {
                $output->writeln("User #${user_id} not found.");
                continue;
            }

            $from_name = $this->helper->getShopName($user->getDomain());

            foreach ($notifications as $notification) { # send notification
                $mail_template = $this->helper->getEmailTemplate(
                    $user_id, 
                    $notification['mail_id'], 
                    $notification['return_id'], 
                    ['name'=> $from_name], 
                    ['address'=> $notification['to_address'], 'name'=> $notification['to_name']], 
                    ['address'=> $user->getCustomerReplyEmail() ? : $user->getCustomerEmail(), 'name'=> $from_name],
                    [],
                    false,
                    ['consolePath'=> true]
                );

                $output->writeln("Notification ".$notification['mail_id'].' is sent for return #'.$notification['return_id'].' successfully: '.$mail_template);
                $notification_sent[] = $notification['id'];
            }
        }

        $output->writeln('sent notifications: '. json_encode($notification_sent));

        if(!empty($notification_sent)) {
            $deleted_notifications = $this->em->getRepository(CronNotifications::class)->deleteCronNotificationsByIds(implode(',', $notification_sent));

            $output->writeln('deleted notifications: '. $deleted_notifications);
        }

        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;

        // or return this to indicate incorrect command usage; e.g. invalid options
        // or missing arguments (it's equivalent to returning int(2))
        // return Command::INVALID
    }
}