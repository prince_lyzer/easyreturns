<?php
namespace App\Controller;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Max-Age: 1000');

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

use App\Service\Shopify;
use App\Service\ShopifyApi;
use App\Service\Helper;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\UserProvidedReasons;
use App\Entity\CustomerDetails;
use App\Entity\OrderDetails;
use App\Entity\OrderLineItems;
use App\Entity\ReturnLineItems;
use App\Entity\ReturnRequest;
use App\Entity\PaymentOptions;
use App\Entity\DefaultReturnLabels;
use App\Entity\ReturnImages;

/**
 * @Route("/front/api", requirements={}, name="frontApi_", priority=2)
 */
class FrontApiController extends AbstractController
{
    private $logger;
    private $helper;
    private $em;
    private $api_version;
    private $serializer;

    public function __construct(LoggerInterface $logger, Helper $helper, EntityManagerInterface $em, ParameterBagInterface $params) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;
        $this->api_version = $params->get('APP_API_VERSION');
        $this->serializer = new Serializer([new ObjectNormalizer()]);
    }

    /**
     * @Route("/get_orders_return_link", methods={"POST"})
     */
    public function getOrderReturnStatusAction(Request $request)
    {
        $shop = $request->get('shop');
        $order_ids = $request->get('order_ids');

        if (!$this->helper->verifyMyshopifyDomain($shop)) {
            return new JsonResponse(['err'=> 'Shop is incorrect.'], Response::HTTP_BAD_REQUEST);
        }

        if (empty($order_ids)) {
            return new JsonResponse(['err'=> 'No orders provided.'], Response::HTTP_OK);
        }

        $user_settings_obj = $this->getUserSettings($shop);
        if (!$user_settings_obj) {
            return new Response(false, Response::HTTP_NOT_FOUND);
        }

        $user_id = $user_settings_obj->getId();
        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);
        $fields = ['ids'=> implode($order_ids, ','), 'fields'=> 'id,fulfillment_status,fulfillments', 'status'=> 'any'];
        $orders_data = $shopifyApi->getOrders($fields);

        $resulted_data = [];
        foreach ($orders_data as $order_data) {
            if (!$order_data['fulfillment_status']) { # order not fulfilled
                $resulted_data[$order_data['id']] = 0;
            } else {
                # order is either fulfilled or partial fulfilled, RETURN is applicable in this order
                $resulted_data[$order_data['id']] = 1;
            }
        }

        $orders_not_found = array_diff_key(array_flip($order_ids), $resulted_data);
        if ($orders_not_found) {
            # if order olders than 60 days, we did not get those orders data from api
            foreach ($orders_not_found as $order_id=> $order) {
                $resulted_data[$order_id] = 0; //-2;
            }
        }

        # Get return acceptable days
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $return_page_handle = $this->getParameter('RETURN_PAGE_HANDLE');

        if (!empty($resulted_data)) {
            return new JsonResponse(['err'=> '', 'msg'=> '', 'data'=> ['orders'=> $resulted_data, 'handle'=> $return_page_handle]], Response::HTTP_OK);
        }

        return new Response(['err'=> 'Something went wrong.', 'err_code'=> 4], Response::HTTP_OK);
    }

    /**
     * @Route("/return-page-configuration", methods={"GET"})
     */
    public function getReturnPageConfigurationsAction(Request $request)
    {
        $shop = $request->get('shop');

        if (!$this->helper->verifyMyshopifyDomain($shop)) {
            return new JsonResponse(['err'=> 'Shop is incorrect.'], Response::HTTP_BAD_REQUEST);
        }

        $user_settings_obj = $this->getUserSettings($shop);
        if (!$user_settings_obj) {
            return new JsonResponse(['err'=> 'Something went wrong.', 'err_code'=> 4], Response::HTTP_NOT_FOUND);
        }

        # Get return acceptable days
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_settings_obj->getId()]);

        $default_return_labels = $this->em->getRepository(DefaultReturnLabels::class)->findAll();
        $default_return_labels = $this->serializer->normalize($default_return_labels, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'name']]);
        $return_labels = array_column($default_return_labels, 'name', 'id');

        if ($user_configs && $user_configs->getReturnLabels()) {
            $user_defined_return_labels = json_decode($user_configs->getReturnLabels(), true);

            if (count($return_labels) !== count($user_defined_return_labels)) {
                foreach ($user_defined_return_labels as $id=> $value) {
                    if (array_key_exists($id, $return_labels)) {
                        $return_labels[$id] = $value;
                    }
                }
            } else {
                $return_labels = $user_defined_return_labels;
            }
        }

        if ($return_labels[5]) {
            $return_labels[5] = str_replace('{{ return_window_time }}', $user_configs->getReturnAcceptableDays(), $return_labels[5]);
        }

        $data = [
            'return_labels'=> $return_labels, 
            'show_return_links'=> $user_configs ? $user_configs->getShowReturnLinks() : 0,
            'return_note'=> $user_configs ? $user_configs->getCustomerNote() : 0,
            'required_return_note'=> $user_configs ? $user_configs->getRequiredReturnNote() : 1,
            'exchange_note'=> $user_configs ? $user_configs->getExchangeNote() : 1,
            'required_exchange_note'=> $user_configs ? $user_configs->getRequiredExchangeNote() : 0,
            'return_methods'=> $user_configs ? explode(',', $user_configs->getReturnMethods()) : [1],
            'guest_checkout'=> $user_configs ? $user_configs->getGuestCheckout() : 0,
        ];

        return new JsonResponse(['err'=> '', 'msg'=> '', 'data'=> $data], Response::HTTP_OK);
    }

    /**
     * @Route("/view-order-return", methods={"POST"})
     * Comment: check a particular order return available status,
     * If=> return is not applicable then show error msg .
     * else=> Show return order page .
     */
    public function OrderReturnPage(Request $request)
    {
        $shop = $request->get('shop'); // Store myshopify.com
        $sp_order_id = (int)$request->get('order_id'); // Shopify Order Id
        $customer_id = (int)$request->get('customer_id');

        if (!$this->helper->verifyMyshopifyDomain($shop)) {
            return new JsonResponse(['err'=> 'Shop is incorrect.'], Response::HTTP_BAD_REQUEST);
        }

        if (!$this->helper->verifyShopifyOrderId($sp_order_id) || $customer_id == '') {
            return new JsonResponse(['err'=> 'Required request parameters missing or incorrect.', 'err_code'=> 1], Response::HTTP_BAD_REQUEST);
        }

        $user_settings_obj = $this->getUserSettings($shop);

        if (!$user_settings_obj) {
            return new JsonResponse(['err'=> 'Shop not found.'], Response::HTTP_BAD_REQUEST);
        }

        # get shopify order details
        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);

        // dump($sp_order_id, $customer_id);die;
        $order_data = $shopifyApi->getOrderById($sp_order_id);
        if(isset($order_data['error'])) {
            return new JsonResponse(['err'=> 'Requested Order details not found.', 'err_code'=> 13], Response::HTTP_NOT_FOUND);
        }

        if ($order_data['customer']['id'] !== $customer_id) {
            return new JsonResponse(['err'=> 'Requested Order details not found.', 'err_code'=> 13], Response::HTTP_NOT_FOUND);
        }

        // dump($order_data);die;

        if (!isset($order_data['fulfillment_status']) || $order_data['fulfillment_status'] === 'unfulfilled') {
            return new JsonResponse(['err'=> 'Return is not available for this order.', 'err_code'=> 2], Response::HTTP_OK);
        }

        $order_detail = $this->em->getRepository(OrderDetails::class)->findOneBy(['orderId'=> $sp_order_id]);
        $return_requests = false;
        $return_line_items = false;
        $line_items_not_returned = false;
        $order_line_items = false;

        if ($order_detail) {
            $order_id = $order_detail->getId();
            $order_line_items_obj = $this->em->getRepository(OrderLineItems::class)->findBy(['orderId'=> $order_id]);
            $order_line_items = $this->serializer->normalize($order_line_items_obj, null);
            $order_line_items = array_column($order_line_items, null, 'lineItemId');

            # get return request for this order
            $return_requests = $this->em->getRepository(ReturnRequest::class)->findBy(['orderId'=> $order_id]);

            # check for return on this order
            if ($return_requests) {
                $return_line_items = $this->em->getRepository(ReturnLineItems::class)->getReturnLineItemsByOrderId($order_id);
            }
        }

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_settings_obj->getId()]);
        $return_acceptable_days = $user_configs->getReturnAcceptableDays();
        $return_methods = explode(',', $user_configs->getReturnMethods());

        $is_cod_order = $this->helper->isCODOrder($order_data['payment_gateway_names']);

        $payment_options = $available_payment_options = false;
        if ($is_cod_order) {
            # get return payment options
            $payment_options = $user_configs->getReturnPaymentOptions() ? json_decode($user_configs->getReturnPaymentOptions()) : false;

            # get available payment options
            $available_payment_options_obj = $this->em->getRepository(PaymentOptions::class)->findBy(['active'=> 1]);
            $available_payment_options = $this->serializer->normalize($available_payment_options_obj, null, [AbstractNormalizer::IGNORED_ATTRIBUTES=> ['dateAdd']]);
        }

        $return_payment_options = [];
        if (!empty($payment_options)) {
            foreach ($available_payment_options as $payment_option) {
                if (in_array($payment_option['handle'], $payment_options)) {
                    $return_payment_options[] = $payment_option;
                }
            }
        }

        // dump($return_payment_options);die;
        # get order line items
        $orderActualLineItems = [];
        foreach ($order_data['line_items'] as $key => $oline_item) {
            if($oline_item['product_exists']) {
                $orderActualLineItems[] = $oline_item;
            }
        }

        if(empty($orderActualLineItems)) {
            return new JsonResponse(['err'=> 'Something went wrong.', 'err_code'=> 4], Response::HTTP_NOT_FOUND);
        }

        // dump($orderActualLineItems);die;
        $orderActualLineItemsQtyArr = array_column($orderActualLineItems, 'quantity', 'id');

        if ($order_data['fulfillment_status'] === 'fulfilled') {
            if (!$return_line_items) {
                # check line_items for return applicable
                $line_items_not_returned = $orderActualLineItemsQtyArr;
            } else {
                $total_order_line_items_qty = array_sum($orderActualLineItemsQtyArr);
                // dump('total_order_line_items_qty', $total_order_line_items_qty);                    
                $return_line_items_qty_arr = [];
                $return_type = 0; // 1 -> Full return, 0-> No return, 2-> Partial return

                foreach ($return_line_items as $returned_line_item) {
                    # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                $total_return_line_items_qty = array_sum($return_line_items_qty_arr);
                // dump('total_return_line_items_qty', $total_return_line_items_qty);                    
                // dump('return_type', $return_type);       //die;       

                if ($total_order_line_items_qty - $total_return_line_items_qty == 0) {
                    # full return
                    $return_type = 1;
                } else {
                    $return_type = 2;
                    foreach ($orderActualLineItemsQtyArr as $line_item_id => $qty) {
                        if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                            $line_items_not_returned[$line_item_id] = $qty;
                        } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                            $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                        }
                    }
                }
            }
        } elseif ($order_data['fulfillment_status'] === 'partial') {
            # line_items not fulfilled
            $line_items_not_fulfilled = $line_items_fulfilled = [];

            foreach ($orderActualLineItems as $line_item) {
                switch ($line_item['fulfillment_status']) {
                    case 'fulfilled':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'];
                        break;
                    case 'partial':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'] - $line_item['fulfillable_quantity'];
                    case null:
                        $line_items_not_fulfilled[$line_item['id']] = $line_item['fulfillable_quantity'];
                        break;
                }
            }

            if (!$return_requests) { # check line_items for return applicable
                $line_items_not_returned = $line_items_fulfilled;
            } else if ($return_line_items) {
                $return_line_items_qty_arr = [];
                $return_type = 2;

                foreach ($return_line_items as $returned_line_item) {
                    # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                foreach ($line_items_fulfilled as $line_item_id=> $qty) {
                    if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                        $line_items_not_returned[$line_item_id] = $qty;
                    } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                        $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                    }
                }
            }
        }

        // dump($line_items_not_returned); die;

        # items which are fulfilled but not returned by customer
        $return_applicable_line_items = [];
        if (!empty($line_items_not_returned)) {
            # get fulfilled line items fulfillment date
            $line_items_fulfillment = [];
            foreach ($order_data['fulfillments'] as $order_fulfillment) {
                foreach ($order_fulfillment['line_items'] as $line_item) {
                    $line_items_fulfillment[$line_item['id']][] = ['fulfillment_date'=> $order_fulfillment['created_at']];
                }
            }

            # check return acceptable for fulfilled line items (Return applicable Items)
            $return_applicable_line_items = $this->helper->checkOrderLineItemsReturnApplicable($line_items_not_returned, $line_items_fulfillment, $return_acceptable_days);
        }

        // dump($return_applicable_line_items);die;
        if (empty($return_line_items) && empty($return_applicable_line_items)) {
            return new JsonResponse(['err'=> "Oops! We found your order but can't offer you the option to return as the returns window of $return_acceptable_days days has elapsed.", 'err_code'=> 5], Response::HTTP_OK);
        }

        // dump($return_line_items);die;
        # returned line items
        $returned_line_items = [];
        if (!empty($return_line_items)) {
            foreach ($return_line_items as $return_line_item) {
                $return_request_id = $return_line_item['return_request_id'];
                $final_returned_item_qty[$return_line_item['order_line_item_id']] = ($final_returned_item_qty[$return_line_item['order_line_item_id']] ?? 0) + $return_line_item['return_qty'];

                if (!isset($returned_line_items[$return_request_id])) {
                    $returned_line_items[$return_request_id] = [
                        'return_status'=> $return_line_item['return_status'],
                        'return_status_label'=> $return_line_item['return_status_label'],
                        'return_type'=> $return_line_item['return_type'],
                        'payment_details'=> json_decode($return_line_item['payment_details'], true),
                        'date_add'=> $this->helper->convertGmtDateTimeToUserDateTime($user_settings_obj->getIanaTimezone(),  $return_line_item['date_add']),
                        'canceled_reason'=> $return_line_item['canceled_reason'],
                        'customer_note'=> $return_line_item['customer_note'],
                    ];
                }

                $returned_line_items[$return_request_id]['return_line_items'][] = [
                    'line_item_id'=> $return_line_item['line_item_id'],
                    'return_item_id'=> $return_line_item['return_item_id'],
                    'order_line_item_id'=> $return_line_item['order_line_item_id'],
                    'return_qty'=> $return_line_item['return_qty'],
                    'return_reason'=> $return_line_item['return_reason'],
                ];
            }
        }

        // dump($returned_line_items);die;

        # get order line items
        $line_items = [];
        foreach ($orderActualLineItems as $line_item) {
            $image_url = null;

            if ($order_line_items && array_key_exists($line_item['id'], $order_line_items)) {
                $image_url = $order_line_items[$line_item['id']]['imageUrl'];
            }
            else {
                if ($line_item['variant_title']) { # get image_id of product variant
                    $variant_obj = $shopifyApi->getVariantById($line_item['variant_id']);

                    if(!isset($variant_obj['error']) && !empty($variant_obj['image_id'])) {
                        $image_obj = $shopifyApi->getProductImageById($line_item['product_id'], $variant_obj['image_id']);

                        if(!isset($image_obj['error'])) {
                            $image_url = $image_obj['src'];
                        }
                    }
                } 
                
                if(!isset($image_url)) { # get image of product variant
                    $product_obj = $shopifyApi->getProductById($line_item['product_id'], ['fields'=> 'image']);

                    if(!isset($product_obj['error']) && !empty($product_obj['image'])) {
                        $image_url = $product_obj['image']['src'];
                    }
                }
            }

            $line_items[$line_item['id']] = [
                'id'=> $line_item['id'],
                'product_id'=> $line_item['product_id'],
                'variant_id'=> $line_item['variant_id'],
                'title'=> $line_item['title'] . ($line_item['variant_title'] != '' ? ' - '.$line_item['variant_title'] : ''),
                'sku'=> $line_item['sku'],
                'quantity'=> $line_item['quantity'],
                'price'=> $line_item['price'],
                'src'=> $image_url,
            ];
        }

        // dump($line_items);//die;

        # get return reasons
        $return_reasons = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_settings_obj->getId(), 'type'=> 1]);
        $money_format = $user_settings_obj->getMoneyFormat();

        $data = [
            'order_details'=> ['id'=> $sp_order_id, 'name'=> $order_data['name']],
            'line_items'=> $line_items,
            'return_applicable_line_items'=> $return_applicable_line_items,
            'returned_line_items'=> $returned_line_items,
            'return_reasons'=> $return_reasons,
            'money_format'=> $money_format,
            'is_cod_order'=> $is_cod_order,
            'has_customer_note'=> $user_configs->getCustomerNote() ?? 0,
            'payment_options'=> $return_payment_options,
            'return_methods'=> $return_methods,
            'req_customer_note'=> $user_configs->getRequiredReturnNote(),
            'exchange_note'=> $user_configs->getExchangeNote() ?? 0,
            'req_exchange_note'=> $user_configs->getRequiredExchangeNote(),
            'no_img'=> "https://easyreturns.lyzer.io/no-image.gif",
            'image_upload'=> $user_configs->getImageUpload() ?? 0,
            'required_image_upload'=> $user_configs->getRequiredImageUpload() ?? 0,
            'notification_email'=> $order_data['customer']['email'] ?? ''
        ];

        if ($user_configs && $user_configs->getReturnOrderPageCode()) {
            $return_order_form_view = $this->get('twig')->createTemplate($user_configs->getReturnOrderPageCode())->render($data);
        } else {
            $return_order_form_view = $this->renderView('front_api/order_return_page.html.twig', $data);
        }

        // $this->logger->info('return_order_form_view', [$return_order_form_view]);
        return new JsonResponse(['err'=> '', 'msg'=> '', 'data'=> $return_order_form_view], Response::HTTP_OK);
    }

    /**
     * @Route("/order_return_request", methods={"POST"})
     */
    public function orderReturnRequestAction(Request $request)
    {
        $shop = $request->get('shop');
        $sp_order_id = $request->get('order_id');
        $sp_customer_id = $request->get('customer_id');
        $return_qty_arr = $request->get('return_qty');
        $return_reason_arr = $request->get('return_reason');
        $payment_option = $request->get('payment_option');
        $payment_details = $request->get('payment_details');
        $customer_note = $request->get('customer_note') ? trim($request->get('customer_note')) : '';
        $preview = $request->get('preview') ? trim($request->get('preview')) : null;
        $return_method = (int) $request->get('return_method');
        $notification_email = $request->get('notification_email') ?? '';
        $uploaded_images = $request->files->all();
        $uploaded_images = $uploaded_images['files'] ?? [];
        // dump($uploaded_images, count($uploaded_images));die;
        // dump(count($uploaded_images));die;

        if ($preview && $preview === '1') {
            return new JsonResponse(['err'=> 'Preview return page cannot be saved.'], Response::HTTP_OK);
        }

        // $this->logger->info('return_qty_arr', $return_qty_arr);
        // $this->logger->info(print_r($return_reason_arr, true));

        if (!$this->helper->verifyMyshopifyDomain($shop)) {
            return new JsonResponse(['err'=> 'Shop is incorrect.'], Response::HTTP_BAD_REQUEST);
        }

        if (!$this->helper->verifyShopifyOrderId($sp_order_id) || $sp_customer_id == '') {
            return new JsonResponse(['err'=> 'Required request parameters missing or incorrect.', 'err_code'=> 1], Response::HTTP_BAD_REQUEST);
        }

        $user_settings_obj = $this->getUserSettings($shop);

        if (!$user_settings_obj) {
            return new JsonResponse(['err'=> 'Shop not found.'], Response::HTTP_BAD_REQUEST);
        }

        $user_id = $user_settings_obj->getId();
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if(!in_array($return_method, explode(',', $user_configs->getReturnMethods()))){
            return new JsonResponse(['err'=> 'Return method not provided.', 'err_code'=> ''], Response::HTTP_NOT_FOUND);
        }

        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);

        $order_data = $shopifyApi->getOrderById($sp_order_id);
        if(isset($order_data['error'])) {
            return new JsonResponse(['err'=> 'Requested Order details not found.', 'err_code'=> 13], Response::HTTP_NOT_FOUND);
        }

        if ($order_data['customer']['id'] !== (int) $sp_customer_id) {
            return new JsonResponse(['err'=> 'Requested Order details not found.', 'err_code'=> 13], Response::HTTP_NOT_FOUND);
        }

        $customer_email = $order_data['customer']['email'];
        $notification_email = trim($notification_email);

        if($customer_email) {
            $notification_email = $customer_email;
        }
        else if(!$this->helper->verifyEmail($notification_email)) {
            return new JsonResponse(['err'=> 'Notification Email is not right.', 'err_code'=> 31], Response::HTTP_OK);
        }

        if (!isset($order_data['fulfillment_status']) || $order_data['fulfillment_status'] === 'unfulfilled') {
            return new JsonResponse(['err'=> 'Return is not available for this order.', 'err_code'=> 2], Response::HTTP_OK);
        }

        # validate customer return line items data
        $user_return_reasons_obj = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_id, 'type'=> 1]);
        $user_return_reasons = $this->serializer->normalize($user_return_reasons_obj, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'label']]);
        $user_return_reasons = array_column($user_return_reasons, 'label', 'id');

        $error_arr = ['status'=> false, 'msg'=> ''];
        $actual_return_items = [];

        foreach ($return_qty_arr as $line_item_id=> $return_qty) {
            if ($return_qty > 0) {
                # check return reason
                if (!empty($return_reason_arr[$line_item_id])) {
                    # check return reason id
                    if (!array_key_exists($return_reason_arr[$line_item_id], $user_return_reasons)) {
                        $error_arr['status'] = true;
                        $error_arr['msg'] = 'Return Reason is incorrect';
                    } else {
                        $actual_return_items[$line_item_id] = ['qty'=> $return_qty, 'reason'=> $user_return_reasons[$return_reason_arr[$line_item_id]], 'reason_id'=> $return_reason_arr[$line_item_id]];
                    }
                } else {
                    $error_arr['status'] = true;
                    $error_arr['msg'] = 'Return Reason is incorrect';
                }

                if ($error_arr['status']) {
                    break;
                }
            }
        }

        if ($error_arr['status']) {
            return new JsonResponse(['err'=> $error_arr['msg']], Response::HTTP_OK);
        }

        // $this->logger->info('actual_return_items', $actual_return_items);

        if (empty($actual_return_items)) {
            return new JsonResponse(['err'=> 'No Return Line items provided'], Response::HTTP_OK);
        }

        $order_detail = $this->em->getRepository(OrderDetails::class)->findOneBy(['orderId'=> $sp_order_id]);
        
        $return_requests = $return_line_items = $line_items_not_returned = $order_line_items = false;

        if ($order_detail) {
            $order_id = $order_detail->getId();
            $order_line_items_obj = $this->em->getRepository(OrderLineItems::class)->findBy(['orderId'=> $order_id]);
            $order_line_items = $this->serializer->normalize($order_line_items_obj, null);
            $order_line_items = array_column($order_line_items, null, 'lineItemId');

            # get return request for this order
            $return_requests = $this->em->getRepository(ReturnRequest::class)->findBy(['orderId'=> $order_id]);

            # check for return on this order
            if ($return_requests) {
                $return_line_items = $this->em->getRepository(ReturnLineItems::class)->getReturnLineItemsByOrderId($order_id);
            }
        }

        $has_customer_note = $user_configs->getCustomerNote() ?? 0;

        if($return_method === 1 && ($has_customer_note && $user_configs->getRequiredReturnNote() && $customer_note === '')) {
            return new JsonResponse(['err'=> 'Customer note is required.', 'err_code'=> 11], Response::HTTP_OK);
        }
        else if($return_method === 2 && ($user_configs->getExchangeNote() && $user_configs->getRequiredExchangeNote() && $customer_note === '')) {
            return new JsonResponse(['err'=> 'Customer note is required.', 'err_code'=> 11], Response::HTTP_OK);
        }

        if($user_configs->getImageUpload() && $user_configs->getRequiredImageUpload() && empty($uploaded_images)) {
            return new JsonResponse(['err'=> 'Upload item images is required.', 'err_code'=> 27], Response::HTTP_OK);
        }

        if($user_configs->getImageUpload() && count($uploaded_images) > 10) {
            return new JsonResponse(['err'=> 'Max 10 item images can be uploaded.', 'err_code'=> 28], Response::HTTP_OK);
        }

        if($user_configs->getImageUpload()) {
            foreach($uploaded_images as $image) {
                // dump($image);
                if($image->getError()) {
                    return new JsonResponse(['err'=> 'Error in uploading file, File size should be less than 2MB.', 'err_code'=> 32], Response::HTTP_OK);
                }
                
                $image_size = $image->getSize();
                // dump('size', $image_size);die;

                if($image_size > (1024*1024)) { // 2 MB 
                    return new JsonResponse(['err'=> 'Error in uploading files, File size should be less than 2MB.', 'err_code'=> 32], Response::HTTP_OK);
                }
            }
        }

        $is_cod_order = $this->helper->isCODOrder($order_data['payment_gateway_names']);

        $payment_options = $available_payment_options = false;
        $verified_payment_details = [];

        if ($is_cod_order && $return_method === 1) {
            # get return payment options
            if ($user_configs) {
                $payment_options = $user_configs->getReturnPaymentOptions() ? json_decode($user_configs->getReturnPaymentOptions()) : false;
            }

            if (!in_array($payment_option, $payment_options)) {
                return new JsonResponse(['err'=> 'Payment option is not provided.', 'err_code'=> 3], Response::HTTP_OK);
            }

            $verified_payment_details = $this->helper->verifyPaymentDetails($payment_details, $payment_option);
            // print_r($verified_payment_details);die;
            // $this->logger->info('verified_payment_details', $verified_payment_details);

            if ($verified_payment_details === false) {
                return new JsonResponse(['err'=> 'Payment details are incorrect', 'err_code'=> 4], Response::HTTP_OK);
            } elseif (isset($verified_payment_details['err'])) {
                return new JsonResponse($verified_payment_details, Response::HTTP_OK);
            }
        }

        // dump($order_line_items);//die;

        # get order line items
        $orderActualLineItems = [];
        foreach ($order_data['line_items'] as $key => $oline_item) {
            if($oline_item['product_exists']) {
                $orderActualLineItems[] = $oline_item;
            }
        }

        if(empty($orderActualLineItems)) {
            return new JsonResponse(['err'=> 'Something went wrong.', 'err_code'=> 4], Response::HTTP_NOT_FOUND);
        }

        // dump($orderActualLineItems);die;
        $orderActualLineItemsQtyArr = array_column($orderActualLineItems, 'quantity', 'id');

        # get returnable line items
        if ($order_data['fulfillment_status'] === 'fulfilled') {
            if (!$return_line_items) {
                # no line items is returned by customer before
                $line_items_not_returned = $orderActualLineItemsQtyArr;
            } else {
                $total_order_line_items_qty = array_sum($orderActualLineItemsQtyArr);
                $return_line_items_qty_arr = [];

                foreach ($return_line_items as $returned_line_item) {
                    # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                $total_return_line_items_qty = array_sum($return_line_items_qty_arr);

                if ($total_order_line_items_qty - $total_return_line_items_qty > 0) {
                    # partial return
                    foreach ($orderActualLineItemsQtyArr as $line_item_id=> $qty) {
                        if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                            $line_items_not_returned[$line_item_id] = $qty;
                        } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                            $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                        }
                    }
                }
            }
        } elseif ($order_data['fulfillment_status'] === 'partial') {
            # line_items not fulfilled
            $line_items_fulfilled = [];

            foreach ($orderActualLineItems as $line_item) {
                switch ($line_item['fulfillment_status']) {
                    case 'fulfilled':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'];
                        break;
                    case 'partial':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'] - $line_item['fulfillable_quantity'];
                        break;
                }
            }

            if (!$return_line_items) {
                # check line_items for return applicable
                $line_items_not_returned = $line_items_fulfilled;
            } else {
                $return_line_items_qty_arr = [];
                $return_type = 2;

                foreach ($return_line_items as $returned_line_item) {
                    # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                foreach ($line_items_fulfilled as $line_item_id=> $qty) {
                    if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                        $line_items_not_returned[$line_item_id] = $qty;
                    } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                        $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                    }
                }
            }
        }

        // dump($line_items_not_returned);die;

        # items which are fulfilled but not returned by customer
        $return_applicable_line_items = [];
        if (!empty($line_items_not_returned)) {
            # Get return acceptable days
            $return_acceptable_days = $user_configs->getReturnAcceptableDays();

            # get fulfilled line items fulfillment date
            $line_items_fulfillment = [];
            foreach ($order_data['fulfillments'] as $order_fulfillment) {
                foreach ($order_fulfillment['line_items'] as $line_item) {
                    $line_items_fulfillment[$line_item['id']][] = ['fulfillment_date'=> $order_fulfillment['created_at']];
                }
            }

            # check return acceptable for fulfilled line items (Return applicable Items)
            $return_applicable_line_items = $this->helper->checkOrderLineItemsReturnApplicable($line_items_not_returned, $line_items_fulfillment, $return_acceptable_days);
        }

        if (empty($return_applicable_line_items)) {
            return new JsonResponse(['err'=> 'No Return available for this order.', 'err_code'=> 10], Response::HTTP_OK);
        }

        // dump($return_applicable_line_items);die;

        # get order line items
        $line_items = [];
        foreach ($orderActualLineItems as $line_item) {
            $order_line_item_id = null;
            $image_url = NULL;

            if ($order_line_items && array_key_exists($line_item['id'], $order_line_items)) {
                $image_url = $order_line_items[$line_item['id']]['imageUrl'];
                $order_line_item_id = $order_line_items[$line_item['id']]['id'];
            } else {
                if ($line_item['variant_title']) { # get image_id of product variant
                    $variant_obj = $shopifyApi->getVariantById($line_item['variant_id']);

                    if(!isset($variant_obj['error']) && !empty($variant_obj['image_id'])) {
                        $image_obj = $shopifyApi->getProductImageById($line_item['product_id'], $variant_obj['image_id']);

                        if(!isset($image_obj['error'])) {
                            $image_url = $image_obj['src'];
                        }
                    }
                }

                if(!isset($image_url)) { # get image of product variant
                    $product_obj = $shopifyApi->getProductById($line_item['product_id'], ['fields'=> 'image']);

                    if(!isset($product_obj['error']) && !empty($product_obj['image'])) {
                        $image_url = $product_obj['image']['src'];
                    }
                }
            }

            $line_items[$line_item['id']] = [
                'order_line_item_id'=> $order_line_item_id,
                'id'=> $line_item['id'],
                'product_id'=> $line_item['product_id'],
                'variant_id'=> $line_item['variant_id'],
                'product_title'=> $line_item['title'],
                'variant_title'=> $line_item['variant_title'],
                'sku'=> $line_item['sku'],
                'quantity'=> $line_item['quantity'],
                'price'=> $line_item['price'],
                'src'=> $image_url,
            ];
        }

        // dump($line_items);die;

        # check customer returned line items with return_applicable_line_items
        $not_applicable_return_line_items = [];
        foreach ($actual_return_items as $line_item_id=> $arr) {
            if (!array_key_exists($line_item_id, $return_applicable_line_items)) {
                # return not applicable
                $not_applicable_return_line_items[$line_item_id] = 'Not Available';
            } elseif ($return_applicable_line_items[$line_item_id] - $arr['qty'] < 0) {
                # return qty more than applicable
                $not_applicable_return_line_items[$line_item_id] = $return_applicable_line_items[$line_item_id];
            }
        }

        // dump($not_applicable_return_line_items);die;

        if (!empty($not_applicable_return_line_items)) {
            $return_err = '<table><thead><tr><th>Product Name</th><th>Reason</th></tr></thead><tbody>';

            foreach ($not_applicable_return_line_items as $line_item_id=> $tmp) {
                $return_err .= '<tr><td>' . $line_items[$line_item_id]['product_title'] . ($line_items[$line_item_id]['variant_title'] ? ' - ' . $line_items[$line_item_id]['variant_title'] : '') . '</td>';

                if ($tmp === 'Not Available') {
                    $return_err .= '<td>Return not applicable in this item</td></tr>';
                } else {
                    $return_err .= '<td>Only ' . $tmp . ' quantity are returnable</td></tr>';
                }
            }

            return new JsonResponse(['err'=> 'No Return available for these items.', 'err_code'=> 9, 'data'=> $return_err], Response::HTTP_OK);
        }

        # saving data in db , everything is alright
        # check customer
        $customer_details = $this->em->getRepository(CustomerDetails::class)->findOneBy(['customerId'=> $sp_customer_id]);
        $customer_name = ucfirst($order_data['customer']['first_name']) . ' ' . ucwords($order_data['customer']['last_name']);

        if ($customer_details) {
            $customer_details->setDateUpd(new \DateTime());
        } else { # create new customer
            $customer_details = new CustomerDetails();
            $customer_details->setCustomerId($order_data['customer']['id']);
            $customer_details->setDateAdd(new \DateTime());
            $customer_details->setCustomerSince(new \DateTime($order_data['customer']['created_at']));
        }

        $customer_details->setEmail($customer_email);
        $customer_details->setName($customer_name);
        $customer_details->setPhone($order_data['customer']['phone']);
        $this->em->persist($customer_details);
        $this->em->flush();

        $customer_id = $customer_details->getId();

        # save order details
        if (!$order_detail) {
            $order_detail = new OrderDetails();
            $order_detail->setOrderId($order_data['id']);
            $order_detail->setOrderName($order_data['name']);
            $order_detail->setFulfillmentStatus($order_data['fulfillment_status']);
            $order_detail->setPaymentStatus($order_data['financial_status']);
            $order_detail->setCustomerId($customer_id);
            $order_detail->setOrderCreatedAt(new \DateTime($order_data['created_at']));
            $order_detail->setShippingAddress(isset($order_data['shipping_address']) ? json_encode($order_data['shipping_address']) : NULL);
            $order_detail->setIsCod($is_cod_order);
            $order_detail->setDateAdd(new \DateTime());
            $this->em->persist($order_detail);
            $this->em->flush();
        }

        $order_id = $order_detail->getId();

        # save order line item details
        foreach ($line_items as $line_item_id => $line_item) {
            if (!$line_item['order_line_item_id']) { # line item not in records
                $order_line_items_obj = new OrderLineItems();
                $order_line_items_obj->setOrderId($order_id);
                $order_line_items_obj->setLineItemId($line_item_id);
                $order_line_items_obj->setProductId($line_item['product_id']);
                $order_line_items_obj->setVariantId($line_item['variant_id']);
                $order_line_items_obj->setProductTitle($line_item['product_title']);
                $order_line_items_obj->setVariantTitle($line_item['variant_title']);
                $order_line_items_obj->setPrice($line_item['price']);
                $order_line_items_obj->setQuantity($line_item['quantity']);
                $order_line_items_obj->setItemSku($line_item['sku']);
                $order_line_items_obj->setImageUrl($line_item['src']);
                $order_line_items_obj->setDateAdd(new \DateTime());
                $this->em->persist($order_line_items_obj);
                $this->em->flush();

                $line_items[$line_item_id]['order_line_item_id'] = $order_line_items_obj->getId();
            }
        }

        $return_status = 1;
        if($return_method === 1 && $user_configs->getAutoApproveRefundReturns()) { // accept refund return
            $return_status = 2;
        }
        else if($return_method === 2 && $user_configs->getAutoApproveExchangeReturns()) { // accept exchange return
            $return_status = 2;
        }

        # create return request
        $return_request = new ReturnRequest();
        $return_request->setUserId($user_id);
        $return_request->setSpOrderId($sp_order_id);
        $return_request->setOrderId($order_id);
        $return_request->setCustomerId($customer_id);
        $return_request->setReturnStatus($return_status); // Pending
        $return_request->setPaymentDetails(!empty($verified_payment_details) ? json_encode($verified_payment_details) : NULL);
        $return_request->setCustomerNote($customer_note);
        $return_request->setReturnType($return_method);
        $return_request->setNotificationEmail($notification_email);
        $return_request->setDateAdd(new \DateTime());
        $this->em->persist($return_request);
        $this->em->flush();
        $return_request_id = $return_request->getId();

        # add return images
        if($user_configs->getImageUpload()) {
            foreach($uploaded_images as $image) {
                $image_name = $this->helper->generateRandomString() . "_{$return_request_id}." . $image->guessExtension();
                $image->move($this->getParameter('RETURN_IMAGES_DIRECTORY'), $image_name);
                
                $return_images_obj = new ReturnImages();
                $return_images_obj->setReturnId($return_request_id);
                $return_images_obj->setOriginalName($image->getClientOriginalName());
                $return_images_obj->setName($image_name);
                $return_images_obj->setSize($image->getSize() / 1024);
                $return_images_obj->setDateAdd(new \DateTime());
                $this->em->persist($return_images_obj);
                $this->em->flush();
            }
        }

        # save return line items details
        foreach ($actual_return_items as $line_item_id=> $arr) {
            $return_line_items = new ReturnLineItems();
            $return_line_items->setReturnRequestId($return_request_id);
            $return_line_items->setOrderLineItemId($line_items[$line_item_id]['order_line_item_id']);
            $return_line_items->setReturnQty($arr['qty']);
            $return_line_items->setReturnReason($arr['reason']);
            $return_line_items->setReturnReasonId($arr['reason_id']);
            $return_line_items->setDateAdd(new \DateTime());
            $this->em->persist($return_line_items);
            $this->em->flush();
        }

        # send to merchant
        $mail_id = 5;
        $from_name = $this->helper->getShopName($user_settings_obj->getDomain());

        $merchant_new_request_mail_send = $this->helper->getEmailTemplate(
            $user_id, 
            $mail_id, 
            $return_request_id,
            ['name'=> $this->getParameter('APP_NAME')],
            ['address'=> $user_settings_obj->getEmail(), 'name'=> $user_settings_obj->getShopOwner()] 
        );
        $this->logger->info('merchant_new_request_mail_send', [$merchant_new_request_mail_send]);

        # send to customer
        if($return_status === 1) {
            $mail_id = 1;
        }
        else if($return_status === 2) {
            $mail_id = 2;
        }
        
        $customer_confirmation_mail_send = $this->helper->getEmailTemplate(
            $user_id, 
            $mail_id, 
            $return_request_id, 
            ['name'=> $from_name],
            ['address'=> $notification_email, 'name'=> $customer_name], 
            ['address'=> $user_settings_obj->getCustomerReplyEmail() ? : $user_settings_obj->getCustomerEmail(), 'name'=> $from_name] 
        );
        $this->logger->info('customer_confirmation_mail_send', [$customer_confirmation_mail_send,]);

        $data = [
            'order_id'=> $order_id,
            'return_items'=> $actual_return_items,
            'request_id'=> $return_request_id,
            'order_details'=> ['id'=> $sp_order_id, 'name'=> $order_data['name']],
            'line_items'=> $line_items,
            'money_format'=> $user_settings_obj->getMoneyFormat(),
            'is_cod_order'=> $is_cod_order,
            'payment_option'=> $payment_option,
            'payment_details'=> $verified_payment_details ? : NULL,
            'customer_note'=> $customer_note,
            'return_method'=> $return_method,
            'no_img'=> "https://easyreturns.lyzer.io/no-image.gif"
        ];

        if ($user_configs->getReturnOrderResponseCode()) {
            $return_order_response_view = $this->get('twig')->createTemplate($user_configs->getReturnOrderResponseCode())->render($data);
        } else {
            $return_order_response_view = $this->renderView('front_api/return_order_response.html.twig', $data);
        }

        return new JsonResponse(['data'=> $return_order_response_view], Response::HTTP_OK);
    }

    public function getUserSettings($shop) {
        return $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
    }

    

    /**
     * @Route("/preview-order-return", methods={"POST"}, name="preview_order_return")
     * Comment: show preview return page,
     */
    public function previewOrderReturnPage(Request $request)
    {
        $shop = $request->get('shop'); // Store myshopify.com
        $sp_order_id = $request->get('order_id'); // Shopify Order Id
        $customer_id = $request->get('customer_id');
        $preview = $request->get('preview');
        $preview_type = $request->get('type');
        // dump($shop, $sp_order_id, $preview, $preview_type);die;

        if (!$preview) {
            return new Response('');
        }

        if ($preview && !in_array($preview_type, ['orderReturnPageCode', 'orderReturnResponseCode'])) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        $user_settings_obj = $this->getUserSettings($shop);

        $order_detail = $this->em->getRepository(OrderDetails::class)->findOneBy(['orderId'=> $sp_order_id]);
        $return_requests = false;
        $return_line_items = false;
        $line_items_not_returned = false;
        $order_line_items = false;

        if ($order_detail) {
            $order_id = $order_detail->getId();
            $order_line_items_obj = $this->em->getRepository(OrderLineItems::class)->findBy(['orderId'=> $order_id]);
            $order_line_items = $this->serializer->normalize($order_line_items_obj, null);
            $order_line_items = array_column($order_line_items, null, 'lineItemId');

            # get return request for this order
            $return_requests = $this->em->getRepository(ReturnRequest::class)->findBy(['orderId'=> $order_id]);

            # check for return on this order
            if ($return_requests && ($preview && $preview_type === 'orderReturnResponseCode')) {
                $return_line_items = $this->em->getRepository(ReturnLineItems::class)->getReturnLineItemsByOrderId($order_id);
            }
        }

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_settings_obj->getId()]);
        $has_customer_note = $user_configs->getCustomerNote() ?? 0;

        $is_cod_order = $order_detail->getIsCod() ? true : false;

        $payment_options = $available_payment_options = false;
        if ($is_cod_order) {
            # get return payment options
            $payment_options = $user_configs->getReturnPaymentOptions()
                ? json_decode($user_configs->getReturnPaymentOptions())
                : false;

            # get available payment options
            $available_payment_options_obj = $this->em->getRepository(PaymentOptions::class)->findBy(['active'=> 1]);
            $available_payment_options = $this->serializer->normalize($available_payment_options_obj, null, [AbstractNormalizer::IGNORED_ATTRIBUTES=> ['dateAdd']]);
        }

        $return_payment_options = [];
        if (!empty($payment_options)) {
            foreach ($available_payment_options as $payment_option) {
                if (in_array($payment_option['handle'], $payment_options)) {
                    $return_payment_options[] = $payment_option;
                }
            }
        }

        # get return reasons
        $return_reasons = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_settings_obj->getId(), 'type'=> 1]);
        $return_reasons = $this->serializer->normalize($return_reasons, null, [
            AbstractNormalizer::IGNORED_ATTRIBUTES=> ['userId', 'dateAdd', 'dateUpd', 'type'],
        ]);
        $money_format = $user_settings_obj->getMoneyFormat();

        foreach ($order_line_items as $line_item_id=> $line_item) {
            $order_line_items[$line_item_id]['id'] = $line_item_id;
            $order_line_items[$line_item_id]['src'] = $order_line_items[$line_item_id]['imageUrl'];
            $order_line_items[$line_item_id]['title'] = $order_line_items[$line_item_id]['productTitle'];
            $order_line_items[$line_item_id]['product_title'] = $order_line_items[$line_item_id]['productTitle'];
            $order_line_items[$line_item_id]['variant_title'] = $order_line_items[$line_item_id]['variantTitle'];
            $order_line_items[$line_item_id]['sku'] = $order_line_items[$line_item_id]['itemSku'];
            unset($order_line_items[$line_item_id]['imageUrl'], $order_line_items[$line_item_id]['itemSku']);
        }

        if ($preview_type === 'orderReturnPageCode') {
            $return_applicable_line_items = array_column($order_line_items, 'quantity', 'lineItemId');
            $returned_line_items = [];

            $data = ['returned_line_items'=> $returned_line_items];
        } elseif ($preview_type === 'orderReturnResponseCode') {
            $return_applicable_line_items = [];
            // dump($return_line_items);die;
            $returned_line_items = [];
            if (!empty($return_line_items)) {
                foreach ($return_line_items as $return_line_item) {
                    $return_request_id = $return_line_item['return_request_id'];

                    if (!isset($returned_line_items[$return_request_id])) {
                        $returned_line_items[$return_request_id] = [
                            // 'return_status'=> $return_line_item['return_status'],
                            // 'return_status_label'=> $return_line_item['return_status_label'],
                            // 'return_type'=> $return_line_item['return_type'],
                            'payment_details'=> json_decode($return_line_item['payment_details'], true),
                            // 'date_add'=> $this->helper->convertGmtDateTimeToUserDateTime($user_settings_obj->getIanaTimezone(),  $return_line_item['date_add']),
                            // 'canceled_reason'=> $return_line_item['canceled_reason'],
                            'customer_note'=> $return_line_item['customer_note'],
                            'return_method'=> $return_line_item['return_type']
                        ];
                    }

                    $returned_line_items[$return_request_id]['return_line_items'][$return_line_item['line_item_id']] = [
                        'qty'=> $return_line_item['return_qty'],
                        'reason'=> $return_line_item['return_reason'],
                        'reason_id'=> $return_line_item['return_reason_id'],
                    ];
                }
            }

            $data = [
                'return_items'=> $returned_line_items[$return_request_id]['return_line_items'], 
                'request_id'=> $return_request_id ?? '', 
                'customer_note'=> $returned_line_items[$return_request_id]['customer_note'],
                'return_method'=> $returned_line_items[$return_request_id]['return_method'],
                'payment_details'=> $returned_line_items[$return_request_id]['payment_details']
            ];
        }

        // dump($returned_line_items);die;
        // dump($returned_line_items[$return_request_id]['payment_details']);die;

        $data += [
            'order_details'=> ['id'=> $sp_order_id, 'name'=> $order_detail->getOrderName()],
            'line_items'=> $order_line_items,
            'return_reasons'=> $return_reasons,
            'money_format'=> $money_format,
            'is_cod_order'=> $is_cod_order,
            'payment_options'=> $return_payment_options,
            'return_applicable_line_items'=> $return_applicable_line_items,
            'return_methods'=> explode(',', $user_configs->getReturnMethods()),
            'has_customer_note'=> $has_customer_note,
            'image_upload'=> $user_configs->getImageUpload() ?? 0,
            'required_image_upload'=> $user_configs->getRequiredImageUpload() ?? 0,
            'notification_email'=> $order_data['customer']['email'] ?? ''
        ];

        // dump($data);die;
        if ($user_configs && $user_configs->getPreviewCode()) {
            $return_order_form_view = $this->get('twig')->createTemplate($user_configs->getPreviewCode())->render($data);
        } else {
            return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // dump($return_order_form_view);die;

        // $this->logger->info('return_order_form_view', [$return_order_form_view]);
        return new JsonResponse(['err'=> '', 'msg'=> '', 'data'=> $return_order_form_view], Response::HTTP_OK);
    }

    /**
     * @Route("/order_return_check", name="check_order_return", methods={"POST"})
     */
    public function checkOrderReturn(Request $request) {
        $shop = $request->get('shop');
        $email = $request->get('email');
        $order_number = $request->get('order_number');
        // dump($email, $order_number);die;

        if (!$this->helper->verifyMyshopifyDomain($shop)) {
            return new JsonResponse(['err'=> 'Shop is incorrect.'], Response::HTTP_BAD_REQUEST);
        }
        
        $user_settings_obj = $this->getUserSettings($shop);

        if (!$user_settings_obj) {
            return new JsonResponse(['err'=> 'Shop not found.'], Response::HTTP_BAD_REQUEST);
        }

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_settings_obj->getId()]);
        
        if(!$user_configs->getGuestCheckout()) {
            return new JsonResponse(['err'=> 'Page not found.'], Response::HTTP_NOT_FOUND);
        }

        # get shopify order details
        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);
        $by_email = false;
        $order_data = [];
        
        if($this->helper->verifyEmail($email)) {
            $order_data = $shopifyApi->getOrderByOrderNumber(['name'=> $order_number, 'email'=> $email, 'status'=> 'any']);
            $by_email = true;
        }
        else if($this->helper->verifyMobileNumber($email)) {
            $order_data = $shopifyApi->getOrderByOrderNumber(['name'=> $order_number, 'status'=> 'any']);
        }

        // dump($order_data);die;
        // dump($by_email, $order_data['email'], $email);die;

        if(isset($order_data['error']) || empty($order_data)) {
            return new JsonResponse(['err'=> "Oops! We couldn't find your order. Please check if you entered the right email address / phone number and order number.", 'err_code'=> 29], Response::HTTP_OK);
        }

        if ($by_email && $order_data['email'] !== $email) {
            return new JsonResponse(['err'=> "Oops! We couldn't find your order. The order with this order number is under a different email address / phone number. Please check if you entered the right email address / phone number.", 'err_code'=> 30], Response::HTTP_OK);
        }

        if(!$by_email && $order_data['phone'] !== $email) {
            return new JsonResponse(['err'=> "Oops! We couldn't find your order. The order with this order number is under a different email address / phone number. Please check if you entered the right email address / phone number.", 'err_code'=> 30], Response::HTTP_OK);
        }

        // dump($order_data);die;

        if (!isset($order_data['fulfillment_status']) || $order_data['fulfillment_status'] === 'unfulfilled') {
            return new JsonResponse(['err'=> 'Return is not available for this order.', 'err_code'=> 2], Response::HTTP_OK);
        }

        $token = $order_data['token'];
        $return_page_handle = $this->getParameter('RETURN_PAGE_HANDLE');

        return new JsonResponse(['data'=> ['oid'=> $order_data['id'], 'cid'=> $order_data['customer']['id'], 'handle'=> $return_page_handle, 'token'=> $token]], Response::HTTP_OK);
    }

    /**
     * @Route("/view-return-center-page", methods={"POST"})
     * Comment: Get return center page code,
     */
    public function ReturnCenterPage(Request $request)
    {
        $shop = $request->get('shop'); // Store myshopify.com

        if (!$this->helper->verifyMyshopifyDomain($shop)) {
            return new JsonResponse(['err'=> 'Shop is incorrect.'], Response::HTTP_BAD_REQUEST);
        }

        $user_settings_obj = $this->getUserSettings($shop);

        if (!$user_settings_obj) {
            return new JsonResponse(['err'=> 'Shop not found.'], Response::HTTP_BAD_REQUEST);
        }

        # get shopify order details
        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_settings_obj->getId()]);

        if(!$user_configs->getGuestCheckout()) {
            return new JsonResponse(['err'=> 'Page not found.'], Response::HTTP_NOT_FOUND);
        }

        $data = [];

        if ($user_configs && $user_configs->getReturnCenterPageCode()) {
            $return_center_page_view = $this->get('twig')->createTemplate($user_configs->getReturnCenterPageCode())->render($data);
        } else {
            $return_center_page_view = $this->renderView('front_api/return_center_page.html.twig', $data);
        }

        // $this->logger->info('return_center_page_view', [$return_center_page_view]);
        return new JsonResponse(['err'=> '', 'msg'=> '', 'data'=> $return_center_page_view], Response::HTTP_OK);
    }

    /**
     * @Route("/preview-return-center", methods={"POST"}, name="preview_return_center")
     * Comment: show preview return page,
     */
    public function previewReturnCenterPage(Request $request)
    {
        $shop = $request->get('shop'); // Store myshopify.com
        $preview = $request->get('preview');
        $preview_type = $request->get('type');

        if (!$preview) {
            return new Response('');
        }

        if ($preview && !in_array($preview_type, ['returnCenterPageCode'])) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }

        $user_settings_obj = $this->getUserSettings($shop);
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_settings_obj->getId()]);

        $data = [];

        if ($user_configs && $user_configs->getPreviewCode()) {
            $return_center_page_preview = $this->get('twig')->createTemplate($user_configs->getPreviewCode())->render($data);
        } else {
            return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // dump($return_center_page_preview);die;
        return new JsonResponse(['err'=> '', 'msg'=> '', 'data'=> $return_center_page_preview], Response::HTTP_OK);
    }
}
