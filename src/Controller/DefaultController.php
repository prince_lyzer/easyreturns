<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

use App\Service\Helper;

use App\Entity\ReturnRequest;
use App\Entity\ReturnImages;


class DefaultController extends AbstractController
{
    private $logger;
    private $helper;
    private $em;
    private $serializer;

    public function __construct(LoggerInterface $logger, Helper $helper, EntityManagerInterface $em) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())];
        $this->serializer = new Serializer($normalizers);
    }

    /**
     * @Route("/default", name="default")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/external", name="external_request")
     */
    public function externalWindowAction(Request $request)
    {
        $token = $request->get('token'); 
        $image_id = $request->get('id');

        $data = $this->helper->decrypt($token, $this->getParameter('APP_SECRET'));
        $data = explode('.', $data);

        if(!is_array($data) || count($data) !== 4) {
            return $this->render('return-image-view-external.html.twig', ['error'=> 'Link is Broken. Please try with fresh link.']);
        }

        list($oid, $cid, $rid, $timestamp) = $data;

        if(time() > $timestamp) {
            return $this->render('return-image-view-external.html.twig', ['error'=> 'Link is Expired. Please try with fresh link.']);
        }

        $request_details = $this->em->getRepository(ReturnRequest::class)->findOneBy(['id'=> $rid, 'orderId'=> $oid, 'customerId'=> $cid]);

        if(!$request_details) {
            return new JsonResponse(['error'=> 'Request Not found', 'data'=> ''], Response::HTTP_NOT_FOUND);
        }

        $return_images = $this->em->getRepository(ReturnImages::class)->findReturnImagesByReturnId($rid);

        $response = $this->render('return-image-view-external.html.twig', ['return_images'=> $return_images, 'return_id'=> $rid, 'image_id'=> $image_id, 'host'=> $this->getParameter('HOST_URL')]);
        return $response;
    }
}
