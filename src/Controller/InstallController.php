<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use Firebase\JWT\JWT;
use App\Service\Shopify;
use App\Service\Helper;
use App\Service\ShopifyApi;

use App\Entity\UserSettings;
use App\Entity\DummyShops;
use App\Entity\ApplicationCharges;
use App\Entity\UserConfigurations;
use App\Entity\DefaultReasons;
use App\Entity\UserProvidedReasons;
use App\Entity\FaqAnswers;
use App\Entity\AppNotifications;
use App\Entity\CustomerQueries;

class InstallController extends AbstractController
{
    private $logger;
    private $helper;
    private $api_version;
    private $em;

    public function __construct(
        LoggerInterface $logger,
        Helper $helper,
        ParameterBagInterface $params,
        EntityManagerInterface $em
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->api_version = $params->get('APP_API_VERSION');
        $this->host_url = $params->get('HOST_URL');
        $this->em = $em;
    }

    /**
     * @Route("/landing-page", name="landing_page")
     * @Route("/")
     */
    public function landingPageAction(Request $request)
    {
        $shop = $request->query->get('shop');
        $shop = trim($shop);
        // $mail_send = $this->helper->sendMail(['name'=> 'Prince Agrawal'], ['address'=> 'prince@lyzer.io', 'name'=> 'Prince'], ['address'=> 'princeag1995@gmail.com', 'name'=> 'prince ag'], 'test', '<b>Test</b>', null, ''); echo $mail_send;die;
        // $mail_send = $this->helper->getEmailTemplate(10, 1, 100, ['name'=> 'Lyzer Easy Returns'], ['address'=> 'prince@lyzer.io', 'name'=> 'Prince Lyzer'], ['address'=> 'princeag1995@gmail.com', 'name'=> 'Prince AG'], [], true); die;
        // $mail_content = $this->renderView('mail/welcome_mail.html.twig', ['shop_owner'=> 'prince agrawal', 'shop'=> 'prince-psh3.myshopify.com']);
                
        // $mail_send = $this->helper->sendMail(
        //     ['name'=> $this->getParameter('APP_NAME')], 
        //     ['address'=> 'prince@lyzer.io', 'name'=> 'Prince Agrawal'], 
        //     [],
        //     [],
        //     "Welcome to ".$this->getParameter('APP_NAME')." App!", 
        //     $mail_content
        // );die;
        if ($shop == '') {
            return $this->render('landing_page.html.twig');
        } else {
            if (!$this->helper->verifyMyshopifyDomain($shop)) {
                return $this->redirectToRoute('landing_page');
            }

            # check shop in our records
            $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneByMyshopifyDomain($shop);

            if (!$user_settings_obj || ($user_settings_obj && $user_settings_obj->getStatus() == 0)) {
                $api = new Shopify($shop, ['api_key'=> $this->getParameter('APP_KEY'), 'secret'=> $this->getParameter('APP_SECRET')]);

                $install_url = $api->getAuthorizeUrl($this->getParameter('APP_SCOPES'), $this->host_url . 'install', $this->getNonce($shop), false);

                return $this->redirect($install_url);
            } else {
                return $this->redirect('https://' . $shop . '/admin/apps');
            }
        }
    }

    /**
     * @Route("/top-level-integration", name="top_level")
     */
    public function testAction(Request $request)
    {
        $shop = $request->query->get('shop');
        $shop = trim($shop);

        if ($shop == '') {
            return $this->render('landing_page.html.twig');
        } else {
            if (!$this->helper->verifyMyshopifyDomain($shop)) {
                return $this->redirectToRoute('landing_page');
            }

            $api = new Shopify($shop, ['api_key'=> $this->getParameter('APP_KEY'), 'secret'=> $this->getParameter('APP_SECRET')]);

            $install_url = $api->getAuthorizeUrl($this->getParameter('APP_SCOPES'), $this->host_url . 'install', $this->getNonce($shop), false);

            return $this->redirect($install_url);
        }
    }

    /**
     * @Route("/install", name="install")
     */
    public function installAction(Request $request)
    {
        // $this->logger->info(json_encode($request->headers->all()));

        # verify Hmac
        if (!$this->helper->verifyHmac($request->query->all(), $this->getParameter('APP_SECRET'))) {
            return $this->redirectToRoute('landing_page');
        }

        $shop = $request->query->get('shop');
        $host = $request->query->get('host');

        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneByMyshopifyDomain($shop);

        # if scopes updated then handle response
        $scope_updated = false;
        $api = new Shopify($shop, ['api_key'=> $this->getParameter('APP_KEY'), 'secret'=> $this->getParameter('APP_SECRET')], $this->api_version);

        if ($user_settings_obj && $user_settings_obj->getStatus() != 0 && $request->query->get('code') != '' && $scope_updated) {

            try {
                $token_details = $api->authorizeApplication($this->checkNonce($shop), $request->query->all());
            } catch (\Exception $e) {
                $exc_msg = $e->getMessage();
                return new Response('Something went wrong, <b>' . $exc_msg . '<b>');
            }

            if (!$token_details || !isset($token_details['access_token'])) {
                return new Response('Something went wrong');
            }

            $access_token = $token_details['access_token'];

            # updated access token
            $user_settings_obj->setAccessToken($access_token);
            $user_settings_obj->setDateUpd(new \DateTime());
            $this->em->flush();

            return $this->redirectToRoute('home_route', ['shop'=> $shop, 'host'=> $host]);
        }
        elseif (!$user_settings_obj || ($user_settings_obj && $user_settings_obj->getStatus() == 0)) {
            try {
                if ($request->query->get('code') === null || $request->query->get('code') == '') {
                    $install_url = $api->getAuthorizeUrl($this->getParameter('APP_SCOPES'), $this->host_url . 'install', $this->getNonce($shop), false);
                    return $this->redirect($install_url);
                }

                try {
                    $token_details = $api->authorizeApplication($this->checkNonce($shop), $request->query->all());
                } catch (\Exception $e) {
                    $exc_msg = $e->getMessage();
                    return new Response('Something went wrong, <b>' . $exc_msg . '<b>');
                }

                if (!$token_details || !isset($token_details['access_token'])) {
                    return new Response('Something went wrong');
                }

                $access_token = $token_details['access_token'];
                # get shopify shop details
                $sp_shop_details = $api->call('GET', 'shop.json');

                $status = 1;

                # uninstalled shop
                if ($user_settings_obj && $user_settings_obj->getStatus() == 0) {
                    $user_settings_obj->setDateUpd(new \DateTime());
                } else {
                    # new shop, create a new user settings record
                    $user_settings_obj = new UserSettings();
                    $user_settings_obj->setDateAdd(new \DateTime());
                }

                $user_settings_obj->setAccessToken($access_token);
                $user_settings_obj->setMerchantId($sp_shop_details['id']);
                $user_settings_obj->setEmail($sp_shop_details['email']);
                $user_settings_obj->setDomain($sp_shop_details['domain']);
                $user_settings_obj->setCustomerEmail($sp_shop_details['customer_email']);
                $user_settings_obj->setTimezone($sp_shop_details['timezone']);
                $user_settings_obj->setIanaTimezone($sp_shop_details['iana_timezone']);
                $user_settings_obj->setShopOwner($sp_shop_details['shop_owner']);
                $user_settings_obj->setMyShopifyDomain($sp_shop_details['myshopify_domain']);
                $user_settings_obj->setMoneyFormat($sp_shop_details['money_format']);
                $user_settings_obj->setMoneyWithCurrencyFormat($sp_shop_details['money_with_currency_format']);
                $user_settings_obj->setMoneyInEmailFormat($sp_shop_details['money_in_emails_format']);
                $user_settings_obj->setMoneyWithCurrencyInEmailsFormat($sp_shop_details['money_with_currency_in_emails_format']);
                $user_settings_obj->setAddress1($sp_shop_details['address1']);
                $user_settings_obj->setAddress2($sp_shop_details['address2']);
                $user_settings_obj->setProvince($sp_shop_details['province']);
                $user_settings_obj->setProvinceCode($sp_shop_details['province_code']);
                $user_settings_obj->setCity($sp_shop_details['city']);
                $user_settings_obj->setCountry($sp_shop_details['country']);
                $user_settings_obj->setCountryCode($sp_shop_details['country_code']);
                $user_settings_obj->setCountryName($sp_shop_details['country_name']);
                $user_settings_obj->setZip($sp_shop_details['zip']);
                $user_settings_obj->setPhone($sp_shop_details['phone']);
                $user_settings_obj->setPrimaryLocationId($sp_shop_details['primary_location_id']);
                $user_settings_obj->setStatus($status);
                $user_settings_obj->setIsPasswordEnabled((int) $sp_shop_details['password_enabled']);
                $this->em->persist($user_settings_obj);
                $this->em->flush();

                $recurring_application_charge = $this->createRecurringApplicationCharge($shop, $access_token, ['shop'=> $shop, 'host'=> $host]);
                $confirmation_url = $recurring_application_charge['confirmation_url'];

                # create a entry in application_charge table
                $app_charge_id = $this->createApplicationChargeRecord($user_settings_obj->getId(), $recurring_application_charge['id']);

                return $this->redirectToRoute('recurring_application_charge', ['confirmation_url'=> $confirmation_url]);
            } catch (Exception $e) {
                $this->logger->info('Something went wrong' . $e->getMessage());
            }
        } else {
            if ($user_settings_obj->getStatus() == 1) {
                # payment pending
                $recurring_application_charge = $this->createRecurringApplicationCharge($shop, $user_settings_obj->getAccessToken(), ['shop'=> $shop, 'host'=> $host]);
                $confirmation_url = $recurring_application_charge['confirmation_url'];

                # create a entry in application_charge table
                $app_charge_id = $this->createApplicationChargeRecord($user_settings_obj->getId(), $recurring_application_charge['id']);

                return $this->redirectToRoute('recurring_application_charge', ['confirmation_url'=> $confirmation_url]);
            } elseif ($user_settings_obj->getStatus() == 2) {
                # payment accepted
                # if app scopes updates
                if ($scope_updated) {
                    return $this->render('app_authorize.html.twig', ['authorize_url'=> $this->appScopesUpdated($shop)]);
                }

                # check for app setup completed
                if(!$user_settings_obj->getIsOnboarding()) {
                    return $this->redirectToRoute('home_route', ['shop'=> $shop, 'host'=> $host, 'setup'=> 1]);
                }

                return $this->redirectToRoute('home_route', ['shop'=> $shop, 'host'=> $host]);
            }
        }
    }

    public function appScopesUpdated($shop) {
        $api = new Shopify($shop, ['api_key'=> $this->getParameter('APP_KEY'), 'secret'=> $this->getParameter('APP_SECRET')]);
        return $api->getAuthorizeUrl($this->getParameter('APP_SCOPES'), $this->host_url . 'install', $this->getNonce($shop), false);
    }

    public function createApplicationChargeRecord($user_id, $charge_id, $active = 0) {
        $application_charges_obj = new ApplicationCharges();
        $application_charges_obj->setUserId($user_id);
        $application_charges_obj->setChargeId($charge_id);
        $application_charges_obj->setCreatedAt(new \DateTime());
        $application_charges_obj->setActive($active);

        $this->em->persist($application_charges_obj);
        $this->em->flush();

        return $application_charges_obj->getId();
    }

    /**
     * @Route("/recurring_application_charge", name="recurring_application_charge")
     */
    public function recurringApplicationChargeAction(Request $request)
    {
        return $this->render('recurring_application_charge.html.twig', ['confirmation_url'=> $request->query->get('confirmation_url')]);
    }

    /**
     * @Route("/payment_notification", name="payment_notification")
     */
    public function paymentNotificationAction(Request $request)
    {
        $this->logger->info('payment notification');
        $charge_id = $request->query->get('charge_id');
        $this->logger->info('charge_id: ' . $charge_id);
        // $this->logger->info(json_encode($request));
        $host = $request->get('host');

        $application_charges_obj = $this->em->getRepository(ApplicationCharges::class)->findOneByChargeId($charge_id);
        // $this->logger->info('application_charges_obj: ', $application_charges_obj);

        if ($application_charges_obj) {
            $user_id = $application_charges_obj->getUserId();
            $user = $this->em->getRepository(UserSettings::class)->find($user_id);
            $shop = $user->getMyshopifyDomain();

            if ($application_charges_obj->getActive() == 0) {
                # activate charge id
                $application_charges_obj->setActive(1);
                $application_charges_obj->setUpdatedAt(new \DateTime());

                # user status update to approved
                $user->setStatus(2);
                $user->setDateUpd(new \DateTime());
                $this->em->flush();

                # register uninstall shop webhooks
                $shopifyApi = new ShopifyApi($shop, $user->getAccessToken(), $this->api_version, $this->logger);
                $uninstall_hook_created = $shopifyApi->createHook([
                    'webhook'=> [
                        'topic'=> 'app/uninstalled',
                        'address'=> $this->generateUrl('app_uninstallation', [], UrlGeneratorInterface::ABSOLUTE_URL),
                        'format'=> 'json',
                    ],
                ]);

                # sending welcome mail
                $mail_content = $this->renderView('mail/welcome_mail.html.twig', ['shop_owner'=> $user->getShopOwner(), 'shop'=> $shop]);
                
                $mail_send = $this->helper->sendMail(
                    ['name'=> $this->getParameter('APP_NAME')], 
                    ['address'=> $user->getEmail(), 'name'=> $user->getShopOwner()], 
                    [],
                    [],
                    "Welcome to ".$this->getParameter('APP_NAME')." App!", 
                    $mail_content
                );
                $this->logger->info("Is Welcome mail sends to ${shop} : ".$mail_send);

                return $this->redirectToRoute('home_route', ['shop'=> $shop, 'host'=> $host, 'setup'=> 1]);
            }
            else {
                return $this->redirectToRoute('home_route', ['shop'=> $shop, 'host'=> $host]);
            }
        } else {
            return new Response('Payment failed.', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Route("/app_uninstallation", name="app_uninstallation")
     */
    public function appUninstallation(Request $request)
    {
        $this->logger->info('app_uninstallation hits-');
        $hmac = $request->headers->get('X-Shopify-Hmac-Sha256');
        $payload = $request->getContent();

        # verify Hmac
        if (!$this->helper->verifyWebhook($payload, $hmac, $this->getParameter('APP_SECRET'))) {
            return new Response('', Response::HTTP_FORBIDDEN, []);
        }

        $payload_data = $request->toArray();
        $shop = $payload_data['myshopify_domain'];
        $this->logger->info('shop_name-'.$shop);

        $user = $this->em->getRepository(UserSettings::class)->findOneByMyshopifyDomain($shop);

        if(!$user) {
            return new Response('', Response::HTTP_NOT_FOUND, []);
        }

        $user->setStatus(0);
        $user->setReturnPageId(NULL);
        $user->setIdScriptTag(NULL);
        $user->setDateUpd(new \DateTime());
        $this->em->persist($user);

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user->getId()]);

        if($user_configs) {
            $user_configs->setIsPageTemplateCreated(0);
            $user_configs->setIsStyleSnippetCreated(0);
            $user_configs->setGuestCheckout(0);
            $user_configs->setReturnCenterPageId(NULL);
            $user_configs->setIsReturnCenterPageTemplateCreated(0);
            $this->em->persist($user_configs);
        }

        $this->em->flush();

        # sending uninstallation mail
        $mail_content = $this->renderView('mail/app_uninstallation_mail.html.twig', ['shop'=> $shop, 'shop_owner'=> $user->getShopOwner()]);

        $mail_send = $this->helper->sendMail(
            ['name'=> $this->getParameter('APP_NAME')], 
            ['address'=> $user->getEmail(), 'name'=> $user->getShopOwner()], 
            ['address'=> $this->getParameter('BUSINESS_EMAIL')],
            [],
            "Why uninstall ".$this->getParameter('APP_NAME')." App? Please tell us!", 
            $mail_content
        );
        $this->logger->info("Is uninstallation mail sends to ${shop} : ".$mail_send);

        return new Response('', Response::HTTP_OK, []);
    }

    /**
     * @Route("/shop_update", name="shop_update")
     */
    public function shopUpdate(Request $request)
    {
        $this->logger->info('shop_update hits-');
        $hmac = $request->headers->get('X-Shopify-Hmac-Sha256');

        # verify Hmac
        if (!$this->helper->verifyWebhook($request->getContent(), $hmac, $this->getParameter('APP_SECRET'))) {
            return new Response('', Response::HTTP_FORBIDDEN, []);
        }

        $payload_data = $request->toArray();
        $shop = $payload_data['myshopify_domain'];
        $this->logger->info('shop_name-'.$shop);

        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneByMyshopifyDomain($shop);

        if ($user_settings_obj) {
            $user_settings_obj->setEmail($payload_data['email']);
            $user_settings_obj->setDomain($payload_data['domain']);
            $user_settings_obj->setCustomerEmail($payload_data['customer_email']);
            $user_settings_obj->setTimezone($payload_data['timezone']);
            $user_settings_obj->setIanaTimezone($payload_data['iana_timezone']);
            $user_settings_obj->setShopOwner($payload_data['shop_owner']);
            $user_settings_obj->setMoneyFormat($payload_data['money_format']);
            $user_settings_obj->setMoneyWithCurrencyFormat($payload_data['money_with_currency_format']);
            $user_settings_obj->setMoneyInEmailFormat($payload_data['money_in_emails_format']);
            $user_settings_obj->setMoneyWithCurrencyInEmailsFormat($payload_data['money_with_currency_in_emails_format']);
            $user_settings_obj->setAddress1($payload_data['address1']);
            $user_settings_obj->setAddress2($payload_data['address2']);
            $user_settings_obj->setProvince($payload_data['province']);
            $user_settings_obj->setProvinceCode($payload_data['province_code']);
            $user_settings_obj->setCity($payload_data['city']);
            $user_settings_obj->setCountry($payload_data['country']);
            $user_settings_obj->setCountryCode($payload_data['country_code']);
            $user_settings_obj->setCountryName($payload_data['country_name']);
            $user_settings_obj->setZip($payload_data['zip']);
            $user_settings_obj->setPhone($payload_data['phone']);
            $user_settings_obj->setPrimaryLocationId($payload_data['primary_location_id']);
            $user_settings_obj->setIsPasswordEnabled((int) $payload_data['password_enabled']);
            $user_settings_obj->setDateUpd(new \DateTime());
            $this->em->persist($user_settings_obj);
            $this->em->flush();
            return new Response('', Response::HTTP_OK, []);
        }

        return new Response('', Response::HTTP_NOT_FOUND, []);
    }

    /**
     * @Route("/theme/publish", name="shopify_theme_webhook")
     */
    public function shopifyThemeUpdates(Request $request)
    {
        $this->logger->info('THEME_PUBLISH_HITS-');
        $hmac = $request->headers->get('X-Shopify-Hmac-Sha256');

        # verify Hmac
        if (!$this->helper->verifyWebhook($request->getContent(), $hmac, $this->getParameter('APP_SECRET'))) {
            return new Response('', Response::HTTP_FORBIDDEN, []);
        }

        $payload_data = $request->toArray();
        // $this->logger->info(json_encode($payload_data));
        // $this->logger->info(json_encode($request->headers->all()));

        $shop = $request->headers->get('x-shopify-shop-domain');
        $this->logger->info('shop_name : '.$shop);

        if($shop) {
            if($payload_data['role'] === 'main') {
                $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneByMyshopifyDomain($shop);
                $theme_id = $payload_data['id'];

                if ($user_settings_obj && ($user_settings_obj->getThemeId() != $theme_id)) {
                    $this->logger->info('theme updated to #'.$theme_id.' - '.$payload_data['name'].' from '.$user_settings_obj->getThemeId());
                    $user_settings_obj->setThemeId($theme_id);
                    $user_settings_obj->setDateUpd(new \DateTime());
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();

                    $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);
                    $asset_created_res = $this->createThemeAssetAndSnippets($shopifyApi, $theme_id, $user_settings_obj->getId());
                    $this->logger->info("Result of asset created: ", $asset_created_res);

                }
            }
        }

        return new Response('', Response::HTTP_OK, []);
    }

    public function createRecurringApplicationCharge($shop, $access_token, $params)
    {
        $app_price = $this->getParameter('APP_PRICE');
        $app_trial_days = $this->getParameter('APP_TRIAL_DAYS');
        # check dummy shop
        $dummy_shop_details = $this->em->getRepository(DummyShops::class)->findOneBy(['myshopifyDomain'=> $shop]);
        $testing_store = $dummy_shop_details && $dummy_shop_details->getActive() == 1 ? true : null;

        # recurring application charge
        $api = new Shopify($shop, $access_token, $this->api_version);

        $shop_recurring_charge = $api->call(
            'POST',
            'recurring_application_charges.json',
            [
                'recurring_application_charge'=> [
                    'name'=> 'Seed Pricing Plan',
                    'price'=> $app_price,
                    'return_url'=> $this->generateUrl(
                        'payment_notification',
                        $params,
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    'trial_days'=> $app_trial_days,
                    'test'=> $testing_store,
                ],
            ]
        );

        // $this->logger->info('shop_recurring_charge');
        // $this->logger->info(json_encode($shop_recurring_charge));

        return $shop_recurring_charge;
    }

    public function createMerchantDefaultReasons($user_id)
    {
        $default_Reasons = $this->em->getRepository(DefaultReasons::class)->findBy(['active'=> 1, 'type'=> 1]);
        
        foreach ($default_Reasons as $default_reason) {
            $UserProvidedReasons = new UserProvidedReasons();
            $UserProvidedReasons->setUserId($user_id);
            $UserProvidedReasons->setLabel($default_reason->getLabel());
            $UserProvidedReasons->setType($default_reason->getType());
            $UserProvidedReasons->setDateAdd(new \DateTime());
            $this->em->persist($UserProvidedReasons);
        }

        $this->em->flush();
        return true;
    }

    public function index(Request $request)
    {
        $easy_return_shop = $request->cookies->get('easy_return_SHOP');
        $easy_return_host = $request->cookies->get('easy_return_HOST');

        $shop = $request->get('shop');
        $host = $request->get('host');
        $setup = $request->get('setup') ?: 0;

        $shop = isset($shop) && $shop !== '' ? $shop : $easy_return_shop;
        $host = isset($host) && $host !== '' ? $host : $easy_return_host;

        if (!$shop) {
            return new Response('Please try to open app from shopify app listing section.');
        }

        $response = $this->render('home.html.twig', ['shop'=> $shop, 'host'=> $host, 'setup'=> $setup]);

        $response->headers->setCookie(new Cookie('easy_return_SHOP', $shop, time() + 24 * 60 * 60, '/', '', true, true, true, Cookie::SAMESITE_NONE));

        $response->headers->setCookie(new Cookie('easy_return_HOST', $host, time() + 24 * 60 * 60, '/', '', true, true, true, Cookie::SAMESITE_NONE));

        // if ($setup) {
        //     $verification_id = $shop . ';' . (time() + 60 * 5);
        //     $verification_id = $this->helper->encrypt($verification_id, $this->getParameter('APP_SECRET'));

        //     $response->headers->setCookie(new Cookie('_verify_action_id', $verification_id, time() + 60 * 5, '/', '.lyzer.io', true, false, false, Cookie::SAMESITE_NONE));
        // }

        return $response;
    }

    /**
     * @Route("/contact-us", name="contact-us", methods="POST")
     */
    public function contactFormAction(Request $request)
    {
        $full_name = $request->request->get('full_name');
        $contact_email = $request->request->get('contact_email');
        $subject = $request->request->get('subject');
        $message = $request->request->get('message');
        $created_at = $request->request->get('created_at');

        $full_name = trim($full_name);
        $contact_email = trim($contact_email);
        $message = trim($message);
        $subject = trim($subject);

        if (!$this->helper->verifyHumanName($full_name)) {
            return new Response(json_encode(['error'=> 'Full name is not correct']), Response::HTTP_OK);
        } elseif (!$this->helper->verifyEmail($contact_email)) {
            return new Response(json_encode(['error'=> 'Contact email is not correct']), Response::HTTP_OK);
        } elseif ($message === '') {
            return new Response(json_encode(['error'=> 'Message is required']), Response::HTTP_OK);
        }

        $customer_queries = new CustomerQueries();
        $customer_queries->setName($full_name);
        $customer_queries->setEmail($contact_email);
        $customer_queries->setSubject($subject);
        $customer_queries->setQuery($message);
        $customer_queries->setCreatedAt(new \DateTime());
        $this->em->persist($customer_queries);
        $this->em->flush();

        $html_content = $this->renderView('mail/contact_us_form_query.html.twig', ['full_name'=> $full_name, 'contact_email'=> $contact_email, 'message'=> $message, 'date'=> $created_at, 'subject'=> $subject]);
        
        $mail_send = $this->helper->sendMail(['name'=> $this->getParameter('APP_NAME')], ['address'=> $this->getParameter('CONTACT_EMAIL')], [], [], 'Contact us form query', $html_content);

        if(!$mail_send) {
            $this->em->remove($customer_queries);
            $this->em->flush();
        }

        return new Response(json_encode(['notice'=> 'Query submitted successfully.']), Response::HTTP_OK);
    }

    public function appSetupAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        $this->logger->info('signatureValid: ' . $signatureValid);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();

        # create default configuration
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if (!$user_configs) {
            $user_configs = new UserConfigurations();
            $user_configs->setUserId($user_id);
            $user_configs->setReturnWindowStartsFrom(0);
            $user_configs->setReturnAcceptableDays($this->getParameter('APP_RETURN_ACCEPTABLE_DAYS'));
            $user_configs->setReturnPageTitle($this->getParameter('RETURN_PAGE_NAME'));
            $user_configs->setReturnPageHandle($this->getParameter('RETURN_PAGE_HANDLE'));
            $user_configs->setReturnPaymentOptions($this->getParameter('SELECTED_PAYMENT_OPTIONS'));
            // $user_configs->setShowReturnLinks(0);
            $user_configs->setAutoArchive(0);
            $user_configs->setDateAdd(new \DateTime());
            $this->em->persist($user_configs);
            $this->em->flush();
        }

        # create default reasons
        $user_provided_reasons = $this->em->getRepository(UserProvidedReasons::class)->findOneBy(['userId'=> $user_id]);
        if (!$user_provided_reasons) {
            $this->createMerchantDefaultReasons($user_id);
        }

        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->api_version, $this->logger);
        
        # register shop update webhooks
        $shop_update_webhook = $shopifyApi->createHook([
            'webhook'=> [
                'topic'=> 'shop/update',
                'address'=> $this->generateUrl('shop_update', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'format'=> 'json',
            ],
        ]);
        
        if(!isset($shop_update_webhook['error'])) {
            $this->logger->info("Shop update webhook created");
        }

        # register theme publish hook
        $theme_publish_webhook = $shopifyApi->createHook([
            'webhook'=> [
                'topic'=> 'themes/publish',
                'address'=> $this->generateUrl('shopify_theme_webhook', [], UrlGeneratorInterface::ABSOLUTE_URL),
                'format'=> 'json',
            ],
        ]);

        if(!isset($theme_publish_webhook['error'])) {
            $this->logger->info("Theme publish webhook created");
        }
        
        # create script tag
        $old_script_id = $user_settings_obj->getIdScriptTag();
        if(!$old_script_id) {
            $script_src = $this->host_url . 'js/easy_returns.js';
            $script_tag = $shopifyApi->createScriptTag([
                'script_tag'=> [
                    'event'=> 'onload',
                    'src'=> $script_src,
                ],
            ]);
    
            if(!isset($script_tag['error'])) {
                $script_id = $script_tag['id'];
        
                $user_settings_obj->setIdScriptTag($script_id);
                $this->em->persist($user_settings_obj);
                $this->em->flush();
            }
        }

        $theme_id = $shopifyApi->getStoreThemeId();

        if(!isset($theme_id['error'])) {
            # create style and page asset here
            $asset_created_res = $this->createThemeAssetAndSnippets($shopifyApi, $theme_id, $user_id);
            $this->logger->info("Result of asset created: ", $asset_created_res);

            # create return page
            $template_suffix = 'easy_returns';
            $create_page = $shopifyApi->createPage([
                'page'=> [
                    'title'=> $this->getParameter('RETURN_PAGE_NAME'),
                    'handle'=> $this->getParameter('RETURN_PAGE_HANDLE'),
                    'author'=> $this->getParameter('APP_NAME'),
                    'template_suffix'=> $template_suffix,
                ],
            ]);
    
            if(!isset($create_page['error'])) {
                $user_settings_obj->setReturnPageId($create_page['id']);
            }
            else if($create_page['code'] === 422) {
                $return_page = $shopifyApi->getPageByHandle($this->getParameter('RETURN_PAGE_HANDLE'));

                if(!isset($return_page['error'])) {
                    $user_settings_obj->setReturnPageId($return_page[0]['id']);
                }
            }

            $user_settings_obj->setThemeId($theme_id);
        }
        
        $user_settings_obj->setIsOnboarding(1);
        $this->em->persist($user_settings_obj);
        $this->em->flush();
        
        $response = new Response(json_encode(['notice'=> 'Set up completed successfully.']), Response::HTTP_OK);
        return $response;
    }

    private function createThemeAssetAndSnippets($shopifyApi, $theme_id, $user_id) {
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $is_style_asset_created = $is_page_asset_created = 0;

        # create style snippet
        $asset_key = 'snippets/easy_returns.css.liquid';
        $get_asset = $shopifyApi->getThemeAsset($theme_id, $asset_key);

        if (isset($get_asset['error'])) {
            $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                'asset'=> [
                    'key'=> $asset_key,
                    'value'=> $this->renderView(
                        'front_configure/easy_returns_style.html.twig'
                    ),
                ],
            ]);
            
            if(!isset($create_asset['error'])) {
                $is_style_asset_created = 1;
            }
        }
        else {
            $is_style_asset_created = 1;
        }

        # create return page template
        $template_key = 'templates/page.easy_returns.liquid';
        $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

        if (isset($get_asset['error'])) {
            $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                'asset'=> [
                    'key'=> $template_key,
                    'value'=> $this->renderView(
                        'front_configure/easy_returns.html.twig'
                    ),
                ],
            ]);
    
            if(!isset($create_asset['error'])) {
                $is_page_asset_created = 1;
            }
        }
        else {
            $is_page_asset_created = 1;
        }

        $user_configs->setIsStyleSnippetCreated($is_style_asset_created);
        $user_configs->setIsPageTemplateCreated($is_page_asset_created);
        $this->em->persist($user_configs);
        $this->em->flush();
        return [$is_style_asset_created, $is_page_asset_created];
    }

    /**
     * @Route("/landing-page/faqs/{id}", name="faq_page", requirements={"id"= "\d+"})
     */
    public function faqPageAction(Request $request, $id = null)
    {
        if (!is_null($id)) {
            $faq_answer = $this->em->getRepository(FaqAnswers::class)->findOneById($id);

            if (!$faq_answer) {
                return $this->redirectToRoute('faq_page');
            }
            return $this->render('faq_page.html.twig', ['faq_answer'=> $faq_answer]);
        } else {
            $faq_answers = $this->em->getRepository(FaqAnswers::class)->getAllFaqs();
            return $this->render('faq_page.html.twig', ['faq_answers'=> $faq_answers]);
        }
    }

    /**
     * @Route("/terms", name="policy_page")
     */
    public function policyPageAction(Request $request)
    {
        return $this->render('policy_page.html.twig');
    }

    private function getNonce($shop)
    {
        $nonce = bin2hex(random_bytes(10));

        // Store a record of the shop attempting to authenticate and the nonce provided
        $finder = new Finder();
        $finder->files()->in(__DIR__)->name('authattempts.json');

        foreach ($finder as $file) {
            $path = $file->getRealPath();
        }

        $storedAttempts = file_get_contents($path);
        $storedAttempts = $storedAttempts ? json_decode($storedAttempts, true) : [];

        $is_new_installation = true;
        if (!empty($storedAttempts)) {
            foreach ($storedAttempts as $key=> $attempt) {
                if ($attempt['shop'] === $shop) {
                    $storedAttempts[$key]['nonce'] = $nonce;
                    $is_new_installation = false;
                    break;
                }
            }
        }

        if ($is_new_installation) {
            $storedAttempts[] = ['shop'=> $shop, 'nonce'=> $nonce];
        }

        file_put_contents($path, json_encode($storedAttempts));

        return $nonce;
    }

    private function checkNonce($shop)
    {
        $finder = new Finder();
        $finder->files()->in(__DIR__)->name('authattempts.json');

        foreach ($finder as $file) {
            $path = $file->getRealPath();
        }

        $storedAttempt = null;
        $attempts = json_decode(file_get_contents($path));

        foreach ($attempts as $attempt) {
            if ($attempt->shop === $shop) {
                $storedAttempt = $attempt;
                break;
            }
        }

        return $storedAttempt->nonce;
    }

    /**
     * @Route("/api/notifications", name="app-notifications")
     */
    public function getAppNotification(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        // $user_id = $user_settings_obj->getId();
        $notification = $this->em->getRepository(AppNotifications::class)->findOneBy(['isActive'=> 1]);
        // dump($notification);die;

        if(!$notification) {
            return new JsonResponse([], Response::HTTP_OK);
        }

        $return_data = ['title'=> $notification->getTitle(), 'content'=> $notification->getContent(), 'type'=> $notification->getType(), 'actions'=> $notification->getActions() ? json_decode($notification->getActions(), true) : null];

        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }
}
