<?php 
// src/TwigExtension/customFilters.php 
 
namespace App\Controller; 
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
 
class CustomFilters extends AbstractExtension { 
 
    public function getFilters() { 
    return array( 
            new TwigFilter('base64_encode', array($this, 'base64_en')), // base64_encode => name of custom filter, base64_en => name of function to execute when this filter is called.
            new TwigFilter('base64_decode', array($this, 'base64_dec')),
            new TwigFilter('preg_replace', array($this, 'regex_replace'))
        );
    }
 
    public function base64_en($input)
    {
       return base64_encode($input);
    }
 
    public function base64_dec($input)
    {
        return base64_decode($input);
    }

    public function regex_replace($subject, $pattern, $replacement) {
        return preg_replace($pattern, $replacement, $subject);
    }

    public function getName()
    {
        return 'twig_extension';
    }
 
}