<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

use \Firebase\JWT\JWT;
use App\Service\Shopify;
use App\Service\Helper;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\UserProvidedReasons;
use App\Entity\PaymentOptions;
use App\Entity\EmailTemplates;
use App\Entity\UserEmailTemplates;
use App\Entity\ReturnLineItems;
use App\Entity\OrderDetails;

class MailController extends AbstractController{
    private $logger;
    private $helper;
    private $em;
    private $serializer;

    public function __construct(LoggerInterface $logger, Helper $helper, EntityManagerInterface $em) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())];

        $this->serializer = new Serializer($normalizers);
    }

    public function mailTemplatesAction(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        
        $email_templates = $this->em->getRepository(EmailTemplates::class)->findAll();
        $email_templates = $this->serializer->normalize($email_templates, null, [AbstractNormalizer::ATTRIBUTES => ['id', 'title', 'helpText', 'mailType']]);

        $user_email_details = $this->em->getRepository(UserEmailTemplates::class)->getUserEmailDetails($user_id);
        // dump($user_email_details);die;
        $email_style = $user->getEmailStyle() ? : $this->renderView("mail/default_email_style.html.twig");
        $email_header = $user->getEmailHeader() ? : $this->renderView("mail/mail_header.html.twig");
        $email_signature = $user->getEmailSignature() ? : $this->renderView("mail/mail_signature.html.twig");

        $return_data = array(
            "email_templates"=> $email_templates,
            "user_email_details"=> $user_email_details,
            "email_style"=> $email_style,
            "email_header"=> $email_header,
            "email_signature"=> $email_signature,
            "brand_logo"=> $user->getBrandLogo(),
            "customer_reply_email"=> $user->getCustomerReplyEmail() ? : $user->getCustomerEmail()
        );

        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }

    public function editMailTemplate(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # verify email_id
        if(!$this->helper->verifyId($mail_id)) {
            return new JsonResponse(['error'=> 'Email id is incorrect'], Response::HTTP_BAD_REQUEST);
        }

        $mail_id = (int) $mail_id;
        $user_id = $user->getId();
        
        $default_email_template = $this->em->getRepository(EmailTemplates::class)->findOneBy(['id'=> $mail_id]);
        $email_template = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=> $mail_id]);
        $recipients = [];

        if($mail_id === 5) {
            if($email_template && $email_template->getRecipients())
                $recipients = json_decode($email_template->getRecipients(), true);
            else
                $recipients[] = ['email'=> $user->getEmail(), 'default'=> true];
        }

        $return_data = array(
            "id"=> $email_template ? $email_template->getId() : '',
            'description'=> $default_email_template->getHelpText(),
            "content"=> $email_template ? $email_template->getContent() : $default_email_template->getDefaultContent(), 
            "subject"=> $email_template ? $email_template->getSubject() : $default_email_template->getSubject(), 
            "title"=> $default_email_template->getTitle(),
            "recipients"=> $recipients,
            "active"=> $email_template ? $email_template->getActive() : 1,
            "merchant_mail_id"=> $user->getEmail()
        );

        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }
   
    public function editMailTemplateAction(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $subject = $request_obj['subject'];
        $subject = trim($subject);
        $content = $request_obj['content'];

        if(empty($subject)) {
            return new JsonResponse(['error'=> 'Subject is required'], Response::HTTP_BAD_REQUEST);
        }

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=> $mail_id]);

        if(!$user_email_templates) {
            $user_email_templates = new UserEmailTemplates();
            $user_email_templates->setUserId($user_id);
            $user_email_templates->setMailId($mail_id);
            $user_email_templates->setActive(1);
            $user_email_templates->setDateAdd(new \DateTime());
        }
        else {
            $user_email_templates->setDateUpd(new \DateTime());
        }
        
        $user_email_templates->setContent($content);
        $user_email_templates->setSubject($subject);
        $this->em->persist($user_email_templates);
        $this->em->flush();

        if(!$user_email_templates->getId()) {
            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        // return new JsonResponse($payload);
        $return_data = array('id'=> $user_email_templates->getId());

        return new JsonResponse(['notice'=> 'Updated successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function sendTestEmailAction(Request $request, $mail_id, MailerInterface $mailer) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }
        
        # return request 
        $return_request_id = 12;

        $is_mail_send = $this->helper->getEmailTemplate(
            $user_id, 
            $mail_id, 
            $return_request_id, 
            ['name'=> $this->getParameter('APP_NAME')],
            ['address'=> $user->getEmail(), 'name'=> $user->getShopOwner()], 
            [],
            [],
            true
        );

        if($is_mail_send === true) {
            return new JsonResponse(['notice'=> 'Mail sends successfully'], Response::HTTP_OK);
        }
        else {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }
    }

    public function addEmailRecipientAction(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        # check recipient email
        $recipient_email = $request->get('recipient_email');
        
        if(!$this->helper->verifyEmail($recipient_email)) {
            return new JsonResponse(['error'=> 'Recipient Email is invalid'], Response::HTTP_BAD_REQUEST);
        }

        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=>$mail_id]);

        if(!$user_email_templates) {
            $default_email_template = $this->em->getRepository(EmailTemplates::class)->findOneBy(['id'=> $mail_id]);
            $recipients = [["email"=> $user->getEmail(), "default"=> 1], ["email"=> $recipient_email, "default"=> 0]];
            $user_email_templates = new UserEmailTemplates();
            $user_email_templates->setUserId($user_id);
            $user_email_templates->setMailId($mail_id);
            $user_email_templates->setDateAdd(new \DateTime());
            $user_email_templates->setContent($default_email_template->getDefaultContent());
            $user_email_templates->setSubject($default_email_template->getSubject());
        }
        else {
            $recipients = $user_email_templates->getRecipients() ? json_decode($user_email_templates->getRecipients(), true) : [["email"=> $user->getEmail(), "default"=> 1]];

            foreach ($recipients as $key => $recipient) {
                if($recipient['email'] === $recipient_email) {
                    return new JsonResponse(['error'=> 'Recipient Email already added'], Response::HTTP_BAD_REQUEST);
                }
            }
      
            $recipients[]= ["email"=> $recipient_email, "default"=> 0]; // new recipient
           
            $user_email_templates->setDateUpd(new \DateTime());
        }
        
        $user_email_templates->setRecipients(json_encode($recipients));

        $this->em->persist($user_email_templates);
        $this->em->flush();

        if(!$user_email_templates->getId()) {
            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $return_data = array('recipients'=> $recipients);

        return new JsonResponse(['notice'=> 'Recipient added successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function deleteEmailRecipientAction(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        # check recipient email
        $recipient_email = $request->get('recipient_email');
        
        if(!$this->helper->verifyEmail($recipient_email)) {
            return new JsonResponse(['error'=> 'Invalid request'], Response::HTTP_BAD_REQUEST);
        }

        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=> $mail_id]);

        if(!$user_email_templates) {
            return new JsonResponse(['error'=> 'Invalid request'], Response::HTTP_BAD_REQUEST);
        }

        $recipients = json_decode($user_email_templates->getRecipients(), true);

        $recipients = array_filter($recipients, function($recipient) use($recipient_email) {
            return $recipient['email'] !== $recipient_email;
        });

        $recipients = array_values($recipients);

        // dump($recipients, json_encode($recipients));die;

        if(empty($recipients)) {
            return new JsonResponse(['error'=> 'Add another Recipient address before deleting this one'], Response::HTTP_BAD_REQUEST);
        }
        
        $user_email_templates->setRecipients(json_encode($recipients));
        $user_email_templates->setDateUpd(new \DateTime());

        $this->em->persist($user_email_templates);
        $this->em->flush();

        if(!$user_email_templates->getId()) {
            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $return_data = array('recipients'=> $recipients);

        return new JsonResponse(['notice'=> 'Recipient deleted successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function updateEmailStatusAction(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=> $mail_id]);

        if(!$user_email_templates) {
            $default_email_template = $this->em->getRepository(EmailTemplates::class)->findOneBy(['id'=> $mail_id]);
            $user_email_templates = new UserEmailTemplates();
            $user_email_templates->setUserId($user_id);
            $user_email_templates->setMailId($mail_id);
            $user_email_templates->setActive(0);
            $user_email_templates->setDateAdd(new \DateTime());
            $user_email_templates->setContent($default_email_template->getDefaultContent());
            $user_email_templates->setSubject($default_email_template->getSubject());
        }
        else {
            $user_email_templates->setActive(!$user_email_templates->getActive());
            $user_email_templates->setDateUpd(new \DateTime());
        }

        $this->em->persist($user_email_templates);
        $this->em->flush();

        if(!$user_email_templates->getId()) {
            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $return_data = array();

        return new JsonResponse(['notice'=> "Email " . ($user_email_templates->getActive() ? 'enabled' : 'disabled')." successfully", 'data'=> $return_data], Response::HTTP_OK);
    }

    public function configureEmailAction(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $email_style = $request_obj['email_style'];
        $email_header = $request_obj['email_header'];
        $brand_logo = $request_obj['brand_logo'];
        $email_signature = $request_obj['email_signature'];
        $customer_reply_email = $request_obj['customer_reply_email'];
        $email_style = trim($email_style);
        $email_header = trim($email_header);
        $brand_logo = trim($brand_logo);
        $email_signature = trim($email_signature);
        $customer_reply_email = trim($customer_reply_email);

        if($customer_reply_email && !$this->helper->verifyEmail($customer_reply_email)) {
            return new JsonResponse(['error'=> 'Customer Notification address is invalid'], Response::HTTP_BAD_REQUEST);
        }

        $user->setEmailStyle($email_style);
        $user->setEmailHeader($email_header);
        $user->setBrandLogo($brand_logo);
        $user->setEmailSignature($email_signature);
        $user->setCustomerReplyEmail($customer_reply_email ? : NULL);
        $user->setDateUpd(new \DateTime());
        $this->em->persist($user);
        $this->em->flush();

        if(!$user->getId()) {
            return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        $return_data = array("customer_reply_email"=> $user->getCustomerReplyEmail() ? : $user->getCustomerEmail());
        return new JsonResponse(['notice'=> 'Changes saved successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function previewEmailAction(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $subject = $request_obj['subject'];
        $subject = trim($subject);
        $content = $request_obj['content'];

        if(empty($subject)) {
            return new JsonResponse(['error'=> 'Subject is required'], Response::HTTP_BAD_REQUEST);
        }

        if(empty($content)) {
            return new JsonResponse(['error'=> 'mail content is required'], Response::HTTP_BAD_REQUEST);
        }

        # return request 
        $return_request_id = 12;
        $mail_id = (int) $mail_id;

        $data = [];
        if($mail_id === 6) {
            $data['query'] = 'Return query text goes here...';
        }

        list($subject, $html_content) = $this->helper->getEmailTemplateRaw($mail_id, $user_id, $subject, $content, $return_request_id, $data);
        
        $html_content = "<div style='margin-bottom:25px; border-bottom: 1px solid #e6e6e6;font-size: 18px; padding-bottom: 15px;'><strong>Subject: </strong>&nbsp;&nbsp;$subject</div>".$html_content;

        return new Response($html_content);
    }

    public function resetMailTemplateAction(Request $request, $mail_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($mail_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=>$mail_id]);

        if(!$user_email_templates) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $email_details = $this->em->getRepository(EmailTemplates::class)->findOneById($mail_id);

        $user_email_templates->setContent($email_details->getDefaultContent());
        $user_email_templates->setSubject($email_details->getSubject());
        $this->em->persist($user_email_templates);
        $this->em->flush();

        $return_data = array(
            'content'=> $email_details->getDefaultContent(),
            'subject'=> $email_details->getSubject()
        );

        return new JsonResponse(['notice'=> 'Reset successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function resetMailStyleAction(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        $reset_code = $request->get('reset_code');

        if($reset_code === 'resetEmailStyleCode') {
            $email_style = $this->renderView("mail/default_email_style.html.twig");

            $user->setEmailStyle($email_style);
            $this->em->persist($user);
            $this->em->flush();
    
            $return_data = array('email_style'=> $email_style);
    
            return new JsonResponse(['notice'=> 'Reset successfully', 'data'=> $return_data], Response::HTTP_OK);
        }

        return new JsonResponse(['error'=> 'Invalid request'], Response::HTTP_BAD_REQUEST);
    }
}