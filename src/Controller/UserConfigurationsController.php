<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

use Firebase\JWT\JWT;
use App\Service\Shopify;
use App\Service\Helper;
use App\Service\ShopifyApi;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\UserProvidedReasons;
use App\Entity\PaymentOptions;

/**
 * @Route("/api", requirements={}, name="Api_")
 */
class UserConfigurationsController extends AbstractController
{
    private $logger;
    private $helper;
    private $em;
    private $serializer;

    public function __construct(LoggerInterface $logger, Helper $helper, EntityManagerInterface $em) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $this->serializer = new Serializer($normalizers);
    }

    public function generalConfiguration(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        # get user provided return reasons
        $return_reasons_obj = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_id, 'type'=> 1]);
        $return_reasons = $this->serializer->normalize($return_reasons_obj, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'label']]);

        # get available payment options
        $payment_options_obj = $this->em->getRepository(PaymentOptions::class)->findBy(['active'=> 1]);
        $payment_options = $this->serializer->normalize($payment_options_obj, null, [AbstractNormalizer::IGNORED_ATTRIBUTES=> ['dateAdd']]);

        $return_center_link = '';
        if($user_configs->getGuestCheckout()) {
            $return_center_link = "https://".$user_settings_obj->getDomain()."/pages/".$this->getParameter('RETURN_CENTER_PAGE_HANDLE');
        }

        $return_data = [
            'return_window_starts_from'=> $user_configs->getReturnWindowStartsFrom(),
            'return_acceptable_days'=> $user_configs->getReturnAcceptableDays(),
            'return_reasons'=> $return_reasons,
            'payment_options'=> $payment_options,
            'selected_payment_options'=> $user_configs->getReturnPaymentOptions() ? json_decode($user_configs->getReturnPaymentOptions()) : null,
            'auto_archive'=> $user_configs->getAutoArchive(),
            'return_note'=> $user_configs->getCustomerNote(),
            'required_customer_note'=> $user_configs->getRequiredReturnNote(),
            'exchange_note'=> $user_configs->getExchangeNote(),
            'required_exchange_note'=> $user_configs->getRequiredExchangeNote(),
            'return_methods'=> $user_configs->getReturnMethods() ? array_map(function($m) { return (int) $m; }, explode(',', $user_configs->getReturnMethods()))  : [1],
            'image_upload'=> $user_configs->getImageUpload(),
            'required_image_upload'=> $user_configs->getRequiredImageUpload(),
            'guest_checkout'=> $user_configs->getGuestCheckout(),
            'return_center_link'=> $return_center_link,
            'auto_approve_refund_request'=> $user_configs->getAutoApproveRefundReturns(),
            'auto_approve_exchange_request'=> $user_configs->getAutoApproveExchangeReturns()
        ];

        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }

    public function generalConfigurationAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $auto_archive = $request_obj['auto_archive'];
        $return_window_starts_from = (int) $request_obj['return_window_starts_from'];
        $return_acceptable_days = $request_obj['return_acceptable_days'];
        $return_reasons = $request_obj['return_reasons'];
        $selected_payment_options = $request_obj['payment_options'];
        $return_reasons = array_column($return_reasons, 'label', 'id');
        $customer_note = $request_obj['customer_note'];
        $required_customer_note = $request_obj['required_customer_note'];
        $exchange_note = $request_obj['exchange_note'];
        $required_exchange_note = $request_obj['required_exchange_note'];
        $return_methods = $request_obj['return_methods'];
        $image_upload = $request_obj['image_upload'];
        $required_image_upload = $request_obj['required_image_upload'];
        $guest_checkout = $request_obj['guest_checkout'];
        $auto_approve_refund_request = $request_obj['auto_approve_refund_request'];
        $auto_approve_exchange_request = $request_obj['auto_approve_exchange_request'];
        // dump($guest_checkout, gettype($guest_checkout));die;
        
        if (!is_finite($return_acceptable_days) || $return_acceptable_days < 0) {
            return new JsonResponse(['error'=> 'Invalid return acceptable days'], Response::HTTP_BAD_REQUEST);
        }

        if (!in_array($return_window_starts_from, [0, 1])) {
            return new JsonResponse(['error'=> 'Invalid return windows starts from'], Response::HTTP_BAD_REQUEST);
        }

        if(empty($return_methods)) {
            return new JsonResponse(['error'=> 'Select Return method'], Response::HTTP_BAD_REQUEST);
        }

        sort($return_methods);

        if (!empty($return_reasons)) {
            $old_return_reasons = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_id, 'type'=> 1]);
            $old_return_reasons_arr = $this->serializer->normalize($old_return_reasons, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'label']]);
            $old_return_reasons_arr = array_column($old_return_reasons_arr, 'label', 'id');

            # check empty reasons
            $has_empty_reasons = array_filter($return_reasons, function ($label) { return trim($label) == ''; });

            if (!empty($has_empty_reasons)) {
                return new JsonResponse(['error'=> 'Reason values cannot be empty'], Response::HTTP_BAD_REQUEST);
            }

            $deleted_reasons = array_diff_key($old_return_reasons_arr, $return_reasons);
            $old_reasons = array_intersect_key($old_return_reasons_arr, $return_reasons);
            $new_reasons = array_diff($return_reasons, $old_return_reasons_arr);

            foreach ($old_reasons as $id=> $label) { # old reason updated
                if ($label !== $old_return_reasons_arr[$id]) {
                    $return_reason_obj = $this->em->getRepository(UserProvidedReasons::class)->findOneById($id);
                    $return_reason_obj->setLabel($label);
                    $return_reason_obj->setDateUpd(new \DateTime());
                    $this->em->persist($return_reason_obj);
                }
            }

            foreach ($new_reasons as $id=> $label) { # new reason
                $user_provided_reason_obj = new UserProvidedReasons();
                $user_provided_reason_obj->setUserId($user_id);
                $user_provided_reason_obj->setLabel($label);
                $user_provided_reason_obj->setType(1);
                $user_provided_reason_obj->setDateAdd(new \DateTime());
                $this->em->persist($user_provided_reason_obj);
            }

            foreach ($deleted_reasons as $id=> $label) { # delete reason
                $return_reason_obj = $this->em->getRepository(UserProvidedReasons::class)->findOneById($id);
                $this->em->remove($return_reason_obj);
            }

            $this->em->flush();
        } else {
            $return_reasons_deleted = $this->em->getRepository(UserProvidedReasons::class)->deleteReturnReasonsByUserId($user_id);
        }

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $old_guest_checkout = $user_configs->getGuestCheckout();

        $user_configs->setAutoArchive($auto_archive);
        $user_configs->setReturnWindowStartsFrom($return_window_starts_from);
        $user_configs->setReturnAcceptableDays($return_acceptable_days);
        $user_configs->setReturnPaymentOptions(json_encode($selected_payment_options));
        $user_configs->setCustomerNote($customer_note);
        $user_configs->setRequiredReturnNote($required_customer_note);
        $user_configs->setExchangeNote($exchange_note);
        $user_configs->setRequiredExchangeNote($required_exchange_note);
        $user_configs->setReturnMethods(implode(',', $return_methods));
        $user_configs->setImageUpload($image_upload);
        $user_configs->setRequiredImageUpload($required_image_upload);
        $user_configs->setGuestCheckout($guest_checkout);
        $user_configs->setAutoApproveRefundReturns($auto_approve_refund_request);
        $user_configs->setAutoApproveExchangeReturns($auto_approve_exchange_request);
        $user_configs->setDateUpd(new \DateTime());
        $this->em->persist($user_configs);
        $this->em->flush();

        $return_center_link = '';
        $access_token = $user_settings_obj->getAccessToken();

        if($guest_checkout) {
            $return_center_link = "https://".$user_settings_obj->getDomain()."/pages/".$this->getParameter('RETURN_CENTER_PAGE_HANDLE');
        }

        if($old_guest_checkout != $guest_checkout) {
            if($guest_checkout) {
                $page_created = $this->guestCheckoutAction('create_return_center_page', $shop, $access_token, $user_id);
                $template_created = $this->guestCheckoutAction('create_return_center_page_template', $shop, $access_token, $user_id);
                // dump($page_created, $template_created);die;
            }
            else {
                $page_deleted = $this->guestCheckoutAction('delete_return_center_page', $shop, $access_token, $user_id);
                $template_deleted = $this->guestCheckoutAction('delete_return_center_page_template', $shop, $access_token, $user_id);
                // dump($page_deleted, $template_deleted);die;
            }
        }

        $return_reasons = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_id, 'type'=> 1]);
        $return_reasons_arr = $this->serializer->normalize($return_reasons, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'label']]);
        $return_data = ['return_reasons'=> $return_reasons_arr, 'return_center_link'=> $return_center_link];

        return new JsonResponse(['notice'=> 'Configurations Updated Successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function guestCheckoutAction($type, $shop, $access_token, $user_id)
    {
        $shopifyApi = new ShopifyApi($shop, $access_token, $this->getParameter('APP_API_VERSION'), $this->logger);
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if ($type === 'create_return_center_page') {
            $template_suffix = 'easy_return_center';
            $old_page_id = $user_configs->getReturnCenterPageId();
            $not_found = true;

            if ($old_page_id) {
                $get_page = $shopifyApi->getPageById($old_page_id);

                if (!isset($get_page['error'])) {
                    $not_found = false;
                    $new_page_id = $old_page_id;
                }
            } else {
                $get_pages = $shopifyApi->getAllPages();

                if (!isset($get_pages['error']) && !empty($get_pages)) {
                    foreach ($get_pages as $page) {
                        if ($page['template_suffix'] === $template_suffix) {
                            if ($not_found) {
                                $not_found = false;
                                $new_page_id = $page['id'];
                            } else {
                                $shopifyApi->deletePage($page['id']);
                            }
                        }
                    }
                }
            }

            if (!$not_found) { # found
                if ($old_page_id !== $new_page_id) {
                    $user_configs->setReturnCenterPageId($new_page_id);
                    $this->em->persist($user_configs);
                    $this->em->flush();
                }

                return 2; // Page already created
            } else {
                $return_center_page_title = $this->getParameter('RETURN_CENTER_PAGE_NAME');
                $return_center_page_handle = $this->getParameter('RETURN_CENTER_PAGE_HANDLE');

                $create_page = $shopifyApi->createPage([
                    'page'=> [
                        'title'=> $return_center_page_title,
                        'handle'=> $return_center_page_handle,
                        'author'=> $this->getParameter('APP_NAME'),
                        'template_suffix'=> $template_suffix,
                    ],
                ]);

                if (!isset($create_page['error'])) {
                    $user_configs->setReturnCenterPageId($create_page['id']);
                    $this->em->persist($user_configs);
                    $this->em->flush();

                    return 1; // Page created successfully
                }
            }
        } else if ($type === 'delete_return_center_page') {
            $template_suffix = 'easy_return_center';
            $old_page_id = $user_configs->getReturnCenterPageId();
            $not_found = true;

            if ($old_page_id) {
                $get_page = $shopifyApi->getPageById($old_page_id);

                if (!isset($get_page['error'])) {
                    $not_found = false;
                    $new_page_id = $old_page_id;
                }
            } else {
                $get_pages = $shopifyApi->getAllPages();

                if (!isset($get_pages['error'])) {
                    if (!empty($get_pages)) {
                        foreach ($get_pages as $page) {
                            if ($page['template_suffix'] === $template_suffix) {
                                if ($not_found) {
                                    $not_found = false;
                                    $new_page_id = $page['id'];
                                } else {
                                    $shopifyApi->deletePage($page['id']);
                                }
                            }
                        }
                    }
                }
            }

            if (!$not_found) { # found
                $page_deleted = $shopifyApi->deletePage($new_page_id);

                if ($page_deleted === true) {
                    $user_configs->setReturnCenterPageId(null);
                    $this->em->persist($user_configs);
                    $this->em->flush();

                    return 1; // Page deleted successfully
                }
            } else {
                $user_configs->setReturnCenterPageId(null);
                $this->em->persist($user_configs);
                $this->em->flush();

                return 2; // Page not exist
            }
        } else if ($type === 'create_return_center_page_template') {
            $not_found = false;
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {

                $template_key = 'templates/page.easy_return_center.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    if (!$user_configs->getIsReturnCenterPageTemplateCreated()) {
                        $user_configs->setIsReturnCenterPageTemplateCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                    }

                    return 2; // Template already exist
                } else if ($get_asset['code'] === 404) {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $template_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_return_center_template.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        $user_configs->setIsReturnCenterPageTemplateCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return 1;  //Template created successfully
                    }
                }
            }
        } else if ($type === 'delete_return_center_page_template') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_return_center.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    $delete_asset = $shopifyApi->deleteThemeAsset($theme_id, $template_key);

                    if ($delete_asset) {
                        $user_configs->setIsReturnCenterPageTemplateCreated(0);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return 1; // Template deleted successfully
                    }
                } else {
                    return 2; // Template not exist
                }
            }
        }

        return false;
    }
}
