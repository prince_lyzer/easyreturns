<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

use \Firebase\JWT\JWT;
use App\Service\Shopify;
use App\Service\ShopifyApi;
use App\Service\Helper;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\UserProvidedReasons;
use App\Entity\ReturnRequest;
use App\Entity\OrderDetails;
use App\Entity\CustomerDetails;
use App\Entity\OrderLineItems;
use App\Entity\ReturnLineItems;
use App\Entity\Attachments;
use App\Entity\MessageHistory;
use App\Entity\EmailTemplates;
use App\Entity\UserEmailTemplates;
use App\Entity\ReturnImages;
use App\Entity\CronNotifications;
use App\Entity\PaymentOptions;

class ReturnController extends AbstractController
{
    private $logger;
    private $helper;
    private $em;
    private $serializer;

    public function __construct(LoggerInterface $logger, Helper $helper, EntityManagerInterface $em) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())];
        $this->serializer = new Serializer($normalizers);
    }

    public function returnRequests(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');
        $page = $request->get('page');
        $page = trim($page);
        $status = $request->get('status');
        $status = trim($status);
        $search = $request->get('search');
        $search = trim($search);
        $order = $request->get('order');
        $order = trim($order);
        $type = $request->get('type');
        $type = trim($type);

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        $limit = $this->getParameter('PAGE_LIMIT');

        if(!empty($status)) {
            $status = explode(",", $status);
            $status = array_filter($status, function($s) { return in_array($s, [1,2,3,4,5]); });
        }

        $where = '';
        $params = [];
        if($status && count($status) == 1) {
            $where .= "AND rr.return_status = :status ";
            $params['status'] = implode(',', $status);
        }
        else {
            $where .= "AND rr.return_status != :status ";
            $params['status'] = "5";
        }

        if(!empty($type)) {
            $type = explode(",", $type);
            $type = array_filter($type, function($s) { return in_array($s, [1,2]); });
        }

        if($type && count($type) == 1) {
            $where .= "AND rr.return_type = :type ";
            $params['type'] = implode(',', $type);
        }

        if($search !== '') {
            $where .= "AND (rr.id LIKE '%{$search}%' OR od.order_name LIKE '%{$search}%' OR cd.email LIKE '%{$search}%') ";
        }

        $start = trim($request->get('start'));
        $end = trim($request->get('end'));
        // dump($start, $end);die;
        if($start && $end) {
            list($from_datetime, $to_datetime) = $this->getSelectedDateTimeToGmt($start, $end, $user->getIanaTimezone());

            $where .= "AND rr.date_add >= :from_datetime AND rr.date_add <= :to_datetime ";
            $params['from_datetime'] = $from_datetime;
            $params['to_datetime'] = $to_datetime;
        }

        # get total return requests
        $total_return_requests = $this->em->getRepository(ReturnRequest::class)->countReturnRequestsByUserId($user_id, $where, $params);
        $count_all_return_requests = $this->em->getRepository(ReturnRequest::class)->countAllReturnRequestsByUserId($user_id);
        // dump($count_all_return_requests);die;
        list($page, $total_pages, $offset, $limit) = $this->helper->pagination($page, $limit, $total_return_requests);
        list($sort_by, $sort_order) = $this->helper->sortOrder($order);
        // dump($offset, $limit, $sort_by, $sort_order);die;

        $return_requests = $this->em->getRepository(ReturnRequest::class)->getReturnRequestsByUserId($user_id, $offset, $limit, $sort_by, $sort_order, $where, $params);
        // dump($return_requests);die;

        $return_data = array(
            "return_requests"=> $return_requests,
            "total_return_requests"=> (int) $total_return_requests,
            "count_all_return_requests"=> (int) $count_all_return_requests,
            'page'=> $page,
            'selected_status'=> $status,
            'selected_type'=> $type,
            'search'=>$search,
            'total_pages'=> $total_pages
        );
        
        return new JsonResponse(['error'=> '', 'notice'=> '', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function returnRequest(Request $request, $request_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        
        # get return requests per page
        $request_details = $this->em->getRepository(ReturnRequest::class)->findOneBy(['id'=> $request_id, 'userId'=> $user_id]);

        if(!$request_details) {
            return new JsonResponse(['error'=> 'Request Not found', 'data'=> ''], Response::HTTP_OK);
        }

        $order_id = $request_details->getOrderId();
        $customer_id = $request_details->getCustomerId();
        $request_at = $request_details->getDateAdd()->format('Y-m-d H:i:s');
        $request_details = $this->serializer->normalize($request_details, null, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['dateAdd', 'dateUpd']]);
        $request_details['date_add'] = $request_at;

        # get return line items
        $return_line_items =$this->em->getRepository(ReturnLineItems::class)->getReturnLineItemsByReturnId($request_id);
        if(!$return_line_items) {
            return new JsonResponse(['error'=> 'Something went wrong', 'data'=> ''], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        # get order line item details
        $order_line_items_obj = $this->em->getRepository(OrderLineItems::class)->findBy(['orderId'=> $order_id]);
        $order_line_items = $this->serializer->normalize($order_line_items_obj, null, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['dateAdd', 'dateUpd']]);
        $order_line_items = array_column($order_line_items, null, 'line_item_id');

        # line items not returned
        $line_items_not_returned = [];
        $order_line_item_qty_arr = array_column($order_line_items, 'quantity', 'line_item_id');
        $total_order_line_items_qty = array_sum($order_line_item_qty_arr);
        $return_line_items_qty_arr = [];

        foreach($return_line_items as $returned_line_item) { # get return_line_items_qty_arr
            $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
        }

        $total_return_line_items_qty = array_sum($return_line_items_qty_arr);

        if($total_order_line_items_qty - $total_return_line_items_qty > 0) { # full return
            foreach($order_line_item_qty_arr as $line_item_id => $qty) {
                if(!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                    $line_items_not_returned[$line_item_id] = $qty;
                }
                else if($qty - $return_line_items_qty_arr[$line_item_id] > 0){
                    $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                }
            }
        }

        # get order details
        $order_details = $this->em->getRepository(OrderDetails::class)->findOneBy(['id'=> $order_id, 'customerId'=> $customer_id]);

        if(!$order_details) {
            return new JsonResponse(['error'=> 'Something went wrong', 'data'=> ''], Response::HTTP_OK);
        }

        $order_created_at = $order_details->getOrderCreatedAt()->format('Y-m-d H:i:s');
        $order_details = $this->serializer->normalize($order_details, null, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['dateAdd', 'dateUpd']]);
        $order_details['order_created_at'] = $order_created_at;

        # get customer details
        $customer_details = $this->em->getRepository(CustomerDetails::class)->findOneBy(['id'=> $customer_id]);

        if(!$customer_details) {
            return new JsonResponse(['error'=> 'Something went wrong', 'data'=> ''], Response::HTTP_OK);
        }

        $customer_since = '';
        if($customer_details->getCustomerSince()) {
            $customer_since = $this->helper->convertGmtDateTimeToUserDateTime($user->getIanaTimezone(), $customer_details->getCustomerSince()->format('Y-m-d'), 'Y-m-d');
        }
        $customer_details = $this->serializer->normalize($customer_details, null, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['dateAdd', 'dateUpd']]);
        $customer_details['customer_since'] = $customer_since;

        # check for next and prev request
        $has_prev = $this->em->getRepository(ReturnRequest::class)->hasPrev($request_id, $user_id);
        $has_next = $this->em->getRepository(ReturnRequest::class)->hasNext($request_id, $user_id);

        $return_queries = $this->em->getRepository(MessageHistory::class)->findReturnQueriesByReturnId($request_id);
        foreach ($return_queries as $key => $value) {
            $return_queries[$key]['created_at'] = $this->helper->convertGmtDateTimeToUserDateTime($user->getIanaTimezone(), $value['created_at']);
        }

        $return_images = $this->em->getRepository(ReturnImages::class)->findReturnImagesByReturnId($request_id);

        $return_data = array(
            "request_details"=> $request_details,
            "order_details"=> $order_details,
            "customer_details"=> $customer_details,
            "order_line_items"=> $order_line_items,
            "return_line_items"=> $return_line_items,
            "line_items_not_returned"=> $line_items_not_returned,
            "has_prev"=> $has_prev,
            "has_next"=> $has_next,
            "money_format"=> $user->getMoneyFormat(),
            "return_queries"=> $return_queries,
            "return_images"=> $return_images
        );
        
        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }

    public function updateReturnRequestStatus(Request $request, $request_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $return_status = $request->get('status');
        $cancel_reason = $request->get('cancel_reason');
        $cancel_reason = trim($cancel_reason);

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        
        # get return requests per page
        $return_request = $this->em->getRepository(ReturnRequest::class)->findOneBy(['id'=> $request_id, 'userId'=> $user_id]);

        if(!$return_request) {
            return new JsonResponse(['error'=> 'Request Not found', 'data'=> ''], Response::HTTP_NOT_FOUND);
        }

        $return_status = (int) $return_status;
        if(!in_array($return_status, [2, 3, 4, 5])) {
            return new JsonResponse(['error'=> 'Request status is invalid', 'data'=> ''], Response::HTTP_NOT_ACCEPTABLE);
        }

        if($return_status === 4 && $cancel_reason === '') {
            return new JsonResponse(['error'=> 'Cancel reason is required', 'data'=> ''], Response::HTTP_NOT_ACCEPTABLE);
        }

        $order_id = $return_request->getOrderId();
        $customer_id = $return_request->getCustomerId();

        $customer = $this->em->getRepository(CustomerDetails::class)->findOneBy(['id'=> $customer_id]);
        
        # update status
        $return_request->setReturnStatus($return_status);

        if($return_status === 3) {
            $payment_details = $return_request->getPaymentDetails();
            
            if($payment_details) {
                $coupon_code = $request->get('coupon_code');
                $payment_details = json_decode($payment_details, true);
                $payment_type = array_keys($payment_details)[0];
                // dump($payment_details, $coupon_code);die;

                if($payment_type === 'coupon_code') {
                    if(trim($coupon_code) === '') {
                        return new JsonResponse(['error'=> 'Coupon code is required'], Response::HTTP_NOT_ACCEPTABLE);
                    }
                    else {
                        $payment_details['coupon_code'] = ['coupon_code'=> $coupon_code];
                        $return_request->setPaymentDetails(json_encode($payment_details));
                    }
                }
            }

            $attachments = $request->files->all();
            $attachments = $attachments['attachments'] ?? [];
            // dump($attachments);die;
            $attachments_id = [];

            foreach($attachments as $attachment) {
                $attachment_name = time() . "_{$request_id}." . $attachment->guessExtension();
                $attachment->move ($this->getParameter( 'FILE_DIRECTORY' ), $attachment_name );
    
                $attachments_obj = new Attachments();
                $attachments_obj->setOriginalName($attachment->getClientOriginalName());
                $attachments_obj->setName($attachment_name);
                $attachments_obj->setSize($attachment->getSize());
                $attachments_obj->setDateAdd(new \DateTime());
                $this->em->persist($attachments_obj);
                $this->em->flush();
                $attachments_id[] = $attachments_obj->getId();
            }

            if(!empty($attachments_id)) {
                $return_request->setAttachmentsId(implode(',', $attachments_id));
            }

        }

        if($return_status === 4) {
            $return_request->setCanceledReason($cancel_reason);
        }

        $this->em->persist($return_request);
        $this->em->flush();
        
        $request_at = $return_request->getDateAdd()->format('Y-m-d H:i:s');
        $request_details = $this->serializer->normalize($return_request, null, [AbstractNormalizer::IGNORED_ATTRIBUTES => ['dateAdd', 'dateUpd']]);
        $request_details['date_add'] = $request_at;

        # send to customer about status update
        if($return_status !== 5) {
            $mail_id = $return_status;
            $from_name = $this->helper->getShopName($user->getDomain());

            $mail_template = $this->helper->getEmailTemplate(
                $user_id, 
                $mail_id, 
                $request_id, 
                ['name'=> $from_name], 
                ['address'=> $return_request->getNotificationEmail() ?: $customer->getEmail(), 'name'=> $customer->getName()], 
                ['address'=> $user->getCustomerReplyEmail() ? : $user->getCustomerEmail(), 'name'=> $from_name]
            );
        }

        # auto archive after completion
        if($return_status === 3) {
            $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneByUserId($user_id);
    
            if($user_configs->getAutoArchive()) {
                $return_request->setReturnStatus(5);
                $this->em->persist($return_request);
                $this->em->flush();
            }
        }

        $return_data = array("request_details"=> $request_details);

        if($return_status === 4) {
            $return_data['cancel_reason'] = $cancel_reason;
        }

        $notice = 'Request ';
        switch($return_status) {
            CASE 2:
                $notice .= 'accepted';
                break;
            CASE 3:
                $notice .= 'completed';
                break;
            CASE 4:
                $notice .= 'canceled';
                break;
            CASE 5:
                $notice .= 'archived';
                break;
        }

        $notice = $notice !== '' ? ($notice . ' successfully') : '';
        
        return new JsonResponse(['notice'=> $notice, 'data'=> $return_data], Response::HTTP_OK);
    }

    public function getReturnRequestsStateBydate(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        $start = trim($request->get('start'));
        $end = trim($request->get('end'));
        list($from_datetime, $to_datetime) = $this->getSelectedDateTimeToGmt($start, $end, $user->getIanaTimezone());

        # get total return requests
        $return_requests_state = $this->em->getRepository(ReturnRequest::class)->getReturnRequestsStateBtwDate($user_id, $from_datetime, $to_datetime);

        $return_data = array(
            'count'=> ["total_pending_request"=> array_sum(array_column($return_requests_state, 'pending_request')),
            "total_accepted_requests"=> array_sum(array_column($return_requests_state, 'accepted_request')),
            "total_completed_requests"=> array_sum(array_column($return_requests_state, 'completed_request')),
            "total_cancelled_requests"=> array_sum(array_column($return_requests_state, 'cancelled_request'))],
            'return_requests_state'=> $return_requests_state
        );
        
        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }

    public function exportDataAction(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        
        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        
        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }
        
        $user_id = $user->getId();
        $now_obj = new \DateTime();
        
        $start = $request->get('start');
        $start = trim($start);
        $end = $request->get('end');
        $end = trim($end);
        
        if(empty($start) && empty($end)) {
            return new JsonResponse(['error'=> 'Please select dates'], Response::HTTP_BAD_REQUEST);
        }
        
        if($start !== '' && strtotime($start)) {
            $start = $this->helper->convertGmtDateTimeToUserDateTime($user->getIanaTimezone(), $start);
            $start = new \DateTime($start);
            $from_datetime = $start->format('Y-m-d 00:00:00');
            $from_datetime_gmt = $this->helper->convertUserDateTimeToGmt($user->getIanaTimezone(), $from_datetime);
        }
        else {
            $from_datetime_gmt = $now_obj->format('Y-m-d 00:00:00');
        }

        if($end !== '' && strtotime($end)) {
            $end = $this->helper->convertGmtDateTimeToUserDateTime($user->getIanaTimezone(), $end);
            $end = new \DateTime($end); 
            $to_datetime = $end->format('Y-m-d 23:59:59');
            $to_datetime_gmt = $this->helper->convertUserDateTimeToGmt($user->getIanaTimezone(), $to_datetime);
        }
        else {
            $to_datetime_gmt = $now_obj->format('Y-m-d H:i:s');
        }

        # get total return requests
        $export_datas = $this->em->getRepository(ReturnRequest::class)->getExportDataBtwDate($user_id, $from_datetime_gmt, $to_datetime_gmt);

        if(!$export_datas) {
            return new JsonResponse(['notice'=> "No return data between selected dates"], Response::HTTP_OK);
        }

        $attachment_name = "export_by_sku_data_".$from_datetime.' - '.$to_datetime.'.csv';
        $csv_data = $this->helper->createCsvString($export_datas, ['Product Id', 'Variant Id', 'Item SKU', 'Return Reason', 'Count Request']);

        $mail_send = $this->helper->sendMail(
            ['name'=> $this->helper->getShopName($user->getDomain())], 
            ['address'=> $user->getEmail(), 'name'=> $user->getShopOwner()], 
            [],
            [], 
            'Export by Sku data', 
            'Your request for export data by sku have been completed. find below attached csv file.', 
            $csv_data, 
            $attachment_name
        );

        $return_data = array();
        
        return new JsonResponse(['notice'=> "Data will be sent to your email id", 'data'=> $return_data], Response::HTTP_OK);
    }

    public function contactAction(Request $request, $request_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $query = $request_obj['query'];
        $query = trim($query);

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        
        # get return requests per page
        $return_request = $this->em->getRepository(ReturnRequest::class)->findOneBy(['id'=> $request_id, 'userId'=> $user_id]);

        if(!$return_request) {
            return new JsonResponse(['error'=> 'Request Not found', 'data'=> ''], Response::HTTP_NOT_FOUND);
        }

        if($query === '') {
            return new JsonResponse(['error'=> 'Query is required'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $customer_details = $this->em->getRepository(CustomerDetails::class)->findOneById($return_request->getCustomerId());
        if(!$customer_details) {
            return new JsonResponse(['error'=> 'Customer details not found'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $customer_email = $return_request->getNotificationEmail() ?: $customer_details->getEmail();
        if(!$customer_email) {
            return new JsonResponse(['error'=> 'Customer email not found'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $mail_id = 6;
        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=>$mail_id]);

        if($user_email_templates && !$user_email_templates->getActive()) {
            return new JsonResponse(['error'=> 'Query mail is disabled'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $message_history = new MessageHistory();
        $message_history->setReturnId($request_id);
        $message_history->setMessage($query);
        $message_history->setCreatedAt(new \DateTime());
        $this->em->persist($message_history);
        $this->em->flush();

        if($message_history) {
            // send to customer
            $from_name = $this->helper->getShopName($user->getDomain());

            $return_query_mail_send = $this->helper->getEmailTemplate(
                $user_id, 
                $mail_id, 
                $request_id, 
                ['name'=> $from_name],
                ['address'=> $customer_email, 'name'=> $customer_details->getName()], 
                ['address'=> $user->getCustomerReplyEmail() ? : $user->getCustomerEmail(), 'name'=> $from_name], 
                [],
                false, 
                ['query'=> $query]
            );
            // $this->logger->info('return_query_mail_send', [$return_query_mail_send,]);

            $return_queries = $this->em->getRepository(MessageHistory::class)->findReturnQueriesByReturnId($request_id);
            foreach ($return_queries as $key => $value) {
                $return_queries[$key]['created_at'] = $this->helper->convertGmtDateTimeToUserDateTime($user->getIanaTimezone(), $value['created_at']);
            }

            return new JsonResponse(['notice'=> 'Query has been send', 'data'=> ['return_queries'=> $return_queries]], Response::HTTP_OK);
        }
        
        return new JsonResponse([], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function getSelectedDateTimeToGmt($start, $end, $user_timezone) {
        // dump($start, $end);
        $now = new \DateTime();
        $from = $now->format('Y-m-d 00:00');
        $to = $now->format('Y-m-d 23:59');

        if($start !== '' && strtotime($start)) {
            $start = new \DateTime($start);
            $from_datetime = $start->format('Y-m-d 00:00');
        }
        else {
            $from_datetime = $this->helper->convertGmtDateTimeToUserDateTime($user_timezone, $from);
        }

        if($end !== '' && strtotime($end)) {
            $end = new \DateTime($end);
            $to_datetime = $end->format('Y-m-d 23:59');
        }
        else {
            $to_datetime = $this->helper->convertGmtDateTimeToUserDateTime($user_timezone, $to);
        }

        // dump($from_datetime, $to_datetime);

        $from_datetime_gmt = $this->helper->convertUserDateTimeToGmt($user_timezone, $from_datetime);
        $to_datetime_gmt = $this->helper->convertUserDateTimeToGmt($user_timezone, $to_datetime);
        // dump($from_datetime_gmt, $to_datetime_gmt);die;

        return [$from_datetime_gmt, $to_datetime_gmt];
    }

    public function previewContactMailAction(Request $request, $request_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();

        if(empty($request_id)) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $query = $request_obj['query'];

        if(empty($query)) {
            return new JsonResponse(['error'=> 'Query is required'], Response::HTTP_BAD_REQUEST);
        }

        # return request 
        $return_request_id = 12;
        $mail_id = 6;      
        $data['query'] = $query;

        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=>$mail_id]);

        if(!$user_email_templates) {
            $default_email_template = $this->em->getRepository(EmailTemplates::class)->findOneBy(['id'=> $mail_id]);
        }

        $email_body = $user_email_templates ? $user_email_templates->getContent() : $default_email_template->getDefaultContent();
        $subject = $user_email_templates ? $user_email_templates->getSubject() : $default_email_template->getSubject();

        list($subject, $html_content) = $this->helper->getEmailTemplateRaw($mail_id, $user_id, $subject, $email_body, $return_request_id, $data);
        
        $html_content = "<div style='margin-bottom:25px; border-bottom: 1px solid #e6e6e6;font-size: 18px; padding-bottom: 15px;'><strong>Subject: </strong>&nbsp;&nbsp;{$subject}</div>".$html_content;

        return new Response($html_content);
    }

    public function generateExternalTokenAction(Request $request, $request_id) {
        $image_id = trim($request->get('image_id'));
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();
        
        $request_details = $this->em->getRepository(ReturnRequest::class)->findOneBy(['id'=> $request_id, 'userId'=> $user_id]);

        if(!$request_details) {
            return new JsonResponse(['error'=> 'Request Not found', 'data'=> ''], Response::HTTP_OK);
        }

        if($image_id === '' || !is_finite($image_id)) {
            return new JsonResponse(['error'=> 'Image not found', 'data'=> ''], Response::HTTP_OK);
        }

        $order_id = $request_details->getOrderId();
        $customer_id = $request_details->getCustomerId();
        $return_images = $this->em->getRepository(ReturnImages::class)->findReturnImagesByReturnId($request_id);

        $timestamp = time() + 60 * 5; // only 5 minutes
        $token_secret = $order_id . '.' . $customer_id . '.' . $request_id . '.' . $timestamp;
        $token_secret = $this->helper->encrypt($token_secret, $this->getParameter('APP_SECRET'));
        $token_secret = urlencode($token_secret);

        $return_data = array("token"=> $token_secret, "img_id"=> $image_id);
        
        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }

    public function BulkReturnRequestAction(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $return_ids = $request_obj['return_ids'];
        $return_status = $request_obj['status'];

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($return_ids) || !is_array($return_ids)) {
            return new JsonResponse(['error'=> 'Please select return requests for process'], Response::HTTP_NOT_ACCEPTABLE);
        }
        
        $return_status = (int) $return_status;

        if(!in_array($return_status, [2, 3, 5])) {
            return new JsonResponse(['error'=> 'Requested return status is not supported'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $return_data = [];
        $return_requests = $this->em->getRepository(ReturnRequest::class)->getReturnRequestsByUserIdNIds($user_id, implode(',', $return_ids));
        // dump($return_requests);die;

        if(empty($return_requests)) {
            return new JsonResponse(['error'=> 'Selected return requests not found'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $coupon_code_returns = [];
        
        foreach($return_requests as $request_id => $return_request) {
            if($return_status == 2 && $return_request['return_status'] != 1)
                continue;
            else if($return_status == 3 && $return_request['return_status'] != 2)
                continue;
            else if($return_status == 5 && !in_array($return_request['return_status'], [3,4]))
                continue;

            if($return_status == 3) {
                $payment_details = $return_request['payment_details'];

                if($payment_details) {
                    $payment_details = json_decode($payment_details, true);
                    $payment_type = array_keys($payment_details)[0];
                    // dump($payment_details);die;
    
                    if($payment_type === 'coupon_code' && empty($payment_details['coupon_code'])) {
                        $coupon_code_returns[] = $request_id;
                    }
                }
            }
            
            $return_data[] = $request_id;
        }

        if(!empty($coupon_code_returns)) {
            return new JsonResponse(['error'=> 'Some return requests required coupon code for refund', 'data'=> $coupon_code_returns], Response::HTTP_NOT_ACCEPTABLE);
        }
        
        $statusText = '';
        switch($return_status) {
            CASE 2:
                $statusText = 'accepted';
                break;
            CASE 3:
                $statusText = 'completed';
                break;
            CASE 4:
                $statusText = 'canceled';
                break;
            CASE 5:
                $statusText = 'archived';
                break;
        }
            
        if(empty($return_data)) {
            return new JsonResponse(['error'=> "0 Returns ${statusText}."], Response::HTTP_NOT_ACCEPTABLE);
        }
            
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneByUserId($user_id);
        $new_return_status = $return_status;
        if($return_status === 3 && $user_configs->getAutoArchive()) {
            $new_return_status = 5;
        }
            
        $updated_rows = $this->em->getRepository(ReturnRequest::class)->updateReturnStatusInBulk($new_return_status, implode(',', $return_data), $user_id);
        // dump($updated_rows);die;
        
        // $from_name = $this->helper->getShopName($user->getDomain());
        if($return_status !== 5) {
            $mail_id = $return_status;
            $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=> $mail_id]);
            
            if($user_email_templates && !$user_email_templates->getActive()) {
                $this->logger->info("User ${user_id} disabled the mail #${mail_id} notification.");
            }
            else {
                $mail_data = [];
                foreach ($return_data as $request_id) {
                    $cron_notification_obj = new CronNotifications();
                    $cron_notification_obj->setUserId($user_id);
                    $cron_notification_obj->setMailId($mail_id);
                    $cron_notification_obj->setReturnId($request_id);
                    $cron_notification_obj->setToAddress($return_requests[$request_id]['email']);
                    $cron_notification_obj->setToName($return_requests[$request_id]['name']);
                    $cron_notification_obj->setDateAdd(new \DateTime());
                    $this->em->persist($cron_notification_obj);
                    // $mail_data[$request_id] = ['address'=> $return_requests[$request_id]['email'], 'name'=> $return_requests[$request_id]['name']];
                    // $mail_template = $this->helper->getEmailTemplate(
                    //     $user_id, 
                    //     $mail_id, 
                    //     $request_id, 
                    //     ['name'=> $from_name], 
                    //     ['address'=> $return_requests[$request_id]['email'], 'name'=> $return_requests[$request_id]['name']], 
                    //     ['address'=> $user->getCustomerReplyEmail() ? : $user->getCustomerEmail(), 'name'=> $from_name]
                    // );
                }
                $this->em->flush();
    
                // $this->em->getRepository(CronNotifications::class)->insertNotificationsInBulk($user_id, $mail_id, $mail_data);
            }
        }
        
        $total_processed_returns = count($return_data);
        $notice = $total_processed_returns . ($total_processed_returns > 1 ? ' Returns ' : ' Return ') . " ${statusText} successfully";
        
        return new JsonResponse(['notice'=> $notice, 'data'=> $return_data], Response::HTTP_OK);
    }

    public function findOrder(Request $request) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');
        $search_query = $request->get('q');
        $search_query = trim($search_query);

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user->getId();

        if(empty($search_query)) {
            return new JsonResponse(['error'=> 'Shopify order number/id is required.'], Response::HTTP_BAD_REQUEST);
        }

        $shopifyApi = new ShopifyApi($shop, $user->getAccessToken(), $this->getParameter('APP_API_VERSION'), $this->logger);
        $order_data = $shopifyApi->getOrderByOrderNumber(['name'=> $search_query, 'status'=> 'any']);

        // dump($order_data);die;
        if(isset($order_data['error']) || empty($order_data)) {
            return new JsonResponse(['error'=> "Order does not exists."], Response::HTTP_OK);
        }

        if (!isset($order_data['fulfillment_status']) || $order_data['fulfillment_status'] === 'unfulfilled') {
            return new JsonResponse(['error'=> 'The order is not fulfilled yet.', 'data'=> ['id'=> $order_data['id']]], Response::HTTP_OK);
        }

        $order_detail = $this->em->getRepository(OrderDetails::class)->findOneBy(['orderId'=> $order_data['id']]);
        $return_requests = $return_line_items = $line_items_not_returned = $order_line_items = false;

        if ($order_detail) {
            $order_id = $order_detail->getId();
            $order_line_items_obj = $this->em->getRepository(OrderLineItems::class)->findBy(['orderId'=> $order_id]);
            $order_line_items = $this->serializer->normalize($order_line_items_obj, null);
            $order_line_items = array_column($order_line_items, null, 'lineItemId');

            # get return request for this order
            $return_requests = $this->em->getRepository(ReturnRequest::class)->findBy(['orderId'=> $order_id]);

            if ($return_requests) {
                $return_line_items = $this->em->getRepository(ReturnLineItems::class)->getReturnLineItemsByOrderId($order_id);
            }
        }

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $return_methods = explode(',', $user_configs->getReturnMethods());
        $is_cod_order = $this->helper->isCODOrder($order_data['payment_gateway_names']);
        $return_payment_options = [];

        if ($is_cod_order) {
            $payment_options = $user_configs->getReturnPaymentOptions() ? json_decode($user_configs->getReturnPaymentOptions()) : false;
            $return_payment_options = $this->em->getRepository(PaymentOptions::class)->getReturnPaymentOptions(implode("','", $payment_options));
            // dump($return_payment_options);die;
        }

        # get order actual physical line items (other than gift or tip products)
        $orderActualLineItems = [];
        foreach ($order_data['line_items'] as $key => $oline_item) {
            if($oline_item['product_exists']) {
                $orderActualLineItems[] = $oline_item;
            }
        }

        if(empty($orderActualLineItems)) {
            return new JsonResponse(['error'=> 'No physical product exists in this order.', 'data'=> ['id'=> $order_data['id']]], Response::HTTP_NOT_FOUND);
        }

        $orderActualLineItemsQtyArr = array_column($orderActualLineItems, 'quantity', 'id');

        if ($order_data['fulfillment_status'] === 'fulfilled') {
            if (!$return_line_items) { # check line_items for return applicable
                $line_items_not_returned = $orderActualLineItemsQtyArr;
            } else {
                $total_order_line_items_qty = array_sum($orderActualLineItemsQtyArr);
                $return_line_items_qty_arr = [];
                // $return_type = 0; // 1 -> Full return, 0-> No return, 2-> Partial return

                foreach ($return_line_items as $returned_line_item) { # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                $total_return_line_items_qty = array_sum($return_line_items_qty_arr);

                if ($total_order_line_items_qty - $total_return_line_items_qty == 0) { # full return
                    // $return_type = 1;
                } else {
                    // $return_type = 2;
                    foreach ($orderActualLineItemsQtyArr as $line_item_id => $qty) {
                        if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                            $line_items_not_returned[$line_item_id] = $qty;
                        } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                            $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                        }
                    }
                }
            }
        } elseif ($order_data['fulfillment_status'] === 'partial') {
            $line_items_not_fulfilled = $line_items_fulfilled = [];

            foreach ($orderActualLineItems as $line_item) {
                switch ($line_item['fulfillment_status']) {
                    case 'fulfilled':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'];
                        break;
                    case 'partial':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'] - $line_item['fulfillable_quantity'];
                    case null:
                        $line_items_not_fulfilled[$line_item['id']] = $line_item['fulfillable_quantity'];
                        break;
                }
            }

            if (!$return_requests) { # check line_items for return applicable
                $line_items_not_returned = $line_items_fulfilled;
            } else if ($return_line_items) {
                $return_line_items_qty_arr = [];
                // $return_type = 2;

                foreach ($return_line_items as $returned_line_item) {
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                foreach ($line_items_fulfilled as $line_item_id=> $qty) {
                    if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                        $line_items_not_returned[$line_item_id] = $qty;
                    } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                        $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                    }
                }
            }
        }

        // dump($line_items_not_returned);die;

        # items which are fulfilled but not returned by customer
        $return_applicable_line_items = [];
        if (!empty($line_items_not_returned)) {
            # get fulfilled line items fulfillment date
            $line_items_fulfillment = [];
            foreach ($order_data['fulfillments'] as $order_fulfillment) {
                foreach ($order_fulfillment['line_items'] as $line_item) {
                    $line_items_fulfillment[$line_item['id']][] = ['fulfillment_date'=> $order_fulfillment['created_at']];
                }
            }

            # check return acceptable for fulfilled line items (Return applicable Items)
            // dump($line_items_not_returned, $line_items_fulfillment, $user_configs->getReturnAcceptableDays());
            $return_applicable_line_items = $this->helper->checkOrderLineItemsReturnApplicable($line_items_not_returned, $line_items_fulfillment, $user_configs->getReturnAcceptableDays());
        }
        else {
            return new JsonResponse(['error'=> "No return available on this order, may be order is already returned or cancelled.", 'data'=> ['id'=> $order_data['id']]], Response::HTTP_OK);
        }

        // dump($return_applicable_line_items);die;
        if (empty($return_applicable_line_items)) {
            return new JsonResponse(['error'=> "Return expired on this order, because the returns time of {$user_configs->getReturnAcceptableDays()} days has elapsed.", 'data'=> ['id'=> $order_data['id']]], Response::HTTP_OK);
        }

        # returned line items
        $returned_line_items = [];
        if (!empty($return_line_items)) {
            foreach ($return_line_items as $return_line_item) {
                $return_request_id = $return_line_item['return_request_id'];
                $final_returned_item_qty[$return_line_item['order_line_item_id']] = ($final_returned_item_qty[$return_line_item['order_line_item_id']] ?? 0) + $return_line_item['return_qty'];

                if (!isset($returned_line_items[$return_request_id])) {
                    $returned_line_items[$return_request_id] = [
                        'return_status'=> $return_line_item['return_status'],
                        'return_status_label'=> $return_line_item['return_status_label'],
                        'return_type'=> $return_line_item['return_type'],
                        'payment_details'=> json_decode($return_line_item['payment_details'], true),
                        'date_add'=> $return_line_item['date_add'],
                        'canceled_reason'=> $return_line_item['canceled_reason'],
                        'customer_note'=> $return_line_item['customer_note'],
                    ];
                }

                $returned_line_items[$return_request_id]['return_line_items'][] = [
                    'line_item_id'=> $return_line_item['line_item_id'],
                    'return_item_id'=> $return_line_item['return_item_id'],
                    'order_line_item_id'=> $return_line_item['order_line_item_id'],
                    'return_qty'=> $return_line_item['return_qty'],
                    'return_reason'=> $return_line_item['return_reason'],
                ];
            }
        }

        # get order line items
        $line_items = [];
        foreach ($orderActualLineItems as $line_item) {
            $image_url = null;

            if ($order_line_items && array_key_exists($line_item['id'], $order_line_items)) {
                $image_url = $order_line_items[$line_item['id']]['imageUrl'];
            }
            else {
                if ($line_item['variant_title']) { # get image_id of product variant
                    $variant_obj = $shopifyApi->getVariantById($line_item['variant_id']);

                    if(!isset($variant_obj['error']) && !empty($variant_obj['image_id'])) {
                        $image_obj = $shopifyApi->getProductImageById($line_item['product_id'], $variant_obj['image_id']);

                        if(!isset($image_obj['error'])) {
                            $image_url = $image_obj['src'];
                        }
                    }
                } 
                
                if(!isset($image_url)) { # get image of product variant
                    $product_obj = $shopifyApi->getProductById($line_item['product_id'], ['fields'=> 'image']);

                    if(!isset($product_obj['error']) && !empty($product_obj['image'])) {
                        $image_url = $product_obj['image']['src'];
                    }
                }
            }

            $line_items[$line_item['id']] = [
                'id'=> $line_item['id'],
                'product_id'=> $line_item['product_id'],
                'variant_id'=> $line_item['variant_id'],
                'title'=> $line_item['title'] . ($line_item['variant_title'] != '' ? ' - '.$line_item['variant_title'] : ''),
                'sku'=> $line_item['sku'],
                'quantity'=> $line_item['quantity'],
                'price'=> $line_item['price'],
                'src'=> $image_url,
            ];
        }

        $return_reasons = $this->em->getRepository(UserProvidedReasons::class)->getReturnReasonsByUserId($user_id);
        $created_at = new \DateTime($order_data['created_at']);
        $created_at = $created_at->format('Y-m-d H:i:s P');
        
        $return_data = [
            'order_details'=> [
                'id'=> $order_data['id'], 
                'name'=> $order_data['name'], 
                'created_at'=> $created_at,
                'fulfillment_status'=> $order_data['fulfillment_status'],
                'payment_status'=> $order_data['financial_status'],
                'shipping_address'=> $order_data['shipping_address'],
                'customer_details'=> [
                    'id'=> $order_data['customer']['id'],
                    'name'=> ucfirst($order_data['customer']['first_name']) . ' ' . ucwords($order_data['customer']['last_name']),
                    'email'=> $order_data['customer']['email']
                ]
            ],
            'line_items'=> $line_items,
            'return_line_items'=> $return_applicable_line_items,
            'returned_line_items'=> $returned_line_items,
            'return_reasons'=> $return_reasons,
            'is_cod_order'=> $is_cod_order,
            'payment_options'=> $return_payment_options,
            'configs'=> [
                'money_format'=> $user->getMoneyFormat(),
                'has_customer_note'=> $user_configs->getCustomerNote() ?? 0,
                'return_methods'=> $return_methods,
                'req_customer_note'=> $user_configs->getRequiredReturnNote(),
                'exchange_note'=> $user_configs->getExchangeNote() ?? 0,
                'req_exchange_note'=> $user_configs->getRequiredExchangeNote(),
                'image_upload'=> $user_configs->getImageUpload() ?? 0,
                'required_image_upload'=> $user_configs->getRequiredImageUpload() ?? 0,
            ],
            'notification_email'=> $order_data['customer']['email'] ?? ''
        ];
        
        return new JsonResponse(['data'=> $return_data], Response::HTTP_OK);
    }

    public function createReturnAction(Request $request, $order_id) {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if(!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, array('HS256'));
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if(!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();

        $sp_order_id = $order_id;
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $return_items = $request_obj['return_items'];
        $return_method = (int) $request_obj['return_method'];
        $customer_note = trim($request_obj['customer_note']);
        $payment_option = $request_obj['payment_option'];
        $payment_details = $request_obj['payment_details'];
        $notification_email = $request_obj['notification_email'] ?? '';

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if(!in_array($return_method, explode(',', $user_configs->getReturnMethods()))){
            return new JsonResponse(['error'=> 'Return method not supported.'], Response::HTTP_BAD_REQUEST);
        }

        $shopifyApi = new ShopifyApi($shop, $user_settings_obj->getAccessToken(), $this->getParameter('APP_API_VERSION'), $this->logger);

        $order_data = $shopifyApi->getOrderById($sp_order_id);
        if(isset($order_data['error'])) {
            return new JsonResponse(['error'=> 'Order not found.'], Response::HTTP_NOT_FOUND);
        }

        $customer_email = $order_data['customer']['email'];
        $notification_email = trim($notification_email);

        if($customer_email) {
            $notification_email = $customer_email;
        }
        else if(!$this->helper->verifyEmail($notification_email)) {
            return new JsonResponse(['error'=> 'Notification email is not right.'], Response::HTTP_OK);
        }

        if (!isset($order_data['fulfillment_status']) || $order_data['fulfillment_status'] === 'unfulfilled') {
            return new JsonResponse(['error'=> 'Return cannot processed on a unfulfilled order.'], Response::HTTP_OK);
        }

        $return_qty_arr = $return_reason_arr = [];
        foreach ($return_items as $key => $value) {
            $return_qty_arr[$key] = $value['qty'] ?? null;
            $return_reason_arr[$key] = $value['reason'] ?? null;
        }

        # validate customer return line items data
        $user_return_reasons_obj = $this->em->getRepository(UserProvidedReasons::class)->findBy(['userId'=> $user_id, 'type'=> 1]);
        $user_return_reasons = $this->serializer->normalize($user_return_reasons_obj, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'label']]);
        $user_return_reasons = array_column($user_return_reasons, 'label', 'id');

        $actual_return_items = [];

        foreach ($return_qty_arr as $line_item_id=> $return_qty) {
            if ($return_qty > 0) { # check return reason
                $return_reason_id = $return_reason_arr[$line_item_id];
                if (!empty($return_reason_id)) { # check return reason id
                    if (!array_key_exists($return_reason_id, $user_return_reasons)) {
                        return new JsonResponse(['error'=> 'Return Reason is incorrect'], Response::HTTP_OK);
                    } else {
                        $actual_return_items[$line_item_id] = ['qty'=> $return_qty, 'reason'=> $user_return_reasons[$return_reason_id], 'reason_id'=> $return_reason_id];
                    }
                } else {
                    return new JsonResponse(['error'=> 'Return Reason is required'], Response::HTTP_OK);
                }
            }
        }

        if (empty($actual_return_items)) {
            return new JsonResponse(['error'=> "Select an item's quantity to return"], Response::HTTP_OK);
        }

        $order_detail = $this->em->getRepository(OrderDetails::class)->findOneBy(['orderId'=> $sp_order_id]);
        
        $return_requests = $return_line_items = $line_items_not_returned = $order_line_items = false;

        if ($order_detail) {
            $order_id = $order_detail->getId();
            $order_line_items_obj = $this->em->getRepository(OrderLineItems::class)->findBy(['orderId'=> $order_id]);
            $order_line_items = $this->serializer->normalize($order_line_items_obj, null);
            $order_line_items = array_column($order_line_items, null, 'lineItemId');

            # get return request for this order
            $return_requests = $this->em->getRepository(ReturnRequest::class)->findBy(['orderId'=> $order_id]);

            # check for return on this order
            if ($return_requests) {
                $return_line_items = $this->em->getRepository(ReturnLineItems::class)->getReturnLineItemsByOrderId($order_id);
            }
        }

        $has_customer_note = $user_configs->getCustomerNote() ?? 0;

        if($return_method === 1 && ($has_customer_note && $user_configs->getRequiredReturnNote() && $customer_note === '')) {
            return new JsonResponse(['error'=> 'Customer note is required.'], Response::HTTP_OK);
        }
        else if($return_method === 2 && ($user_configs->getExchangeNote() && $user_configs->getRequiredExchangeNote() && $customer_note === '')) {
            return new JsonResponse(['error'=> 'Customer note is required.'], Response::HTTP_OK);
        }

        $is_cod_order = $this->helper->isCODOrder($order_data['payment_gateway_names']);

        $payment_options = $available_payment_options = false;
        $verified_payment_details = [];

        if ($is_cod_order && $return_method === 1) { # get return payment options
            if ($user_configs) {
                $payment_options = $user_configs->getReturnPaymentOptions() ? json_decode($user_configs->getReturnPaymentOptions()) : false;
            }
            if (!in_array($payment_option, $payment_options, true)) {
                return new JsonResponse(['error'=> 'Payment option is not selected.'], Response::HTTP_OK);
            }

            $verified_payment_details = $this->helper->verifyPaymentDetails($payment_details, $payment_option);
            // print_r($verified_payment_details);die;
            // $this->logger->info('verified_payment_details', $verified_payment_details);

            if ($verified_payment_details === false) {
                return new JsonResponse(['error'=> 'Payment details are incorrect'], Response::HTTP_OK);
            } elseif (isset($verified_payment_details['err'])) {
                return new JsonResponse(['error'=> $verified_payment_details['err']], Response::HTTP_OK);
            }
        }

        # get order line items
        $orderActualLineItems = [];
        foreach ($order_data['line_items'] as $key => $oline_item) {
            if($oline_item['product_exists']) {
                $orderActualLineItems[] = $oline_item;
            }
        }

        if(empty($orderActualLineItems)) {
            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_NOT_FOUND);
        }

        // dump($orderActualLineItems);die;
        $orderActualLineItemsQtyArr = array_column($orderActualLineItems, 'quantity', 'id');

        # get returnable line items
        if ($order_data['fulfillment_status'] === 'fulfilled') {
            if (!$return_line_items) {
                # no line items is returned by customer before
                $line_items_not_returned = $orderActualLineItemsQtyArr;
            } else {
                $total_order_line_items_qty = array_sum($orderActualLineItemsQtyArr);
                $return_line_items_qty_arr = [];

                foreach ($return_line_items as $returned_line_item) {
                    # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                $total_return_line_items_qty = array_sum($return_line_items_qty_arr);

                if ($total_order_line_items_qty - $total_return_line_items_qty > 0) {
                    # partial return
                    foreach ($orderActualLineItemsQtyArr as $line_item_id=> $qty) {
                        if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                            $line_items_not_returned[$line_item_id] = $qty;
                        } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                            $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                        }
                    }
                }
            }
        } elseif ($order_data['fulfillment_status'] === 'partial') {
            # line_items not fulfilled
            $line_items_fulfilled = [];

            foreach ($orderActualLineItems as $line_item) {
                switch ($line_item['fulfillment_status']) {
                    case 'fulfilled':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'];
                        break;
                    case 'partial':
                        $line_items_fulfilled[$line_item['id']] = $line_item['quantity'] - $line_item['fulfillable_quantity'];
                        break;
                }
            }

            if (!$return_line_items) {
                # check line_items for return applicable
                $line_items_not_returned = $line_items_fulfilled;
            } else {
                $return_line_items_qty_arr = [];
                $return_type = 2;

                foreach ($return_line_items as $returned_line_item) {
                    # get return_line_items_qty_arr
                    $return_line_items_qty_arr[$returned_line_item['line_item_id']] = ($return_line_items_qty_arr[$returned_line_item['line_item_id']] ?? 0) + $returned_line_item['return_qty'];
                }

                foreach ($line_items_fulfilled as $line_item_id=> $qty) {
                    if (!array_key_exists($line_item_id, $return_line_items_qty_arr)) {
                        $line_items_not_returned[$line_item_id] = $qty;
                    } elseif ($qty - $return_line_items_qty_arr[$line_item_id] > 0) {
                        $line_items_not_returned[$line_item_id] = $qty - $return_line_items_qty_arr[$line_item_id];
                    }
                }
            }
        }

        // dump($line_items_not_returned);die;

        # items which are fulfilled but not returned by customer
        $return_applicable_line_items = [];
        if (!empty($line_items_not_returned)) {
            # Get return acceptable days
            $return_acceptable_days = $user_configs->getReturnAcceptableDays();

            # get fulfilled line items fulfillment date
            $line_items_fulfillment = [];
            foreach ($order_data['fulfillments'] as $order_fulfillment) {
                foreach ($order_fulfillment['line_items'] as $line_item) {
                    $line_items_fulfillment[$line_item['id']][] = ['fulfillment_date'=> $order_fulfillment['created_at']];
                }
            }

            # check return acceptable for fulfilled line items (Return applicable Items)
            $return_applicable_line_items = $this->helper->checkOrderLineItemsReturnApplicable($line_items_not_returned, $line_items_fulfillment, $return_acceptable_days);
        }

        if (empty($return_applicable_line_items)) {
            return new JsonResponse(['error'=> 'No Return available for this order.'], Response::HTTP_OK);
        }

        // dump($return_applicable_line_items);die;

        # get order line items
        $line_items = [];
        foreach ($orderActualLineItems as $line_item) {
            $order_line_item_id = null;
            $image_url = NULL;

            if ($order_line_items && array_key_exists($line_item['id'], $order_line_items)) {
                $image_url = $order_line_items[$line_item['id']]['imageUrl'];
                $order_line_item_id = $order_line_items[$line_item['id']]['id'];
            } else {
                if ($line_item['variant_title']) { # get image_id of product variant
                    $variant_obj = $shopifyApi->getVariantById($line_item['variant_id']);

                    if(!isset($variant_obj['error']) && !empty($variant_obj['image_id'])) {
                        $image_obj = $shopifyApi->getProductImageById($line_item['product_id'], $variant_obj['image_id']);

                        if(!isset($image_obj['error'])) {
                            $image_url = $image_obj['src'];
                        }
                    }
                }

                if(!isset($image_url)) { # get image of product variant
                    $product_obj = $shopifyApi->getProductById($line_item['product_id'], ['fields'=> 'image']);

                    if(!isset($product_obj['error']) && !empty($product_obj['image'])) {
                        $image_url = $product_obj['image']['src'];
                    }
                }
            }

            $line_items[$line_item['id']] = [
                'order_line_item_id'=> $order_line_item_id,
                'id'=> $line_item['id'],
                'product_id'=> $line_item['product_id'],
                'variant_id'=> $line_item['variant_id'],
                'product_title'=> $line_item['title'],
                'variant_title'=> $line_item['variant_title'],
                'sku'=> $line_item['sku'],
                'quantity'=> $line_item['quantity'],
                'price'=> $line_item['price'],
                'src'=> $image_url,
            ];
        }

        // dump($line_items);die;

        # check customer returned line items with return_applicable_line_items
        $not_applicable_return_line_items = [];
        $key = 0;
        foreach ($actual_return_items as $line_item_id => $arr) {
            if (!array_key_exists($line_item_id, $return_applicable_line_items)) { # return not applicable
                $not_applicable_return_line_items[$key]['err'] = -1;
            } elseif ($return_applicable_line_items[$line_item_id] - $arr['qty'] < 0) { # return qty more than applicable
                $not_applicable_return_line_items[$key]['err'] = $return_applicable_line_items[$line_item_id];
            }

            if(!empty($not_applicable_return_line_items[$key])) {
                $not_applicable_return_line_items[$key]['name'] = $line_items[$line_item_id]['product_title'] . ($line_items[$line_item_id]['variant_title'] ? ' - ' . $line_items[$line_item_id]['variant_title'] : '');
            }

            $key++;
        }

        // dump($not_applicable_return_line_items);die;

        if (!empty($not_applicable_return_line_items)) {
            return new JsonResponse(['error'=> 'No return available for these items.', 'data'=> $not_applicable_return_line_items], Response::HTTP_OK);
        }

        # check customer
        $customer_details = $this->em->getRepository(CustomerDetails::class)->findOneBy(['customerId'=> $order_data['customer']['id']]);
        $customer_name = ucfirst($order_data['customer']['first_name']) . ' ' . ucwords($order_data['customer']['last_name']);

        if ($customer_details) {
            $customer_details->setDateUpd(new \DateTime());
        } else { # create new customer
            $customer_details = new CustomerDetails();
            $customer_details->setCustomerId($order_data['customer']['id']);
            $customer_details->setDateAdd(new \DateTime());
            $customer_details->setCustomerSince(new \DateTime($order_data['customer']['created_at']));
        }

        $customer_details->setEmail($customer_email);
        $customer_details->setName($customer_name);
        $customer_details->setPhone($order_data['customer']['phone']);
        $this->em->persist($customer_details);
        $this->em->flush();

        $customer_id = $customer_details->getId();

        # save order details
        if (!$order_detail) {
            $order_detail = new OrderDetails();
            $order_detail->setOrderId($order_data['id']);
            $order_detail->setOrderName($order_data['name']);
            $order_detail->setFulfillmentStatus($order_data['fulfillment_status']);
            $order_detail->setPaymentStatus($order_data['financial_status']);
            $order_detail->setCustomerId($customer_id);
            $order_detail->setOrderCreatedAt(new \DateTime($order_data['created_at']));
            $order_detail->setShippingAddress(json_encode($order_data['shipping_address']));
            $order_detail->setIsCod($is_cod_order);
            $order_detail->setDateAdd(new \DateTime());
            $this->em->persist($order_detail);
            $this->em->flush();
        }

        $order_id = $order_detail->getId();

        # save order line item details
        foreach ($line_items as $line_item_id => $line_item) {
            if (!$line_item['order_line_item_id']) { # line item not in records
                $order_line_items_obj = new OrderLineItems();
                $order_line_items_obj->setOrderId($order_id);
                $order_line_items_obj->setLineItemId($line_item_id);
                $order_line_items_obj->setProductId($line_item['product_id']);
                $order_line_items_obj->setVariantId($line_item['variant_id']);
                $order_line_items_obj->setProductTitle($line_item['product_title']);
                $order_line_items_obj->setVariantTitle($line_item['variant_title']);
                $order_line_items_obj->setPrice($line_item['price']);
                $order_line_items_obj->setQuantity($line_item['quantity']);
                $order_line_items_obj->setItemSku($line_item['sku']);
                $order_line_items_obj->setImageUrl($line_item['src']);
                $order_line_items_obj->setDateAdd(new \DateTime());
                $this->em->persist($order_line_items_obj);
                $this->em->flush();

                $line_items[$line_item_id]['order_line_item_id'] = $order_line_items_obj->getId();
            }
        }

        $return_status = 1;
        if($return_method === 1 && $user_configs->getAutoApproveRefundReturns()) { // accept refund return
            $return_status = 2;
        }
        else if($return_method === 2 && $user_configs->getAutoApproveExchangeReturns()) { // accept exchange return
            $return_status = 2;
        }

        # create return request
        $return_request = new ReturnRequest();
        $return_request->setUserId($user_id);
        $return_request->setSpOrderId($sp_order_id);
        $return_request->setOrderId($order_id);
        $return_request->setCustomerId($customer_id);
        $return_request->setReturnStatus($return_status); // Pending
        $return_request->setPaymentDetails(!empty($verified_payment_details) ? json_encode($verified_payment_details) : NULL);
        $return_request->setCustomerNote($customer_note);
        $return_request->setReturnType($return_method);
        $return_request->setNotificationEmail($notification_email);
        $return_request->setMedium(1);
        $return_request->setDateAdd(new \DateTime());
        $this->em->persist($return_request);
        $this->em->flush();
        $return_request_id = $return_request->getId();

        # save return line items details
        foreach ($actual_return_items as $line_item_id=> $arr) {
            $return_line_items = new ReturnLineItems();
            $return_line_items->setReturnRequestId($return_request_id);
            $return_line_items->setOrderLineItemId($line_items[$line_item_id]['order_line_item_id']);
            $return_line_items->setReturnQty($arr['qty']);
            $return_line_items->setReturnReason($arr['reason']);
            $return_line_items->setReturnReasonId($arr['reason_id']);
            $return_line_items->setDateAdd(new \DateTime());
            $this->em->persist($return_line_items);
            $this->em->flush();
        }

        # send to merchant
        $mail_id = 5;
        $from_name = $this->helper->getShopName($user_settings_obj->getDomain());

        $merchant_new_request_mail_send = $this->helper->getEmailTemplate(
            $user_id, 
            $mail_id, 
            $return_request_id,
            ['name'=> $this->getParameter('APP_NAME')],
            ['address'=> $user_settings_obj->getEmail(), 'name'=> $user_settings_obj->getShopOwner()] 
        );
        $this->logger->info('merchant_new_request_mail_send', [$merchant_new_request_mail_send]);

        # send to customer
        if($return_status === 1) {
            $mail_id = 1;
        }
        else if($return_status === 2) {
            $mail_id = 2;
        }
        
        $customer_confirmation_mail_send = $this->helper->getEmailTemplate(
            $user_id, 
            $mail_id, 
            $return_request_id, 
            ['name'=> $from_name],
            ['address'=> $notification_email, 'name'=> $customer_name], 
            ['address'=> $user_settings_obj->getCustomerReplyEmail() ? : $user_settings_obj->getCustomerEmail(), 'name'=> $from_name] 
        );
        $this->logger->info('customer_confirmation_mail_send', [$customer_confirmation_mail_send,]);

        return new JsonResponse(['notice'=> 'Return is created successfully', 'data'=> ['id'=> $return_request_id]], Response::HTTP_OK);
    }
}
