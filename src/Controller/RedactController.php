<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Service\Shopify;
use App\Service\Helper;

use App\Entity\UserSettings;
use App\Entity\DummyShops;
use App\Entity\ApplicationCharges;
use App\Entity\UserConfigurations;
use App\Entity\DefaultReasons;
use App\Entity\CustomerDetails;

class RedactController extends AbstractController
{
    private $logger;
    private $helper;
    private $api_version;
    private $em;

    public function __construct(LoggerInterface $logger, Helper $helper, ParameterBagInterface $params, EntityManagerInterface $em) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;
        $this->api_version = $params->get('APP_API_VERSION');
    }

    /**
     * @Route("/customer-data-request", name="customer-data-request")
     */
    public function customerDataRequestAction(Request $request)
    {
        $content = $request->getContent();
        $data = json_decode($content, true);
        // $this->logger->info(json_encode($params));

        // {
        //     "shop_id": 954889,
        //     "shop_domain": "snowdevil.myshopify.com",
        //     "orders_requested": [299938, 280263, 220458],
        //     "customer": {
        //       "id": 191167,
        //       "email": "john@email.com",
        //       "phone":  "555-625-1199"
        //     },
        //     "data_request": {
        //       "id": 9999
        //     }
        //   }

        if(!$this->helper->verifyWebhook($content, $request->headers->get('x-shopify-hmac-sha256'), $this->getParameter('APP_SECRET'))) { # verify Hmac
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $shop = $data['shop_domain'];
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);

        if(!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $merchant_id = $data['shop_id'];
        $customer = $data['customer'];
        $orders_requested = $data['orders_requested'];
        $customer_details = $this->em->getRepository(CustomerDetails::class)->findOneBy(['customerId'=> $customer['id'], 'email'=> $customer['email']]);

        $html_content = $this->renderView("mail/redact/customer_data_request_confirmation.html.twig", ['email'=> $customer['email'], 'customer_details'=> $customer_details ? true : false]);
        # send confirmation email
        $confirmation_mail_send = $this->helper->sendMail(
            ['name'=> $this->getParameter('APP_NAME')], 
            ['address'=> $user_settings_obj->getEmail(), 'name'=> $user_settings_obj->getShopOwner()], 
            [],
            [],
            'Customer data request confirmation', 
            $html_content
        );

        if($confirmation_mail_send && $customer_details) {
            // $this->logger->info('data send');
            $customer_data = [[$customer_details->getId(), $customer_details->getName(), $customer_details->getEmail(), $customer_details->getPhone()]];
            $customer_csv_data = $this->helper->createCsvString($customer_data, ['Customer Name', 'Customer Email', 'Customer Phone']);
            $html_content = $this->renderView("mail/redact/customer_data_request.html.twig", ['email'=> $customer['email']]);
            $confirmation_mail_send = $this->helper->sendMail(
                ['name'=> $this->getParameter('APP_NAME')], 
                ['address'=> $user_settings_obj->getEmail(), 'name'=> $user_settings_obj->getShopOwner()], 
                [],
                [],
                'Download requested Customer data', 
                $html_content, 
                $customer_csv_data, 
                'customers_export.csv'
            );
        }

        return new Response('', Response::HTTP_OK);
    }

    /**
     * @Route("/customer-redact", name="customer-redact")
     * customer data erasure request
     */
    public function customerRedactAction(Request $request)
    {
        $content = $request->getContent();
        $data = json_decode($content, true);

        // {
        //     "shop_id": 954889,
        //     "shop_domain": "snowdevil.myshopify.com",
        //     "customer": {
        //       "id": 191167,
        //       "email": "john@email.com",
        //       "phone": "555-625-1199"
        //     },
        //     "orders_to_redact": [299938, 280263, 220458]
        //   }

        # verify Hmac
        if(!$this->helper->verifyWebhook($content, $request->headers->get('x-shopify-hmac-sha256'), $this->getParameter('APP_SECRET'))) { 
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $shop = $data['shop_domain'];
        $merchant_id = $data['shop_id'];
        $customer = $data['customer'];
        $orders_to_redact = $data['orders_to_redact'];

        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'merchantId'=> $merchant_id]);

        if(!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get customer details
        $customer_details = $this->em->getRepository(CustomerDetails::class)->findOneBy(['customerId'=> $customer['id'], 'email'=> $customer['email']]);

        if($customer_details) {
            $customer_details->setEmail(NULL);
            $customer_details->setName(NULL);
            $customer_details->setPhone(NULL);
            $this->em->persist($customer_details);
            $this->em->flush();
        }

        $html_content = $this->renderView("mail/redact/customer_redact_confirmation.html.twig", ['email'=> $customer['email']]);    
        $customer_redact_send = $this->helper->sendMail(
            ['name'=> $this->getParameter('APP_NAME')], 
            ['address'=> $user_settings_obj->getEmail(), 'name'=> $user_settings_obj->getShopOwner()], 
            [],
            [], 
            'Customer data erasure confirmation', 
            $html_content
        );
        return new Response('', Response::HTTP_OK);
    }

    /**
     * @Route("/shop-redact", name="shop-redact")
     */
    public function shopRedactAction(Request $request)
    {
        $content = $request->getContent();
        $data = json_decode($content, true);

        // {
        //     "shop_id": 954889,
        //     "shop_domain": "snowdevil.myshopify.com"
        // }

        # verify Hmac
        if(!$this->helper->verifyWebhook($content, $request->headers->get('x-shopify-hmac-sha256'), $this->getParameter('APP_SECRET'))) { 
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $shop = $data['shop_domain'];
        $merchant_id = $data['shop_id'];
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'merchantId'=> $merchant_id]);

        if(!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $html_content = $this->renderView("mail/redact/shop_redact_confirmation.html.twig", ['shop'=> $shop]);
        $confirmation_mail_send = $this->helper->sendMail(
            ['name'=> $this->getParameter('APP_NAME')], 
            ['address'=> $user_settings_obj->getEmail(), 'name'=> $user_settings_obj->getShopOwner()], 
            [],
            [],
            'Shop data erasure confirmation', 
            $html_content
        );
        return new Response('', Response::HTTP_OK);
    }
}
