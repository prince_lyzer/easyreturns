<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Symfony\Component\Finder\Finder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

use Firebase\JWT\JWT;
use App\Service\Shopify;
use App\Service\Helper;
use App\Service\ShopifyApi;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\UserProvidedReasons;
use App\Entity\PaymentOptions;
use App\Entity\DefaultReturnLabels;

class FrontConfigureController extends AbstractController
{
    private $logger;
    private $helper;
    private $em;
    private $serializer;

    public function __construct(LoggerInterface $logger, Helper $helper, EntityManagerInterface $em) {
        $this->logger = $logger;
        $this->helper = $helper;
        $this->em = $em;

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())];
        $this->serializer = new Serializer($normalizers);
    }

    public function frontConfigure(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();

        # get user configurations
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if ($user_configs) {
            $order_return_page_code = $user_configs->getReturnOrderPageCode();
            $order_return_response_code = $user_configs->getReturnOrderResponseCode();
            $return_page_title = $user_configs->getReturnPageTitle();
            $return_page_handle = $user_configs->getReturnPageHandle();
            $show_return_links = $user_configs->getShowReturnLinks();
            $return_center_page_code = $user_configs->getReturnCenterPageCode();
        }

        $order_return_page_code = $order_return_page_code ?? file_get_contents('../templates/front_api/order_return_page.html.twig');
        $order_return_response_code = $order_return_response_code ?? file_get_contents('../templates/front_api/return_order_response.html.twig');
        $return_center_page_code = $return_center_page_code ?? file_get_contents('../templates/front_api/return_center_page.html.twig');
        $return_page_title = $return_page_title ?? $this->getParameter('RETURN_PAGE_NAME');
        $return_page_handle = $return_page_handle ?? $this->getParameter('RETURN_PAGE_HANDLE');
        $show_return_links = $show_return_links ?? 0;

        $return_data = [
            'order_return_page_code'=> $order_return_page_code,
            'order_return_response_code'=> $order_return_response_code,
            'return_page_title'=> $return_page_title,
            'return_page_handle'=> $return_page_handle,
            'show_return_links'=> $show_return_links,
            'is_return_page_created'=> $user_settings_obj->getReturnPageId() ? 1 : 0,
            'is_return_template_created'=> $user_configs->getIsPageTemplateCreated(),
            'is_style_snippet_created'=> $user_configs->getIsStyleSnippetCreated(),
            'is_script_tag_created'=> $user_settings_obj->getIdScriptTag() ? 1 : 0,
            'password_enabled'=> $user_settings_obj->getIsPasswordEnabled(),
            'return_center_page_code'=> $return_center_page_code,
            'is_return_center_page_created'=> $user_configs->getReturnCenterPageId() ? 1 : 0,
            'is_return_center_page_template_created'=> $user_configs->getIsReturnCenterPageTemplateCreated(),
            'return_center_page_handle'=> $this->getParameter('RETURN_CENTER_PAGE_HANDLE')
        ];

        return new JsonResponse(['msg'=> '', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function frontConfigureAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $order_return_page_code = trim($request_obj['order_return_page_code']);
        $order_return_response_code = trim($request_obj['order_return_response_code']);
        $return_center_page_code = trim($request_obj['return_center_page_code']);
        $return_page_title = trim($request_obj['return_page_title']);
        $return_page_handle = trim($request_obj['return_page_handle']);
        $show_return_links = $request_obj['show_return_links'];

        if (empty($order_return_page_code)) {
            return new JsonResponse(['error'=> 'Order return page code required'], Response::HTTP_BAD_REQUEST);
        }

        if (empty($order_return_response_code)) {
            return new JsonResponse(['error'=> 'Order return response code required'], Response::HTTP_BAD_REQUEST);
        }

        if (empty($return_center_page_code)) {
            return new JsonResponse(['error'=> 'Return center page code required'], Response::HTTP_BAD_REQUEST);
        }

        if ($return_page_title === '' || $return_page_handle === '') {
            return new JsonResponse(['error'=> 'Return page title, handle required'], Response::HTTP_BAD_REQUEST);
        }

        # get user configurations
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $user_configs->setDateUpd(new \DateTime());
        $user_configs->setReturnOrderPageCode($order_return_page_code);
        $user_configs->setReturnOrderResponseCode($order_return_response_code);
        // $user_configs->setReturnPageTitle($return_page_title);
        // $user_configs->setReturnPageHandle($return_page_handle);
        $user_configs->setShowReturnLinks($show_return_links);
        $user_configs->setReturnCenterPageCode($return_center_page_code);
        $this->em->persist($user_configs);
        $this->em->flush();

        $return_data = [];
        return new JsonResponse(['notice'=> 'Configure updated successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function frontConfigureResetAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $reset_code = $request->get('reset_code');
        $reset_code = trim($reset_code);

        if ($reset_code === '') {
            return new JsonResponse(['error'=> 'Required request param is missing'], Response::HTTP_BAD_REQUEST);
        }

        # get user configurations
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if (!$user_configs) {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        if ($reset_code === 'orderReturnPageCode') {
            $order_return_page_code = file_get_contents('../templates/front_api/order_return_page.html.twig');
            $user_configs->setReturnOrderPageCode($order_return_page_code);
            $return_data = [$reset_code=> $order_return_page_code];
        } else if ($reset_code === 'orderReturnResponseCode') {
            $order_return_response_code = file_get_contents('../templates/front_api/return_order_response.html.twig');
            $user_configs->setReturnOrderResponseCode($order_return_response_code);
            $return_data = [$reset_code=> $order_return_response_code];
        } else if ($reset_code === 'returnCenterPageCode') {
            $return_center_page_code = file_get_contents('../templates/front_api/return_center_page.html.twig');
            $user_configs->setReturnCenterPageCode($return_center_page_code);
            $return_data = [$reset_code=> $return_center_page_code];
        } else {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
        }

        $user_configs->setDateUpd(new \DateTime());
        $this->em->persist($user_configs);
        $this->em->flush();

        return new JsonResponse(['notice'=> 'Reset successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function frontConfigureRegenerateAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $type = $request->get('type');
        $type = trim($type);

        if ($type === '') {
            return new JsonResponse(['error'=> 'Required request param is missing'], Response::HTTP_BAD_REQUEST);
        }

        $shop = $user_settings_obj->getMyshopifyDomain();
        $access_token = $user_settings_obj->getAccessToken();
        $shopifyApi = new ShopifyApi($shop, $access_token, $this->getParameter('APP_API_VERSION'), $this->logger);
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if ($type === 'create_return_page') {
            $template_suffix = 'easy_returns';
            $old_page_id = $user_settings_obj->getReturnPageId();
            $not_found = true;

            if ($old_page_id) {
                $get_page = $shopifyApi->getPageById($old_page_id);

                if (!isset($get_page['error'])) {
                    $not_found = false;
                    $new_page_id = $old_page_id;
                }
            } else {
                $get_pages = $shopifyApi->getAllPages();

                if (!isset($get_pages['error'])) {
                    if (!empty($get_pages)) {
                        foreach ($get_pages as $page) {
                            if ($page['template_suffix'] === $template_suffix) {
                                if ($not_found) {
                                    $not_found = false;
                                    $new_page_id = $page['id'];
                                } else {
                                    $shopifyApi->deletePage($page['id']);
                                }
                            }
                        }
                    }
                }
            }

            if (!$not_found) { # found
                if ($old_page_id !== $new_page_id) {
                    $user_settings_obj->setReturnPageId($new_page_id);
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();
                }

                return new JsonResponse(['notice'=> 'Page already created', 'data'=> ['is_return_page_created'=> 1]], Response::HTTP_OK);
            } else {
                $return_page_title = $this->getParameter('RETURN_PAGE_NAME');
                $return_page_handle = $this->getParameter('RETURN_PAGE_HANDLE');

                $create_page = $shopifyApi->createPage([
                    'page'=> [
                        'title'=> $return_page_title,
                        'handle'=> $return_page_handle,
                        'author'=> $this->getParameter('APP_NAME'),
                        'template_suffix'=> $template_suffix,
                    ],
                ]);

                if (!isset($create_page['error'])) {
                    $user_settings_obj->setReturnPageId($create_page['id']);
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();

                    return new JsonResponse(['notice'=> 'Page created successfully', 'data'=> ['is_return_page_created'=> 1]], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'delete_return_page') {
            $template_suffix = 'easy_returns';
            $old_page_id = $user_settings_obj->getReturnPageId();
            $not_found = true;

            if ($old_page_id) {
                $get_page = $shopifyApi->getPageById($old_page_id);

                if (!isset($get_page['error'])) {
                    $not_found = false;
                    $new_page_id = $old_page_id;
                }
            } else {
                $get_pages = $shopifyApi->getAllPages();

                if (!isset($get_pages['error'])) {
                    if (!empty($get_pages)) {
                        foreach ($get_pages as $page) {
                            if ($page['template_suffix'] === $template_suffix) {
                                if ($not_found) {
                                    $not_found = false;
                                    $new_page_id = $page['id'];
                                } else {
                                    $shopifyApi->deletePage($page['id']);
                                }
                            }
                        }
                    }
                }
            }

            if (!$not_found) { # found
                $page_deleted = $shopifyApi->deletePage($new_page_id);

                if ($page_deleted === true) {
                    $user_settings_obj->setReturnPageId(null);
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();

                    return new JsonResponse(['notice'=> 'Page deleted successfully', 'data'=> ['is_return_page_created'=> 0]], Response::HTTP_OK);
                }
            } else {
                $user_settings_obj->setReturnPageId(null);
                $this->em->persist($user_settings_obj);
                $this->em->flush();

                return new JsonResponse(['notice'=> 'Page not exist'], Response::HTTP_OK);
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'create_template') {
            $not_found = false;
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {

                $template_key = 'templates/page.easy_returns.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    if (!$user_configs->getIsPageTemplateCreated()) {
                        $user_configs->setIsPageTemplateCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                    }

                    return new JsonResponse(['notice'=> 'Template already exist', 'data'=> ['is_return_template_created'=> 1]], Response::HTTP_OK);
                } else if ($get_asset['code'] === 404) {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $template_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_returns.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        $user_configs->setIsPageTemplateCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return new JsonResponse(['notice'=> 'Template created successfully', 'data'=> ['is_return_template_created'=> 1]], Response::HTTP_OK);
                    }
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'reset_template') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_returns.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $template_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_returns.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        return new JsonResponse(['notice'=> 'Template reset successfully'], Response::HTTP_OK);
                    }
                } else {
                    return new JsonResponse(['error'=> 'Template not exist'], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'delete_template') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_returns.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    $delete_asset = $shopifyApi->deleteThemeAsset($theme_id, $template_key);

                    if ($delete_asset) {
                        $user_configs->setIsPageTemplateCreated(0);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return new JsonResponse(['notice'=> 'Template deleted successfully', 'data'=> ['is_return_template_created'=> 0]], Response::HTTP_OK);
                    }
                } else {
                    return new JsonResponse(['error'=> 'Template not exist'], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'create_style_snippet') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $asset_key = 'snippets/easy_returns.css.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $asset_key);

                if (!isset($get_asset['error'])) {
                    if (!$user_configs->getIsStyleSnippetCreated()) {
                        $user_configs->setIsStyleSnippetCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                    }
                    return new JsonResponse(['notice'=> 'Snippet already exist', 'data'=> ['is_style_snippet_created'=> 1]], Response::HTTP_OK);
                } else {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $asset_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_returns_style.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        $user_configs->setIsStyleSnippetCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return new JsonResponse(['notice'=> 'Snippet created successfully', 'data'=> ['is_style_snippet_created'=> 1]], Response::HTTP_OK);
                    }
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'reset_style_snippet') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $asset_key = 'snippets/easy_returns.css.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $asset_key);

                if (!isset($get_asset['error'])) {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $asset_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_returns_style.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        return new JsonResponse(['notice'=> 'Snippet reset successfully'], Response::HTTP_OK);
                    }
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'delete_style_snippet') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $asset_key = 'snippets/easy_returns.css.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $asset_key);

                if (!isset($get_asset['error'])) {
                    $delete_asset = $shopifyApi->deleteThemeAsset($theme_id, $asset_key);

                    if ($delete_asset) {
                        $user_configs->setIsStyleSnippetCreated(0);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return new JsonResponse(['notice'=> 'Snippet deleted successfully', 'data'=> ['is_style_snippet_created'=> 0]], Response::HTTP_OK);
                    }
                } else {
                    return new JsonResponse(['error'=> 'Snippet not exist'], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'create_script_tag') {
            $script_src = $this->getParameter('HOST_URL') . 'js/easy_returns.js';
            $old_script_id = $user_settings_obj->getIdScriptTag();

            if ($old_script_id) {
                $script_tag = $shopifyApi->getScriptTagById($old_script_id);
                $new_script_id = $old_script_id;
            } else {
                $script_tag = $shopifyApi->getScriptTagBySrc($script_src);

                if (!isset($script_tag['error'])) {
                    if (count($script_tag) > 1) { # delete other script tags
                        for ($i = 1; $i < count($script_tag); $i++) {
                            $delete_script_tag = $shopifyApi->deleteScriptTag($script_tag[$i]['id']);
                        }
                    } else {
                        $new_script_id = $script_tag[0]['id'];
                    }
                }
            }

            if (!isset($script_tag['error'])) {
                if ($old_script_id !== $new_script_id) {
                    $user_settings_obj->setIdScriptTag($new_script_id);
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();
                }

                return new JsonResponse(['notice'=> 'Script tag already created', 'data'=> ['is_script_tag_created'=> 1]], Response::HTTP_OK);
            } else {
                $script_tag = $shopifyApi->createScriptTag([
                    'script_tag'=> [
                        'event'=> 'onload',
                        'src'=> $script_src,
                    ],
                ]);

                if (!isset($script_tag['error'])) {
                    $script_id = $script_tag['id'];

                    $user_settings_obj->setIdScriptTag($script_id);
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();

                    return new JsonResponse(['notice'=> 'Script tag created successfully', 'data'=> ['is_script_tag_created'=> 1]], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'delete_script_tag') {
            $script_src = $this->getParameter('HOST_URL') . 'js/easy_returns.js';
            $old_script_id = $user_settings_obj->getIdScriptTag();

            if ($old_script_id) {
                $script_tag = $shopifyApi->getScriptTagById($old_script_id);
            } else {
                $script_tag = $shopifyApi->getScriptTagBySrc($script_src);
            }

            if (!isset($script_tag['error'])) {
                $new_script_id = $script_tag['id'];
                $delete_script_tag = $shopifyApi->deleteScriptTag($new_script_id);

                if ($delete_script_tag) {
                    $user_settings_obj->setIdScriptTag(null);
                    $this->em->persist($user_settings_obj);
                    $this->em->flush();

                    return new JsonResponse(['notice'=> 'Script tag deleted successfully', 'data'=> ['is_script_tag_created'=> 0]], Response::HTTP_OK);
                }
            } else {
                return new JsonResponse(['error'=> 'Script tag not exist'], Response::HTTP_OK);
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'create_return_center_page') {
            if(!$user_configs->getGuestCheckout()) {
                return new JsonResponse(['error'=> 'Guest returns should be enabled.'], Response::HTTP_OK);
            }

            $template_suffix = 'easy_return_center';
            $old_page_id = $user_configs->getReturnCenterPageId();
            $not_found = true;

            if ($old_page_id) {
                $get_page = $shopifyApi->getPageById($old_page_id);

                if (!isset($get_page['error'])) {
                    $not_found = false;
                    $new_page_id = $old_page_id;
                }
            } else {
                $get_pages = $shopifyApi->getAllPages();

                if (!isset($get_pages['error']) && !empty($get_pages)) {
                    foreach ($get_pages as $page) {
                        if ($page['template_suffix'] === $template_suffix) {
                            if ($not_found) {
                                $not_found = false;
                                $new_page_id = $page['id'];
                            } else {
                                $shopifyApi->deletePage($page['id']);
                            }
                        }
                    }
                }
            }

            if (!$not_found) { # found
                if ($old_page_id !== $new_page_id) {
                    $user_configs->setReturnCenterPageId($new_page_id);
                    $this->em->persist($user_configs);
                    $this->em->flush();
                }

                return new JsonResponse(['notice'=> 'Page already created', 'data'=> ['is_return_center_page_created'=> 1]], Response::HTTP_OK);
            } else {
                $return_center_page_title = $this->getParameter('RETURN_CENTER_PAGE_NAME');
                $return_center_page_handle = $this->getParameter('RETURN_CENTER_PAGE_HANDLE');

                $create_page = $shopifyApi->createPage([
                    'page'=> [
                        'title'=> $return_center_page_title,
                        'handle'=> $return_center_page_handle,
                        'author'=> $this->getParameter('APP_NAME'),
                        'template_suffix'=> $template_suffix,
                    ],
                ]);

                if (!isset($create_page['error'])) {
                    $user_configs->setReturnCenterPageId($create_page['id']);
                    $this->em->persist($user_configs);
                    $this->em->flush();

                    return new JsonResponse(['notice'=> 'Page created successfully', 'data'=> ['is_return_center_page_created'=> 1]], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'delete_return_center_page') {
            $template_suffix = 'easy_return_center';
            $old_page_id = $user_configs->getReturnCenterPageId();
            $not_found = true;

            if ($old_page_id) {
                $get_page = $shopifyApi->getPageById($old_page_id);

                if (!isset($get_page['error'])) {
                    $not_found = false;
                    $new_page_id = $old_page_id;
                }
            } else {
                $get_pages = $shopifyApi->getAllPages();

                if (!isset($get_pages['error'])) {
                    if (!empty($get_pages)) {
                        foreach ($get_pages as $page) {
                            if ($page['template_suffix'] === $template_suffix) {
                                if ($not_found) {
                                    $not_found = false;
                                    $new_page_id = $page['id'];
                                } else {
                                    $shopifyApi->deletePage($page['id']);
                                }
                            }
                        }
                    }
                }
            }

            if (!$not_found) { # found
                $page_deleted = $shopifyApi->deletePage($new_page_id);

                if ($page_deleted === true) {
                    $user_configs->setReturnCenterPageId(null);
                    $this->em->persist($user_configs);
                    $this->em->flush();

                    return new JsonResponse(['notice'=> 'Page deleted successfully', 'data'=> ['is_return_center_page_created'=> 0]], Response::HTTP_OK);
                }
            } else {
                $user_configs->setReturnCenterPageId(null);
                $this->em->persist($user_configs);
                $this->em->flush();

                return new JsonResponse(['notice'=> 'Page not exist'], Response::HTTP_OK);
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'create_return_center_page_template') {
            if(!$user_configs->getGuestCheckout()) {
                return new JsonResponse(['error'=> 'Guest returns should be enabled.'], Response::HTTP_OK);
            }

            $not_found = false;
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {

                $template_key = 'templates/page.easy_return_center.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    if (!$user_configs->getIsReturnCenterPageTemplateCreated()) {
                        $user_configs->setIsReturnCenterPageTemplateCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                    }

                    return new JsonResponse(['notice'=> 'Template already exist', 'data'=> ['is_return_center_page_template_created'=> 1]], Response::HTTP_OK);
                } else if ($get_asset['code'] === 404) {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $template_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_return_center_template.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        $user_configs->setIsReturnCenterPageTemplateCreated(1);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return new JsonResponse(['notice'=> 'Template created successfully', 'data'=> ['is_return_center_page_template_created'=> 1]], Response::HTTP_OK);
                    }
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'reset_return_center_page_template') {
            if(!$user_configs->getGuestCheckout()) {
                return new JsonResponse(['error'=> 'Guest returns should be enabled.'], Response::HTTP_OK);
            }

            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_return_center.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                        'asset'=> [
                            'key'=> $template_key,
                            'value'=> $this->renderView(
                                'front_configure/easy_return_center_template.html.twig'
                            ),
                        ],
                    ]);

                    if (!isset($create_asset['error'])) {
                        return new JsonResponse(['notice'=> 'Template reset successfully'], Response::HTTP_OK);
                    }
                } else {
                    return new JsonResponse(['error'=> 'Template not exist'], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        } else if ($type === 'delete_return_center_page_template') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_return_center.liquid';
                $get_asset = $shopifyApi->getThemeAsset($theme_id, $template_key);

                if (!isset($get_asset['error'])) {
                    $delete_asset = $shopifyApi->deleteThemeAsset($theme_id, $template_key);

                    if ($delete_asset) {
                        $user_configs->setIsReturnCenterPageTemplateCreated(0);
                        $this->em->persist($user_configs);
                        $this->em->flush();
                        return new JsonResponse(['notice'=> 'Template deleted successfully', 'data'=> ['is_return_center_page_template_created'=> 0]], Response::HTTP_OK);
                    }
                } else {
                    return new JsonResponse(['error'=> 'Template not exist'], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_BAD_REQUEST);
    }

    public function frontConfigurePreviewAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $type = trim($request_obj['type']);
        $order_return_page_code = trim($request_obj['order_return_page_code'] ?? '');
        $order_return_response_code = trim($request_obj['order_return_response_code'] ?? '');
        $return_center_page_code = trim($request_obj['return_center_page_code'] ?? '');

        if ($type === 'orderReturnPageCode' && empty($order_return_page_code)) {
            return new JsonResponse(['error'=> 'Order return page code required'], Response::HTTP_BAD_REQUEST);
        }
        elseif ($type == 'orderReturnResponseCode' && empty($order_return_response_code)) {
            return new JsonResponse(['error'=> 'Order return response code required'], Response::HTTP_BAD_REQUEST);
        }
        elseif ($type == 'returnCenterPageCode' && empty($return_center_page_code)) {
            return new JsonResponse(['error'=> 'Return center page code required'], Response::HTTP_BAD_REQUEST);
        }
        elseif ($type == '') {
            return new JsonResponse(['error'=> 'Something went wrong'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        # get user configurations
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $user_configs->setDateUpd(new \DateTime());
        
        if ($type === 'orderReturnPageCode') {
            $user_configs->setPreviewCode($order_return_page_code);
        } elseif ($type === 'orderReturnResponseCode') {
            $user_configs->setPreviewCode($order_return_response_code);
        } elseif ($type === 'returnCenterPageCode') {
            if(!$user_configs->getGuestCheckout()) {
                return new JsonResponse(['error'=> 'Guest returns should be enabled.'], Response::HTTP_BAD_REQUEST);
            }

            $user_configs->setPreviewCode($return_center_page_code);
        }

        $this->em->persist($user_configs);
        $this->em->flush();

        // $code = $this->forward('App\Controller\FrontApiController::previewOrderReturnPage', ['shop'=> $shop, 'order_id'=> 3664397041837, 'preview'=> 1, 'type'=> 'orderReturnPageCode']);
        // dump($code);die;
        // return $code;
        // $return_data = array();
        return new JsonResponse([], Response::HTTP_OK);
    }

    public function labelTranslate(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();

        # get user configurations
        $default_return_labels = $this->em->getRepository(DefaultReturnLabels::class)->findAll();
        $default_return_labels = $this->serializer->normalize($default_return_labels, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'name', 'description']]);

        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        $return_labels = [];
        if ($user_configs->getReturnLabels()) {
            $return_labels = json_decode($user_configs->getReturnLabels(), true);
        }

        foreach ($default_return_labels as $key=> $return_label) {
            if (array_key_exists($return_label['id'], $return_labels))
                $default_return_labels[$key]['value'] = $return_labels[$return_label['id']];
            else
                $default_return_labels[$key]['value'] = $return_label['name'];
        }

        $return_data = ['return_labels'=> $default_return_labels];

        return new JsonResponse(['msg'=> '', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function labelTranslateAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $return_labels = $request_obj['return_labels'];
        $return_labels = array_column($return_labels, 'value', 'id');

        # get user configurations
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $user_configs->setDateUpd(new \DateTime());
        $user_configs->setReturnLabels(json_encode($return_labels));
        $this->em->persist($user_configs);
        $this->em->flush();

        $return_data = ['return_labels'=> $return_labels];
        return new JsonResponse(['notice'=> 'Changes Saved successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function resetLabelTranslateAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $default_return_labels = $this->em->getRepository(DefaultReturnLabels::class)->findAll();
        $default_return_labels = $this->serializer->normalize($default_return_labels, null, [AbstractNormalizer::ATTRIBUTES=> ['id', 'name']]);
        $return_labels = array_column($default_return_labels, 'name', 'id');
        // dump($return_labels);die;
        # get user configurations
        $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
        $user_configs->setDateUpd(new \DateTime());
        $user_configs->setReturnLabels(json_encode($return_labels));
        $this->em->persist($user_configs);
        $this->em->flush();

        $return_data = [];
        return new JsonResponse(['notice'=> 'Reset successfully', 'data'=> $return_data], Response::HTTP_OK);
    }

    public function frontConfigureUpdateAction(Request $request)
    {
        $bearer_token = $request->headers->get('Authorization');
        $token = $this->helper->getBearerToken($bearer_token);
        $app_secret_key = $this->getParameter('APP_SECRET');

        $signatureValid = $this->helper->verifyTokenSignature($token, $app_secret_key);
        if (!$signatureValid) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $payload = JWT::decode($token, $app_secret_key, ['HS256']);
        $shop = $this->helper->verifyTokenAuthenticity($payload, $this->logger);
        if ($shop === false) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        # get store details
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['myshopifyDomain'=> $shop, 'status'=> 2]);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $user_id = $user_settings_obj->getId();
        $content = $request->getContent();
        $request_obj = json_decode($content, true);
        $requestFor = $request_obj['type'];
        
        if($requestFor === 'toggleReturnLinks') {
            $show_return_links = $request_obj['show_return_links'];
            # get user configurations
            $user_configs = $this->em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);
            $user_configs->setDateUpd(new \DateTime());
            $user_configs->setShowReturnLinks($show_return_links);
            $this->em->persist($user_configs);
            $this->em->flush();
            $return_data = [];
            return new JsonResponse(['notice'=> 'Updated successfully', 'data'=> $return_data], Response::HTTP_OK);
        }

        return new JsonResponse(['error'=> 'Request not allowed'], Response::HTTP_NOT_FOUND);
    }
}
