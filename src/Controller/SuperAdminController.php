<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

use App\Service\ShopifyApi;
use App\Service\Helper;
use App\Entity\FaqAnswers;
use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\AppNotifications;
use App\Entity\CustomerQueries;

class SuperAdminController extends AbstractController
{
    private $serializer;
    private $logger;
    private $helper;

    public function __construct(LoggerInterface $logger, Helper $helper) {
        $encoders = [new JsonEncoder()];
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter())];

        $this->serializer = new Serializer($normalizers);
        $this->logger = $logger;
        $this->helper = $helper;
    }

    public function dashboardAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        // new customer queries
        $count_customer_queries = $em->getRepository(CustomerQueries::class)->countOpenQueries();
        // active shops count
        $count_active_shops = $em->getRepository(UserSettings::class)->countActiveShops();
        
        return $this->render('super_admin/dashboard.html.twig', ['count_customer_queries'=> $count_customer_queries, 'count_active_shops'=> $count_active_shops]);
    }

    public function faqsAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $faq_answers = $em->getRepository(FaqAnswers::class)->findAll();
        $faq_answers = $this->serializer->normalize($faq_answers, null);
      
        return $this->render('super_admin/faqs.html.twig', ['faq_answers'=> $faq_answers]);
    }

    public function addFaq(Request $request, $id): Response
    {
        if($id) {
            $em = $this->getDoctrine()->getManager();
            $faq_answer = $em->getRepository(FaqAnswers::class)->findOneById($id);
            
            if(!$faq_answer) {
                return $this->redirectToRoute('sa_faqs');
            }

            $faq_answer = $this->serializer->normalize($faq_answer, null);
        }

        return $this->render('super_admin/add_faq.html.twig', ['faq_answer'=> $faq_answer ?? null]);
    }

    public function addFaqAction(Request $request, $id): Response
    {
        $question = $request->get('question');
        $answer = $request->get('answer');

        $question = $question ? trim($question) : '';
        $answer = $answer ? trim($answer) : '';

        // dump($question, $answer);die;

        if($question === '') {
            return new Response(json_encode(['err'=> 'Question is required.']));
        }

        $em = $this->getDoctrine()->getManager();
        // dump($id);die;
        if($id) {
            $faq_answer = $em->getRepository(FaqAnswers::class)->findOneById($id);

            if(!$faq_answer) {
                return $this->redirectToRoute('sa_faqs');
            }

            $faq_answer->setDateUpd(new \DateTime());
        }
        else {
            $faq_answer = new FaqAnswers();
            $faq_answer->setDateAdd(new \DateTime());
            $faq_answer->setActive(1);
        }

        $faq_answer->setQuestion($question);
        $faq_answer->setAnswer($answer);

        $em->persist($faq_answer);
        $em->flush();

        if($faq_answer->getId()) {
            return new Response(json_encode(['notice'=> 'Question saved successfully.', 'data'=> ['id'=> $faq_answer->getId()]]));
        }

        return new Response(json_encode(['err'=> 'Something went wrong.']), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function deleteFaqAction(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($id) {
            $faq_answer = $em->getRepository(FaqAnswers::class)->findOneById($id);
            
            if($faq_answer) {
                $em->remove($faq_answer);
                $em->flush();

                if(is_null($faq_answer->getId()))
                    return new Response(json_encode(['notice'=> 'Question deleted successfully.']));
            }
        }

        return new Response(json_encode(['err'=> 'Something went wrong.']), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function shopsAction(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $shops = $em->getRepository(UserSettings::class)->getAllShops();
        $shops = $this->serializer->normalize($shops, null);
        // dump($shops);die;
        $active_shops = array_filter($shops, function($shop) { return $shop['status'] == 2; });
        $inactive_shops = array_filter($shops, function($shop) { return $shop['status'] == 0; });
        $pending_shops = array_filter($shops, function($shop) { return $shop['status'] == 1; });

        return $this->render('super_admin/shops.html.twig', ['active_shops'=> $active_shops, 'inactive_shops'=> $inactive_shops, 'pending_shops'=> $pending_shops]);
    }

    public function shopsByIdAction(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $shop = $em->getRepository(UserSettings::class)->findOneById($id);

        if(!$shop) {
            return $this->redirectToRoute('sa_shops');
        }

        $shop_details = [
            'id'=> $shop->getId(),
            'merchant_id'=> $shop->getMerchantId(),
            'myshopify_domain'=> $shop->getMyshopifyDomain(),
            'domain'=> $shop->getDomain(),
            'status'=> $shop->getStatus(),
            'date_add'=> $shop->getDateAdd(),
            'email'=> $shop->getEmail(),
        ];
        
        $user_configurations = $em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $shop->getId()]);
        
        if($shop->getStatus() == 2) {
            $shopifyApi = new ShopifyApi($shop_details['myshopify_domain'], $shop->getAccessToken(), $this->getParameter('APP_API_VERSION'), $this->logger);
            $count_script_tags = $shopifyApi->countScriptTags();

            if($count_script_tags > 1) {
                $script_tags = $shopifyApi->allScriptTags();
            }

            $webhooks = $shopifyApi->getWebHooks() ? : [];
        }

        $shop_details['count_script_tags'] = $count_script_tags ?? 0;
        $shop_details['script_tags'] = $script_tags ?? [];
        $shop_details['return_page_created'] = $shop->getReturnPageId() ? true : false;
        $shop_details['return_page_template_created'] = $user_configurations->getIsPageTemplateCreated() ? true : false;
        $shop_details['style_snippet_created'] = $user_configurations->getIsStyleSnippetCreated() ? true : false;
        $shop_details['script_tag_created'] = $shop->getIdScriptTag() ? true : false;
        $shop_details['return_center_page_created'] = $user_configurations->getReturnCenterPageId() ? true : false;
        $shop_details['return_center_page_template_created'] = $user_configurations->getIsReturnCenterPageTemplateCreated() ? true : false;
        $shop_details['webhooks'] = $webhooks ?? NULL;

        return $this->render('super_admin/shop.html.twig', ['shop'=> $shop_details ?? null, 'webhooks'=> $webhooks ?? null]);
    }

    public function shopActions(Request $request): Response
    {
        $action_type = $request->get('action_type');
        $em = $this->getDoctrine()->getManager();

        if($action_type === 'update_script_tags') {
            $active_shops = $em->getRepository(UserSettings::class)->getActiveShopsScriptTagIds();
            // dump($active_shops);die;
    
            if(!$active_shops) {
                return new Response(json_encode(['notice'=> 'No active shops found.']));
            }
    
            $v = time();
            $updated_shop = $not_updated_shops = [];
    
            foreach ($active_shops as $key => $shop) {
                $shopifyApi = new ShopifyApi($shop['myshopify_domain'], $shop['access_token'], $this->getParameter('APP_API_VERSION'), $this->logger);
                
                $is_updated = $shopifyApi->updateScriptTag($shop['id_script_tag'], ["script_tag"=> ["src"=> $this->getParameter('HOST_URL') . "js/easy_returns.js?v={$v}"]]);
    
                if(!isset($is_updated['error'])) {
                    $updated_shop[$shop['id']] = $shop['myshopify_domain'];
                }
                else {
                    $not_updated_shop[$shop['id']] = $shop['myshopify_domain'];
                }
            }
    
            return new JsonResponse(['notice'=> 'Shop script tag operation completed', 'data'=> ['updated_shop'=> $updated_shop, 'not_updated_shop'=> $not_updated_shops]], Response::HTTP_OK);
        }

        $user_id = $request->get('user_id');
        $user_settings_obj = $em->getRepository(UserSettings::class)->findOneById($user_id);
        if (!$user_settings_obj) {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $shop = $user_settings_obj->getMyshopifyDomain();
        $access_token = $user_settings_obj->getAccessToken();
        $shopifyApi = new ShopifyApi($shop, $access_token, $this->getParameter('APP_API_VERSION'), $this->logger);
        $user_configs = $em->getRepository(UserConfigurations::class)->findOneBy(['userId'=> $user_id]);

        if($action_type === 'style_snippet') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $asset_key = 'snippets/easy_returns.css.liquid';

                $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                    'asset'=> [
                        'key'=> $asset_key,
                        'value'=> $this->renderView(
                            'front_configure/easy_returns_style.html.twig'
                        ),
                    ],
                ]);

                if (!isset($create_asset['error'])) {
                    $user_configs->setIsStyleSnippetCreated(1);
                    $em->persist($user_configs);
                    $em->flush();
                    return new JsonResponse(['notice'=> 'Style Snippet created successfully', 'data'=> ['is_style_snippet_created'=> 1]], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        else if($action_type === 'script_tag') {
            $v = time();
            $script_src = $this->getParameter('HOST_URL') . "js/easy_returns.js?v={$v}";
            $script_tag = $shopifyApi->createScriptTag(['script_tag'=> ['event'=> 'onload', 'src'=> $script_src]]);

            if (!isset($script_tag['error'])) {
                $user_settings_obj->setIdScriptTag($script_tag['id']);
                $em->persist($user_settings_obj);
                $em->flush();

                return new JsonResponse(['notice'=> 'Script tag created successfully', 'data'=> ['is_script_tag_created'=> 1]], Response::HTTP_OK);
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        else if($action_type === 'return_page_template') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_returns.liquid';
                
                $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                    'asset'=> [
                        'key'=> $template_key,
                        'value'=> $this->renderView(
                            'front_configure/easy_returns.html.twig'
                        ),
                    ],
                ]);

                if (!isset($create_asset['error'])) {
                    $user_configs->setIsPageTemplateCreated(1);
                    $em->persist($user_configs);
                    $em->flush();
                    return new JsonResponse(['notice'=> 'Return page Template created successfully', 'data'=> ['is_return_template_created'=> 1]], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        else if($action_type === 'return_center_page_template') {
            $theme_id = $shopifyApi->getStoreThemeId();

            if (!isset($theme_id['error'])) {
                $template_key = 'templates/page.easy_return_center.liquid';
               
                $create_asset = $shopifyApi->createThemeAsset($theme_id, [
                    'asset'=> [
                        'key'=> $template_key,
                        'value'=> $this->renderView(
                            'front_configure/easy_return_center_template.html.twig'
                        ),
                    ],
                ]);

                if (!isset($create_asset['error'])) {
                    $user_configs->setIsReturnCenterPageTemplateCreated(1);
                    $em->persist($user_configs);
                    $em->flush();
                    return new JsonResponse(['notice'=> 'Return Center Template created successfully', 'data'=> ['is_return_center_page_template_created'=> 1]], Response::HTTP_OK);
                }
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        else if($action_type === 'delete_script_tag') {
            $id = $request->get('id');
            $delete_script_tag = $shopifyApi->deleteScriptTag($id);

            if ($delete_script_tag) {
                return new JsonResponse(['notice'=> 'Script tag deleted successfully', 'data'=> ['is_script_tag_created'=> 0]], Response::HTTP_OK);
            }

            return new JsonResponse(['error'=> 'Something went wrong.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        return new Response(json_encode(['err'=> 'Bad request.']), Response::HTTP_BAD_REQUEST);
    }

    public function appNotifications(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $app_notifications = $em->getRepository(AppNotifications::class)->findAll();
        $app_notifications = $this->serializer->normalize($app_notifications, null);
      
        return $this->render('super_admin/app_notifications.html.twig', ['notifications'=> $app_notifications]);
    }

    public function appNotificationByIdAction(Request $request, $id): Response
    {
        if($id !== 'new') {
            $em = $this->getDoctrine()->getManager();
            $notification = $em->getRepository(AppNotifications::class)->findOneById($id);
            
            if(!$notification) {
                return $this->redirectToRoute('sa_app_notifications');
            }

            
            $notification = $this->serializer->normalize($notification, null);
            $notification['actions'] = $notification['actions'] ? json_decode($notification['actions'], true) : null;
        }

        return $this->render('super_admin/add_app_notification.html.twig', ['notification'=> $notification ?? null]);
    }

    public function addAppNotificationAction(Request $request, $id): Response
    {
        $id = $id === 'new' ? null : $id;
        $heading = $request->get('heading');
        $title = $request->get('title');
        $content = $request->get('content');
        $type = $request->get('type');
        $is_active = $request->get('is_active');
        $action = $request->get('action');
        $is_active = $is_active === 'on' ? 1 : 0;
        
        if(trim($action['content']) == '') {
            $action = null;
        }
        
        // dump($action);die;
        if(trim($content) === '') {
            return new Response(json_encode(['err'=> 'Content is required.']));
        }

        $em = $this->getDoctrine()->getManager();
        if($id) {
            $app_notifications = $em->getRepository(AppNotifications::class)->findOneById($id);

            if(!$app_notifications) {
                return new Response(json_encode(['err'=> 'Not Found.']), Response::HTTP_NOT_FOUND);
            }

            $app_notifications->setDateUpd(new \DateTime());
        }
        else {
            $app_notifications = new AppNotifications();
            $app_notifications->setDateAdd(new \DateTime());
        }

        $app_notifications->setHeading($heading);
        $app_notifications->setTitle($title);
        $app_notifications->setContent($content);
        $app_notifications->setType($type);
        $app_notifications->setIsActive($is_active);
        $app_notifications->setActions($action ? json_encode($action) : null);

        $em->persist($app_notifications);
        $em->flush();

        if($app_notifications->getId()) {
            return new Response(json_encode(['notice'=> 'Notification saved successfully.', 'data'=> ['id'=> $app_notifications->getId()]]));
        }

        return new Response(json_encode(['err'=> 'Something went wrong.']), Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function deleteAppNotificationAction(Request $request, $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($id) {
            $notification = $em->getRepository(AppNotifications::class)->findOneById($id);
            
            if($notification) {
                $em->remove($notification);
                $em->flush();

                if(is_null($notification->getId()))
                    return new Response(json_encode(['notice'=> 'Notification deleted successfully.']));
            }
        }

        return new Response(json_encode(['err'=> 'Not Found.']), Response::HTTP_NOT_FOUND);
    }

    public function customerQueries(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $customer_queries = $em->getRepository(CustomerQueries::class)->getQueries();
        // $customer_queries = $this->serializer->normalize($customer_queries, null);
      
        return $this->render('super_admin/customer_queries.html.twig', ['customer_queries'=> $customer_queries]);
    }

    public function customerQueryByIdAction(Request $request, $id): Response
    {
        if($id !== '') {
            $em = $this->getDoctrine()->getManager();
            $customer_query = $em->getRepository(CustomerQueries::class)->findOneById($id);
            
            if(!$customer_query) {
                return $this->redirectToRoute('sa_customer_queries');
            }
            
            $customer_query = $this->serializer->normalize($customer_query, null);
        }

        return $this->render('super_admin/view_customer_query.html.twig', ['customer_query'=> $customer_query ?? null]);
    }

    public function customerQueryReplyAction(Request $request, $id): Response
    {
        $id = $id === '' ? null : $id;
        $message = $request->get('message');

        if(trim($message) === '') {
            return new Response(json_encode(['err'=> 'Message is required.']));
        }

        $em = $this->getDoctrine()->getManager();
        $customer_query = $em->getRepository(CustomerQueries::class)->findOneById($id);

        if(!$customer_query) {
            return $this->redirectToRoute('sa_customer_queries');
        }

        $customer_query->setStatus(1);
        $em->persist($customer_query);
        $em->flush();

        $subject = $customer_query->getSubject() != '' ? $customer_query->getSubject() : 'Your Query';

        $html_content = $this->renderView('mail/reply_to_customer_query.html.twig', ['name'=> $customer_query->getName(), 'query'=> $customer_query->getQuery(), 'reply'=> $message, 'created_at'=> $customer_query->getCreatedAt()]);
        
        $mail_send = $this->helper->sendMail(['name'=> $this->getParameter('APP_NAME')], ['address'=> $customer_query->getEmail(), 'name'=> $customer_query->getName()], [], [], "Reply to: ${subject}", $html_content);

        return new Response(json_encode(['notice'=> 'Reply send to customer successfully.']), Response::HTTP_OK);
    }

    public function updateCustomerQueryAction(Request $request, $id): Response
    {
        $id = $id === '' ? null : $id;
        $status_to = $request->get('status_to');

        if(trim($status_to) === '') {
            return new Response(json_encode(['err'=> 'Something went wrong.']), HTTP_INTERNAL_SERVER_ERROR);
        }

        $em = $this->getDoctrine()->getManager();
        $customer_query = $em->getRepository(CustomerQueries::class)->findOneById($id);

        if(!$customer_query) {
            return $this->redirectToRoute('sa_customer_query', ['id'=> $id]);
        }

        $customer_query->setStatus($status_to);
        $em->persist($customer_query);
        $em->flush();

        return new Response(json_encode(['notice'=> 'Query status updated successfully.']), Response::HTTP_OK);
    }
}
