<?php

namespace App\Repository;

use App\Entity\OrderDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderDetails[]    findAll()
 * @method OrderDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderDetails::class);
    }

    // /**
    //  * @return OrderDetails[] Returns an array of OrderDetails objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderDetails
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getOrderDetailsByReturnId($return_request_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT od.order_name, od.order_id sp_order_id, od.customer_id, cd.email, cd.name, od.is_cod, rr.payment_details, rr.canceled_reason, rr.customer_note, rr.return_status, rr.attachments_id, rr.return_type  
        FROM return_request rr 
        INNER JOIN order_details od ON od.id = rr.order_id
        INNER JOIN customer_details cd ON cd.id = rr.customer_id
        WHERE rr.id = :id LIMIT 1";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam('id', $return_request_id);
        $stmt->execute();

        return $stmt->fetch();
    }
}
