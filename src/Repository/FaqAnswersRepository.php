<?php

namespace App\Repository;

use App\Entity\FaqAnswers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FaqAnswers|null find($id, $lockMode = null, $lockVersion = null)
 * @method FaqAnswers|null findOneBy(array $criteria, array $orderBy = null)
 * @method FaqAnswers[]    findAll()
 * @method FaqAnswers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaqAnswersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FaqAnswers::class);
    }

    // /**
    //  * @return FaqAnswers[] Returns an array of FaqAnswers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FaqAnswers
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAllFaqs() {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT id, question FROM faq_answers WHERE active = :active";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('active', 1);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
