<?php

namespace App\Repository;

use App\Entity\ReturnImages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReturnImages|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnImages|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnImages[]    findAll()
 * @method ReturnImages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnImagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnImages::class);
    }

    // /**
    //  * @return ReturnImages[] Returns an array of ReturnImages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReturnImages
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findReturnImagesByReturnId($return_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT id, original_name, name, size FROM return_images WHERE return_id = :return_id";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam('return_id', $return_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
