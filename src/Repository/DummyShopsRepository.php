<?php

namespace App\Repository;

use App\Entity\DummyShops;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DummyShops|null find($id, $lockMode = null, $lockVersion = null)
 * @method DummyShops|null findOneBy(array $criteria, array $orderBy = null)
 * @method DummyShops[]    findAll()
 * @method DummyShops[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DummyShopsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DummyShops::class);
    }

    // /**
    //  * @return DummyShops[] Returns an array of DummyShops objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DummyShops
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
