<?php

namespace App\Repository;

use App\Entity\OrderLineItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderLineItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderLineItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderLineItems[]    findAll()
 * @method OrderLineItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderLineItemsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderLineItems::class);
    }

    // /**
    //  * @return OrderLineItems[] Returns an array of OrderLineItems objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderLineItems
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
