<?php

namespace App\Repository;

use App\Entity\ReturnStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReturnStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnStatus[]    findAll()
 * @method ReturnStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnStatus::class);
    }

    // /**
    //  * @return ReturnStatus[] Returns an array of ReturnStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReturnStatus
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
