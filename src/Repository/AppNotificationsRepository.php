<?php

namespace App\Repository;

use App\Entity\AppNotifications;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AppNotifications|null find($id, $lockMode = null, $lockVersion = null)
 * @method AppNotifications|null findOneBy(array $criteria, array $orderBy = null)
 * @method AppNotifications[]    findAll()
 * @method AppNotifications[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppNotificationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AppNotifications::class);
    }

    // /**
    //  * @return AppNotifications[] Returns an array of AppNotifications objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AppNotifications
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
