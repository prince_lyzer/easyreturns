<?php

namespace App\Repository;

use App\Entity\DefaultReturnLabels;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DefaultReturnLabels|null find($id, $lockMode = null, $lockVersion = null)
 * @method DefaultReturnLabels|null findOneBy(array $criteria, array $orderBy = null)
 * @method DefaultReturnLabels[]    findAll()
 * @method DefaultReturnLabels[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefaultReturnLabelsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DefaultReturnLabels::class);
    }

    // /**
    //  * @return DefaultReturnLabels[] Returns an array of DefaultReturnLabels objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DefaultReturnLabels
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
