<?php

namespace App\Repository;

use App\Entity\UserSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserSettings[]    findAll()
 * @method UserSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserSettingsRepository extends ServiceEntityRepository
{
    private $em;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserSettings::class);
        $this->em = $this->getEntityManager();
    }

    // /**
    //  * @return UserSettings[] Returns an array of UserSettings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserSettings
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getShopDetails($myshopify_domain)
    {
        $conn = $this->em->getConnection();
        $sql = "SELECT id, `status`, access_token FROM user_settings WHERE myshopify_domain = :myshopify_domain AND status != :status LIMIT 1";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':myshopify_domain', $myshopify_domain);
        $stmt->bindValue(':status', 0);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function getAllShops()
    {
        $conn = $this->em->getConnection();
        $sql = "SELECT id, merchant_id, status, myshopify_domain, domain, date_add, date_upd FROM user_settings ORDER BY status DESC, date_add DESC";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getActiveShopsScriptTagIds()
    {
        $conn = $this->em->getConnection();
        $sql = "SELECT id, myshopify_domain, access_token, id_script_tag FROM user_settings WHERE status = :status";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':status', 2);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function countActiveShops()
    {
        $conn = $this->em->getConnection();
        $sql = "SELECT count(*) FROM user_settings WHERE status = :status";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(':status', 2);
        $stmt->execute();

        return $stmt->fetchColumn();
    }
}
