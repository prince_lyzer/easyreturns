<?php

namespace App\Repository;

use App\Entity\CronNotifications;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CronNotifications|null find($id, $lockMode = null, $lockVersion = null)
 * @method CronNotifications|null findOneBy(array $criteria, array $orderBy = null)
 * @method CronNotifications[]    findAll()
 * @method CronNotifications[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronNotificationsRepository extends ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CronNotifications::class);
        $this->em = $this->getEntityManager();
    }

    // /**
    //  * @return CronNotifications[] Returns an array of CronNotifications objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CronNotifications
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAllCronNotifications()
    {
        $conn = $this->em->getConnection();
        $sql = "SELECT user_id, id, mail_id, return_id, to_address, to_name FROM cron_notifications ORDER BY date_add ASC";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_ASSOC);
    }

    public function deleteCronNotificationsByIds($ids)
    {
        $conn = $this->em->getConnection();
        $sql = "DELETE FROM cron_notifications WHERE id IN (${ids})";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->rowCount();
    }
}
