<?php

namespace App\Repository;

use App\Entity\DefaultReasons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DefaultReasons|null find($id, $lockMode = null, $lockVersion = null)
 * @method DefaultReasons|null findOneBy(array $criteria, array $orderBy = null)
 * @method DefaultReasons[]    findAll()
 * @method DefaultReasons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DefaultReasonsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DefaultReasons::class);
    }

    // /**
    //  * @return DefaultReasons[] Returns an array of DefaultReasons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DefaultReasons
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
