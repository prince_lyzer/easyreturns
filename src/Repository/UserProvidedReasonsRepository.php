<?php

namespace App\Repository;

use App\Entity\UserProvidedReasons;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserProvidedReasons|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProvidedReasons|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProvidedReasons[]    findAll()
 * @method UserProvidedReasons[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProvidedReasonsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserProvidedReasons::class);
    }

    // /**
    //  * @return UserProvidedReasons[] Returns an array of UserProvidedReasons objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserProvidedReasons
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    // delete all return reason
    public function deleteReturnReasonsByUserId($user_id, $type = 1)
    {
        $sql =
            'DELETE upr FROM user_provided_reasons AS upr WHERE upr.user_id = :user_id AND upr.type = :type';

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':type', $type);
        $stmt->execute();
    }

    public function getReturnReasonsByUserId($user_id, $type = 1)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT label, id AS value FROM user_provided_reasons WHERE user_id= :user_id AND type = :type";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':type', $type);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
