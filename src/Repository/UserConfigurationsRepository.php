<?php

namespace App\Repository;

use App\Entity\UserConfigurations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserConfigurations|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserConfigurations|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserConfigurations[]    findAll()
 * @method UserConfigurations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserConfigurationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserConfigurations::class);
    }

    // /**
    //  * @return UserConfigurations[] Returns an array of UserConfigurations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserConfigurations
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
