<?php

namespace App\Repository;

use App\Entity\CustomerQueries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomerQueries|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerQueries|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerQueries[]    findAll()
 * @method CustomerQueries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerQueriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerQueries::class);
    }

    // /**
    //  * @return CustomerQueries[] Returns an array of CustomerQueries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomerQueries
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    function getQueries() {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT id, name, email, status, subject, created_at FROM customer_queries ORDER BY created_at DESC, status ASC";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    function countOpenQueries() {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT count(*) FROM customer_queries WHERE status = :status";
        
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(":status", 0);
        $stmt->execute();
        return $stmt->fetchColumn();
    }
}
