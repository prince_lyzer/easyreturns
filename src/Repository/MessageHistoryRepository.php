<?php

namespace App\Repository;

use App\Entity\MessageHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MessageHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageHistory[]    findAll()
 * @method MessageHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessageHistory::class);
    }

    // /**
    //  * @return MessageHistory[] Returns an array of MessageHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessageHistory
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findReturnQueriesByReturnId($return_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT id, message, created_at FROM message_history WHERE return_id = :return_id ORDER BY created_at DESC";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam('return_id', $return_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
