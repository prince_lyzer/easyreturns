<?php

namespace App\Repository;

use App\Entity\ReturnLineItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReturnLineItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnLineItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnLineItems[]    findAll()
 * @method ReturnLineItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnLineItemsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnLineItems::class);
    }

    // /**
    //  * @return ReturnLineItems[] Returns an array of ReturnLineItems objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReturnLineItems
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getReturnLineItemsByOrderId($order_id)//: ?ReturnLineItems
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT rr.id AS return_request_id, rr.return_status, rr.payment_details, rs.label return_status_label, rr.canceled_reason, rr.customer_note, rr.return_type, ril.id AS return_item_id, ril.order_line_item_id, oli.line_item_id, ril.return_qty, ril.return_reason, ril.return_reason_id, ril.date_add 
        FROM return_line_items ril 
        INNER JOIN return_request rr ON rr.order_id = :order_id 
        INNER JOIN order_line_items oli ON ril.order_line_item_id = oli.id 
        LEFT JOIN return_status rs ON rs.id = rr.return_status 
        WHERE ril.return_request_id = rr.id";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam('order_id', $order_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getReturnLineItemsByReturnId($return_request_id)//: ?ReturnLineItems
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT ril.id, ril.return_request_id, ril.order_line_item_id, ril.return_qty, ril.return_reason, ril.date_add, oli.line_item_id 
        FROM return_line_items ril 
        LEFT JOIN order_line_items oli ON ril.order_line_item_id = oli.id 
        WHERE ril.return_request_id = :return_request_id";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam('return_request_id', $return_request_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getReturnLineItemDetailsByReturnId($return_request_id)//: ?ReturnLineItems
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT rli.id, rli.order_line_item_id, rli.return_qty, rli.return_reason, oli.product_title, oli.variant_title, oli.price, oli.item_sku, oli.image_url  
                FROM return_line_items rli 
                LEFT JOIN order_line_items oli ON oli.id = rli.order_line_item_id 
                WHERE rli.return_request_id = :return_request_id";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam('return_request_id', $return_request_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
