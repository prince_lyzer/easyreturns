<?php

namespace App\Repository;

use App\Entity\ReturnRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ReturnRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnRequest[]    findAll()
 * @method ReturnRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnRequest::class);
    }

    // /**
    //  * @return ReturnRequest[] Returns an array of ReturnRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReturnRequest
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function countReturnRequestsByUserId($user_id, $where = '', $params = []) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT count(rr.id) 
        FROM return_request rr 
        INNER JOIN order_details od ON od.id = rr.order_id AND od.customer_id = rr.customer_id 
        INNER JOIN customer_details cd ON cd.id = rr.customer_id 
        WHERE rr.user_id = :user_id $where";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue($key, $value);
            }
        }

        $stmt->execute();
        return $stmt->fetchColumn();
    }

    public function countAllReturnRequestsByUserId($user_id) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT count(rr.id) FROM return_request rr WHERE rr.user_id = :user_id AND rr.return_status != :status";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindValue(':status', 5);
        $stmt->execute();
        return $stmt->fetchColumn();
    }

    public function getReturnRequestsByUserId($user_id, $offset, $limit, $sort_by, $sort_order, $where = '', $params = []) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT rr.id, rr.sp_order_id, rr.order_id, rr.customer_id, rr.return_status, rr.date_add, rr.return_type,  od.order_name, (CASE WHEN rr.notification_email IS NOT NULL THEN rr.notification_email ELSE cd.email END) AS email   
        FROM return_request rr 
        INNER JOIN order_details od ON od.id = rr.order_id AND od.customer_id = rr.customer_id 
        INNER JOIN customer_details cd ON cd.id = rr.customer_id 
        WHERE rr.user_id = :user_id $where 
        ORDER BY $sort_by $sort_order
        LIMIT $limit OFFSET $offset";

        // return $sql;

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue($key, $value);
            }
        }

        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function hasNext($request_id, $user_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT id FROM return_request WHERE id > :id AND user_id = :user_id ORDER BY id LIMIT 1';

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $request_id);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function hasPrev($request_id, $user_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT id FROM return_request WHERE id < :id AND user_id = :user_id ORDER BY id DESC LIMIT 1';

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $request_id);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        return $stmt->fetchColumn();
    }

    public function getReturnRequestsStateBtwDate($user_id, $from_datetime, $to_datetime) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT COUNT((CASE WHEN return_status = 1 THEN id END)) AS pending_request , 
            COUNT((CASE WHEN return_status = 2 THEN id END)) AS accepted_request ,
            COUNT((CASE WHEN return_status = 3 THEN id END)) AS completed_request , 
            COUNT((CASE WHEN return_status = 4 THEN id END)) AS cancelled_request , 
            DATE_FORMAT(date_add, '%Y-%m-%d') dateZ FROM return_request 
            WHERE user_id = :user_id AND date_add >= '$from_datetime' AND date_add <= '$to_datetime' 
            GROUP BY dateZ";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getExportDataBtwDate($user_id, $from_datetime, $to_datetime)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT rli.order_line_item_id, oli.product_id, oli.variant_id, oli.item_sku, rli.return_reason , COUNT(rli.id) total_request
                FROM return_request rr 
                INNER JOIN return_line_items rli ON rli.return_request_id = rr.id 
                INNER JOIN order_line_items oli ON oli.id = rli.order_line_item_id
                WHERE rr.user_id = :user_id and oli.item_sku != '' AND rr.date_add >= '$from_datetime' AND rr.date_add <= '$to_datetime'
                GROUP BY rli.order_line_item_id , rli.return_reason";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getReturnRequestsByUserIdNIds($user_id, $ids) {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT rr.id, rr.sp_order_id, rr.order_id, rr.customer_id, rr.return_status, rr.return_type, rr.payment_details, cd.name, (CASE WHEN rr.notification_email IS NOT NULL THEN rr.notification_email ELSE cd.email END) AS email 
        FROM return_request rr 
        INNER JOIN customer_details cd ON cd.id = rr.customer_id 
        WHERE rr.user_id = :user_id AND rr.id IN (${ids})";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC|\PDO::FETCH_UNIQUE);
    }

    public function updateReturnStatusInBulk($status, $ids, $user_id) {
        $dt = new \DateTime();
        $conn = $this->getEntityManager()->getConnection();

        $sql = "UPDATE return_request SET return_status = :return_status, date_upd = :date_upd WHERE user_id = :user_id AND id IN (${ids})";

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':return_status', $status);
        $stmt->bindValue(':date_upd', $dt->format('Y-m-d H:i:s'));
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();
        return $stmt->rowCount();
    }
}
