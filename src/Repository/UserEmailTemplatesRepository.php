<?php

namespace App\Repository;

use App\Entity\UserEmailTemplates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserEmailTemplates|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserEmailTemplates|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserEmailTemplates[]    findAll()
 * @method UserEmailTemplates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserEmailTemplatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserEmailTemplates::class);
    }

    // /**
    //  * @return UserEmailTemplates[] Returns an array of UserEmailTemplates objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserEmailTemplates
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getUserEmailDetails($user_id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT mail_id, active FROM user_email_templates WHERE user_id = :user_id ORDER BY id ASC';

        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':user_id', $user_id);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}
