<?php

namespace App\Service;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Environment;
use App\Service\Shopify;

use App\Entity\UserSettings;
use App\Entity\UserConfigurations;
use App\Entity\UserProvidedReasons;
use App\Entity\PaymentOptions;
use App\Entity\EmailTemplates;
use App\Entity\UserEmailTemplates;
use App\Entity\ReturnLineItems;
use App\Entity\OrderDetails;
use App\Entity\Attachments;

class Helper
{
    private $params;
    private $mailer;
    private $em;
    private $twig;
    
    public function __construct(ContainerBagInterface $params, MailerInterface $mailer, EntityManagerInterface $em, Environment $twig) {
        $this->params = $params;
        $this->mailer = $mailer;
        $this->em = $em;
        $this->twig = $twig;
    }

    public function verifyHmac($request_data, $app_secret_key) {
        $hmacSource = [];

        foreach ($request_data as $key => $value) {
            
            if ($key === 'hmac') { continue; } // Skip the hmac key

            // Replace the characters as specified by Shopify in the keys and values
            $valuePatterns = ['&' => '%26', '%' => '%25'];
            $keyPatterns = $valuePatterns + ['=' => '%3D'];
            $key = str_replace(array_keys($keyPatterns), array_values($keyPatterns), $key);
            $value = str_replace(array_keys($valuePatterns), array_values($valuePatterns), $value);

            $hmacSource[] = $key . '=' . $value;
        }

        // Sort the key value pairs lexographically and then generate the HMAC signature of the provided data
        sort($hmacSource);
        $hmacBase = implode('&', $hmacSource);
        $hmacString = hash_hmac('sha256', $hmacBase, $app_secret_key);

        if ($hmacString !== $request_data['hmac']) { // Verify that the signatures match
            return false;
        }

        return true;
    }

    public function encrypt($message, $encryption_key){
        // $key = hex2bin($encryption_key);
        $nonceSize = openssl_cipher_iv_length('aes-256-ctr');
        $nonce = openssl_random_pseudo_bytes($nonceSize);
        $cipher_text = openssl_encrypt(
          $message, 
          'aes-256-ctr', 
          $encryption_key,
          OPENSSL_RAW_DATA,
          $nonce
        );

        return base64_encode($nonce.$cipher_text);
      }

      function decrypt($message,$encryption_key){
        // $key = hex2bin($encryption_key);
        $message = base64_decode($message);
        $nonceSize = openssl_cipher_iv_length('aes-256-ctr');
        $nonce = mb_substr($message, 0, $nonceSize, '8bit');
        $cipher_text = mb_substr($message, $nonceSize, null, '8bit');
        $plaintext= openssl_decrypt($cipher_text, 'aes-256-ctr', $encryption_key, OPENSSL_RAW_DATA, $nonce);
        return $plaintext;
      }

    function verifyWebhook($data, $hmac_header, $app_secret_key) {
        if($hmac_header == '') return false;
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $app_secret_key, true));
        return hash_equals($hmac_header, $calculated_hmac);
    }

    function verifyTokenSignature($token, $app_secret_key) {
        if($token == '') return false;

        $token_parts = explode('.', $token);
        $data = ($token_parts[0] . "." . $token_parts[1]);
        $calculated_hmac = $this->base64url_encode(hash_hmac('sha256', $data, $app_secret_key, true));
        return hash_equals($token_parts[2], $calculated_hmac);
    }

    function getBearerToken($bearer_token) {
        if (!empty($bearer_token)) {
            if (preg_match('/Bearer\s(\S+)/', $bearer_token, $matches))
                return $matches[1];
        }
        return null;
    }

    function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function curl($url, $method = "GET", $params = []) {
        try{
            $ch = curl_init();

            if($method === 'GET' && !empty($params)) {
                $build_query = http_build_query($params);
                $url .= '?'.$build_query;
            }
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    
            if($method === 'POST') {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
            }

            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    
            $buffer = curl_exec($ch);
            curl_close($ch);
    
            if (empty($buffer)){
                throw new Exception(curl_error($ch), curl_errno($ch));
            }
            
            return json_decode($buffer, true);
        }
        catch(Exception $e) {
            return ['err'=> 'failed'];
        }
    }

    function verifyTokenAuthenticity($payload, $logger) {
        $payload = (array) $payload;
        
        $exp = $payload['exp']; // When the session token expires. (In Future)
        // $logger->info('exp: '.$exp);

        $dt_obj = new \DateTime();
        $now = $dt_obj->getTimestamp();
        // $logger->info('now: '.$now);

        if($exp <= $now) {
            // $logger->info('exp expired');
            return false;
        }

        $nbf = $payload['nbf']; // When the session token activates. (In Past)
        // $logger->info('nbf: '.$nbf);

        if($nbf > $now) {
            // $logger->info('nbf expired');
            return false;
        }

        $iss = $payload['iss']; // The shop's admin domain.
        $dest = $payload['dest']; // The shop's domain.
        $shop = parse_url($dest, PHP_URL_HOST);

        if($iss !== $dest.'/admin') {
            // $logger->info('dest, iss not match');
            return false;
        }

        if(!$this->verifyMyshopifyDomain($shop)) {
            // $logger->info('shop, is not valid');
            return false;
        }

        $aud = $payload['aud']; // The API key of the receiving app.

        if($aud !== $this->params->get('APP_KEY')) {
            // $logger->info('api_key, does not match');
            return false;
        }

        return $shop;
    }

    public function verifyMyshopifyDomain($shop) {
        return preg_match("/\A[a-zA-Z0-9][a-zA-Z0-9\-]*\.myshopify\.com\z/", $shop);
    }
    
    function verifyEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    function verifyId($id) {
        return filter_var($id, FILTER_VALIDATE_INT);
    }

    function verifyShopifyOrderId($orderId) {
        return preg_match("/^[1-9]+\d{12,}$/", $orderId);
    }

    function verifyMobileNumber($mobile_no, $country_code = 'IN') {
        return preg_match("/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/", $mobile_no);
    }

    function verifyUpiId($upi_id) {
        return preg_match("/^[\w.-]+@[\w.-]+$/", $upi_id);
    }

    function verifyBankIFSC($ifsc_code) {
        $return = $this->curl("https://ifsc.razorpay.com/".$ifsc_code);
        # return "Not Found" in case invalid IFSC code 
        return (isset($return['IFSC']) && $return['IFSC'] != '') ? true : false;
    }

    function verifyHumanName($name) {
        return preg_match('/^\w[\w ]*[a-zA-Z]$/', $name);
    }

    function verifyPaymentDetails($details, $payment_option) {
        if($payment_option === 'bank_details') {
            $bank_account_no = trim($details['bank_account_no'] ?? '');
            $bank_ifsc_code = trim($details['bank_ifsc_code'] ?? '');
            $bank_holder_name = trim($details['bank_holder_name'] ?? '');      

            if(empty($bank_account_no) || empty($bank_ifsc_code) || empty($bank_holder_name)) {
                return ['err'=> 'All bank details are required', 'err_code'=> 23];
            }
            else if(!preg_match("/\d{10,}/", $bank_account_no)) {
                return ['err'=> 'Bank Account Number is invalid', 'err_code'=> 24];
            }
            else if(!$this->verifyHumanName($bank_holder_name)) {
                return ['err'=> 'Bank holder name is invalid', 'err_code'=> 25];
            }

            # check Bank IFSC CODE
            // return $this->verifyBankIFSC($bank_ifsc_code);
            if(!$this->verifyBankIFSC($bank_ifsc_code)) {
                return ['err'=> 'Bank Account IFSC code is invalid', 'err_code'=> 26];
            }

            $data = ['bank_account_no'=> $details['bank_account_no'], 'bank_holder_name'=> $details['bank_holder_name'], 'bank_ifsc_code'=> $details['bank_ifsc_code']];
        }
        else if($payment_option === 'paytm_mobile_no') {
            $paytm_mobile_no = trim($details['paytm_mobile_no'] ?? '');

            if(empty($paytm_mobile_no)) {
                return ['err'=> 'Paytm mobile no is required', 'err_code'=> 15];
            }

            # check mobile no
            if(!$this->verifyMobileNumber($paytm_mobile_no)) {
                return ['err'=> 'Paytm mobile number is invalid', 'err_code'=> 16];
            }

            $data = ['paytm_mobile_no'=> $details['paytm_mobile_no']];
        }
        else if($payment_option === 'upi_id') {
            $upi_id = trim($details['upi_id'] ?? '');

            if(empty($upi_id)) {
                return ['err'=> 'UPI Id is required', 'err_code'=> 21];
            }

            # check UPI id
            if(!$this->verifyUpiId($upi_id)) {
                return ['err'=> 'Upi Id is invalid', 'err_code'=> 22];
            }

            $data = ['upi_id'=> $details['upi_id']];
        }
        else if($payment_option === 'phonepe_mobile_no') {
            $phonepe_mobile_no = trim($details['phonepe_mobile_no'] ?? '');

            if(empty($phonepe_mobile_no)) {
                return ['err'=> 'Phonepe mobile number is required', 'err_code'=> 17];
            }

            # check mobile no
            if(!$this->verifyMobileNumber($phonepe_mobile_no)) {
                return ['err'=> 'Phonepe mobile number is invalid', 'err_code'=> 18];
            }

            $data = ['phonepe_mobile_no'=> $details['phonepe_mobile_no']];
        }
        else if($payment_option === 'google_pay') {
            $google_pay = trim($details['google_pay'] ?? '');

            if(empty($google_pay)) {
                return ['err'=> 'Google pay mobile number is required', 'err_code'=> 19];
            }

            # check mobile no
            if(!$this->verifyMobileNumber($google_pay)) {
                return ['err'=> 'Google pay mobile number is invalid', 'err_code'=> 20];
            }

            $data = ['google_pay'=> $details['google_pay']];
        }
        else if($payment_option === 'coupon_code') {
            $data = [];
        }

        return isset($data) ? [$payment_option => $data] : false;
    }

    function pagination($page, $limit, $total_result) {
        if(!$this->verifyId($page)) $page = 1;

        $total_pages = ceil($total_result / $limit);

        if($page > $total_pages) $page = 1;

        $offset = ($page - 1) * $limit;

        return [$page, $total_pages, $offset, $limit];
    }

    function sortOrder($order, $by = 'date_add', $ord = 'asc') {

        $order = trim($order);
        if($order === '') {
            return [$by, $ord];
        }
        else {
            $sort_orders = ['asc', 'desc'];
            $sort_bys = ['date_add', 'email', 'order_name'];

            list($sort_by, $sort_order) = explode(' ', $order);

            if(!in_array($sort_by, $sort_bys)) $sort_by = $by;
            if(!in_array($sort_order, $sort_orders)) $sort_order = $ord;

            return [$sort_by, $sort_order];
        }
    }

    public function getEmailTemplate($user_id, $mail_id, $return_id, $from=[], $to=[], $reply_to=[], $cc=[], $test_email=false, $data=[]) {
        $mail_id = (int) $mail_id;
        $user_email_templates = $this->em->getRepository(UserEmailTemplates::class)->findOneBy(['userId'=> $user_id, 'mailId'=>$mail_id]);
        // dump($user_email_templates); die;
        if($user_email_templates && !$user_email_templates->getActive() && !$test_email) {
            return ['err'=> 'Mail id '. $mail_id .' is disabled by userId: '. $user_id];
        }

        if(!$user_email_templates) {
            $default_email_template = $this->em->getRepository(EmailTemplates::class)->findOneBy(['id'=> $mail_id]);
        }

        $merchant_mail = in_array($mail_id, [5]) ? true : false;
        // dump("test");die;
        if($merchant_mail) { # to merchant
            if($user_email_templates && $user_email_templates->getRecipients()) {
                $recipient_emails = json_decode($user_email_templates->getRecipients(), true);
                $recipient_emails = array_column($recipient_emails, 'email');
                $to = ['address'=> array_shift($recipient_emails)];
                $cc = $recipient_emails;
            }
        }

        // dump($to, $cc);die;

        if(empty($to)) return ['err'=> 'Mail tos is required'];

        $email_body = $user_email_templates ? $user_email_templates->getContent() : $default_email_template->getDefaultContent();
        $subject = $user_email_templates ? $user_email_templates->getSubject() : $default_email_template->getSubject();

        if($test_email) {
            $subject = 'TEST MAIL: ' . $subject;
        }
        
        list($subject, $html_content) = $this->getEmailTemplateRaw($mail_id, $user_id, $subject, $email_body, $return_id, $data);

        $mail_send = $this->sendMail($from, $to, $reply_to, $cc, $subject, $html_content);
        return $mail_send;
    }

    public function getEmailTemplateRaw($mail_id, $user_id, $subject, $email_body, $return_request_id, $data=[]) {
        $mail_id = (int) $mail_id;
        $user_settings_obj = $this->em->getRepository(UserSettings::class)->findOneBy(['id'=> $user_id, 'status'=> 2]);
        $money_format = $user_settings_obj->getMoneyFormat();
        $brand_logo = $user_settings_obj->getBrandLogo();
        # get order details
        $order_details = $this->em->getRepository(OrderDetails::class)->getOrderDetailsByReturnId($return_request_id);

        # return request 
        $return_line_items = $this->em->getRepository(ReturnLineItems::class)->getReturnLineItemDetailsByReturnId($return_request_id);

        // $subject = str_replace("{{ return_request_id }}", $return_request_id, $subject);
        // $subject = str_replace("{{ order_name }}", $order_details['order_name'], $subject);
        $subject = $this->twig->createTemplate($subject)->render([
            'return_request_id'=> $return_request_id, 
            'order_name'=> $order_details['order_name'],
            'return_method'=> $order_details['return_type'] == 1 ? 'Refund' : 'Exchange'
        ]);

        $payment_details = ($order_details['return_type'] == 1 && $order_details['is_cod']) ? json_decode($order_details['payment_details'], true) : [] ;
        $payment_type_handle = '';
        $payment_type = '';
        $coupon_code = null;

        if($payment_details) {
            $payment_type_handle = array_key_first($payment_details);
            $payment_details = $payment_details[$payment_type_handle];

            $payment_option_details = $this->em->getRepository(PaymentOptions::class)->findOneByHandle($payment_type_handle);
            $payment_type = $payment_option_details->getName();

            if($payment_type_handle === 'coupon_code')
                $coupon_code = $payment_details['coupon_code'] ?? null;
        }

        $email_style = $user_settings_obj->getEmailStyle() ? : $this->twig->render("mail/default_email_style.html.twig");
        $email_header = $user_settings_obj->getEmailHeader() ? : $this->twig->render("mail/mail_header.html.twig");
        $email_header = str_replace("{{ brand_logo }}", $brand_logo, $email_header);
        $email_signature = $mail_id === 5 ? '' : ($user_settings_obj->getEmailSignature() ? : $this->twig->render("mail/mail_signature.html.twig"));
        $email_body = str_replace("{{ return_line_items }}", "{% include 'mail/return_line_items.html.twig' %}", $email_body);
        
        if($order_details['return_status'] == 3 && $order_details['attachments_id']) {
            $attachments = $this->em->getRepository(Attachments::class)->findAttachmentsByIds($order_details['attachments_id']);
            
            if($attachments) {
                $attachments_content = $this->twig->render("mail/mail_attachments_snippet.html.twig", ['attachments'=> $attachments]);
                // $email_body = str_replace("{{ attachments }}", "{% include 'mail/mail_attachments_snippet.html.twig' %}", $email_body);
                // dump($attachments_content);die;
            }
        }

        $values = [
                "order_name"=> $order_details['order_name'], 
                "return_request_id"=> $return_request_id, 
                "return_line_items"=> $return_line_items, 
                'return_method'=> $order_details['return_type'] == 1 ? 'Refund' : 'Exchange',
                "customer_name"=> $order_details['name'], 
                "money_format"=> $money_format, 
            ];

        if(!empty($data)) {
            $values += $data;
        }

        if($mail_id === 1) {
            $values = array_merge($values, ['customer_note'=> $order_details['customer_note'], "is_cod_order"=> $order_details['is_cod'], "payment_type"=> $payment_type, "payment_type_handle"=> $payment_type_handle], $payment_details);
        }
        else if($mail_id === 3) {
            $values = array_merge($values, ['customer_note'=> $order_details['customer_note'], 'attachments'=> $attachments_content ?? '', 'coupon_code'=> $coupon_code]);
        }
        else if($mail_id === 4) {
            $values = array_merge($values, ['cancelled_reason'=> $order_details['canceled_reason']]);
        }
        else if($mail_id === 5) {
            $values = array_merge($values, ["customer_email"=> $order_details['email'], 'customer_note'=> $order_details['customer_note']]);
        }
        else if($mail_id === 6) {
            $values = array_merge(["order_name"=> $order_details['order_name'], "return_request_id"=> $return_request_id, "return_line_items"=> $return_line_items, "money_format"=> $money_format], $data);
        }

        // dump($values);

        $email_body = $this->twig->createTemplate($email_body)->render($values);
        // dump($email_body);die;

        $html_content = $this->twig->render("mail/mail_template.html.twig", ['css_style'=> $email_style, 'header'=> $email_header, 'body'=> $email_body, 'signature'=> $email_signature]);

        return [$subject, $html_content];
    }

    public function sendMail($from, $to, $reply_to=[], $cc=[], $subject, $html_content, $attachment=null, $attachment_name='') {
        if(is_array($from)) {
            $from_address = $from['address'] ?? $this->params->get('BUSINESS_EMAIL');
            $from_name = $from['name'];
        }
        else if($from == '') {
            $from_address = $this->params->get('BUSINESS_EMAIL'); // "pim@primesellerhub.com";
            $from_name = '';
        }

        $mail_to = [new Address($to['address'], $to['name'] ?? '')];

        if(empty($mail_to)) {
            return false;
        }

        $email = (new Email())
            ->from(new Address($from_address, $from_name))
            ->to(...$mail_to)
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->priority(Email::PRIORITY_HIGH)

            ->subject($subject)
            // ->text('Test')
            ->html($html_content);

        if(!empty($reply_to)) {
            $email->replyTo(new Address($reply_to['address'], $reply_to['name'] ?? ''));
        }

        if(!empty($cc)) {
            foreach ($cc as $key => $cc_addr) {
                if($key === 0)
                    $email->cc($cc_addr);

                $email->addCc($cc_addr);
            }
        }
            
        if($attachment) {
            $email->attach($attachment, $attachment_name, 'application/csv');
        }

        $this->mailer->send($email);
        return true;
       
    }

    public function convertUserDateTimeToGmt($user_timezone, $datetime) {
        $date = date_create($datetime, timezone_open($user_timezone));
        date_timezone_set($date, timezone_open('GMT'));
        $gmt_datetime = $date->format('Y-m-d H:i:s');

        // dump($datetime, $gmt_datetime);die;
        return $gmt_datetime;
    }

    public function convertGmtDateTimeToUserDateTime($user_timezone, $datetime, $format='Y-m-d H:i:s') {
        $gmtTimezone = new \DateTimeZone('GMT');
        $userTimezone = new \DateTimeZone($user_timezone);
        $gmt_datetime = new \DateTime($datetime, $gmtTimezone);
        $offset = $userTimezone->getOffset($gmt_datetime);

        $myInterval = \DateInterval::createFromDateString((string)$offset . 'seconds');
        $gmt_datetime->add($myInterval);
        $user_datetime = $gmt_datetime->format($format);
        return $user_datetime;
        // dump($user_timezone, $gmtTimezone, $gmt_datetime, $offset, $user_datetime);die;
    }

    public function createCsvString($data, $headers) {

        // Open temp file pointer
        if (!$fp = fopen('php://temp', 'w+')) return FALSE;
      
        // Loop data and write to file pointer

        fputcsv($fp, $headers);
        foreach ($data as $line) {
            fputcsv($fp, array_map(function($column) { return $column."\t";  }, array_slice($line, 1)));
        }
      
        // Place stream pointer at beginning
        rewind($fp);
      
        // Return the data
        return stream_get_contents($fp);
    }

    public function getShopName($domain) {
        return explode('.', $domain)[0];
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function br2nl($input) {
        return preg_replace('/<br\s?\/?>/ius', "\n", str_replace("\n","",str_replace("\r","", htmlspecialchars_decode($input))));
    }

    function isCODOrder($payment_gateway_names) {
        if(is_array($payment_gateway_names) && !empty($payment_gateway_names)) {
            foreach ($payment_gateway_names as $name) {
                if ($name === 'Cash on Delivery (COD)' || $name === 'cash_on_delivery') {
                    $is_cod_order = true;
                    break;
                }
            }
        }

        return $is_cod_order ?? false;
    }

    public function checkOrderLineItemsReturnApplicable($line_items, $line_items_fulfillments, $return_acceptable_days) {
        $now_obj = new \DateTime();
        $now_ts = $now_obj->getTimeStamp();
        $applicable_line_items = [];

        foreach ($line_items as $line_item_id => $qty) {
            foreach ($line_items_fulfillments[$line_item_id] as $line_items_fulfillment) {
                $fulfillment_date_obj = new \DateTime($line_items_fulfillment['fulfillment_date']);
                $fulfillment_date_ts = $fulfillment_date_obj->getTimeStamp();

                # Get return acceptable days
                $return_acceptable_days_ts = $fulfillment_date_obj->modify('+' . $return_acceptable_days . ' day')->getTimeStamp();

                if ($return_acceptable_days_ts > $now_ts) {
                    $applicable_line_items[$line_item_id] = ($applicable_line_items[$line_item_id] ?? 0) + $qty;
                }
            }
        }

        return $applicable_line_items;
    }
}