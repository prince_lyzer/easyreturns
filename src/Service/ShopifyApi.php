<?php

namespace App\Service;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Psr\Log\LoggerInterface;
use App\Service\Shopify;

class ShopifyApi {
    private $api;
    private $logger;

    public function __construct($shop, $access_token, $api_version, LoggerInterface $logger) {
        $this->logger = $logger;
        $this->api = new Shopify($shop, $access_token, $api_version);
    }

    public function getStoreThemeId() {
        try{
            $store_themes = $this->api->call('GET', 'themes.json');
    
            if ($store_themes) {
                $main_theme = array_filter($store_themes, function ($theme) {
                    return $theme['role'] === 'main';
                });

                $main_theme = array_shift($main_theme);
                $theme_id = $main_theme['id'];
                return $theme_id;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getThemeAsset($theme_id, $template_key) {
        try{
            $theme_asset = $this->api->call('GET', 'themes/' . $theme_id . '/assets.json', ['asset[key]' => $template_key]);
            // $this->logger->info('getThemeAsset ', $theme_asset);

            if ($theme_asset) {
                return $theme_asset;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function createThemeAsset($theme_id, $data) {
        try{
            $theme_asset = $this->api->call('PUT', 'themes/' . $theme_id . '/assets.json', $data);
    
            if ($theme_asset) {
                return $theme_asset;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function deleteThemeAsset($theme_id, $template_key) {
        try{
            $theme_asset = $this->api->call('DELETE', 'themes/' . $theme_id . '/assets.json', ['asset[key]' => $template_key]);
    
            if ($theme_asset && $theme_asset['message']) {
                return true;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function countScriptTags() {
        try{
            $script_tag = $this->api->call('GET', 'script_tags/count.json');
    
            if ($script_tag) {
                return $script_tag;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getScriptTagById($id) {
        try{
            $script_tag = $this->api->call('GET', 'script_tags/'.$id.'.json');
    
            if ($script_tag) {
                return $script_tag;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getScriptTagBySrc($src) {
        try{
            $script_tag = $this->api->call('GET', 'script_tags.json', ['src' => $src]);
    
            if ($script_tag) {
                return $script_tag;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function createScriptTag($data) {
        try{
            $script_tag = $this->api->call('POST', 'script_tags.json', $data);
    
            if ($script_tag) {
                return $script_tag;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function deleteScriptTag($id) {
        try{
            $script_tag = $this->api->call('DELETE', 'script_tags/'.$id.'.json');
    
            if (empty($script_tag)) {
                return true;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function updateScriptTag($id, $data) {
        try{
            $script_tag = $this->api->call('PUT', "script_tags/${id}.json", $data);
    
            if ($script_tag) {
                return $script_tag;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getAllPages() {
        try{
            $pages = $this->api->call('GET', 'pages.json');
            
            if ($pages) {
                return $pages;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getPageById($id) {
        try{
            $page = $this->api->call('GET', 'pages/'.$id.'.json');
    
            if ($page) {
                return $page;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getPageByHandle($handle) {
        try{
            $page = $this->api->call('GET', 'pages.json', ['handle'=> $handle]);
    
            if ($page) {
                return $page;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function createPage($data) {
        try{
            $create_page = $this->api->call('POST', 'pages.json', $data);
    
            if ($create_page) {
                return $create_page;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function deletePage($id) {
        try{
            $delete_page = $this->api->call('DELETE', 'pages/'.$id.'.json');
    
            if (empty($delete_page)) {
                return true;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getWebHooks() {
        try{
            $webhooks = $this->api->call('GET', 'webhooks.json');
    
            if ($webhooks) {
                return $webhooks;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function createHook($data) {
        try{
            $create_hook = $this->api->call('POST', 'webhooks.json', $data);
    
            if ($create_hook) {
                return $create_hook;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getProductById($id, $fields) {
        try{
            $product = $this->api->call('GET', 'products/'.$id.'.json', $fields);
            return $product ? : ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getVariantById($id) {
        try{
            $variant = $this->api->call('GET', 'variants/'.$id.'.json');
            return $variant ? : ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getProductImageById($product_id, $id) {
        try{
            $image = $this->api->call('GET', 'products/'.$product_id.'/images/'.$id.'.json');
            return $image ? : ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getOrderById($id) {
        try{
            $order = $this->api->call('GET', 'orders/'.$id.'.json');
            return $order ? : ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getOrders($fields) {
        try{
            $orders = $this->api->call('GET', 'orders.json', $fields);
            return $orders ? : ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function getOrderByOrderNumber($fields) {
        try{
            $orders = $this->api->call('GET', 'orders.json', $fields);

            if(is_array($orders))
                return empty($orders) ? [] : $orders[0];

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }

    public function allScriptTags() {
        try{
            $script_tags = $this->api->call('GET', 'script_tags.json');
    
            if ($script_tags) {
                return $script_tags;
            }

            return ['error'=> true, 'code'=> 500, 'msg'=> 'Something went wrong'];
        }
        catch(\Exception $e) {
            $this->logger->error('Order Exception', [$e->getMessage(), $e->getCode()]);

            return ['error'=> true, 'code'=> $e->getCode()];
        }
    }
}