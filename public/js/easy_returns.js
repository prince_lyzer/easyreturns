;(function () {
  window.frfi = (function () {
    function frfi() {}

    frfi.loadjQuery = function (afterLoad) {
      return frfi.loadScript(
        '//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',
        function () {
          frfi.$ = jQuery.noConflict(true)
          return afterLoad()
        },
      )
    }

    frfi.loadScript = function (url, callback) {
      var script = document.createElement('script')
      script.type = 'text/javascript'

      if (script.readyState) {
        script.onreadystatechange = function () {
          if (
            script.readyState === 'loaded' ||
            script.readyState === 'complete'
          ) {
            script.onreadystatechange = null
            return callback()
          }
        }
      } else {
        script.onload = function () {
          return callback()
        }
      }
      script.src = url
      return document.getElementsByTagName('head')[0].appendChild(script)
    }

    frfi.removeLoader = function () {
      frfi.$('body').find('.frfi-loader').hide()
    }

    frfi.showLoader = function () {
      frfi.$('body').find('.frfi-loader').show()
    }

    frfi.getRequestParam = function (req_param) {
      var params = window.location.search.substring(1).split('&')
      for (var i = 0; i < params.length; i++) {
        param = params[i].split('=')
        if (param[0] == req_param) {
          return param[1]
        }
      }
      return 0
    }

    frfi.getTotalReturnQty = function () {
      let total_return_qty = 0
      frfi.$.each(frfi.$('.return_qty_input'), function (k, v) {
        let return_qty = parseInt(frfi.$(v).val())
        total_return_qty += return_qty
      })

      return total_return_qty
    }

    frfi.getReturnPageConfigurations = async () => {
      try {
        result = await frfi.$.ajax({
          url: frfi.api_url + 'return-page-configuration',
          method: 'Get',
          crossDomain: true,
          data: {
            shop: frfi.shop,
          },
          success: function (data) {
            if (data.err == '' && typeof data.data != 'undefined') {
              returned_data = data.data
              frfi.return_labels = returned_data.return_labels
              frfi.show_return_links = returned_data.show_return_links
              frfi.guest_checkout = returned_data.guest_checkout
              frfi.return_note = returned_data.return_note
              frfi.required_return_note = returned_data.required_return_note
              frfi.exchange_note = returned_data.exchange_note
              frfi.required_exchange_note = returned_data.required_exchange_note
              frfi.return_methods = returned_data.return_methods
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            // console.log(xhr.status);
            // console.log(xhr.responseText);
            // console.log(thrownError);
            // Promise.reject(xhr.responseText)
          },
        })

        return Promise.resolve(result)
      } catch (error) {
        // console.log(error)
        return Promise.reject(error.responseText)
      }
    }

    frfi.checkOrderReturnAvailableStatus = function () {
      let preview = frfi.getRequestParam('preview')
      let preview_type = frfi.getRequestParam('type')
      let data = {
        shop: frfi.shop,
        order_id: frfi.order_id,
        customer_id: frfi.customer_id,
      }

      if (preview && preview_type !== '') {
        data.preview = preview
        data.type = preview_type
      }

      frfi.$('.loader_wrapper').show()
      frfi.$.ajax({
        url:
          frfi.api_url +
          (preview && preview_type !== ''
            ? 'preview-order-return'
            : 'view-order-return'),
        method: 'POST',
        crossDomain: true,
        data: data,
        success: function (data) {
          if (data.err == '' && typeof data.data != 'undefined') {
            // console.log(data.data);
            frfi.$('#return_order_div').html(data.data)

            if (frfi.$('input.return_method:checked').val() === '1') {
              if (frfi.return_note) {
                frfi.$('#note-div').show()
              } else {
                frfi.$('#note-div').hide()
              }

              if (frfi.required_return_note) {
                frfi.$('.note-required').show()
                frfi.$('.note-optional').hide()
              } else {
                frfi.$('.note-optional').show()
                frfi.$('.note-required').hide()
              }
            } else if (frfi.$('input.return_method:checked').val() === '2') {
              if (frfi.exchange_note) {
                frfi.$('#note-div').show()
              } else {
                frfi.$('#note-div').hide()
              }

              frfi.$('#payment-div').hide()
              frfi.$('#exchange-helptext').show()

              if (frfi.required_exchange_note) {
                frfi.$('.note-required').show()
                frfi.$('.note-optional').hide()
              } else {
                frfi.$('.note-optional').show()
                frfi.$('.note-required').hide()
              }
            }
          } else {
            err = frfi.getErrorMsg(data)
            frfi.$('#return_order_failed').html(err).show()
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          // console.log(xhr.status)
          // console.log(xhr.responseText)
          // console.log(typeof xhr.responseText)
          // console.log(thrownError)
          let responseText = xhr.responseText || ''
          try {
            responseText = responseText != '' ? JSON.parse(responseText) : ''
            // console.log(responseText)
            err = frfi.getErrorMsg(responseText)
            frfi.$('#return_order_failed').html(err).show()
          } catch (e) {
            frfi.$('#return_order_failed').html(thrownError).show()
          }
        },
        complete: function () {
          frfi.$('.loader_wrapper').hide()
        },
      })
    }

    frfi.viewReturnCenterPage = function () {
      let preview = frfi.getRequestParam('preview')
      let preview_type = frfi.getRequestParam('type')
      let data = { shop: frfi.shop }

      if (preview && preview_type !== '') {
        data.preview = preview
        data.type = preview_type
      }

      frfi.$('.loader_wrapper').show()
      frfi.$.ajax({
        url:
          frfi.api_url +
          (preview && preview_type !== ''
            ? 'preview-return-center'
            : 'view-return-center-page'),
        method: 'POST',
        crossDomain: true,
        data: data,
        success: function (data) {
          if (data.err == '' && typeof data.data != 'undefined') {
            frfi.$('#return_center_div').html(data.data)
          } else {
            err = frfi.getErrorMsg(data)
            frfi.$('#return_order_failed').html(err).show()
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          let responseText = xhr.responseText || ''
          try {
            responseText = responseText != '' ? JSON.parse(responseText) : ''
            err = frfi.getErrorMsg(responseText)
            frfi.$('#return_order_failed').html(err).show()
          } catch (e) {
            frfi.$('#return_order_failed').html(thrownError).show()
          }
        },
        complete: function () {
          frfi.$('.loader_wrapper').hide()
        },
      })
    }

    frfi.getErrorMsg = function ({ err_code = 0, err, data }) {
      // console.log(err_code, err)
      if (!err && !err_code) return ''

      if (typeof frfi.return_labels[err_code] != 'undefined') {
        return (
          "<p class='text-danger'>" +
          frfi.return_labels[err_code] +
          '</p>' +
          (typeof data !== 'undefined' && data != '' ? data : '')
        )
      } else {
        return (
          "<p class='text-danger'>" +
          err +
          '</p>' +
          (typeof data !== 'undefined' && data != '' ? data : '')
        )
      }
    }

    frfi.inputCount = function (
      input_ele = '#customer_note',
      counter_ele = '.current_count',
      maxLength = 250,
    ) {
      frfi.$('body').on('input', input_ele, function (e) {
        let _this_val = frfi.$(this).val()
        var currentLength = _this_val.length
        // console.log(currentLength);
        var _this_counter = frfi.$(counter_ele)

        // if (maxLength - currentLength <= 10) {
        //   _this_counter.removeClass("text-warning").addClass("text-danger");
        // } else if (maxLength - currentLength <= 20) {
        //   _this_counter.addClass("text-warning").removeClass("text-danger");
        // } else {
        //   _this_counter.removeClass("text-danger text-warning");
        // }

        if (currentLength >= maxLength) {
          frfi.$(this).val(_this_val.substr(0, 250))
          _this_counter.text(maxLength)
          //console.log("You have reached the maximum number of characters.");
        } else {
          _this_counter.text(currentLength)
          //console.log(maxLength - currentLength + " chars left");
        }
      })
    }

    frfi.getOrdersReturnStatus = function () {
      var return_order_ids = []
      frfi.$.each(frfi.$('.frfi-return-order'), function (k, v) {
        return_order_ids.push(frfi.$(v).attr('data-id'))
        frfi
          .$(".frfi-return-order[data-id='" + frfi.$(v).attr('data-id') + "']")
          .html('<span>fetching...</span>')
      })

      // console.log(return_order_ids);

      if (return_order_ids.length == 0) return false

      // return false;
      frfi.$('.loader_wrapper').show()
      frfi.$.ajax({
        url: frfi.api_url + 'get_orders_return_link',
        method: 'POST',
        crossDomain: true,
        data: { shop: frfi.shop, order_ids: return_order_ids },
        success: function (data) {
          if (data.err == '' && typeof data.data != 'undefined') {
            // console.log(data.data);
            response = data.data
            const orders = response.orders
            const handle = response.handle

            frfi.$.each(orders, function (k, v) {
              if (v === 1) {
                // Fulfilled Order
                frfi
                  .$(".frfi-return-order[data-id='" + k + "']")
                  .html(
                    '<a href="pages/' +
                      handle +
                      '?order_id=' +
                      k +
                      '">' +
                      frfi.return_labels[12] +
                      '</a>',
                  )
              } else if (v === 0) {
                // order not fulfilled
                frfi
                  .$(".frfi-return-order[data-id='" + k + "']")
                  .html(
                    '<span style="color: #b5b5b5;">' +
                      frfi.return_labels[12] +
                      '</span>',
                  )
              }
            })
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          // console.log(xhr.status);
          // console.log(thrownError);
        },
        complete: function () {
          frfi.$('.loader_wrapper').hide()
        },
      })
    }

    return frfi
  })()

  frfi.themeId = 1
  frfi.shop = Shopify.shop
  frfi.path = window.location.pathname.split('/')
  frfi.page = frfi.path.pop(-2)
  frfi.api_url = 'https://prince.lyzer.io/easy-returns/front/api/'
  const ly_easy_page_handles = ['easy_order_return', 'easy_return_center']
  easy_page_handle =
    typeof easy_page_handle === 'undefined' ? '' : easy_page_handle

  if (
    ly_easy_page_handles.includes(easy_page_handle) ||
    frfi.page == 'account'
  ) {
    frfi.loadjQuery(function () {
      frfi.$.ajaxSetup({
        cache: false,
      })

      frfi.getReturnPageConfigurations().then(
        function (result) {
          if (frfi.show_return_links && frfi.page == 'account') {
            frfi
              .$('td.frfi-return-order, th.frfi-return-order-th')
              .css('display', '')
            frfi.getOrdersReturnStatus()
          } else if (easy_page_handle === ly_easy_page_handles[1]) {
            frfi.viewReturnCenterPage()
          } else if (easy_page_handle === ly_easy_page_handles[0]) {
            frfi.inputCount()
            frfi.customer_id =
              typeof ly_customer_id == 'undefined' ? '' : ly_customer_id

            frfi.order_id = frfi.getRequestParam('order_id') // get current order id
            frfi.checkOrderReturnAvailableStatus() // check for return applicable and if then get data
          }
        },
        () => {},
      )

      frfi.$('body').on('input', 'input.return_qty_input', function (e) {
        let return_qty = parseInt(frfi.$(this).val())
        const temp_id = frfi.$(this).data('id')

        if (frfi.themeId === 1) {
          if (return_qty == 0) {
            frfi.$(`.return_reason_${temp_id}`).css({ display: 'none' })
          } else {
            frfi.$(`.return_reason_${temp_id}`).css({ display: 'table-row' })
          }
        } else {
          if (return_qty == 0) {
            frfi.$(`.return_reason_${temp_id}`).css({ display: 'none' })
          } else {
            frfi.$(`.return_reason_${temp_id}`).css({ display: 'flex' })
          }
        }

        // if (frfi.getTotalReturnQty() == 0) {
        //   frfi
        //     .$('#submit_return_order_btn')
        //     .attr('disabled', true)
        //     .addClass('disabled')
        // } else {
        //   frfi
        //     .$('#submit_return_order_btn')
        //     .attr('disabled', false)
        //     .removeClass('disabled')
        // }
      })

      frfi.$('body').on('submit', '#return_order_items_form', function (e) {
        // console.log(uploadedFiles)
        // console.log(uploadedFiles.length)
        // return false
        frfi.$('.loader_wrapper').show()
        frfi.$('#return_order_failed').html('').hide()
        e.preventDefault()
        frfi.$(this).off('submit')
        var submit_btn = frfi.$(this).find("button[type='submit']")
        submit_btn.text('Processing...').prop('disabled', true)

        var error = false

        var formData = new FormData(e.target)
        formData.delete('files[]')

        frfi.$.each(uploadedFiles, function (k, file) {
          formData.append('files[]', file)
        })

        var form_fields = frfi.$('#return_order_items_form').serializeArray()
        var return_items_details = {}
        var total_return_qty = 0
        var frfi_msg = {}

        frfi.$.each(form_fields, function (i, field) {
          // $("#results").append(field.name + ":" + field.value + " ");
          field_name = field.name
          field_value = field.value
          // console.log(field_name + ":" + field_value + " ");
          line_item_id = field_name.match(/[0-9]+/)

          if (/^return_qty\[\d*\]$/.test(field_name)) {
            // return qty input
            return_qty = parseInt(field_value)
            total_return_qty += return_qty
            return_items_details[line_item_id] = { return_qty: return_qty }
          } else if (/^return_reason\[\d*\]$/.test(field_name)) {
            // return reason input
            return_items_details[line_item_id]['return_reason'] = field_value
          }
        })

        let preview = frfi.getRequestParam('preview')
        if (preview && preview === '1') {
          formData.append('preview', preview)
        }

        if (total_return_qty == 0) {
          error = true
          frfi_msg = { err: 'Select qty of an item to return.', err_code: 6 }
        }

        if (!error) {
          frfi.$.each(return_items_details, function (key, value) {
            if (value['return_qty'] != 0 && value['return_reason'] == '') {
              error = true
              frfi_msg = { err: 'Select reason for return item.', err_code: 7 }
              // break;
              return false
            }
          })
        }

        // frfi.$.each(frfi.$(".return_qty_input"), function(key, item){
        //     return_qty = parseInt(frfi.$(item).val());
        //     // console.log(return_qty)
        //     total_return_qty += return_qty;
        // })

        if (error) {
          submit_btn.text('Submit').prop('disabled', false)
          if (frfi_msg != '') {
            err = frfi.getErrorMsg(frfi_msg)
            frfi.$('#return_order_failed').html(err).show()
          }
          frfi.$('.loader_wrapper').hide()
          return false
        } else {
          frfi.$.ajax({
            url:
              frfi.api_url +
              'order_return_request?shop=' +
              frfi.shop +
              '&customer_id=' +
              frfi.customer_id +
              '&order_id=' +
              frfi.order_id,
            method: 'POST',
            crossDomain: true,
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'JSON',
            success: function (data) {
              const serverErr = data.err || ''
              const returnData = data.data || ''
              // console.log(data);
              if (serverErr != '') {
                err = frfi.getErrorMsg(data)
                frfi.$('#return_order_failed').html(err).show()
              } else if (returnData != '') {
                frfi.$('#return_order_div').html(returnData)
              } else {
                frfi_msg = { err: 'Something went wrong', err_code: 4 }
                err = frfi.getErrorMsg(frfi_msg)
                frfi.$('#return_order_failed').html(err).show()
              }
            },
            error: function (xhr, ajaxOptions, thrownError) {
              // console.log(xhr.status);
              // console.log(thrownError);

              frfi.$('#return_order_failed').html(thrownError).show()
            },
            complete: function (res) {
              submit_btn.text('Submit').prop('disabled', false)
              frfi.$('.loader_wrapper').hide()
            },
          })
        }

        return false
      })

      frfi.$('body').on('submit', '#easy_return_center_form', function (e) {
        frfi.$('.loader_wrapper').show()
        frfi.$('#return_order_failed').html('').hide()
        e.preventDefault()
        frfi.$(this).off('submit')
        var submit_btn = frfi.$(this).find("button[type='submit']")
        submit_btn.text('Processing...').prop('disabled', true)

        var error = false

        var formData = new FormData(e.target)
        const email = formData.get('email').trim()
        const orderNumber = formData.get('order_number').trim()

        if (email == '' || orderNumber == '') {
          error = true
          frfi_msg = {
            err:
              'Enter the right email address / phone number and order number.',
          }
        }

        if (error) {
          submit_btn.text('Submit').prop('disabled', false)
          if (frfi_msg != '') {
            err = frfi.getErrorMsg(frfi_msg)
            frfi.$('#return_order_failed').html(err).show()
          }
          frfi.$('.loader_wrapper').hide()
          return false
        } else {
          frfi.$.ajax({
            url: frfi.api_url + 'order_return_check?shop=' + frfi.shop,
            method: 'POST',
            crossDomain: true,
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'JSON',
            success: function (data) {
              const serverErr = data.err || ''
              const returnData = data.data || ''
              // console.log(data);
              if (serverErr != '') {
                err = frfi.getErrorMsg(data)
                frfi.$('#return_order_failed').html(err).show()
              } else if (returnData != '') {
                window.location.href = `/pages/${returnData.handle}?order_id=${returnData.oid}&cid=${returnData.cid}&token=${returnData.token}`
              } else {
                frfi_msg = { err: 'Something went wrong', err_code: 4 }
                err = frfi.getErrorMsg(frfi_msg)
                frfi.$('#return_order_failed').html(err).show()
              }
            },
            error: function (xhr, ajaxOptions, thrownError) {
              // console.log(xhr.status);
              // console.log(thrownError);

              frfi
                .$('#return_order_failed')
                .html(xhr.status + ' - ' + thrownError)
                .show()
            },
            complete: function (res) {
              submit_btn.text('Submit').prop('disabled', false)
              frfi.$('.loader_wrapper').hide()
            },
          })
        }

        return false
      })

      frfi.$('body').on('change', 'input.payment_option', function (e) {
        let selected_payment_option = frfi.$(this).val()

        frfi.$('.payment_cards').hide()
        frfi.$('.payment_cards input').prop('disabled', 'disabled')
        frfi.$('#' + selected_payment_option + '_card').show()
        frfi
          .$('#' + selected_payment_option + '_card')
          .find('input')
          .prop('disabled', false)
      })

      frfi.$('body').on('change', 'input.return_method', function (e) {
        let selected_return_method = frfi.$(this).val().trim()

        if (selected_return_method === '') return

        selected_return_method = parseInt(selected_return_method)
        if (selected_return_method === 1) {
          // refund
          frfi.$('#payment-div').show()
          frfi.$('#exchange-helptext').hide()

          if (frfi.return_note) {
            frfi.$('#note-div').show()

            if (frfi.required_return_note) {
              frfi.$('.note-required').show()
              frfi.$('.note-optional').hide()
            } else {
              frfi.$('.note-optional').show()
              frfi.$('.note-required').hide()
            }
          } else {
            frfi.$('#note-div').hide()
          }
        } else if (selected_return_method === 2) {
          // exchange
          frfi.$('#payment-div').hide()

          if (frfi.exchange_note) {
            frfi.$('#note-div').show()
            frfi.$('#exchange-helptext').show()

            if (frfi.required_exchange_note) {
              frfi.$('.note-required').show()
              frfi.$('.note-optional').hide()
            } else {
              frfi.$('.note-optional').show()
              frfi.$('.note-required').hide()
            }
          } else {
            frfi.$('#note-div').hide()
          }
        }
      })

      var imageTemplateString = (
        image_src,
        image_name,
        image_size,
        image_id,
      ) => `<div class="image-preview-div">
        <div class="image-preview"><img src="${image_src}" /></div>
        <div class="image-preview-details text-center"><span class="image-name">${image_name}</span><span class="image-size">${image_size}</span><button type="button" class="delete-image-btn" data-id="${image_id}">Delete</button></div></div>`
      var uploadedFiles = {}
      var imageCounter = 0

      frfi.$('body').on('change', 'input.upload-files', function (e) {
        showImagePreviews(e.target)
      })

      frfi.$('body').on('click', 'button.delete-image-btn', function (e) {
        const currentBtnId = frfi.$(e.target).attr('data-id')
        // console.log(currentBtnId)
        // console.log(uploadedFiles)

        delete uploadedFiles[currentBtnId]

        // console.log(Object.entries(uploadedFiles).length)

        frfi.$(e.target).parents('div.image-preview-div').remove()
      })

      function showImagePreviews(input) {
        var imageContainer = frfi.$('#uploaded-files')
        for (var i = 0; i < input.files.length; i++) {
          var fileSizeInBytes = input.files[i].size
          var fileSizeInKB = (fileSizeInBytes / 1024).toFixed(2)
          var filename = input.files[i].name
          //alert("File name is : "+filename+" || size : "+fileSizeInKB+" MB || size : "+fileSizeInBytes+" Bytes");
          let imageId = ++imageCounter
          uploadedFiles[imageId] = input.files[i]

          imageContainer.append(
            imageTemplateString(
              URL.createObjectURL(input.files[i]),
              filename,
              fileSizeInKB + ' KB',
              imageId,
            ),
          )
        }
      }
    })
  }
}.call(this))
