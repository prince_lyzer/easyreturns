<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221006054109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE pickup_configurations');
        $this->addSql('ALTER TABLE order_line_items CHANGE item_sku item_sku VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE return_request DROP pickup_carrier');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE pickup_configurations (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, bluedart_details VARCHAR(1000) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, date_add DATETIME NOT NULL, date_upd DATETIME NOT NULL, show_courier_options SMALLINT DEFAULT 0 NOT NULL, enable_bluedart SMALLINT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE order_line_items CHANGE item_sku item_sku VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE return_request ADD pickup_carrier SMALLINT DEFAULT NULL COMMENT \'0- Manual, 1- Bluedart\'');
    }
}
