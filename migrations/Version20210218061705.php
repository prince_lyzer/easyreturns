<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210218061705 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE application_charges (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL COMMENT \'user_settings table id\', charge_id INT NOT NULL COMMENT \'Recurring Application Charge Id\', created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, active SMALLINT DEFAULT 0 NOT NULL COMMENT \'Charge active state, 0- Expired, 1- Active.\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE application_charges');
    }
}
