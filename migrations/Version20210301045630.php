<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210301045630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE order_details (id INT AUTO_INCREMENT NOT NULL, order_id BIGINT NOT NULL, order_name VARCHAR(255) NOT NULL, fulfillment_status SMALLINT NOT NULL COMMENT \'Order Fulfillment Status Id\', payment_status SMALLINT NOT NULL COMMENT \'Order Payment Status Id\', customer_id BIGINT NOT NULL, order_created_at DATETIME NOT NULL, date_add DATETIME NOT NULL, date_upd DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_line_items (id INT AUTO_INCREMENT NOT NULL, order_id INT NOT NULL COMMENT \'Order Details Id\', line_item_id BIGINT NOT NULL, product_id BIGINT NOT NULL, variant_id BIGINT NOT NULL, product_title VARCHAR(255) NOT NULL, variant_title VARCHAR(255) NOT NULL, price NUMERIC(10, 2) NOT NULL, quantity INT NOT NULL, item_sku VARCHAR(255) NOT NULL, date_add DATETIME NOT NULL, date_upd DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE return_line_items (id INT AUTO_INCREMENT NOT NULL, order_line_item_id INT NOT NULL COMMENT \'Order Line Items Id\', return_qty INT NOT NULL, return_reason VARCHAR(255) NOT NULL, return_reason_id INT NOT NULL, date_add DATETIME NOT NULL, date_upd DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE return_request ADD sp_order_id BIGINT NOT NULL COMMENT \'Shopify Order Id\', DROP order_name, DROP fulfillment_status, DROP payment_status, DROP order_created_at, CHANGE order_id order_id INT NOT NULL COMMENT \'Order Details Id\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE order_details');
        $this->addSql('DROP TABLE order_line_items');
        $this->addSql('DROP TABLE return_line_items');
        $this->addSql('ALTER TABLE return_request ADD order_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, ADD fulfillment_status INT NOT NULL COMMENT \'Shopify Order Fullment Status\', ADD payment_status INT NOT NULL COMMENT \'Shopify Order Payment Status\', ADD order_created_at DATETIME NOT NULL COMMENT \'Shopify Order Created At\', DROP sp_order_id, CHANGE order_id order_id BIGINT NOT NULL COMMENT \'Shopify Order Id\'');
    }
}
