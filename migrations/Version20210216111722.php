<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210216111722 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_settings (id INT AUTO_INCREMENT NOT NULL, merchant_id BIGINT NOT NULL, email VARCHAR(255) NOT NULL, domain VARCHAR(255) NOT NULL, customer_email VARCHAR(255) NOT NULL, timezone VARCHAR(255) NOT NULL, iana_timezone VARCHAR(255) NOT NULL, shop_owner VARCHAR(255) NOT NULL, myshopify_domain VARCHAR(255) NOT NULL, money_format VARCHAR(255) DEFAULT NULL, money_with_currency_format VARCHAR(255) DEFAULT NULL, money_in_email_format VARCHAR(255) DEFAULT NULL, money_with_currency_in_emails_format VARCHAR(255) DEFAULT NULL, address1 VARCHAR(255) DEFAULT NULL, address2 VARCHAR(255) DEFAULT NULL, province VARCHAR(255) DEFAULT NULL, province_code VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, country VARCHAR(255) NOT NULL, country_code VARCHAR(3) NOT NULL, country_name VARCHAR(255) NOT NULL, zip VARCHAR(15) NOT NULL, phone VARCHAR(50) DEFAULT NULL, primary_location_id BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_settings');
    }
}
