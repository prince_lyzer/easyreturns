<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210301050818 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_details CHANGE fulfillment_status fulfillment_status VARCHAR(255) NOT NULL COMMENT \'Order Fulfillment Status\', CHANGE payment_status payment_status VARCHAR(255) NOT NULL COMMENT \'Order Payment Status\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_details CHANGE fulfillment_status fulfillment_status SMALLINT NOT NULL COMMENT \'Order Fulfillment Status Id\', CHANGE payment_status payment_status SMALLINT NOT NULL COMMENT \'Order Payment Status Id\'');
    }
}
