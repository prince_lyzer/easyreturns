<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210825052919 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE return_images CHANGE size size NUMERIC(10, 1) NOT NULL');
        $this->addSql('ALTER TABLE user_configurations ADD guest_checkout SMALLINT DEFAULT 0');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE return_images CHANGE size size NUMERIC(10, 1) NOT NULL COMMENT \'in kb\'');
        $this->addSql('ALTER TABLE user_configurations DROP guest_checkout');
    }
}
