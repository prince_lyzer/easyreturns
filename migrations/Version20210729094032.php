<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210729094032 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_configurations ADD required_return_note SMALLINT DEFAULT 1 NOT NULL, ADD exchange_note SMALLINT DEFAULT 1 NOT NULL, ADD required_exchange_note SMALLINT DEFAULT 0 NOT NULL, ADD return_methods VARCHAR(255) DEFAULT \'1\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_configurations DROP required_return_note, DROP exchange_note, DROP required_exchange_note, DROP return_methods');
    }
}
