<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210216112626 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_settings ADD date_add DATETIME NOT NULL, ADD date_upd DATETIME DEFAULT NULL, ADD status SMALLINT NOT NULL COMMENT \'Status have 3 values, 0- Uninstalled, 1- Payment Panding, 2- Payment Approved\', CHANGE merchant_id merchant_id BIGINT NOT NULL COMMENT \'Shopify Merchant Id\', CHANGE email email VARCHAR(255) NOT NULL COMMENT \'Shopify Merchant Contact Email\', CHANGE domain domain VARCHAR(255) NOT NULL COMMENT \'Shopify Store Custom Domain\', CHANGE customer_email customer_email VARCHAR(255) NOT NULL COMMENT \'Shopify Customer Contact Email\', CHANGE shop_owner shop_owner VARCHAR(255) NOT NULL COMMENT \'Shopify Merchant Full Name\', CHANGE myshopify_domain myshopify_domain VARCHAR(255) NOT NULL COMMENT \'Shopify Store myshopify domain\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_settings DROP date_add, DROP date_upd, DROP status, CHANGE merchant_id merchant_id BIGINT NOT NULL, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE domain domain VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE customer_email customer_email VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE shop_owner shop_owner VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE myshopify_domain myshopify_domain VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
