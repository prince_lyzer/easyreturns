<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210326105354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_configurations ADD return_page_title VARCHAR(255) DEFAULT \'Easy Order Return\' COMMENT \'customeor return order page title\', ADD return_page_handle VARCHAR(255) DEFAULT \'easy_order_return\' COMMENT \'customeor return order page handle\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_configurations DROP return_page_title, DROP return_page_handle');
    }
}
