<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324070302 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_configurations ADD auto_archive SMALLINT DEFAULT 0 NOT NULL COMMENT \'Auto archive order on completion\', CHANGE return_order_page_code return_order_page_code LONGTEXT DEFAULT NULL COMMENT \'Return order template page code\', CHANGE return_order_response_code return_order_response_code LONGTEXT DEFAULT NULL COMMENT \'Return order request status code, after request submit\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_configurations DROP auto_archive, CHANGE return_order_page_code return_order_page_code LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE return_order_response_code return_order_response_code LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
