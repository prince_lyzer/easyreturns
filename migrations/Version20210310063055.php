<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210310063055 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_details ADD is_cod TINYINT(1) DEFAULT NULL COMMENT \'Is a Cash On Delivery Order\'');
        $this->addSql('ALTER TABLE return_request ADD payment_details VARCHAR(255) DEFAULT NULL COMMENT \'Refund amount payment details in case of Cash On Delivery Order\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE order_details DROP is_cod');
        $this->addSql('ALTER TABLE return_request DROP payment_details');
    }
}
