<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225123410 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE default_reasons CHANGE label label VARCHAR(255) NOT NULL COMMENT \'Reason Text\', CHANGE active active SMALLINT DEFAULT 1 NOT NULL COMMENT \'Reason Status, 0=> Disabled, 1=> Active\', CHANGE type type SMALLINT DEFAULT 1 NOT NULL COMMENT \'Reason for, 1=> Return, 2=> Exchange\', CHANGE date_add date_add DATETIME NOT NULL COMMENT \'Reason Created At\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE default_reasons CHANGE label label VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE active active SMALLINT DEFAULT 1 NOT NULL, CHANGE type type SMALLINT DEFAULT 1 NOT NULL, CHANGE date_add date_add DATETIME NOT NULL');
    }
}
