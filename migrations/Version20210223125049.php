<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210223125049 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE return_request (id INT AUTO_INCREMENT NOT NULL, order_id BIGINT NOT NULL COMMENT \'Shopify Order Id\', order_name VARCHAR(255) NOT NULL, customer_id BIGINT NOT NULL COMMENT \'Customer Table Id\', return_status INT DEFAULT NULL, fulfillment_status INT NOT NULL COMMENT \'Shopify Order Fullment Status\', payment_status INT NOT NULL COMMENT \'Shopify Order Payment Status\', order_created_at DATETIME NOT NULL COMMENT \'Shopify Order Created At\', date_add DATETIME NOT NULL, date_upd DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE return_request');
    }
}
